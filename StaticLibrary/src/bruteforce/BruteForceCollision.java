/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bruteforce;

import java.util.ArrayList;
import java.util.List;
import poligons.Polygon2D;
import primitives.AABB2D;
import primitives.Pair;

/**
 *
 * @author hmoraga
 */
public class BruteForceCollision {
    private final List<Polygon2D> listaObjetos;
    private final List<Pair<Integer>> listaColisiones;

    public BruteForceCollision(List<Polygon2D> listaObjetos) {
        this.listaObjetos = listaObjetos;
        listaColisiones = new ArrayList<>();
    }
    
    public List<Pair<Integer>> getColisiones(){
        if (listaObjetos.size()>1){
            for (int i = 0; i < listaObjetos.size()-1; i++) {
                AABB2D boxI = listaObjetos.get(i).getBox();
                for (int j = i+1; j < listaObjetos.size(); j++) {
                    AABB2D boxJ = listaObjetos.get(j).getBox();
                    
                    if (boxI.intersects(boxJ))
                        listaColisiones.add(new Pair<>(i, j));
                }
            }
        }
        return listaColisiones;
    }
}
