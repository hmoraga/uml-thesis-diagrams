/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sap;

import java.util.Objects;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class Point2DStaticWrap {
    private Point2D punto;
    private int id;
    private boolean fin;

    public Point2DStaticWrap(Point2D punto, int id, boolean fin) {
        this.punto = punto;
        this.id = id;
        this.fin = fin;
    }

    public Point2D getPunto() {
        return punto;
    }

    public void setPunto(Point2D punto) {
        this.punto = punto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isFin() {
        return fin;
    }

    public void setFin(boolean fin) {
        this.fin = fin;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.punto);
        hash = 37 * hash + this.id;
        hash = 37 * hash + (this.fin ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Point2DStaticWrap other = (Point2DStaticWrap) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.fin != other.fin) {
            return false;
        }
        if (!Objects.equals(this.punto, other.punto)) {
            return false;
        }
        return true;
    }
    
    
}
