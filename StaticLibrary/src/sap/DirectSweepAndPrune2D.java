/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sap;

import comparators.Point2DStaticWrapComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import primitives.AABB2D;
import primitives.Pair;
import primitives.Point2D;

/**
 * Suposicion INICIAL: tendremos los poligonos iniciales, los cuales tendrán 
 * asociado un índice (dado por el orden en que aparecen en la lista) y sus
 * velocidades de desplazamiento (por segundo de simulacion), se necesita además
 * el intervalo de simulación y la cantidad de imágenes que tendrá la simulación
 * (o en su defecto los FPS).
 * 
 * Como atributos estará la lista de cajas que representan a cada polígono,
 * el intervalo de simulacion, el timestep.
 * 
 * Como atributos auxiliares están: la lista de valores extremos de cada caja 
 * (por cada coordenada) y la lista de posiciones que tiene cada objeto en la
 * lista (por cada coordenada).
 * 
 * @author hmoraga
 */
public class DirectSweepAndPrune2D {
    private List<List<Point2DStaticWrap>> listaPuntosCajasPorDimension;
    private List<List<Pair<Integer>>> posicionesPorObjetoEnListaPorDimension;
    private List<Pair<Integer>> listaParesEfectivosColisionando;
    private List<Pair<Integer>> listaParesEnX, listaParesEnY;
    private int frameNumber;

    public DirectSweepAndPrune2D() {}

    public List<List<Point2DStaticWrap>> getListaPuntosCajasPorDimension() {
        return listaPuntosCajasPorDimension;
    }

    public List<List<Pair<Integer>>> getPosicionesPorObjetoEnListaPorDimension() {
        return posicionesPorObjetoEnListaPorDimension;
    }

    public void setPosicionesPorObjetoEnListaPorDimension(List<List<Pair<Integer>>> posicionesPorObjetoEnListaPorDimension) {
        this.posicionesPorObjetoEnListaPorDimension = posicionesPorObjetoEnListaPorDimension;
    }

    public List<Pair<Integer>> getListaParesEfectivosColisionando() {
        return listaParesEfectivosColisionando;
    }

    public void setListaParesEfectivosColisionando(List<Pair<Integer>> listaParesEfectivosColisionando) {
        this.listaParesEfectivosColisionando = listaParesEfectivosColisionando;
    }

    // Con ellas se obtendrá un AABB de cada objeto, en conjunto con su vector
    // de desplazamiento
    public void inicializarEstructuras(List<AABB2D> listaCajas, int frameNumber){
        listaPuntosCajasPorDimension = new ArrayList<>(2);
        this.frameNumber = frameNumber;
        
        List<Point2DStaticWrap> listaPuntosCajasTemp0 = new ArrayList<>(2*listaCajas.size());
        List<Point2DStaticWrap> listaPuntosCajasTemp1 = new ArrayList<>(2*listaCajas.size());
        
        for (int i=0; i<listaCajas.size(); i++) {
            List<Point2D> puntosCaja = listaCajas.get(i).getPoints();
            
            listaPuntosCajasTemp0.add(new Point2DStaticWrap(puntosCaja.get(0), i, false)); // px
            listaPuntosCajasTemp0.add(new Point2DStaticWrap(puntosCaja.get(2), i, true)); // qx
            listaPuntosCajasTemp1.add(new Point2DStaticWrap(puntosCaja.get(0), i, false)); // py
            listaPuntosCajasTemp1.add(new Point2DStaticWrap(puntosCaja.get(2), i, true)); // qy
        }
        
        // ordeno inicialmente la lista de Puntos2DStaticWrap
        sort(listaPuntosCajasTemp0, 0);  // eje X
        sort(listaPuntosCajasTemp1, 1);  // eje Y
        
        this.listaPuntosCajasPorDimension.add(listaPuntosCajasTemp0);
        this.listaPuntosCajasPorDimension.add(listaPuntosCajasTemp1);
        
        // creacion de la lista de posiciones por objeto en cada dimension
        obtenerListaDePosiciones();
        this.listaParesEfectivosColisionando = obtenerListaDeParesColisionando();
    }
    
    private void sort(List<Point2DStaticWrap> listaPuntosCajas, int dimension){
        Collections.sort(listaPuntosCajas, new Point2DStaticWrapComparator(dimension));
    }

    /**
     * Inicializa la lista de posiciones de los objetos. Cada objeto 
     * (identificado con el índice de la lista) tiene asociado un par de enteros
     * que indican la posicion en la lista de puntos
     */
    private void obtenerListaDePosiciones() {
        List<List<Pair<Integer>>> listaTemporal = new ArrayList<>(2);
        
        for (int i=0; i< 2; i++){
            listaTemporal.add(new ArrayList<>(listaPuntosCajasPorDimension.get(0).size()/2));
            List<Point2DStaticWrap> listaPuntosCajas = listaPuntosCajasPorDimension.get(i);
            
            for (int j = 0; j < listaPuntosCajasPorDimension.get(0).size()/2; j++){
                listaTemporal.get(i).add(new Pair<>(-1,-1));
            }
            
            for (int j = 0; j < listaPuntosCajas.size(); j++) {
                Point2DStaticWrap pw = listaPuntosCajas.get(j);
                int idPos = pw.getId();
                Pair<Integer> parTemp = listaTemporal.get(i).get(idPos);
                                
                if (!pw.isFin()){
                    parTemp.first = j;
                } else {
                    parTemp.second = j;
                }
                listaTemporal.get(i).set(idPos, parTemp);  
            }
        }
        this.posicionesPorObjetoEnListaPorDimension = listaTemporal;
    }

    public List<Pair<Integer>> obtenerListaDeParesColisionando(){
        List<Pair<Integer>> listaSalida = new ArrayList<>();

        if (!posicionesPorObjetoEnListaPorDimension.get(0).isEmpty() && 
                !posicionesPorObjetoEnListaPorDimension.get(1).isEmpty()){
            listaParesEnX = new ArrayList<>();
            listaParesEnY = new ArrayList<>();
            
            List<Pair<Integer>> listaTempX = posicionesPorObjetoEnListaPorDimension.get(0);
            List<Pair<Integer>> listaTempY = posicionesPorObjetoEnListaPorDimension.get(1);

            // para lista en X
            for (int j = 0; j < listaTempX.size()-1; j++) {
                Pair<Integer> parUno = listaTempX.get(j);

                for (int k = j+1; k < listaTempX.size(); k++) {
                    Pair<Integer> parDos = listaTempX.get(k);

                    if ((parUno.second < parDos.first) ||
                           (parDos.second < parUno.first)){
                    } else {    
                    //if ((parUno.first < parDos.first && parDos.first < parUno.second) ||
                    //        (parUno.first < parDos.second && parDos.second < parUno.second) ||
                    //        (parDos.first < parUno.first && parUno.first < parDos.second) ||
                    //        (parDos.first < parUno.second && parUno.second < parDos.second)){
                        listaParesEnX.add(new Pair<>(j,k));
                    }
                }
            }
            
            // para lista en Y
            for (int j = 0; j < listaTempY.size()-1; j++) {
                Pair<Integer> parUno = listaTempY.get(j);
                    
                for (int k = j+1; k < listaTempY.size(); k++) {
                    Pair<Integer> parDos = listaTempY.get(k);
                    
                    if ((parUno.second < parDos.first) ||
                           (parDos.second < parUno.first)) {
                    } else {    
                    //if ((parUno.first < parDos.first && parDos.first < parUno.second) ||
                    //        (parUno.first < parDos.second && parDos.second < parUno.second) ||
                    //        (parDos.first < parUno.first && parUno.first < parDos.second) ||
                    //        (parDos.first < parUno.second && parUno.second < parDos.second)){
                        listaParesEnY.add(new Pair<>(j,k));
                    }
                }
            }

            if (listaParesEnX.size() < listaParesEnY.size()){
                for (int i = 0; i < listaParesEnX.size(); i++) {
                    Pair<Integer> par = listaParesEnX.get(i);
                    
                    if (listaParesEnY.contains(par) || listaParesEnY.contains(par.inverted()))
                        listaSalida.add(par);
                }
            } else {
                for (int i = 0; i < listaParesEnY.size(); i++) {
                    Pair<Integer> par = listaParesEnY.get(i);
                    
                    if (listaParesEnX.contains(par) || listaParesEnX.contains(par.inverted()))
                        listaSalida.add(par);
                }
            }
        }
        return listaSalida;
    }
}
