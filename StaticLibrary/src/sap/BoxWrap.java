/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sap;

import java.util.Objects;
import primitives.BV;

/**
 *
 * @author hmoraga
 */
public class BoxWrap implements Cloneable{
    private BV box;
    private int id;

    public BoxWrap(BV box, int id) {
        this.box = box;
        this.id = id;
    }

    public BV getBox() {
        return box;
    }

    public void setBox(BV box) {
        this.box = box;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int compareTo(BoxWrap other, int i){
        return box.compareTo(other.getBox(),i);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.box);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BoxWrap other = (BoxWrap) obj;
        if (!Objects.equals(this.box, other.box)) {
            return false;
        }
        return true;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        BoxWrap nuevaCaja = new BoxWrap(box, id);

        return nuevaCaja;
    }
}
