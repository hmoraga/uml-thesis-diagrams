/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sap;

import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class Point2DWrap {
    private Point2D puntoInicial;
    private Pair<Double> velocidades;
    private int id;
    private boolean fin;

    public Point2DWrap(Point2D puntoInicial, Pair<Double> velocidades, int id, boolean fin) {
        this.puntoInicial = puntoInicial;
        this.velocidades = velocidades;
        this.id = id;
        this.fin = fin;
    }

    public Point2D currentPosition(double time){
        double posX=puntoInicial.getX(), posY = puntoInicial.getY();

        if (time!=0.0){
            posX+=velocidades.first*time;
            posY+=velocidades.second*time;
        }
                
        return new Point2D(posX, posY);
    }

    public Point2D getPuntoInicial() {
        return puntoInicial;
    }

    public void setPuntoInicial(Point2D puntoInicial) {
        this.puntoInicial = puntoInicial;
    }

    public Pair<Double> getVelocidades() {
        return velocidades;
    }

    public void setVelocidades(Pair<Double> velocidades) {
        this.velocidades = velocidades;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isFin() {
        return fin;
    }

    public void setFin(boolean fin) {
        this.fin = fin;
    }    
}
