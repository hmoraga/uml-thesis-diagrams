/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sap;

import comparators.AABB2DWrapperComparator;
import comparators.Point2DStaticWrapComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import poligons.AnyPolygon2D;
import primitives.Pair;
import primitives.Point2D;
import wrappers.AABB2DWrapper;

/**
 * Suposicion INICIAL: tendremos los poligonos iniciales, los cuales tendrán 
 * asociado un índice (dado por el orden en que aparecen en la lista) y sus
 * velocidades de desplazamiento (por segundo de simulacion), se necesita además
 * el intervalo de simulación y la cantidad de imágenes que tendrá la simulación
 * (o en su defecto los FPS).
 * 
 * Como atributos estará la lista de cajas que representan a cada polígono,
 * el intervalo de simulacion, el timestep.
 * 
 * Como atributos auxiliares están: la lista de valores extremos de cada caja 
 * (por cada coordenada) y la lista de posiciones que tiene cada objeto en la
 * lista (por cada coordenada).
 * 
 * @author hmoraga
 */
public class IncrementalSweepAndPrune {
    // un AABB2DWrapper es un AABB2D con un id asociado
    private List<List<AABB2DWrapper>> listaCajasPorDimension;
    // para las simulaciones se necesita el vector velocidad de cada
    // caja.
    private final List<Pair<Double>> listaVelocidades;
    // el objeto i (dado por su posicion en la lista), tiene extremos en la
    // lista de puntos dados por el par (i,j). Esto para cada coordenada
    private List<List<Point2DStaticWrap>> listaPuntosCajasPorDimension;
    //private List<List<Pair<Integer>>> listaOrdenPuntosPorCajaPorDimension;
    
    public IncrementalSweepAndPrune(List<List<Point2D>> listaPoligonos, List<Pair<Double>> listaVelocidades) {
        listaCajasPorDimension = new ArrayList<>(2);
        
        for (int i=0; i<2; i++){
            listaCajasPorDimension.add(new ArrayList<>(listaPoligonos.size()));
            int j=0;
            for (List<Point2D> poly : listaPoligonos) {
                listaCajasPorDimension.get(i).add(new AABB2DWrapper(new AnyPolygon2D(poly, false).getBox(), j++));
            }
        }
        
        this.listaVelocidades = listaVelocidades;
        for (int i=0; i< listaCajasPorDimension.size(); i++)
            Collections.sort(listaCajasPorDimension.get(i), new AABB2DWrapperComparator(i));
        
        // genero la lista de puntos de cajas por dimension
        listaPuntosCajasPorDimension = new ArrayList<>(listaCajasPorDimension.size());

        for (int dim=0; dim < listaCajasPorDimension.size(); dim++){
            listaPuntosCajasPorDimension.add(new ArrayList<>(listaCajasPorDimension.get(dim).size()));

            List<AABB2DWrapper> listBoxWraps = listaCajasPorDimension.get(dim);
            
            for (int i=0; i < listBoxWraps.size(); i++){
                int id = listBoxWraps.get(i).getId();
                Point2D p = listBoxWraps.get(i).getBox().getPoints().get(0);
                Point2D q = listBoxWraps.get(i).getBox().getPoints().get(2);

                listaPuntosCajasPorDimension.get(dim).add(new Point2DStaticWrap(p, id, false));
                listaPuntosCajasPorDimension.get(dim).add(new Point2DStaticWrap(q, id, true));
            }
            
            Collections.sort(listaPuntosCajasPorDimension.get(dim), new Point2DStaticWrapComparator(dim));
        }
    }

    public List<List<Point2DStaticWrap>> getListaPuntosCajasPorDimension() {
        List<List<Point2DStaticWrap>> aux = new ArrayList<>(listaCajasPorDimension.size());

        for (int i=0; i< listaCajasPorDimension.size(); i++) {
            aux.add(new ArrayList<>(2*listaCajasPorDimension.get(i).size()));

            List<AABB2DWrapper> listaBoxes=listaCajasPorDimension.get(i);
            for (int j=0; j< listaBoxes.size(); j++) {
                if (aux.get(i).isEmpty()){
                    aux.get(i).add(new Point2DStaticWrap(listaBoxes.get(j).getBox().getPoints().get(0), listaBoxes.get(j).getId(), false));
                    aux.get(i).add(new Point2DStaticWrap(listaBoxes.get(j).getBox().getPoints().get(2), listaBoxes.get(j).getId(), true));
                } else {
                    // usamos el insertionSort para ingresar los elementos
                    // así no usamos el Collections.sort
                    insertionSort(aux.get(i), listaBoxes.get(j).getBox().getPoints().get(0), 
                            listaBoxes.get(j).getBox().getPoints().get(2), listaBoxes.get(j).getId(), i);
                }
            }
            
            // peor caso: O(n log n)
            //Collections.sort(aux.get(i), new Point2DStaticWrapComparator(i));
        }
        
        return aux;
    }

    /* Algoritmo Sweep and Prune */
    public List<Pair<Integer>> getListaParesEfectivosColisionando() {
        List<Pair<Integer>> resultado = new ArrayList<>();
        List<Pair<Integer>> listaParcialX = new ArrayList<>();
        List<Pair<Integer>> listaParcialY = new ArrayList<>();
        
        for (int dim = 0; dim < listaPuntosCajasPorDimension.size(); dim++){
            List<Point2DStaticWrap> listaPorDimension = listaPuntosCajasPorDimension.get(dim);
            List<Integer> listaAux = new ArrayList<>(listaPorDimension.size()/2);
            
            for (int i = 0; i < listaPorDimension.size(); i++) {
                Integer num = new Integer(listaPorDimension.get(i).getId());
                
                if (listaPorDimension.get(i).isFin()){
                    // buscar si existe el otro punto dentro de la lista y
                    // removerlo
                    if (listaAux.contains(num)){
                        listaAux.remove(num);
                        // Se genera luego una lista de pares con los sobrevivientes
                        // de la lista auxiliar
                        for (Integer objId : listaAux) {
                            if (dim==0)
                                listaParcialX.add(new Pair<>(Math.min(num, objId), Math.max(num, objId)));
                            else if (dim==1)
                                listaParcialY.add(new Pair<>(Math.min(num, objId), Math.max(num, objId)));
                        }
                    }
                } else {
                    listaAux.add(new Integer(num));
                }
            }
            listaAux.clear();
        }
        
        // reviso cuál de ambas listas es la más corta y según esa
        // se hace la lista de salida
        if (!listaParcialX.isEmpty() && !listaParcialY.isEmpty()){
            //selecciono la lista de menos tamaño
            if (listaParcialX.size() < listaParcialY.size()){
                listaParcialX.stream().filter((par) -> (listaParcialY.contains(par) || listaParcialY.contains(par.inverted()))).forEach((par) -> {
                    resultado.add(par);
                });
            } else {
                listaParcialY.stream().filter((par) -> (listaParcialX.contains(par) || listaParcialX.contains(par.inverted()))).forEach((par) -> {
                    resultado.add(par);
                });                    
            }
        }
        
        return resultado;
    }
    
    /**
     * Retorna la lista de cajas de acuerdo al orden interno.
     * @return lista de cajas para cada coordenada
     */
    public List<List<AABB2DWrapper>> getListaCajasPorDimension() {
        return listaCajasPorDimension;
    }

    public void setListaCajasPorDimension(List<List<AABB2DWrapper>> listaCajasPorDimension) {
        this.listaCajasPorDimension = listaCajasPorDimension;
    }
    /**
     * me valida el orden de la lista. Método que 
     * no debiera ser necesario mas allá del inicio 
     * del algoritmo SP.
     * Peor caso: O(n)
     * @return 
     */
    public boolean validarOrdenListaPuntos(){
        List<Point2DStaticWrap> listaAux;
        
        for (int dim=0;dim<listaPuntosCajasPorDimension.size(); dim++) {
            Point2DStaticWrapComparator p2dWrCmp = new Point2DStaticWrapComparator(dim);
            listaAux = listaPuntosCajasPorDimension.get(dim);
            
            for (int i = 1; i < listaAux.size(); i++) {
                if (p2dWrCmp.compare(listaAux.get(i-1), listaAux.get(i))>0)
                    return false;
            }
        }
        
        return true;
    }
    
    /**
     * Me ordena la lista de puntos de las cajas, es un algoritmo in-place
     * y en base a ese nuevo orden, me reordena la lista de orden de objetos.
     */
    public void insertionSort(){
        for (int dim=0; dim < listaPuntosCajasPorDimension.size(); dim++){
            Point2DStaticWrapComparator p2DWrComp = new Point2DStaticWrapComparator(dim);
            
            for (int i = 1; i < listaPuntosCajasPorDimension.get(dim).size(); i++) {
                Point2DStaticWrap newValue = listaPuntosCajasPorDimension.get(dim).get(i);
                int j = i;
                
                while (j > 0 && p2DWrComp.compare(listaPuntosCajasPorDimension.get(dim).get(j - 1), newValue)==1) {
                    listaPuntosCajasPorDimension.get(dim).set(j, listaPuntosCajasPorDimension.get(dim).get(j - 1));
                    j--;
                }
                
                listaPuntosCajasPorDimension.get(dim).set(j, newValue);
            }
        }
    }

    /**
     * Metodo que me altera la posicion actual de las cajas de los objetos, en base
     * a un delta de tiempo (dado por el timeStep).
     * <P>
     * Cada caja que envuelve a los objetos se mueve un factor:<br>
     * p*&Delta(timeStep)<br>
     * donde p es cada uno de los puntos de las cajas
     * @param timeStep 
     */
    public void updateBoxesPositions(double timeStep){
        // se supone que con solo cambiar una de las listas se cambian 
        // todos los objetos, ya que los objetos en la dimension 0 y la 1
        // son solo referencias a las cajas de los objetos originales

        for (int i = 0; i < listaCajasPorDimension.get(0).size(); i++) {
            AABB2DWrapper boxWrap = listaCajasPorDimension.get(0).get(i);
            int objId = boxWrap.getId();
            Pair<Double> velocidadTemp = listaVelocidades.get(objId);
            
            boxWrap.move(new Point2D(velocidadTemp.first*timeStep, velocidadTemp.second*timeStep));
        }
    }

    private void insertionSort(List<Point2DStaticWrap> listaPuntos, Point2D extremeMin, Point2D extremeMax, int pointId, int dim) {
        // debo encontrar los puntos de insercion del minimo y el maximo
        // el problema se da cuando el valor es identico a otro
        // ahí tiene preferencia el que:
        // - si ambos son inicio: va primero el de menor id
        // - si ambos son final: va primero el de mayor id
        // - si son diferentes: va primero el que inicia y despues el
        //   que finaliza
        int posmin =0,posmax=0, i;
        
        if (extremeMin.getCoords(dim) < listaPuntos.get(0).getPunto().getCoords(dim)){
            listaPuntos.add(0, new Point2DStaticWrap(extremeMin, pointId, false));
            posmin=0;
        } else if (listaPuntos.get(listaPuntos.size()-1).getPunto().getCoords(dim) < extremeMin.getCoords(dim)){
            listaPuntos.add(new Point2DStaticWrap(extremeMin, pointId, false));
            posmin = listaPuntos.size();
        } else {
            i=0;
            while(i < listaPuntos.size() && listaPuntos.get(i).getPunto().getCoords(dim) < extremeMin.getCoords(dim)) {
                i++;
            }
        
            if (listaPuntos.get(i).isFin()){
                posmin=i;
            } else if (listaPuntos.get(i).getId() < pointId){
                posmin=i+1;
            } else {
                posmin=i;
            }

            listaPuntos.add(posmin, new Point2DStaticWrap(extremeMin, pointId, false));
        }

        i = posmin;
        if (listaPuntos.get(listaPuntos.size()-1).getPunto().getCoords(dim) < extremeMax.getCoords(dim)){
            listaPuntos.add(new Point2DStaticWrap(extremeMax, pointId, true));
        } else {
            while(i < listaPuntos.size() && listaPuntos.get(i).getPunto().getCoords(dim) < extremeMin.getCoords(dim)) {
                i++;
            }
        
            if (listaPuntos.get(i).isFin()){
                posmax=i;
            } else if (listaPuntos.get(i).getId() < pointId){
                posmax=i+1;
            } else {
                posmax=i;
            }

            listaPuntos.add(posmax, new Point2DStaticWrap(extremeMax, pointId, true));
        }
    }
}