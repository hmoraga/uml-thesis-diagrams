/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sap;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import poligons.AnyPolygon2D;
import primitives.AABB2D;
import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class WrapperParaRealSAP {
    private int frameNumber, numDims;
    private List<AABB2D> listaCajas;
    private double areaObjetos, areaSimulacion;
    
    public WrapperParaRealSAP(String fileName, boolean verbose) throws IOException{
        obtenerEstructuras(fileName, verbose);            
    }
        
    private void obtenerEstructuras(String fileName, boolean verbose) throws IOException{
        Pattern pattern;
        Matcher matcher;
        
        if (verbose) {
            System.out.println("Leyendo archivo " + fileName); // leo los datos del archivo
        }

        Charset utf8 = Charset.forName("UTF-8");
        List<String> lista = Files.readAllLines(Paths.get(fileName), utf8);

        // frame, es crucial para la creacion del archivo de salida 
        pattern = Pattern.compile("//frame:(\\d+)");
        matcher = pattern.matcher(lista.get(0));

        frameNumber = (matcher.find()) ? Integer.valueOf(matcher.group(1)) : 0;
        matcher.reset();

        pattern = Pattern.compile(".*\\((-?\\d+.\\d+),(-?\\d+.\\d+)\\),\\((-?\\d+.\\d+),(-?\\d+.\\d+)\\)");
        matcher = pattern.matcher(lista.get(2));

        List<Pair<Double>> limitesLocal = new ArrayList<>(2);

        if (matcher.find()) {
            String grupo1 = matcher.group(1);
            String grupo2 = matcher.group(2);
            String grupo3 = matcher.group(3);
            String grupo4 = matcher.group(4);

            // dimensiones en x
            limitesLocal.add(new Pair<>(Double.valueOf(grupo1), Double.valueOf(grupo2)));
            // dimensiones en y
            limitesLocal.add(new Pair<>(Double.valueOf(grupo3), Double.valueOf(grupo4)));
        } else {
            // dimensiones en x
            limitesLocal.add(new Pair<>(-10.0, 10.0));
            // dimensiones en y
            limitesLocal.add(new Pair<>(-10.0, 10.0));
        }
        matcher.reset();

        pattern = Pattern.compile("//dimensiones:(\\d+)");
        matcher = pattern.matcher(lista.get(3));

        if (matcher.find()) {
            String grupo1 = matcher.group(1);
            numDims = Integer.valueOf(grupo1);
        } else {
            numDims = 2;
        }
        matcher.reset();

        List<List<Point2D>> listaPuntosObjetosLocal = new ArrayList<>(lista.size() - 4);
        // proceso los datos que se repiten en todos los archivos
        for (String s : lista.subList(4, lista.size())) {
            // primer dato es la cantidad de puntos del objeto
            pattern = Pattern.compile("^(\\d+)\\s+(.*)");
            matcher = pattern.matcher(s);

            if (matcher.find()) {
                int cantPuntos = Integer.valueOf(matcher.group(1));
                List<Point2D> listaPuntos = new ArrayList<>(cantPuntos);
                String[] aux = matcher.group(2).split("\\s+");

                assert (aux.length == numDims * cantPuntos);

                if (numDims == 2) {
                    for (int i = 0; i < cantPuntos; i++) {
                        double posX = Double.valueOf(aux[2 * i]);
                        double posY = Double.valueOf(aux[2 * i + 1]);

                        listaPuntos.add(new Point2D(posX, posY));
                    }

                    listaPuntosObjetosLocal.add(listaPuntos);
                }
            }
            matcher.reset();
        }

        // calculo el area inicial y la inicial de los objetos
        double area = 0.0;
        
        areaObjetos = listaPuntosObjetosLocal.stream().map((listaPuntos) -> new AnyPolygon2D(listaPuntos, false).getArea()).reduce(area, (accumulator, _item) -> accumulator + _item);
        areaSimulacion = (limitesLocal.get(0).second-limitesLocal.get(0).first)*(limitesLocal.get(1).second-limitesLocal.get(1).first);
        listaCajas = new ArrayList<>(listaPuntosObjetosLocal.size());
        
        // calculo la lista de cajas
        for (List<Point2D> poly : listaPuntosObjetosLocal) {
            AABB2D auxBox = new AnyPolygon2D(poly, false).getBox();
            listaCajas.add(auxBox);
        }        
    }

    public int getFrameNumber() {
        return frameNumber;
    }

    public void setFrameNumber(int frameNumber) {
        this.frameNumber = frameNumber;
    }

    public int getNumDims() {
        return numDims;
    }

    public void setNumDims(int numDims) {
        this.numDims = numDims;
    }

    public List<AABB2D> getListaCajas() {
        return listaCajas;
    }

    public void setListaCajas(List<AABB2D> listaCajas) {
        this.listaCajas = listaCajas;
    }

    public double getAreaObjetos() {
        return areaObjetos;
    }

    public void setAreaObjetos(List<List<Point2D>> listaPuntosObjetos) {
        double suma = 0.0;
        
        suma = listaPuntosObjetos.stream().map((poly) -> (new AnyPolygon2D(poly,false).getArea())).reduce(suma, (accumulator, _item) -> accumulator + _item);
        
        this.areaObjetos = suma;
    }

    public double getAreaSimulacion() {
        return areaSimulacion;
    }

    public void setAreaSimulacion(List<Pair<Double>> dimensiones) {
        this.areaSimulacion = (dimensiones.get(0).second-dimensiones.get(0).first)*(dimensiones.get(1).second-dimensiones.get(1).first);
    }
}
