/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

import java.util.Objects;
/**
 *
 * @author hmoraga
 */
public class Edge2D extends Edge implements Cloneable {

    public Edge2D() {
    }

    public Edge2D(Point2D a, Point2D b) {
        super(a,b);
    }
    
    public Pair<Point2D> getPoints(){
        return new Pair<>((Point2D)a,(Point2D)b);
    }
    
    public double signed2DTriArea(Point2D c){
        double ax = a.getCoords(0);
	double ay = a.getCoords(1);
	double bx = b.getCoords(0);
	double by = b.getCoords(1);
	double cx = c.getX();
	double cy = c.getY();

	return (ax-cx)*(by-cy)-(ay-cy)*(bx-cx);
    }
	
    public double signed2DTriArea(Point c){
	return signed2DTriArea((Point2D) c);
    }

    /**
     * Revisa si mi arista intersecta a la arista ingresada como parámetro.
     * <P>
     *    Para ello se debe cumplir que un punto de la arista parametro debe dar signo &gt;= 0 y la otra &lt;=0
     * respecto de this, y los puntos de esta arista arista deben dar signos alternados respecto a la arista
     * parametro. 
     * 
     * @param arista
     * @return 
     */
    public boolean intersects(Edge2D arista) {
        boolean x1 = ((signed2DTriArea(arista.a) >=0) && (signed2DTriArea(arista.b) <=0)) || 
                ((signed2DTriArea(arista.a) <=0) && (signed2DTriArea(arista.b) >=0));
        boolean x2 = ((arista.signed2DTriArea(this.a) >=0) && (arista.signed2DTriArea(this.b) <=0)) || 
                ((arista.signed2DTriArea(this.a) <=0) && (arista.signed2DTriArea(this.b) >=0));
        return (x1 && x2);
    }    
    
    @Override
    public boolean intersects(Edge arista) {
        if (arista==null)
            return false;
        if (getClass()!=arista.getClass())
            return false;
        return intersects((Edge2D)arista);
    }
            
    @Override
    public double getDistance() {
        return a.distanceTo(b);
    }

    @Override
    public void add(Point p){
        a.add(p);
        b.add(p);
    }
    
    @Override
    public String toString() {
        return "Edge2D{a=" + a + ", b=" + b + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final Edge2D other = (Edge2D) obj;
        
        return !((!Objects.equals(this.a, other.a) || !Objects.equals(this.b, other.b)) &&
                (!Objects.equals(this.a, other.b) || !Objects.equals(this.b, other.a)));
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Point2D p0, p1;
        p0 = new Point2D(this.a.getCoords(0), this.a.getCoords(1));
        p1 = new Point2D(this.b.getCoords(0), this.b.getCoords(1));
        
        return new Edge2D(p0, p1);
    }
    
    
}
