package primitives;

import comparators.AABB2DComparator;
import comparators.AABB2DComparatorArea;
import comparators.AABB2DComparatorXAxisByCenter;
import comparators.AABB2DComparatorYAxisByCenter;
import comparators.ElementComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import sap.Element;

/**
 * Caja en 2D que es subclase de BV (bounding Volume), e implementa
 * las interfaces Comparable y Cloneable
 * @author hmoraga
 */
public class AABB2D implements BV, Comparable<AABB2D>, Cloneable{
    private Point2D p; //p corresponde al punto inferior mas a la izq
    private Point2D q; //q corresponde al punto superior mas a la derecha
    private Primitiva arista; // elemento que es envuelto por el AABB2D
    
    /**
     * Constructor por defecto, centrado en (0,0) y con dimensiones alto=0 y 
     * ancho=0, que no contiene ningún elemento.
     */
    public AABB2D() {
	p = new Point2D();
        q = new Point2D();
        arista = null;
    }

    /**
     * Constructor de la caja, con una lista de puntos
     * <P>
     * De la lista de entrada se busca la menor coordenada en X y en Y, 
     * generando el punto p, y la mayor coordenada en X y en Y, generando el 
     * punto q.
     * 
     * @param listaPuntos lista de Point2D
     */
    public AABB2D(List<Point2D> listaPuntos) {
        if (listaPuntos.isEmpty()){
            p = new Point2D();
            q = new Point2D();
            arista = null;
        } else {
            double minX = listaPuntos.get(0).getX(), minY = listaPuntos.get(0).getY();
            double maxX = listaPuntos.get(0).getX(), maxY = listaPuntos.get(0).getY();
	
            //busco las dimensiones maximo y minimo de toda la lista de puntos ingresada
            for (Point2D listaPunto : listaPuntos) {
                if (minX > listaPunto.getCoords(0)) {
                    minX = listaPunto.getCoords(0);
                }
                if (minY > listaPunto.getCoords(1)) {
                    minY = listaPunto.getCoords(1);
                }
                if (maxX < listaPunto.getCoords(0)) {
                    maxX = listaPunto.getCoords(0);
                }
                if (maxY < listaPunto.getCoords(1)) {
                    maxY = listaPunto.getCoords(1);
                }
            }

            this.p = new Point2D(minX, minY);
            this.q = new Point2D(maxX, maxY);
            arista = null;
        }
    }

    /**
     * 
     * @param center
     * @param largoX
     * @param largoY 
     */
    public AABB2D(Point2D center, double largoX, double largoY){
	assert(largoX>=0);
        assert(largoY>=0);
        
        this.p = new Point2D(center.getX()-largoX/2, center.getY()-largoY/2);
        this.q = new Point2D(center.getX()+largoX/2, center.getY()+largoY/2);
        arista = null;
    }

    public AABB2D(Edge2D arista, int objIndex){
	Pair<Point2D> puntos = arista.getPoints();
	this.p = new Point2D(Math.min(puntos.first.getX(), puntos.second.getX()),
                Math.min(puntos.first.getY(), puntos.second.getY()));
        this.q = new Point2D(Math.max(puntos.first.getX(), puntos.second.getX()),
                Math.max(puntos.first.getY(), puntos.second.getY()));
        this.arista = new Primitiva2D(arista, objIndex);
    }
    
    public Point2D getCentro(){
        return new Point2D((p.getX()+q.getX())/2, (p.getY()+q.getY())/2);
    }
    
    public Primitiva getPrimitiva() {
        return (arista==null)?null:arista;
    }

    public Edge getEdge2D(){
        return (arista==null)?null:arista.getEdge();
    }
    
    public AABB2D BVintersection(AABB2D other) {
        double minX, maxX, minY, maxY;

        if (intersects(other) && !isInside(other) && !(other.isInside(this))){
            //busco intersecciones POR CADA EJE
            //eje X
            if ((getMinX() <= other.getMinX()) && (other.getMinX() <= getMaxX())){
		minX = other.getMinX();
		maxX = this.getMaxX();
            } else if ((other.getMinX() <= getMinX()) && (getMinX() <= other.getMaxX())){
		minX = getMinX();
		maxX = other.getMaxX();
            } else if ((getMinX() <= other.getMinX()) && (other.getMaxX() <= getMaxX())){
		minX = other.getMinX();
		maxX = other.getMaxX();
            } else {//if ((other.getMinX()<=this->getMinX()) && (this->getMaxX()<=other.getMaxX())){
		minX = getMinX();
		maxX = getMaxX();
            }

            //eje Y
            if ((getMinY() <= other.getMinY()) && (other.getMinY() <= getMaxY())){
                minY = other.getMinY();
		maxY = getMaxY();
            } else if ((other.getMinY() <= getMinY()) && (getMinY() <= other.getMaxY())){
		minY = getMinY();
		maxY = other.getMaxY();
            } else if ((getMinY() <= other.getMinY()) && (other.getMaxY() <= getMaxY())){
		minY = other.getMinY();
		maxY = other.getMaxY();
            } else {//if ((other.getMinY()<=this->getMinY()) && (this->getMaxY()<=other.getMaxY())){
		minY = getMinY();
		maxY = getMaxY(); 
            }

            System.out.println("X=[" + minX + ";" + maxX +"]");
            System.out.println("Y=[" + minY + ";" + maxY +"]");
            
            if (((maxX-minX)!=0) && (maxY-minY)!=0)
                return new AABB2D(new Point2D((minX+maxX)/2,(minY+maxY)/2),(maxX-minX),(maxY-minY));
            else return null;
	} else if (isInside(other) && !other.isInside(this)){
            return this;
	} else if (other.isInside(this) && !this.isInside(other)){
            return other;
	} else return null;
    }

    public boolean intersects(AABB2D otro) {
        Point2D pthis = getCentro();
	Point2D pthat = otro.getCentro();

        if ((this==null) || (otro==null))
            return false;
        
	if (Math.abs(pthis.getX() - pthat.getX()) > (getLargoX() + otro.getLargoX())/2) 
            return false;
	return Math.abs(pthis.getY() - pthat.getY()) <= (getLargoY() + otro.getLargoY())/2;
    }

    public AABB2D unionBV(AABB2D t) {
	double minimumX = Math.min(getMinX(),t.getMinX());
	double minimumY = Math.min(getMinY(),t.getMinY());
	double maximumX = Math.max(getMaxX(),t.getMaxX());
	double maximumY = Math.max(getMaxY(),t.getMaxY());

	Point2D center= new Point2D((minimumX+maximumX)/2,(minimumY+maximumY)/2);

	return new AABB2D(center, (maximumX-minimumX), (maximumY-minimumY));
    }
    
    public boolean isInside(AABB2D box) {        
	List<Point2D> listaPuntos = this.getPoints();
        
	return (listaPuntos.get(0).isInside(box) && 
		listaPuntos.get(1).isInside(box) &&
		listaPuntos.get(2).isInside(box) &&
		listaPuntos.get(3).isInside(box));
    }
    
    @Override
    public double getProductoDimensiones() {
        return getArea();
    }

    @Override
    public double getMinimum(int dimension) {
        assert(dimension>=0 && dimension<=2);
        
	switch (dimension){
	case 0:
		return getMinX();
	case 1:
		return getMinY();
	default:
		return Double.NEGATIVE_INFINITY;
        }
    }

    @Override
    public double getMaximum(int dimension) {
        assert(dimension>=0 && dimension<=2);
        
	switch (dimension){
	case 0:
		return getMaxX();
	case 1:
		return getMaxY();
	default:
		return Double.POSITIVE_INFINITY;
        }
    }
    
    public double getMinX() {
        return this.p.getX();
    }
    
    public double getMaxX() {
        return q.getX();
    }
    
    public double getMinY() {
        return p.getY();
    }
    
    public double getMaxY() {
        return q.getY();
    }

    public double getArea() {
        double largoX = Math.abs(p.getX()-q.getX());
        double largoY = Math.abs(p.getY()-q.getY());
        return (largoX*largoY);
    }

    @Override
    public String toString() {
        return "AABB2D{" + "p=" + p + ", q=" + q + ", arista=" + arista + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.p);
        hash = 79 * hash + Objects.hashCode(this.q);
        hash = 79 * hash + Objects.hashCode(this.arista);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AABB2D other = (AABB2D) obj;
        if (!Objects.equals(this.p, other.p)) {
            return false;
        }
        if (!Objects.equals(this.q, other.q)) {
            return false;
        }
        if (!Objects.equals(this.arista, other.arista)) {
            return false;
        }
        return true;
    }
    
    public boolean isLeaf() {
        return (arista!=null)?(arista.getIndice()!=-1):false;
    }

    public int getIndex() {
        return (arista!=null)?arista.getIndice():-1;
    }

    public List<Point2D> getPoints() {
        List<Point2D> listPoints = new ArrayList<>();

	//order (minx, miny) to (minx, maxy) right hand rule
	listPoints.add(this.p);
	listPoints.add(new Point2D(q.getX(), p.getY()));
	listPoints.add(this.q);
	listPoints.add(new Point2D(p.getX(), q.getY()));

	return listPoints;
    }

    public static Pair<Double> getMean(List<? extends BV> listaCajas) {
        assert(listaCajas.size()>0);
        double resX=0, resY=0;
        
        Iterator<? extends BV> it = listaCajas.iterator();
        
        while (it.hasNext()){
            BV box = it.next();
            Point2D pto = ((AABB2D)box).getCentro();
            
            double ptoX = pto.getX();
            double ptoY = pto.getY();

            resX+=ptoX;
            resY+=ptoY;
        }

        resX/=listaCajas.size();
	resY/=listaCajas.size();

	return new Pair<>(resX, resY);
    }

    public static Pair<Double> getVariance(List<? extends BV> listaCajas) {
        assert(listaCajas.size()>0);
        
        Pair<Double> mean = getMean(listaCajas); 
	double resX=0, resY=0;

        Iterator<? extends BV> it = listaCajas.iterator();
        
        while (it.hasNext()){
            BV box = it.next();
            Point2D pto = ((AABB2D)box).getCentro();
            
            double ptoX = pto.getX();
            double ptoY = pto.getY();

            resX += Math.pow(ptoX,2.0);
            resY += Math.pow(ptoY,2.0);
        }

	resX /= listaCajas.size();
	resY /= listaCajas.size();

	resX -= Math.pow(mean.first,2.0);
	resY -= Math.pow(mean.second,2.0);

        return new Pair<>(resX, resY);
    }

    public static void collidePrimitives(List<Pair<Integer>> listaColisiones, AABB2D objetoUno, AABB2D objetoDos){
        if (objetoUno.intersects(objetoDos)){
            Edge p = objetoUno.getEdge2D();
            Edge q = objetoDos.getEdge2D();
            
            if (p!=null && q!=null){
                if (p.intersects(q))
                    listaColisiones.add(new Pair<>(objetoUno.getIndex(), objetoDos.getIndex()));
            }
	}
    }
    
    public static void sortFunction(List<AABB2D> listaCajas){
	Pair<Double> varianzas = AABB2D.getVariance(listaCajas);

	if (varianzas.first > varianzas.second)
            Collections.sort(listaCajas, new AABB2DComparatorXAxisByCenter());
        else if (varianzas.first < varianzas.second)
            Collections.sort(listaCajas, new AABB2DComparatorYAxisByCenter());
        else
            Collections.sort(listaCajas, new AABB2DComparatorArea());
    }

    @Override
    public BV unionBV(BV other) {
        return unionBV((AABB2D)other);
    }

    @Override
    public boolean intersects(BV data) {
        return intersects((AABB2D)data);
    }
    
    public Pair<Element> getElements(int dimension){
        if (getIndex()==-1){
            switch (dimension){
                case 0:
                    return new Pair<>(new Element(getMinX(), false), new Element(getMaxX(), true));
                case 1:
                    return new Pair<>(new Element(getMinY(), false), new Element(getMaxY(), true));
                default:
            }
        } else {
            switch (dimension){
                case 0:
                    return new Pair<>(new Element(getMinX(), getIndex(), false), new Element(getMaxX(), getIndex(), true));
                case 1:
                    return new Pair<>(new Element(getMinY(), getIndex(), false), new Element(getMaxY(), getIndex(), true));
                default:
            }
        }
        return null;
    }
    
    public static List<Pair<Integer>> obtenerListaPares(List<AABB2D> listaCajas, int dimension){
        List<Element> listaElementos = new ArrayList<>();
 
        for (int i=0; i< listaCajas.size(); i++) {
            AABB2D caja = listaCajas.get(i);
            Pair<Element> par = caja.getElements(dimension);
            par.first.setId(i);
            par.second.setId(i);
            listaElementos.add(par.first);
            listaElementos.add(par.second);
        }
        
        Collections.sort(listaElementos, new ElementComparator());
                
        return Element.obtenerListaPares(listaElementos);
    }

    public void move(Point2D pto) {
        if (arista!=null){
            arista.getEdge().add(pto);
        }
        
        p.add(pto);
        q.add(pto);
    }

    public double getLargoX() {
        return Math.abs(q.getX()-p.getX());
    }

    public double getLargoY() {
        return Math.abs(q.getY()-p.getY());
    }

    /**
     * Comparador "natural" entre AABB2D, me compara en base al punto mas a la
     * izquierda y mas abajo en ambas cajas.
     * 
     * @param o caja con la que comparar
     * @return 
     */
    @Override
    public int compareTo(AABB2D o) {
        return new AABB2DComparator().compare(this, o);
    }
    
    public int compareTo(AABB2D o, int dimension) {
        return new AABB2DComparator(dimension).compare(this, o);
    }
    
    @Override
    public int compareTo(BV o, int dimension) {
        return new AABB2DComparator(dimension).compare(this, (AABB2D)o);
    }
}
