/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

/**
 *
 * @author hmoraga
 */
public abstract class Edge implements Cloneable{
    public Point a;
    public Point b;

    public Edge() {
    }

    public Edge(Point a, Point b) {
        this.a = a;
        this.b = b;
    }

    public abstract double getDistance();
    public abstract boolean intersects(Edge arista);    
    public abstract void add(Point pto);
}
