package primitives;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author hmoraga
 *
 */
public interface BV extends Cloneable{
    public abstract double getMinimum(int dimension);    
    public abstract double getMaximum(int dimension);
    public abstract double getProductoDimensiones();
    public abstract BV unionBV(BV other);
    public abstract boolean intersects(BV data);
    public int compareTo(BV volume, int dim);
}
