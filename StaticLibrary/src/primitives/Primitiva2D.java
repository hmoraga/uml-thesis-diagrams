/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

import java.util.Objects;

/**
 *
 * @author hmoraga
 */
public class Primitiva2D implements Primitiva, Cloneable{
    private final Edge2D objeto;
    private final int indice;

    public Primitiva2D(Edge2D objeto, int indice) {
        this.objeto = objeto;
        this.indice = indice;
    }

    @Override
    public String toString() {
        return "Primitiva{" + "objeto=" + objeto + ", indice=" + indice + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.objeto);
        hash = 17 * hash + this.indice;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Primitiva2D other = (Primitiva2D) obj;
        if (this.indice != other.indice) {
            return false;
        }
        return Objects.equals(this.objeto, other.objeto);
    }
    
    @Override
    public Edge2D getEdge() {
        return objeto;
    }

    @Override
    public int getIndice() {
        return indice;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Edge2D obj = new Edge2D((Point2D)objeto.a, (Point2D)objeto.b);
        Primitiva2D prim = new Primitiva2D(obj, indice);

        return prim;
    }
    
    
}
