/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author hmoraga
 * @param <T>
 */
public class Pair<T>{
    public T first;
    public T second;
    
    public Pair(T first, T second){
        this.first = first;
        this.second = second;
    }

    @Override
    public String toString() {
        return "(" + first.toString() + "," + second.toString() + ')';
    }
    
    public Pair<T> inverted(){
        return new Pair<>(second, first);
    }
    
    /**
     * If this exists inside an List of Pairs
     * @param listaPares lista de pares
     * @return revisa si el par existe dentro de la lista
     */
    public boolean exists(List<Pair<T>> listaPares){
        return (listaPares.contains(this) || listaPares.contains(this.inverted()));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.first);
        hash = 97 * hash + Objects.hashCode(this.second);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pair<?> other = (Pair<?>) obj;
        
        return (Objects.equals(this.first, other.first) && Objects.equals(this.second, other.second)) ||
                (Objects.equals(this.first, other.second) && Objects.equals(this.second, other.first));
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public void setSecond(T second) {
        this.second = second;
    }
}
