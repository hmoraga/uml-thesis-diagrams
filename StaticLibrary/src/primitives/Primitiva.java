/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

/**
 *
 * @author hmoraga
 */
public interface Primitiva extends Cloneable{
    public Edge getEdge();
    public int getIndice();
}
