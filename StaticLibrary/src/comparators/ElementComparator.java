/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparators;

import java.util.Comparator;
import sap.Element;

/**
 *
 * @author hmoraga
 */
public class ElementComparator implements Comparator<Element>{

    @Override
    public int compare(Element o1, Element o2) {
        if (o1.equals(o2))
            return 0;
        else if (o1.getValue() < o2.getValue())
            return -1;
        else if (o1.getValue() > o2.getValue())
            return 1;
        else {
            if (!o1.isFin() && !o2.isFin())
                return (o1.getId()<o2.getId())?-1:1;
            else if (o1.isFin() && !o2.isFin())
                return 1;
            else if (!o1.isFin() && o2.isFin())
                return -1;
            else return 1;
        }
    }
}
