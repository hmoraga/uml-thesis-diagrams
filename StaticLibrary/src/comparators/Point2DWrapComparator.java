/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparators;

import java.util.Comparator;
import primitives.Point2D;
import sap.Point2DWrap;

/**
 *
 * @author hmoraga
 */
public class Point2DWrapComparator implements Comparator<Point2DWrap>{
    private final double time;
    private final int dimension;

    public Point2DWrapComparator(double time, int dimension) {
        this.time = time;
        this.dimension = dimension;
    }
    
    @Override
    public int compare(Point2DWrap o1, Point2DWrap o2) {
        double delta = 0.00001;
        Point2D thisPoint = o1.currentPosition(time);
        Point2D thatPoint = o2.currentPosition(time);
        Point2D thisExtra = o1.currentPosition(time+delta);
        Point2D thatExtra = o2.currentPosition(time+delta);
        
        if (dimension==0) {
            if ((thisPoint.getX()>thatPoint.getX()) ||
                    thisExtra.getX()>thatExtra.getX())
                return 1;
            else if ((thisPoint.getX()<thatPoint.getX()) ||
                    thisExtra.getX()<thatExtra.getX())
                return -1;
            else return 0;
        } else {
            if ((thisPoint.getY()>thatPoint.getY()) ||
                    thisExtra.getY()>thatExtra.getY())
                return 1;
            else if ((thisPoint.getY()<thatPoint.getY()) ||
                    thisExtra.getY()<thatExtra.getY())
                return -1;
            else return 0;
        }
    }
}
