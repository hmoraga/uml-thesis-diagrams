/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparators;

import java.util.Comparator;
import primitives.AABB2D;
import primitives.BVNode;

/**
 *
 * @author hmoraga
 */
public class BVNodeComparator  implements Comparator<BVNode>{

    @Override
    public int compare(BVNode o1, BVNode o2) {
        if (o1.getData().getClass().getSimpleName().equals("AABB2D") && o2.getData().getClass().getSimpleName().equals("AABB2D")){
            // se compara primero por area, luego por posX y finalmente por posicion Y
            AABB2D obj1 = (AABB2D)o1.getData();
            AABB2D obj2 = (AABB2D)o2.getData();
            
            if (obj1.getArea() == obj2.getArea()){
                if (obj1.getMinX() == obj2.getMinX()){
                    if (obj1.getMinY() < obj2.getMinY())
                        return -1;
                    else if (obj1.getMinY() > obj2.getMinY())
                        return 1;
                    else return 0;
                } else if (obj1.getMinX() < obj2.getMinX())
                    return -1;
                else return 1;
            } else if (obj1.getArea() < obj2.getArea())
                return -1;
            else return 1;
        } else return 0;    // los objetos no son de la misma clase
    }
    
}
