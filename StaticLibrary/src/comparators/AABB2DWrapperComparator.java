/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparators;

import java.util.Comparator;
import wrappers.AABB2DWrapper;

/**
 * Implementa el comparador natural del AABB2D, indicando por cual coordenada
 * hay que ordenar.
 * 
 * @author hmoraga
 */
public class AABB2DWrapperComparator implements Comparator<AABB2DWrapper>{
    private final int dimension;

    public AABB2DWrapperComparator() {
        this.dimension=0;
    }
    
    public AABB2DWrapperComparator(int dimension) {
        this.dimension=dimension;
    }
    
    /**
     * Metodo que me compara en base al punto mas a la izquierda y mas abajo en
     * ambos. 
     * @param o1 primera caja
     * @param o2 segunda caja
     * @return 
     */
    @Override
    public int compare(AABB2DWrapper o1, AABB2DWrapper o2) {
        return new AABB2DComparator(dimension).compare(o1.getBox(), o2.getBox());
    }
}
