/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparators;

import java.util.Comparator;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class Point2DComparatorXaxis implements Comparator<Point2D>{
    @Override
    public int compare(Point2D o1, Point2D o2) {
        if (o1.getX() > o2.getX())
            return 1;
        else if (o1.getX() < o2.getX())
            return -1;
        else
            return 0;
    }
}
