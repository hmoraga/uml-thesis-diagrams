package poligons;

import java.util.Arrays;

/**
 *
 * @author hmoraga
 */
public class Rectangulo extends Cuadrilatero{
    /**
     * Rectangulo inscrito en una circunferencia de radio 1
     * @param ancho proporcion para el ancho 
     * @param alto proporcion para el alto
     * @param rotate si se rota la figura al azar o no
     */
    public Rectangulo(double ancho, double alto, boolean rotate)  throws NumberFormatException{
        super(Arrays.asList(new Double[]{
            Math.atan2(alto, ancho),
            Math.PI-Math.atan2(alto, ancho),
            Math.PI+Math.atan2(alto, ancho),
            2*Math.PI-Math.atan2(alto, ancho)}),
                rotate);
    }

    public Rectangulo(double ancho, double alto, double areaTotal, boolean rotate) throws NumberFormatException{
        this(ancho, alto, rotate);
        super.amplificar(getScale(areaTotal));        
    }
    
    @Override
    public final double getScale(double areaTotal){
        double alpha = Math.atan2(listaPuntos.get(0).getY(), listaPuntos.get(0).getX());
        double area0 = 2*Math.sin(2*alpha);
        return Math.sqrt(areaTotal/area0);
    }
}
