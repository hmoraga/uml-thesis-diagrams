/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

/**
 *
 * @author hmoraga
 */
public class Pentagono extends Polygon2D{

    /**
     * Me genera un pentagono de area final dada
     * @param areaFinal area final del pentagono
     * @param rotate si se rota o no un valor aleatorio
     */
    public Pentagono(double areaFinal, boolean rotate) {
        this(rotate);
        super.amplificar(getScale(areaFinal));
    }

    /**
     * Me genera un pentagono inscrito en una circunferencia de lado 1
     * @param rotate si se rota o no un valor aleatorio
     */    
    public Pentagono(boolean rotate) {
        super(5, rotate);
    }

    @Override
    public double getScale(double areaFinal) {
        double area0 = 2.5*Math.sin(2*Math.PI/5);
        
        return Math.sqrt(areaFinal/area0);
    }
}
