/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import java.util.Arrays;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class Estrella4Puntas extends Polygon2D{
    
    public Estrella4Puntas(boolean rotate) {
        super(Arrays.asList(new Point2D[]{new Point2D(1,0),
            new Point2D(0.5, 0.25), 
            new Point2D(0.25, 0.125),
            new Point2D(0.125, 0.25),
            new Point2D(0.25,0.5),
            new Point2D(0,1),
            new Point2D(-0.25,0.5),
            new Point2D(-0.125,0.25),
            new Point2D(-0.25, 0.125),
            new Point2D(-0.5, 0.25),
            new Point2D(-1,0),
            new Point2D(-0.5,-0.25),
            new Point2D(-0.25,-0.125),
            new Point2D(-0.125,-0.25),
            new Point2D(-0.25,-0.5),
            new Point2D(0,-1),
            new Point2D(0.25,-0.5),
            new Point2D(0.125,-0.25),
            new Point2D(0.25, -0.125),
            new Point2D(0.5,-0.25)}), rotate);
    }
    
    /**
     * 
     * @param areaFinal
     * @param rotate 
     */
    public Estrella4Puntas(double areaFinal, boolean rotate) {
        this(rotate);
        super.amplificar(getScale(areaFinal));
    }
    
    @Override
    public final double getScale(double areaFinal) {
        return Math.sqrt(areaFinal*32/35);
    }
    
}
