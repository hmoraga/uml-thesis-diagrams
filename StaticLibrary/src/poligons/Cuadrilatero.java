package poligons;

import java.util.Arrays;
import java.util.List;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class Cuadrilatero extends Polygon2D {
    private double alfa, beta, gamma, delta;
    /**
     * se puede generar un cuadrilatero cualquiera<P>
     * para un rectangulo: anglesList={alpha, (Math.PI-alpha), (Math.PI+alpha, 2*Math.PI-alpha},
     * donde: 0 < alpha < Math.PI/2<P>
     * para un cuadrado: anglesList={0; Math.PI/2; Math.PI; 3*Math.PI/2}<P>
     * para un trapecio isosceles: anglesList={alpha, (Math.PI-alpha), (Math.PI+beta, 2*Math.PI-beta},
     * donde: 0 < alpha, beta < Math.PI/2 y alpha != beta<P>
     * @param listAngles
     * @param areaFinal
     * @param rotate
     **/
    public Cuadrilatero(List<Double> listAngles, double areaFinal, boolean rotate) throws NumberFormatException{
        this(listAngles, rotate);
        
        double scale = getScale(areaFinal);
        super.amplificar(scale);
    }
    
    public Cuadrilatero(List<Double> listAngles, boolean rotate) throws NumberFormatException{
        super(Arrays.asList(new Point2D[]{
            new Point2D(Math.cos(listAngles.get(0)), Math.sin(listAngles.get(0))),
            new Point2D(Math.cos(listAngles.get(1)), Math.sin(listAngles.get(1))),
            new Point2D(Math.cos(listAngles.get(2)), Math.sin(listAngles.get(2))),
            new Point2D(Math.cos(listAngles.get(3)), Math.sin(listAngles.get(3)))}),
                rotate);
        assert(listAngles.get(3)<=2*Math.PI);
        assert(listAngles.get(3)>listAngles.get(2));
        assert(listAngles.get(2)>listAngles.get(1));
        assert(listAngles.get(1)>listAngles.get(0));
        assert(listAngles.get(0)>=0);
        
        alfa = listAngles.get(0);
        beta = listAngles.get(1)-listAngles.get(0);
        gamma = listAngles.get(2)-listAngles.get(1);
        delta = listAngles.get(3)-listAngles.get(2);
    }

    @Override
    public double getScale(double areaTotal) {
        double areaLargoUno = (Math.sin(beta/2)*Math.cos(beta/2)+Math.sin(gamma/2)*Math.cos(gamma/2)+Math.sin(delta/2)*Math.cos(delta/2)+Math.sin((2*Math.PI-beta-gamma-delta)/2)*Math.cos((2*Math.PI-beta-gamma-delta)/2));
        
        return Math.sqrt(areaTotal/areaLargoUno);
    }
}
