package poligons;

/**
 *
 * @author hmoraga
 */
public class Cuadrado extends Polygon2D {

    /**
     * Me genera un cuadrado de area final dada
     * @param areaFinal area final del cuadrado
     * @param rotate si se rota o no un valor aleatorio
     */
    public Cuadrado(double areaFinal, boolean rotate) {
        this(rotate);
        double scale = getScale(areaFinal);
        super.amplificar(scale);
    }

    /**
     * Me genera un cuadrado inscrito en una circunferencia de lado 1
     * @param rotate si se rota o no un valor aleatorio
     */    
    public Cuadrado(boolean rotate) {
        super(4, rotate);
    }

    @Override
    public double getScale(double areaFinal) {
        return Math.sqrt(areaFinal/2);
    }
}
