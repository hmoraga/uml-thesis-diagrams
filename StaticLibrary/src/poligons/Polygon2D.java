package poligons;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import primitives.AABB2D;
import primitives.Edge2D;
import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public abstract class Polygon2D {
    protected List<Point2D> listaPuntos;
    
    /**
     * Constructor de un poligono regular en 2D, inscrito en una circunferencia de lado 1
     * @param listaPuntos lista de puntos en orden
     * @param rotate verdadero si se desea rotar el poligono de forma aleatoria
     */    
    public Polygon2D(List<Point2D> listaPuntos, boolean rotate){
        assert(listaPuntos.size()>=3);
        this.listaPuntos = new ArrayList<>(listaPuntos);
        
        Random rnd = new Random(System.currentTimeMillis());

        double rndAngle = (rotate)?2*Math.PI*rnd.nextDouble():0.0;

        rotar(rndAngle);
    }
    
    /**
     * Constructor de un n-agono regular en 2D
     * @param verticesNumber cantidad de vertices de un poligono convexo regular
     * @param rotate verdadero si se desea rotar el poligono de forma aleatoria
     */
    public Polygon2D(int verticesNumber, boolean rotate){
        assert(verticesNumber>=3);
        
        listaPuntos = new ArrayList<>(verticesNumber);
        Random rnd = new Random(System.currentTimeMillis());

        double rndAngle = (rotate)?2*Math.PI*rnd.nextDouble():0.0;
        double centralAngle = 2*Math.PI/verticesNumber;        

        for (int k = 0; k < verticesNumber; k++) {
            Point2D p = new Point2D(Math.cos(centralAngle*k), Math.sin(centralAngle*k));
            listaPuntos.add(p);
        }
        
        rotar(rndAngle);
    }
    
    public List<Point2D> getVertices() {
        return listaPuntos;
    }
    
    public void desplazar(double x, double y){
        for (Point2D vertice : listaPuntos)
            vertice.add(x,y);
    }
    
    public void desplazar(Point2D p){
        for (Point2D vertice : listaPuntos)
            vertice.add(p);
    }
    
    public double getArea(){
        //genero un punto al azar 
        Random rnd = new Random(System.currentTimeMillis());
        double x = 2*rnd.nextDouble()-1;
        double y = 2*rnd.nextDouble()-1;
        double area = 0;
        
        Point2D p = new Point2D(x, y);
        
        // calculo el area de cada triangulo entre la arista del poligono y
        // el punto creado al azar
        for (Edge2D arista : getEdgeList()) {
            // sumo o resto el area de acuerdo a la regla de la mano derecha
            area += arista.signed2DTriArea(p)/2;
        }
        
        return area;
    }

    public List<Double> getListX(){
        List<Double> listaCoordsX = new ArrayList<>();
        
        for (Point2D p : this.listaPuntos) {
            listaCoordsX.add(p.getCoords(0));
        }
        
        return listaCoordsX;
    }
    
    public List<Double> getListY(){
        List<Double> listaCoordsY = new ArrayList<>();
        
        for (Point2D p : this.listaPuntos) {
            listaCoordsY.add(p.getCoords(1));
        }
        
        return listaCoordsY;
    }
    
    public List<Edge2D> getEdgeList(){
        List<Edge2D> lista = new ArrayList<>();
        int n = this.listaPuntos.size();
        
        for (int i=0; i<this.listaPuntos.size(); i++)
            lista.add(new Edge2D(listaPuntos.get(i), listaPuntos.get((i+1)%n)));
        
        return lista;
    }

    public AABB2D getBox(){
        if (!listaPuntos.isEmpty()){
            double minX = listaPuntos.get(0).getX(), minY = listaPuntos.get(0).getY();
            double maxX = listaPuntos.get(0).getX(), maxY = listaPuntos.get(0).getY();
        
            for (Point2D p : listaPuntos) {
                if (p.getX() > maxX)
                    maxX = p.getX();
                if (p.getX() < minX)
                    minX = p.getX();
                if (p.getY() > maxY)
                    maxY = p.getY();
                if (p.getY() < minY)
                    minY = p.getY();
            }
        
            return new AABB2D(new Point2D((minX+maxX)/2, (minY+maxY)/2), (maxX-minX), (maxY-minY));
        }
        return null;
    }
    
    private void rotar(double angle){
        for (int i=0; i<listaPuntos.size(); i++) {
            Point2D p = listaPuntos.get(i);
            listaPuntos.set(i, p.rotateBy(angle));
        }
    }

    public void amplificar(double scale){
        for (int i = 0; i < listaPuntos.size(); i++) {
            listaPuntos.set(i, listaPuntos.get(i).multipliedByScalar(scale));
        }
    }
    
    public abstract double getScale(double areaTotal);

    public void clear() {
        listaPuntos.clear();
    }
    
    /**
     * Transformacion a un espacio tipo pantalla, donde el maximo vertical se
     * encuentra hacia abajo y el maximo horizontal a la derecha
     * @param limitesEspacioSimulacion
     * @param limitesEspacioGrafico
     * @return 
     */
    public List<Point2D> toScreenTransformation(Pair<Pair<Double>> limitesEspacioSimulacion, 
            Pair<Pair<Integer>> limitesEspacioGrafico){
        List<Point2D> listaPuntosLocal = new ArrayList<>();
        
        double Asimulacion = (limitesEspacioSimulacion.first.second-limitesEspacioSimulacion.first.first);
        double Bsimulacion = (limitesEspacioSimulacion.second.second-limitesEspacioSimulacion.second.first);
        double Ascreen = (limitesEspacioGrafico.first.second-limitesEspacioGrafico.first.first);
        double Bscreen = (limitesEspacioGrafico.second.second-limitesEspacioGrafico.second.first);
        
        for (Point2D p : this.getVertices()) {
            double xp = limitesEspacioGrafico.first.first + (p.getX()-limitesEspacioSimulacion.first.first)*Ascreen/Asimulacion;
            double yp = limitesEspacioGrafico.second.second - (p.getY()-limitesEspacioSimulacion.second.first)*Bscreen/Bsimulacion;
            
            listaPuntosLocal.add(new Point2D(xp, yp));
        }
        
        return listaPuntosLocal;
    }
}
