/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import java.util.List;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class AnyPolygon2D extends Polygon2D {

    public AnyPolygon2D(List<Point2D> listaPuntos, boolean rotate) {
        super(listaPuntos, rotate);
    }

    public AnyPolygon2D(int verticesNumber, boolean rotate) {
        super(verticesNumber, rotate);
    }
    
    @Override
    public double getScale(double areaTotal) {
        return 1.0;
    }
    
}
