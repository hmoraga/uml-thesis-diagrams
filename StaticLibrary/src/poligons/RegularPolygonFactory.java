/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

/**
 *
 * @author hmoraga
 */
public class RegularPolygonFactory {
    public static Polygon2D getPolygon(PolygonType model, double areaTotal, boolean rotate) {
        Polygon2D poly = null;
        
        switch(model){
            case TRIANGULO:
                poly = new Triangulo(areaTotal, rotate);
                break;
            case CUADRADO:
                poly = new Cuadrado(areaTotal, rotate);
                break;
            /*case RECTANGULO:
                Random rnd = new Random(System.currentTimeMillis());
                int numero = rnd.nextInt(4);
                
                switch (numero){
                    case 0:
                        poly = new Rectangulo(1.0, 1.0, areaTotal, rotate);
                    case 1:
                        poly = new Rectangulo(3.0, 4.0, areaTotal, rotate);
                    case 2:
                        poly = new Rectangulo(5.0, 12.0, areaTotal, rotate);
                    default:
                        poly = new Rectangulo(7.0, 24.0, areaTotal, rotate);
                }
                
                break; */
            case PENTAGONO:
                poly = new Pentagono(areaTotal, rotate);
                break;
            case HEXAGONO:
                poly = new Hexagono(areaTotal, rotate);
                break;
            case ESTRELLA4:
                poly = new Estrella4Puntas(areaTotal, rotate);
                break;
            case ESTRELLA6:
                poly = new Estrella6Puntas(areaTotal, rotate);
                break;
            case BOOMERANG:
                poly = new Boomerang(areaTotal, rotate);
                break;
            default:
                break;    
        }
        
        return poly;
    }
    
}
