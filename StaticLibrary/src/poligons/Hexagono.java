package poligons;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author hmoraga
 */
public class Hexagono extends Polygon2D{

    /**
     * Me genera un hexagono de area final dada
     * @param areaFinal area final del pentagono
     * @param rotate si se rota o no un valor aleatorio
     */
    public Hexagono(double areaFinal, boolean rotate) {
        this(rotate);
        super.amplificar(getScale(areaFinal));
    }

    /**
     * Me genera un pentagono inscrito en una circunferencia de lado 1
     * @param rotate si se rota o no un valor aleatorio
     */    
    public Hexagono(boolean rotate) {
        super(6, rotate);
    }

    @Override
    public double getScale(double areaFinal) {
        double area0 = 3*Math.sin(Math.PI/3);
        
        return Math.sqrt(areaFinal/area0);
    }
}
