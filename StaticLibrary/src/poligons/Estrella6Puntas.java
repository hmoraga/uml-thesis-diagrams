package poligons;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Arrays;
import primitives.Point2D;

/**
 * Clase Estrella que hereda de Polygon2D.
 * @author hmoraga
 */
public class Estrella6Puntas extends Polygon2D{
    
    public Estrella6Puntas(boolean rotate) {
        super(Arrays.asList(new Point2D[]{
            new Point2D(Math.sqrt(3)/3, 0.0),
            new Point2D(Math.sqrt(3)/2,0.5), 
            new Point2D(Math.sqrt(3)/6, 0.5),
            new Point2D(0.0,1.0),
            new Point2D(-Math.sqrt(3)/6, 0.5),
            new Point2D(-Math.sqrt(3)/2, 0.5),
            new Point2D(-Math.sqrt(3)/3, 0.0),
            new Point2D(-Math.sqrt(3)/2, -0.5),
            new Point2D(-Math.sqrt(3)/6, -0.5),
            new Point2D(0.0,-1.0),
            new Point2D(Math.sqrt(3)/6, -0.5),
            new Point2D(Math.sqrt(3)/2, -0.5)}), rotate);
    }
    
    /**
     * Me genera una estrella regular de 6 puntas.
     * @param areaFinal
     * @param rotate si se hace una rotacion al azar o no
     */
    public Estrella6Puntas(double areaFinal, boolean rotate) {
        this(rotate);
        super.amplificar(getScale(areaFinal));
    }

    @Override
    public final double getScale(double areaFinal) {
        return Math.sqrt(areaFinal/Math.sqrt(3));
    }
}
