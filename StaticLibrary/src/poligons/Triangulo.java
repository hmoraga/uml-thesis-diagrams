package poligons;



/**
 * Clase Triangulo, que hereda de la clase Polygon2D
 * @author hmoraga
 */
public class Triangulo extends Polygon2D{
    /**
     * Genera un triangulo de areaFinal dada
     * 
     * @param areaFinal valor del area final del objeto
     * @param rotate si se da una rotacion al azar o no
     */
    public Triangulo(double areaFinal, boolean rotate) {
        this(rotate);   // triangulo generado en un circulo unitario
        // me devuelve el factor por el cual multiplicar cada punto para
        // tener un area final dada
        double scale = getScale(areaFinal);
        super.amplificar(scale);
    }

   /**
     * Genera un triangulo inscrito en una circunferencia de lado 1
     * 
     * @param rotate si se da una rotacion al azar o no
     */    
    public Triangulo(boolean rotate) {
        super(3, rotate);
    }

    @Override
    public double getScale(double areaFinal) {
        double area0 = 0.75*Math.sqrt(3);
        return Math.sqrt(areaFinal/area0);
    }
}
