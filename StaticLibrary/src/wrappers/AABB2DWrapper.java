/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wrappers;

import java.util.Objects;
import primitives.AABB2D;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class AABB2DWrapper {
    private AABB2D box;
    private int id;

    public AABB2DWrapper(AABB2D box, int id) {
        this.box = box;
        this.id = id;
    }

    public AABB2D getBox() {
        return box;
    }

    public void setBox(AABB2D box) {
        this.box = box;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.box);
        hash = 47 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AABB2DWrapper other = (AABB2DWrapper) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.box, other.box)) {
            return false;
        }
        return true;
    }
    
    public void move(Point2D p){
        this.box.move(p);
    }
}
