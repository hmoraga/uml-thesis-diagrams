/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import bruteforce.BruteforceSuite;
import comparators.ComparatorsSuite;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import poligons.PoligonsSuite;
import primitives.PrimitivesSuite;
import sap.SapSuite;
import wrappers.WrappersSuite;

/**
 *
 * @author hmoraga
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({SapSuite.class, PoligonsSuite.class, PrimitivesSuite.class, ComparatorsSuite.class, WrappersSuite.class, BruteforceSuite.class})
public class RootSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
