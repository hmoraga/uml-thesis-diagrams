/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bruteforce;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import poligons.AnyPolygon2D;
import poligons.Polygon2D;
import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class BruteForceCollisionTest {
    // se generara 3 cuadrados con distintas velocidades, las que colisionarán
    // en distintos momentos de la simulacion
    private Polygon2D c1, c2, c3, c4;   // los 4 poligonos
    private final List<Point2D> lista1, lista2, lista3, lista4;  // las 4 listas de puntos iniciales
    private Pair<Double> vel1, vel2, vel3, vel4;    // las 4 velocidades
    
    public BruteForceCollisionTest() {
        lista1 = new ArrayList<>();
        lista2 = new ArrayList<>();
        lista3 = new ArrayList<>();
        lista4 = new ArrayList<>();        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        lista1.add(new Point2D(-3,-3));
        lista1.add(new Point2D(3,-3));
        lista1.add(new Point2D(3,3));
        lista1.add(new Point2D(-3,3));

        lista2.add(new Point2D(-3.5,-2.5));
        lista2.add(new Point2D(2.5,-2.5));
        lista2.add(new Point2D(2.5,3.5));
        lista2.add(new Point2D(-3.5,3.5));
        
        lista3.add(new Point2D(-4,-2));
        lista3.add(new Point2D(2,-2));
        lista3.add(new Point2D(2,4));
        lista3.add(new Point2D(-4,4));

        lista4.add(new Point2D(-4.5,-1.5));
        lista4.add(new Point2D(1.5,-1.5));
        lista4.add(new Point2D(1.5,4.5));
        lista4.add(new Point2D(-4.5,4.5));
        
        c1 = new AnyPolygon2D(lista1, false);
        c2 = new AnyPolygon2D(lista2, false);
        c3 = new AnyPolygon2D(lista3, false);
        c4 = new AnyPolygon2D(lista4, false);
    }
    
    @After
    public void tearDown() {
        lista1.clear();
        lista2.clear();
        lista3.clear();
        lista4.clear();
        
        c1.clear();
        c2.clear();
        c3.clear();
        c4.clear();
    }

    @Test
    public void testGetColisiones() {
        System.out.println("getColisiones");
        List<Polygon2D> listaPoligonos = new ArrayList<>();
        listaPoligonos.add(c1);
        listaPoligonos.add(c2);
        listaPoligonos.add(c3);
        listaPoligonos.add(c4);
        
        BruteForceCollision instance = new BruteForceCollision(listaPoligonos);
        List<Pair<Integer>> expResult = new ArrayList<>();
        expResult.add(new Pair<>(0,1));
        expResult.add(new Pair<>(0,2));
        expResult.add(new Pair<>(0,3));
        expResult.add(new Pair<>(1,2));
        expResult.add(new Pair<>(1,3));
        expResult.add(new Pair<>(2,3));
        
        List<Pair<Integer>> result = instance.getColisiones();
        assertEquals(expResult.get(0), result.get(0));
        assertEquals(expResult.get(1), result.get(1));
        assertEquals(expResult.get(2), result.get(2));
        assertEquals(expResult.get(3), result.get(3));
        assertEquals(expResult.get(4), result.get(4));
        assertEquals(expResult.get(5), result.get(5));
        
        
    }
    
}
