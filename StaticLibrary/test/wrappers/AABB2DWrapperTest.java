/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wrappers;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import primitives.AABB2D;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class AABB2DWrapperTest {
    private AABB2DWrapper boxWrap;
    
    public AABB2DWrapperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        List<Point2D> listaPuntos = new ArrayList<>();
        listaPuntos.add(new Point2D(4,1));
        listaPuntos.add(new Point2D(7,3));
        listaPuntos.add(new Point2D(4,5));
        listaPuntos.add(new Point2D(1,3));
        
        AABB2D box = new AABB2D(listaPuntos);
        boxWrap = new AABB2DWrapper(box, 0);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getBox method, of class AABB2DWrapper.
     */
    @Test
    public void testGetBox() {
        System.out.println("getBox");
        AABB2DWrapper instance = boxWrap;
        
        AABB2D expResult = new AABB2D(new Point2D(4, 3), 6, 4);
        AABB2D result = instance.getBox();
        assertEquals(expResult, result);
    }

    /**
     * Test of setBox method, of class AABB2DWrapper.
     */
    @Test
    public void testSetBox() {
        System.out.println("setBox");
        List<Point2D> listaPuntos = new ArrayList<>();
        listaPuntos.add(new Point2D(4,0));
        listaPuntos.add(new Point2D(6,3));
        listaPuntos.add(new Point2D(4,6));
        listaPuntos.add(new Point2D(2,3));
        
        AABB2D box = new AABB2D(listaPuntos);
        AABB2DWrapper instance = new AABB2DWrapper(box, 0);
        instance.setBox(box);
        AABB2D result = instance.getBox();
        assertEquals(box, result);
    }

    /**
     * Test of getId method, of class AABB2DWrapper.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        List<Point2D> listaPuntos = new ArrayList<>();
        listaPuntos.add(new Point2D(4,0));
        listaPuntos.add(new Point2D(6,3));
        listaPuntos.add(new Point2D(4,6));
        listaPuntos.add(new Point2D(2,3));
        
        AABB2D box = new AABB2D(listaPuntos);
        AABB2DWrapper instance = new AABB2DWrapper(box, 5);

        int expResult = 5;
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class AABB2DWrapper.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        int id = 3;
        List<Point2D> listaPuntos = new ArrayList<>();
        listaPuntos.add(new Point2D(4,0));
        listaPuntos.add(new Point2D(6,3));
        listaPuntos.add(new Point2D(4,6));
        listaPuntos.add(new Point2D(2,3));
        
        AABB2D box = new AABB2D(listaPuntos);
        AABB2DWrapper instance = new AABB2DWrapper(box, 5);        
        
        instance.setId(id);
        int result = instance.getId();
        assertEquals(id, result);
    }
    
}
