/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class CuadradoTest {
    Cuadrado cuadrado1 = new Cuadrado(false);
    Cuadrado cuadrado2 = new Cuadrado(3, false);
    
    public CuadradoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetArea_1() {
        System.out.println("Cuadrado inscrito en circulo unitario");
        assertEquals(2, cuadrado1.getArea(), 0.0001);
    }

    @Test
    public void testGetArea_2() {
        System.out.println("Cuadrado de area dada");
        assertEquals(3, cuadrado2.getArea(), 0.0001);
    }

    /**
     * Test of getScale method, of class Cuadrado.
     */
    @Test
    public void testGetScale() {
        System.out.println("getScale");
        double areaFinal = 5.0;
        Cuadrado instance = cuadrado2;
        double expResult = Math.sqrt(2.5);
        double result = instance.getScale(areaFinal);
        assertEquals(expResult, result, 0.0);
    }
    
}
