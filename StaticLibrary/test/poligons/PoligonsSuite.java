/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author hmoraga
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({PolygonTypeTest.class, RegularPolygonFactoryTest.class, CuadrilateroTest.class, PolygonFactoryTest.class, Estrella6PuntasTest.class, AnyPolygon2DTest.class, Polygon2DTest.class, RectanguloTest.class, PentagonoTest.class, Estrella4PuntasTest.class, BoomerangTest.class, TrianguloTest.class, CuadradoTest.class, HexagonoTest.class})
public class PoligonsSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
