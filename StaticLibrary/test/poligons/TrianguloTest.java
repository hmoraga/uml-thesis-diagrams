/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class TrianguloTest {
    private Polygon2D triangulo1 = new Triangulo(false);
    private Polygon2D triangulo2 = new Triangulo(Math.sqrt(5), false);

    public TrianguloTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetArea_1() {
        System.out.println("Area Triangulo inscrito en circulo unitario");
        assertEquals(0.75*Math.sqrt(3), triangulo1.getArea(), 0.0001);
    }

    @Test
    public void testGetArea_2() {
        System.out.println("Triangulo de un area dada");
        assertEquals(Math.sqrt(5), triangulo2.getArea(), 0.0001);
    }
    
    
    /**
     * Test of getScale method, of class Triangulo.
     */
    @Test
    public void testGetScale() {
        System.out.println("getScale");
        double areaFinal = 40.0;
        Triangulo instance = new Triangulo(areaFinal, true);
        
        double expResult = 5.5490552670504229511054467768954;
        double result = instance.getScale(areaFinal);
        assertEquals(expResult, result, 0.00001);
    }
    
}
