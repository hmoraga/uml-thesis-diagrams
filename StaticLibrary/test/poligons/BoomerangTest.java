/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class BoomerangTest {
    Polygon2D boomerang1 = new Boomerang(true), boomerang2 = new Boomerang(5, false);
    
    public BoomerangTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetArea_1() {
        System.out.println("Area dentro del circulo unitario");
        assertEquals(Math.sqrt(3)/2, boomerang1.getArea(), 0.0001);
    }        

    @Test
    public void testGetArea_2() {
        System.out.println("Area dada");
        assertEquals(5, boomerang2.getArea(), 0.0001);
    }        

    /**
     * Test of getScale method, of class Boomerang.
     */
    @Test
    public void testGetScale() {
        System.out.println("getScale");
        double areaFinal = 5.0*Math.sqrt(3);
        Boomerang instance = (Boomerang)boomerang1;
        double expResult = Math.sqrt(10);
        double result = instance.getScale(areaFinal);
        assertEquals(expResult, result, 0.00001);
    }

}
