/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class HexagonoTest {
    Polygon2D hexagono1 = new Hexagono(true);
    Polygon2D hexagono2 = new Hexagono(6, true);
    
    public HexagonoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetArea_1() {
        System.out.println("Hexagono inscrito en circulo unitario");
        assertEquals(1.5*Math.sqrt(3), hexagono1.getArea(), 0.0001);
    }    

    @Test
    public void testGetArea_2() {
        System.out.println("Hexagono de area dada");
        assertEquals(6, hexagono2.getArea(), 0.0001);
    }    

    /**
     * Test of getScale method, of class Triangulo.
     */
    @Test
    public void testGetScale() {
        System.out.println("getScale");
        double areaFinal = 36.0;
        Polygon2D instance = new Hexagono(true);
        
        double expResult = Math.sqrt(8*Math.sqrt(3));
        double result = instance.getScale(areaFinal);
        assertEquals(expResult, result, 0.00001);
    }
    
}
