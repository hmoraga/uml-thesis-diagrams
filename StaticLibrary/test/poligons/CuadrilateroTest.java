/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class CuadrilateroTest {
    private List<Double> angles;    
    private Polygon2D cuadrilatero;
    
    public CuadrilateroTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        angles = new ArrayList<>();
        angles.add(Math.PI/6);
        angles.add(5*Math.PI/6);
        angles.add(7*Math.PI/6);
        angles.add(11*Math.PI/6);
        
        cuadrilatero = new Cuadrilatero(angles, false);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetArea() {
        System.out.println("getArea");
        assertEquals(Math.sqrt(3), cuadrilatero.getArea(), 0.0001);
    }    

    /**
     * Test of getScale method, of class Cuadrilatero.
     */
    @Test
    public void testGetScale() {
        System.out.println("getScale");
        double areaTotal = 5.0;
        Polygon2D instance = cuadrilatero;
        double expResult = Math.sqrt(5/Math.sqrt(3));
        double result = instance.getScale(areaTotal);
        assertEquals(expResult, result, 0.00001);
    }
}
