/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Estrella4PuntasTest {
    Polygon2D estrella1, estrella2;
    
    public Estrella4PuntasTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        estrella1 = new Estrella4Puntas(false);
        estrella2 = new Estrella4Puntas(4,true);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetArea_1() {
        System.out.println("Area de una estrella de 4 puntas inscrita en circulo unitario");
        assertEquals(35.0/32.0, estrella1.getArea(), 0.0001);
    }        

    @Test
    public void testGetArea_2() {
        System.out.println("Area de una estrella de 4 puntas de area dada");
        assertEquals(4.0, estrella2.getArea(), 0.0001);
    }
    
    public void testGetScale() {
        System.out.println("getScale");
        double scale = estrella1.getScale(4);
        assertEquals(8*Math.sqrt(2/35), scale, 0.0001);
    }

}
