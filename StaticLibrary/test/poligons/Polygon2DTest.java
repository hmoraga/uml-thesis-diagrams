/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import primitives.AABB2D;
import primitives.Edge2D;
import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class Polygon2DTest {
    private Polygon2D poly1, poly2, poly3;
    private List<Point2D> listaPuntos;

    public Polygon2DTest() {
        listaPuntos = new ArrayList<>();
        
        listaPuntos.add(new Point2D(1.0, 0.0));
        listaPuntos.add(new Point2D(2.0, 0.0));
        listaPuntos.add(new Point2D(1.0, 1.0));
        listaPuntos.add(new Point2D(-1.0, 1.0));
        listaPuntos.add(new Point2D(-2.0, 0.0));
        listaPuntos.add(new Point2D(-2.0, -2.0));
        listaPuntos.add(new Point2D(0.0, -1.0));
        listaPuntos.add(new Point2D(-1.0, -3.0));
        listaPuntos.add(new Point2D(1.0, -3.0));
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        // poligono 1: lista de puntos cualquiera (polígono no convexo)
        poly1 = new AnyPolygon2D(listaPuntos, false);
        
        // poligono 2: triangulo rotado irregular
        List<Point2D> listas = new ArrayList<>();
        listas.add(new Point2D(1,0));
        listas.add(new Point2D(Math.cos(5*Math.PI/6),Math.sin(5*Math.PI/6)));
        listas.add(new Point2D(Math.cos(4*Math.PI/3),Math.sin(4*Math.PI/3)));
        
        poly2 = new AnyPolygon2D(listas, false);
        poly2.amplificar(5);
        
        // poligono 3: cuadrado sin rotar
        poly3 = new Cuadrado(false);
        poly3.amplificar(2);
    }
    
    @After
    public void tearDown() {
        poly1.clear();
        poly2.clear();
        poly3.clear();
    }

    @Test
    public void testGetVertices() {
        System.out.println("getVertices");

        Polygon2D instance = poly1;
        List<Point2D> expResult = listaPuntos;
        List<Point2D> result = instance.getVertices();
        assertEquals(expResult, result);
    }

    @Test
    public void testDesplazamiento_double_double() {
        System.out.println("desplazamiento");
        List<Point2D> listaVertices = new ArrayList<>();
        listaVertices.add(new Point2D(3.0, 2.0));
        listaVertices.add(new Point2D(1.0, 4.0));
        listaVertices.add(new Point2D(-1.0, 2.0));
        listaVertices.add(new Point2D(1.0, 0.0));
        
        double x = 1.0;
        double y = 2.0;
        Polygon2D instance = poly3;
        instance.desplazar(x, y);
        int i=0;
        
        for (Point2D p : instance.getVertices()) {
            assertEquals(p.distanceSquaredTo(listaVertices.get(i++)), 0.0, 0.00001);
        }
        
    }

    @Test
    public void testDesplazamiento_Point2D() {
        System.out.println("desplazamiento");
        double x = 1.0;
        double y = 2.0;
        poly1.desplazar(new Point2D(x, y));
        int i=0;
        
        for (Point2D p : poly1.getVertices()) {
            listaPuntos.get(i).add(new Point2D(x, y));
            assertTrue(p.distanceSquaredTo(listaPuntos.get(i++))<0.00001);
        }
    }

    @Test
    public void testGetEdgeList() {
        System.out.println("getEdgeList");
        Polygon2D instance = poly3;
        List<Edge2D> expResult = new ArrayList<>(poly3.getEdgeList());
        
        List<Edge2D> result = instance.getEdgeList();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetBox() {
        System.out.println("getBox");
        Polygon2D instance = poly1;
        AABB2D expResult = new AABB2D(listaPuntos);
        AABB2D result = instance.getBox();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetArea() {
        System.out.println("getArea");
        Polygon2D instance = poly1;
        double expResult = 10.0;
        double result = instance.getArea();
        assertEquals(expResult, result, 0.00001);
    }    

    /**
     * Test of desplazar method, of class Polygon2D.
     */
    @Test
    public void testDesplazar_double_double() {
        System.out.println("desplazar");
        double x = 3.0;
        double y = -2.0;
        Polygon2D instance = new Cuadrado(false);
        instance.desplazar(x, y);
        assertEquals(instance.getVertices().get(0).distanceSquaredTo(new Point2D(4.0, -2.0)), 0.0, 0.00001);
        assertEquals(instance.getVertices().get(1).distanceSquaredTo(new Point2D(3.0, -1.0)), 0.0, 0.00001);
        assertEquals(instance.getVertices().get(2).distanceSquaredTo(new Point2D(2.0, -2.0)), 0.0, 0.00001);
        assertEquals(instance.getVertices().get(3).distanceSquaredTo(new Point2D(3.0, -3.0)), 0.0, 0.00001);
    }

    /**
     * Test of desplazar method, of class Polygon2D.
     */
    @Test
    public void testDesplazar_Point2D() {
        System.out.println("desplazar");
        Point2D p = new Point2D(1.0,-1.0);
        Polygon2D instance = new Triangulo(false);
        instance.desplazar(p);
        assertEquals(instance.getVertices().get(0).distanceSquaredTo(new Point2D(2.0, -1.0)), 0.0, 0.00001);
        assertEquals(instance.getVertices().get(1).distanceSquaredTo(new Point2D(0.5, Math.sqrt(3)/2-1)), 0.0, 0.00001);
        assertEquals(instance.getVertices().get(2).distanceSquaredTo(new Point2D(0.5, -Math.sqrt(3)/2-1)), 0.0, 0.00001);
    }

    /**
     * Test of amplificar method, of class Polygon2D.
     */
    @Test
    public void testAmplificar() {
        System.out.println("amplificar");
        double scale = 2.0;
        Polygon2D instance = new Triangulo(false);
        instance.amplificar(scale);
        assertEquals(instance.getArea(), 3*Math.sqrt(3), 0.00001);
        assertEquals(instance.getVertices().get(0).distanceSquaredTo(new Point2D(2.0, 0.0)), 0.0, 0.00001);
        assertEquals(instance.getVertices().get(1).distanceSquaredTo(new Point2D(-1.0, Math.sqrt(3.0))), 0.0, 0.00001);
        assertEquals(instance.getVertices().get(2).distanceSquaredTo(new Point2D(-1.0, -Math.sqrt(3.0))), 0.0, 0.00001);
    }

    /**
     * Test of getScale method, of class Polygon2D.
     */
    @Test
    public void testGetScale() {
        System.out.println("getScale");
        double areaTotal = 4.0;
        Polygon2D instance = new Cuadrado(true);
        double expResult = Math.sqrt(2.0);
        double result = instance.getScale(areaTotal);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of clear method, of class Polygon2D.
     */
    @Test
    public void testClear() {
        System.out.println("clear");
        Polygon2D instance = new Triangulo(false);
        instance.clear();
        assertTrue(instance.getVertices().isEmpty());
    }

    /**
     * Test of getListX method, of class Polygon2D.
     */
    @Test
    public void testGetListX() {
        System.out.println("getListX");
        Polygon2D instance = poly1;
        List<Double> expResult = Arrays.asList(new Double[]{1.0,2.0,1.0,-1.0,-2.0,-2.0,0.0,-1.0,1.0});
        
        List<Double> result = instance.getListX();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListY method, of class Polygon2D.
     */
    @Test
    public void testGetListY() {
        System.out.println("getListY");
        Polygon2D instance = poly2;

        double[] expResult = new double[]{0.0, 5*0.5, 5*-Math.sqrt(3)/2};
        double[] result = new double[instance.getListY().size()];
        
        for (int i = 0; i < result.length; i++) {
            result[i] = instance.getListY().get(i);
        }
        
        assertArrayEquals(expResult, result, 0.00001);
    }

    /**
     * Test of linearTransformation method, of class Polygon2D.
     */
    @Test
    public void testToScreenTransformation() {
        System.out.println("toScreenTransformation");
        Pair<Pair<Double>> limitesEspacioSimulacion = new Pair<>(new Pair<>(-10.0, 10.0), new Pair<>(-10.0, 10.0));
        Pair<Pair<Integer>> limitesEspacioGrafico = new Pair<>(new Pair<>(-2, 2), new Pair<>(-2, 2));
        Polygon2D instance = poly3;
        List<Point2D> expResult = new ArrayList<>();
        expResult.add(new Point2D(0.4, 0.0));
        expResult.add(new Point2D(0.0, -0.4));
        expResult.add(new Point2D(-0.4, 0.0));
        expResult.add(new Point2D(0.0, 0.4));
        
        List<Point2D> result = instance.toScreenTransformation(limitesEspacioSimulacion, limitesEspacioGrafico);
        
        for (int i = 0; i < result.size(); i++) {
            Point2D p = result.get(i);
            assertTrue(p.distanceSquaredTo(expResult.get(i))<=0.00001);
        }
    }

    public class Polygon2DImpl extends Polygon2D {

        public Polygon2DImpl() {
            super(null, false);
        }

        @Override
        public double getScale(double areaTotal) {
            return 0.0;
        }
    }
}
