/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class PolygonFactoryTest {
    
    public PolygonFactoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getPolygon method, of class PolygonFactory.
     */
    @Test
    public void testGetPolygon_1() {
        System.out.println("getPolygon");
        PolygonType model = PolygonType.BOOMERANG;
        double areaFinal = Math.sqrt(3)/2;
        boolean rotate = false;
        Polygon2D expResult = new Boomerang(rotate);
        Polygon2D result = PolygonFactory.getPolygon(model, areaFinal, rotate);
        assertEquals(expResult.getVertices(), result.getVertices());
    }

    @Test
    public void testGetPolygon_2() {
        System.out.println("getPolygon");
        PolygonType model = PolygonType.TRIANGULO;
        double areaFinal = 0.75*Math.sqrt(3);
        boolean rotate = false;
        Polygon2D expResult = new Triangulo(rotate);
        Polygon2D result = PolygonFactory.getPolygon(model, areaFinal, rotate);
        assertEquals(expResult.getVertices(), result.getVertices());
    }

    /*@Test
    public void testGetPolygon_3() {
        System.out.println("getPolygon");
        PolygonType model = PolygonType.RECTANGULO;
        double areaFinal = Math.sqrt(5.0);
        boolean rotate = false;
        Polygon2D expResult = new Rectangulo(12.0, 5.0, areaFinal, rotate);
        Polygon2D result = PolygonFactory.getPolygon(model, areaFinal, rotate);
        assertEquals(expResult.getArea(), result.getArea(), 0.00001);
    }*/

    /**
     * Test of getPolygon method, of class PolygonFactory.
     */
    @Test
    public void testGetPolygon() {
        System.out.println("getPolygon");
        PolygonType model = PolygonType.HEXAGONO;
        double areaFinal = 3*Math.sqrt(3);
        boolean rotate = false;
        
        List<Point2D> listaPuntos = new ArrayList<>();
        listaPuntos.add(new Point2D(Math.sqrt(2),0));
        listaPuntos.add(new Point2D(Math.sqrt(6)/2,Math.sqrt(2)/2));
        listaPuntos.add(new Point2D(-Math.sqrt(6)/2,Math.sqrt(2)/2));
        listaPuntos.add(new Point2D(-Math.sqrt(2),0));
        listaPuntos.add(new Point2D(-Math.sqrt(6)/2,-Math.sqrt(2)/2));
        listaPuntos.add(new Point2D(Math.sqrt(6)/2,-Math.sqrt(2)/2));
        
        Polygon2D expResult = new Hexagono(areaFinal, rotate);
        Polygon2D result = PolygonFactory.getPolygon(model, areaFinal, rotate);
        assertEquals(expResult.getArea(), result.getArea(), 0.00001);
        assertArrayEquals(expResult.listaPuntos.toArray(), result.listaPuntos.toArray());
    }
    
}
