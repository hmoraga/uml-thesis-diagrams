/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Estrella6PuntasTest {
    Polygon2D estrella1, estrella2;
    
    public Estrella6PuntasTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
         estrella1 = new Estrella6Puntas(false);
         estrella2 = new Estrella6Puntas(12, false);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetArea_1() {
        System.out.println("Area de una estrella de 6 puntas inscrita en una circunferencia de radio 1");
        assertEquals(Math.sqrt(3), estrella1.getArea(), 0.0001);
    }        

    @Test
    public void testGetArea_2() {
        System.out.println("Estrella de 6 puntas de area dada");
        assertEquals(12, estrella2.getArea(), 0.0001);
    }
    
    @Test
    public void testGetScale(){
        System.out.println("getScale");
        double scale = estrella1.getScale(36*Math.sqrt(3));
        assertEquals(6.0, scale, 0.0001);
    }

}
