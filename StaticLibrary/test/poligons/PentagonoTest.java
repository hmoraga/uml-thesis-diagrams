/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class PentagonoTest {
    Polygon2D pentagono1 = new Pentagono(false);
    Polygon2D pentagono2 = new Pentagono(5, false);
    
    public PentagonoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetArea_1() {
        System.out.println("Pentagono inscrito en circulo unitario");
        double area0 = 0.5*5*Math.sin(2*Math.PI/5);
        assertEquals(area0, pentagono1.getArea(), 0.0001);
    }

    @Test
    public void testGetArea_2() {
        System.out.println("Pentagono de un area dada");
        assertEquals(5, pentagono2.getArea(), 0.0001);
    }    

    /**
     * Test of getScale method, of class Pentagono.
     */
    @Test
    public void testGetScale() {
        System.out.println("getScale");
        double areaFinal = 3.0;
        Pentagono instance = (Pentagono)pentagono1;
        double expResult = Math.sqrt(3.0/(2.5*Math.sin(2*Math.PI/5)));
        double result = instance.getScale(areaFinal);
        assertEquals(expResult, result, 0.00001);
    }
    
}
