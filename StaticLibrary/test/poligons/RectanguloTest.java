/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class RectanguloTest {
    private Polygon2D rectangulo1, rectangulo2, rectangulo3, rectangulo4;
    
    public RectanguloTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        rectangulo1 = new Rectangulo(1.0, 1.0, false); // lados iguales, inscrito en circunferencia de radio 1
        rectangulo2 = new Rectangulo(4.0, 3.0, false);  // 3:4:5 proporcion de los lados, area dada=5
        rectangulo3 = new Rectangulo(5.0, 12.0, false);  // 5:12:13  proporcion de los lados, area dada=5
        rectangulo4 = new Rectangulo(7.0, 24.0, false);  // 7:24:25  proporcion de los lados, area dada=5
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetArea_1() {
        System.out.println("Rectangulo inscrito en una circunferencia unitaria");
        assertEquals(2, rectangulo1.getArea(), 0.0001);
    }

    @Test
    public void testGetArea_2() {
        System.out.println("Rectangulo de area dada");
        assertEquals(2*Math.sin(2*Math.atan2(3, 4)), rectangulo2.getArea(), 0.0001);
        assertEquals(2*Math.sin(2*Math.atan2(5, 12)), rectangulo3.getArea(), 0.0001);
        assertEquals(2*Math.sin(2*Math.atan2(7, 24)), rectangulo4.getArea(), 0.0001);
    }
    
    @Test
    public void testGetScale(){
        System.out.println("getScale");
        double scale = rectangulo1.getScale(5.0);
        assertEquals(Math.sqrt(2.5), scale, 0.00001);
        scale = rectangulo2.getScale(5.0);
        assertEquals(Math.sqrt(5/(2*Math.sin(2*Math.atan2(3, 4)))), scale, 0.00001);
        scale = rectangulo3.getScale(5.0);
        assertEquals(Math.sqrt(5/(2*Math.sin(2*Math.atan2(5, 12)))), scale, 0.00001);
        scale = rectangulo4.getScale(5.0);
        assertEquals(Math.sqrt(5/(2*Math.sin(2*Math.atan2(7, 24)))), scale, 0.00001);
    }
}
