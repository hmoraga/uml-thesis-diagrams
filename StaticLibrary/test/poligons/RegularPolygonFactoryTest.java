/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class RegularPolygonFactoryTest {
    
    public RegularPolygonFactoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getPolygon method, of class RegularPolygonFactory.
     */
    @Test
    public void testGetPolygon() {
        System.out.println("getPolygon");
        PolygonType model = PolygonType.CUADRADO;
        double scale = 1.0;
        boolean rotate = false;
        Polygon2D expResult = new Cuadrado(scale, rotate);
        Polygon2D result = PolygonFactory.getPolygon(model, scale, rotate);
        
        for (int i = 0; i < result.getVertices().size(); i++) {
            assertTrue(result.getVertices().get(i).equals(expResult.getVertices().get(i)));
        }
    }
    
}
