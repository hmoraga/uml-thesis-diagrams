/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class AnyPolygon2DTest {
    
    public AnyPolygon2DTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getScale method, of class AnyPolygon2D.
     */
    @Test
    public void testGetScale() {
        System.out.println("getScale");
        double areaTotal = 1.0;
        List<Point2D> listaPuntos = new ArrayList<>();
        listaPuntos.add(new Point2D(1.0, 0.0));
        listaPuntos.add(new Point2D(0.0, 1.0));
        listaPuntos.add(new Point2D(0.0, -1.0));
        AnyPolygon2D instance = new AnyPolygon2D(listaPuntos, false);
        double expResult = 1.0;
        double result = instance.getScale(areaTotal);
        assertEquals(expResult, result, 0.0);
    }
    
}
