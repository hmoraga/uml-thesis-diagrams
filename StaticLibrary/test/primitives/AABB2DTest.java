/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import sap.Element;

/**
 *
 * @author hmoraga
 */
public class AABB2DTest {
    private AABB2D cajaUno, cajaDos, cajaTres, resultado;
    private Point2D p, pp, p0, p1, p2, p3;
    private List<Point2D> listaPuntos;
    private int indice;
    
    public AABB2DTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        listaPuntos = new ArrayList<>();
    }
    
    @After
    public void tearDown() {
        listaPuntos.clear();
    }

    /**
     * Test of getCentro method, of class AABB2D.
     */
    @Test
    public void testGetCentro() {
        System.out.println("getCentro");
        listaPuntos.add(new Point2D(-0.9, -1.1));
        listaPuntos.add(new Point2D(1.2, -1.1));
        listaPuntos.add(new Point2D(1.2, 0.9));
        listaPuntos.add(new Point2D(-0.9, 0.9));
        
        AABB2D instance0 = new AABB2D(listaPuntos);
        Point2D expResult0 = new Point2D(0.15,-0.1);
        Point2D result0 = instance0.getCentro();
        assertEquals(expResult0.distanceTo(result0), 0, 0.0001);

        AABB2D instance1 = new AABB2D(new Point2D(-2,4), 6, 5);
        Point2D expResult1 = new Point2D(-2,4);
        Point2D result1 = instance1.getCentro();
        assertEquals(expResult1, result1);
    }

    /**
     * Test of getPrimitiva method, of class AABB2D.
     */
    @Test
    public void testGetPrimitiva() {
        System.out.println("getPrimitiva");
        AABB2D instance1 = new AABB2D(new Edge2D(new Point2D(2,5), new Point2D(5,1)),4);
        listaPuntos.add(new Point2D(2, 1));
        listaPuntos.add(new Point2D(5, 1));
        listaPuntos.add(new Point2D(5, 5));
        listaPuntos.add(new Point2D(2, 5));
        AABB2D instance2 = new AABB2D(listaPuntos);
        Primitiva expResult1 = new Primitiva2D(new Edge2D(new Point2D(2,5), new Point2D(5,1)),4);
        Primitiva result1 = instance1.getPrimitiva();
        Primitiva result2 = instance2.getPrimitiva();
        assertEquals(expResult1, result1);
        assertNull(result2);
    }

    /**
     * Test of getEdge2D method, of class AABB2D.
     */
    @Test
    public void testGetEdge2D() {
        System.out.println("getEdge2D");
        AABB2D instance = new AABB2D(new Edge2D(new Point2D(-5, 5), new Point2D(-1, 3)), 1);
        Edge2D expResult = new Edge2D(new Point2D(-5, 5), new Point2D(-1, 3));
        Edge result = instance.getEdge2D();
        assertEquals(expResult, result);
    }

    /**
     * Test of BVintersection method, of class AABB2D.
     */
    @Test
    public void testBVintersection() {
        System.out.println("BVintersection");
        AABB2D box0 = new AABB2D(new Point2D(-3,4),4,2);
        AABB2D box1 = new AABB2D(new Point2D(3.5,3),3,4);
        AABB2D box2 = new AABB2D(new Point2D(1,1),4,4);
        AABB2D box3 = new AABB2D(new Point2D(1,1),2,2);

        AABB2D result1 = box0.BVintersection(box1);
        AABB2D result2 = box0.BVintersection(box2);
        AABB2D result3 = box0.BVintersection(box3);
        AABB2D result4 = box1.BVintersection(box2);
        AABB2D result5 = box1.BVintersection(box3);
        AABB2D result6 = box2.BVintersection(box3);

        AABB2D expResult1 = null;
        AABB2D expResult2 = null;
        AABB2D expResult3 = null;
        AABB2D expResult4 = new AABB2D(new Point2D(2.5f, 2), 1, 2);
        AABB2D expResult5 = null;
        AABB2D expResult6 = box3;
        
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
        assertEquals(expResult5, result5);
        assertEquals(expResult6, result6);
        
        System.out.println("BVintersection True");
        cajaUno = new AABB2D(new Point2D(0,0), 20.0, 20.0);
	cajaDos = new AABB2D(new Point2D(5,0), 20.0, 20.0);

        resultado = cajaUno.BVintersection(cajaDos);

	assertTrue(resultado.equals(new AABB2D(new Point2D(2.5,0),15,20)));
	assertEquals(resultado, new AABB2D(new Point2D(2.5,0),15,20));        
    }

    /**
     * Test of intersects method, of class AABB2D.
     */
    @Test
    public void testIntersects_AABB2D() {
        System.out.println("intersects");
        AABB2D box = new AABB2D(new Point2D(-4,-4),6,6);
        AABB2D instance1 = new AABB2D(new Point2D(-3,-3),2,4);
        AABB2D instance2 = new AABB2D(new Point2D(-7,-3),2,2);
        AABB2D instance3 = new AABB2D(new Point2D(-4,4), 4,4);
        boolean expResult1 = true, expResult2 = true, expResult3 = false;
        boolean result1 = instance1.intersects(box), result2 = instance2.intersects(box), result3 = instance3.intersects(box);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);        
    }

    /**
     * Test of unionBV method, of class AABB2D.
     */
    @Test
    public void testUnionBV_AABB2D() {
        System.out.println("unionBV");
        AABB2D box1 = new AABB2D(new Point2D(-3,4),4,2);
        AABB2D box2 = new AABB2D(new Point2D(3.5f,3),3,4);
        AABB2D box3 = new AABB2D(new Point2D(1,1),4,4);
        AABB2D box4 = new AABB2D(new Point2D(1,1),2,2);

        AABB2D union1, union2, union3, union4, union5, union6;
        union1 = box1.unionBV(box2);
        union2 = box1.unionBV(box3);
        union3 = box1.unionBV(box4);
        union4 = box2.unionBV(box3);
        union5 = box2.unionBV(box4);
        union6 = box3.unionBV(box4);
        
        assertEquals(new AABB2D(new Point2D(0,3),10,4), union1);
        assertEquals(new AABB2D(new Point2D(-1,2),8,6), union2);
        assertEquals(new AABB2D(new Point2D(-1.5f,2.5f),7,5), union3);
        assertEquals(new AABB2D(new Point2D(2,2),6,6), union4);
        assertEquals(new AABB2D(new Point2D(2.5f,2.5f),5,5), union5);
        assertEquals(new AABB2D(new Point2D(1,1),4,4), union6);     
    }

    /**
     * Test of isInside method, of class AABB2D.
     */
    @Test
    public void testIsInside() {
        System.out.println("isInside");
        AABB2D box = new AABB2D(new Point2D(-4,-4),6,6);
        AABB2D instance1 = new AABB2D(new Point2D(-3,-3),2,4);
        AABB2D instance2 = new AABB2D(new Point2D(-7,-3),2,2);
        AABB2D instance3 = new AABB2D(new Point2D(-4,4), 4,4);
        boolean expResult1 = true, expResult2 = false, expResult3 = false;
        boolean result1 = instance1.isInside(box), result2 = instance2.isInside(box), result3 = instance3.isInside(box);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);        

        List<Point2D> l1 = new ArrayList<>();
        List<Point2D> l2 = new ArrayList<>();
        List<Point2D> l3 = new ArrayList<>();
        List<Point2D> l4 = new ArrayList<>();
        
        l1.add(new Point2D(-5,3));
        l1.add(new Point2D(-1,3));
        l1.add(new Point2D(-1,5));
        l1.add(new Point2D(-5,5));
        
        l2.add(new Point2D(2,1));
        l2.add(new Point2D(5,1));
        l2.add(new Point2D(5,5));
        l2.add(new Point2D(2,5));

        l3.add(new Point2D(-1,-1));
        l3.add(new Point2D(3,-1));
        l3.add(new Point2D(3,3));
        l3.add(new Point2D(-1,3));

        l4.add(new Point2D(0,0));
        l4.add(new Point2D(2,0));
        l4.add(new Point2D(2,2));
        l4.add(new Point2D(0,2));        
        
        cajaUno = new AABB2D(l1);
        cajaDos = new AABB2D(l2);
        cajaTres = new AABB2D(l3);
        AABB2D cajaCuatro = new AABB2D(l4);
        
        result1 = cajaUno.isInside(cajaTres);
        result2 = cajaDos.isInside(cajaTres);
        result3 = cajaDos.isInside(cajaCuatro);
        boolean result4 = cajaTres.isInside(cajaCuatro);
        boolean result5 = cajaCuatro.isInside(cajaTres);
        
        assertEquals(false, result1);
        assertEquals(false, result2);
        assertEquals(false, result3);
        assertEquals(false, result4);
        assertEquals(true, result5);
    }

    /**
     * Test of getProductoDimensiones method, of class AABB2D.
     */
    @Test
    public void testGetProductoDimensiones() {
        System.out.println("getProductoDimensiones");
        AABB2D instance0 = new AABB2D(new Point2D(2,5),3.5,3.5);
        double expResult0 = 12.25;
        double result0 = instance0.getProductoDimensiones();
        assertEquals(expResult0, result0, 0.0);

        listaPuntos.add(new Point2D(3,7));
        listaPuntos.add(new Point2D(7,7));
        listaPuntos.add(new Point2D(7,12));
        listaPuntos.add(new Point2D(3,12));
        AABB2D instance1 = new AABB2D(listaPuntos);
        double expResult1 = 20.0;
        double result1 = instance1.getProductoDimensiones();
        assertEquals(expResult1, result1, 0.0);
    }

    /**
     * Test of getMinimum method, of class AABB2D.
     */
    @Test
    public void testGetMinimum() {
        System.out.println("getMinimum");
        //ingreso un hexagono regular
        listaPuntos.add(new Point2D(-2,-2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(2,-2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(4,0));
        listaPuntos.add(new Point2D(2,2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(-2,2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(-4,0));
        AABB2D instance0 = new AABB2D(listaPuntos);
        
        double expResult0 = -4.0;
        double expResult1 = -2*Math.sqrt(3);
        double result0 = instance0.getMinimum(0);
        double result1 = instance0.getMinimum(1);
        assertEquals(expResult0, result0, 0.0);
        assertEquals(expResult1, result1, 0.0);

        AABB2D instance1 = new AABB2D(new Point2D(5,5), 8, 12);
        double expResult2 = 1.0;
        double result2 = instance1.getMinimum(0);
        double expResult3 = -1.0;
        double result3 = instance1.getMinimum(1);        
        assertEquals(expResult2, result2, 0.0);
        assertEquals(expResult3, result3, 0.0);
    }

    /**
     * Test of getMaximum method, of class AABB2D.
     */
    @Test
    public void testGetMaximum() {
        System.out.println("getMaximum");
        //ingreso un hexagono regular
        listaPuntos.add(new Point2D(-2,-2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(2,-2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(4,0));
        listaPuntos.add(new Point2D(2,2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(-2,2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(-4,0));
        AABB2D instance0 = new AABB2D(listaPuntos);
        
        double expResult0 = 4.0;
        double expResult1 = 2*Math.sqrt(3);
        double result0 = instance0.getMaximum(0);
        double result1 = instance0.getMaximum(1);
        assertEquals(expResult0, result0, 0.0);
        assertEquals(expResult1, result1, 0.0);

        AABB2D instance1 = new AABB2D(new Point2D(5,5), 8, 12);
        double expResult2 = 9.0;
        double result2 = instance1.getMaximum(0);
        double expResult3 = 11.0;
        double result3 = instance1.getMaximum(1);
        assertEquals(expResult2, result2, 0.0);
        assertEquals(expResult3, result3, 0.0);    
    }

    /**
     * Test of getLargoX method, of class AABB2D.
     */
    @Test
    public void testGetLargoX() {
        System.out.println("getLargoX");
        AABB2D instance1 = new AABB2D(new Point2D(1,3), 3, 5);
        //ingreso un hexagono regular
        listaPuntos.add(new Point2D(-2,-2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(2,-2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(4,0));
        listaPuntos.add(new Point2D(2,2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(-2,2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(-4,0));
        AABB2D instance2 = new AABB2D(listaPuntos);
        
        double expResult1 = 3.0;
        double expResult2 = 8.0;
        double result1 = instance1.getLargoX();
        double result2 = instance2.getLargoX();
        assertEquals(expResult1, result1, 0.0);
        assertEquals(expResult2, result2, 0.0);

        AABB2D instance3 = new AABB2D(new Point2D(-3,-4),6,7);
        double expResult3 = 6.0;
        double result3 = instance3.getLargoX();
        assertEquals(expResult3, result3, 0.0);
    }

    /**
     * Test of getLargoY method, of class AABB2D.
     */
    @Test
    public void testGetLargoY() {
        System.out.println("getLargoY");
        AABB2D instance1 = new AABB2D(new Point2D(1,3), 3, 5);
        //ingreso un hexagono regular
        listaPuntos.add(new Point2D(-2,-2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(2,-2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(4,0));
        listaPuntos.add(new Point2D(2,2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(-2,2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(-4,0));
        AABB2D instance2 = new AABB2D(listaPuntos);
        
        double expResult1 = 5.0;
        double expResult2 = 4*Math.sqrt(3);
        double result1 = instance1.getLargoY();
        double result2 = instance2.getLargoY();
        assertEquals(expResult1, result1, 0.0);
        assertEquals(expResult2, result2, 0.0);

        AABB2D instance3 = new AABB2D(new Point2D(-3,-4),6,7);
        double expResult3 = 7.0;
        double result3 = instance3.getLargoY();
        assertEquals(expResult3, result3, 0.0);        
    }

    /**
     * Test of getMinX method, of class AABB2D.
     */
    @Test
    public void testGetMinX() {
        System.out.println("getMinX");
        listaPuntos.add(new Point2D(-1, -1));
        listaPuntos.add(new Point2D(3, -1));
        listaPuntos.add(new Point2D(3, 3));
        listaPuntos.add(new Point2D(-1, 3));
        
        AABB2D instance0 = new AABB2D(listaPuntos);
        double expResult0 = -1.0;
        double result0 = instance0.getMinX();
        assertEquals(expResult0, result0, 0.0);

        AABB2D instance1 = new AABB2D(new Point2D(2.5,3.5), 5.0, 7.0);
        double expResult1 = 0.0;
        double result1 = instance1.getMinX();
        assertEquals(expResult1, result1, 0.0);
    }

    /**
     * Test of getMaxX method, of class AABB2D.
     */
    @Test
    public void testGetMaxX() {
        System.out.println("getMaxX");
        listaPuntos.add(new Point2D(-1, -1));
        listaPuntos.add(new Point2D(3, -1));
        listaPuntos.add(new Point2D(3, 3));
        listaPuntos.add(new Point2D(-1, 3));
        
        AABB2D instance0 = new AABB2D(listaPuntos);
        double expResult0 = 3.0;
        double result0 = instance0.getMaxX();
        assertEquals(expResult0, result0, 0.0);

        AABB2D instance1 = new AABB2D(new Point2D(2.5,3.5), 5, 7);
        double expResult1 = 5.0;
        double result1 = instance1.getMaxX();
        assertEquals(expResult1, result1, 0.0);
    }

    /**
     * Test of getMinY method, of class AABB2D.
     */
    @Test
    public void testGetMinY() {
        System.out.println("getMinY");
        listaPuntos.add(new Point2D(-1, -1));
        listaPuntos.add(new Point2D(3, -1));
        listaPuntos.add(new Point2D(3, 3));
        listaPuntos.add(new Point2D(-1, 3));
        
        AABB2D instance0 = new AABB2D(listaPuntos);
        double expResult0 = -1.0;
        double result0 = instance0.getMinY();
        assertEquals(expResult0, result0, 0.0);

        AABB2D instance1 = new AABB2D(new Point2D(2.5,3.5), 5, 7);
        double expResult1 = 0.0;
        double result1 = instance1.getMinY();
        assertEquals(expResult1, result1, 0.0);
    }

    /**
     * Test of getMaxY method, of class AABB2D.
     */
    @Test
    public void testGetMaxY() {
        System.out.println("getMaxY");
        listaPuntos.add(new Point2D(-1, -1));
        listaPuntos.add(new Point2D(3, -1));
        listaPuntos.add(new Point2D(3, 3));
        listaPuntos.add(new Point2D(-1, 3));
        
        AABB2D instance0 = new AABB2D(listaPuntos);
        double expResult0 = 3.0;
        double result0 = instance0.getMaxY();
        assertEquals(expResult0, result0, 0.0);

        AABB2D instance1 = new AABB2D(new Point2D(2.5,3.5), 5, 7);
        double expResult1 = 7.0;
        double result1 = instance1.getMaxY();
        assertEquals(expResult1, result1, 0.0);
    }

    /**
     * Test of getArea method, of class AABB2D.
     */
    @Test
    public void testGetArea() {
        System.out.println("getArea");
        listaPuntos.add(new Point2D(-5,3));
        listaPuntos.add(new Point2D(-5,5));
        listaPuntos.add(new Point2D(-1,5));
        listaPuntos.add(new Point2D(-1,3));
        
        AABB2D instance0 = new AABB2D(listaPuntos);
        double expResult0 = 8.0;
        double result0 = instance0.getArea();
        assertEquals(expResult0, result0, 0.0);

        AABB2D instance1 = new AABB2D(new Point2D(0, 0), 10, 14);
        double expResult1 = 140.0;
        double result1 = instance1.getArea();
        assertEquals(expResult1, result1, 0.0);
    }

    /**
     * Test of toString method, of class AABB2D.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        AABB2D instance = new AABB2D(new Edge2D(new Point2D(-2,-1), new Point2D(2,1)), 0);
        String expResult = "AABB2D{p=(-2.0,-1.0), q=(2.0,1.0), arista=Primitiva{objeto=Edge2D{a=(-2.0,-1.0), b=(2.0,1.0)}, indice=0}}";
        String result = instance.toString();
        assertEquals(expResult, result);

        cajaUno = new AABB2D();
        listaPuntos.add(new Point2D(-2,-2));
        listaPuntos.add(new Point2D(2,-2));
        listaPuntos.add(new Point2D(2,2));
        listaPuntos.add(new Point2D(-2,2));
        cajaDos = new AABB2D(listaPuntos);
        cajaTres = new AABB2D(new Point2D(3,4), 5.0, 7.0);
        AABB2D cajaCuatro = new AABB2D(new Point2D(0.0, 0.0), 1.0, 1.0);
        String expResult1 = "AABB2D{p=(0.0,0.0), q=(0.0,0.0), arista=null}";
        String result1 = cajaUno.toString();
        String expResult2 = "AABB2D{p=(-2.0,-2.0), q=(2.0,2.0), arista=null}";
        String result2 = cajaDos.toString();
        String expResult3 = "AABB2D{p=(0.5,0.5), q=(5.5,7.5), arista=null}";
        String result3 = cajaTres.toString();
        String expResult4 = "AABB2D{p=(-0.5,-0.5), q=(0.5,0.5), arista=null}";
        String result4 = cajaCuatro.toString();        
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
    }

    /**
     * Test of hashCode method, of class AABB2D.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        AABB2D instance = new AABB2D(new Point2D(5, 5), 2, 3.5);
        int expResult = 0;
        int result = instance.hashCode();
        assertTrue(expResult != result);
    }

    /**
     * Test of equals method, of class AABB2D.
     */
    @Test
    public void testEquals_WithoutEdge2D() {
        System.out.println("equals");
        Object obj = new AABB2D(new Point2D(3.5, 3), 3, 4);
        listaPuntos.add(new Point2D(2, 1));
        listaPuntos.add(new Point2D(5, 1));
        listaPuntos.add(new Point2D(5, 5));
        listaPuntos.add(new Point2D(2, 5));
        
        AABB2D instance = new AABB2D(listaPuntos);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class AABB2D.
     */
    @Test
    public void testEquals_WithEdge2D() {
        System.out.println("equals");
        Object obj = new AABB2D(new Edge2D(new Point2D(2,5), new Point2D(5,1)), 0);        
        AABB2D instance1 = new AABB2D(new Edge2D(new Point2D(2,5), new Point2D(5,1)), 0);
        AABB2D instance2 = new AABB2D(new Edge2D(new Point2D(1,5), new Point2D(4,1)), 0);
        AABB2D instance3 = new AABB2D(new Edge2D(new Point2D(2,1), new Point2D(5,5)), 0);
        
        boolean expResult1 = true, expResult2 = false, expResult3 = false;
        //se condsidera si genera la misma caja, con el mismo centro
        boolean result1 = instance1.equals(obj), result2 = instance2.equals(obj), result3 = instance3.equals(obj);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
    }

    /**
     * Test of isLeaf method, of class AABB2D.
     */
    @Test
    public void testIsLeaf() {
        System.out.println("isLeaf");
        AABB2D instance1 = new AABB2D(new Edge2D(new Point2D(-1, 3), new Point2D(2, 5)), 1);
        listaPuntos.add(new Point2D(-1,0));
        listaPuntos.add(new Point2D(1,0));
        listaPuntos.add(new Point2D(0,1));
        AABB2D instance2 = new AABB2D(listaPuntos);
        boolean expResult1 = true, expResult2 = false;
        boolean result1 = instance1.isLeaf(), result2 = instance2.isLeaf();
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of getIndex method, of class AABB2D.
     */
    @Test
    public void testGetIndex() {
        System.out.println("getIndex");
        AABB2D instance1 = new AABB2D(new Edge2D(new Point2D(-1, 3), new Point2D(2, 5)), 1);
        listaPuntos.add(new Point2D(-1,0));
        listaPuntos.add(new Point2D(1,0));
        listaPuntos.add(new Point2D(0,1));
        AABB2D instance2 = new AABB2D(listaPuntos);

        int expResult1 = 1, expResult2=-1;
        int result1 = instance1.getIndex(), result2 = instance2.getIndex();
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of getPoints method, of class AABB2D.
     */
    @Test
    public void testGetPoints() {
        System.out.println("getPoints");
        AABB2D instance0 = new AABB2D(new Edge2D(new Point2D(1,2), new Point2D(4,6)), 2);
        listaPuntos.add(new Point2D(1,2));
        listaPuntos.add(new Point2D(4,2));
        listaPuntos.add(new Point2D(4,6));
        listaPuntos.add(new Point2D(1,6));
        
        List<Point2D> result0 = instance0.getPoints();
        assertEquals(listaPuntos, result0);

        System.out.println("getPoints");
        AABB2D instance1 = new AABB2D(new Point2D(0,0), 10, 20);
        p0 = new Point2D(-5,-10);
        p1 = new Point2D(5,-10);
        p2 = new Point2D(5,10);
        p3 = new Point2D(-5,10);
        
        listaPuntos = new ArrayList<>();
        listaPuntos.add(p0);
        listaPuntos.add(p1);
        listaPuntos.add(p2);
        listaPuntos.add(p3);
        
        List<Point2D> result1 = instance1.getPoints();
        assertEquals(listaPuntos, result1);
    }

    /**
     * Test of getMean method, of class AABB2D.
     */
    @Test
    public void testGetMean() {
        System.out.println("getMean");
        List<AABB2D> listaCajas = new ArrayList<>();
        Pair<Double> expResult0 = new Pair<>(1.25,0.0);
        
        listaCajas.add(new AABB2D(new Point2D(-5,0),2,2));
        listaCajas.add(new AABB2D(new Point2D(-1.5,0),3,3));
        listaCajas.add(new AABB2D(new Point2D(3,0),4,4));
        listaCajas.add(new AABB2D(new Point2D(8.5,0),5,5));
        
        Pair<Double> result0 = AABB2D.getMean(listaCajas);
        assertEquals(expResult0, result0);

        cajaUno = new AABB2D(new Point2D(-3.0, 4.0), 4.0, 2.0);
        cajaDos = new AABB2D(new Point2D(3.5, 3.0), 3.0, 4.0);
        cajaTres = new AABB2D(new Point2D(1.0, 1.0), 4.0, 4.0);
        AABB2D cajaCuatro = new AABB2D(new Point2D(3.0, -4.0), 2.0, 2.0);
        listaCajas.clear();
        listaCajas.add(cajaUno);
        listaCajas.add(cajaDos);
        listaCajas.add(cajaTres);
        listaCajas.add(cajaCuatro);
        
        Pair<Double> expResult1 = new Pair<>(1.125, 1.0);
        Pair<Double> result1 = AABB2D.getMean(listaCajas);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of getVariance method, of class AABB2D.
     */
    @Test
    public void testGetVariance() {
        System.out.println("getVariance");
        List<AABB2D> listaCajas = new ArrayList<>();
        Pair<Double> expResult0 = new Pair<>(25.5625,0.0);
        
        listaCajas.add(new AABB2D(new Point2D(-5,0),2,2));
        listaCajas.add(new AABB2D(new Point2D(-1.5,0),3,3));
        listaCajas.add(new AABB2D(new Point2D(3,0),4,4));
        listaCajas.add(new AABB2D(new Point2D(8.5,0),5,5));
        
        Pair<Double> result0 = AABB2D.getVariance(listaCajas);
        assertEquals(expResult0, result0);

        cajaUno = new AABB2D(new Point2D(-3, 4), 4, 2);
        cajaDos = new AABB2D(new Point2D(3.5f, 3), 3, 4);
        cajaTres = new AABB2D(new Point2D(1, 1), 4, 4);
        AABB2D cajaCuatro = new AABB2D(new Point2D(3, -4), 2, 2);
        listaCajas.clear();
        listaCajas.add(cajaUno);
        listaCajas.add(cajaDos);
        listaCajas.add(cajaTres);
        listaCajas.add(cajaCuatro);
        Pair<Double> expResult1 = new Pair<>(6.546875, 9.5);
        Pair<Double> result1 = AABB2D.getVariance(listaCajas);

        assertEquals(expResult1.first, result1.first, 0.0);
        assertEquals(expResult1.second, result1.second, 0.0);
    }

    /**
     * Test of collidePrimitives method, of class AABB2D.
     */
    @Test
    public void testCollidePrimitives() {
        System.out.println("collidePrimitives");
        List<Pair<Integer>> listaColisiones = new ArrayList<>();
        AABB2D objetoUno = new AABB2D(new Edge2D(new Point2D(4, 6), new Point2D(2, 2)),1);
        AABB2D objetoDos = new AABB2D(new Edge2D(new Point2D(3, 7), new Point2D(6, 3)),2);
        AABB2D.collidePrimitives(listaColisiones, objetoUno, objetoDos);
        assertTrue(listaColisiones.size()>0);
        assertEquals(listaColisiones.get(0), new Pair<>(1,2));
    }

    /**
     * Test of sortFunction method, of class AABB2D.
     */
    @Test
    public void testSortFunction() {
        System.out.println("sortFunction");
        AABB2D box0, box1, box2, box3, box4, box5, box6, box7, box8; 
        List<AABB2D> listaCajas1 = new ArrayList<>();
        List<AABB2D> listaCajas2 = new ArrayList<>();
        List<AABB2D> listaCajas3 = new ArrayList<>();
        List<AABB2D> expResult1 = new ArrayList<>();
        List<AABB2D> expResult2 = new ArrayList<>();
        List<AABB2D> expResult3 = new ArrayList<>();
        
        //primer escenario sort por x
        box0 = new AABB2D(new Point2D(-2, 0), 1, 1);
        box1 = new AABB2D(new Point2D(3, 3), 2, 2);
        box2 = new AABB2D(new Point2D(0, -1), 3, 3);
        //segundo escenario sort por y
        box3 = new AABB2D(new Point2D(3, 0), 1, 1);
        box4 = new AABB2D(new Point2D(2.8f, -2), 2, 2);
        box5 = new AABB2D(new Point2D(3.2f, 3), 3, 3);
        //tercer escenario sort por area
        box6 = new AABB2D(new Point2D(1, 1), 2, 2);
        box7 = new AABB2D(new Point2D(1, 1), 4, 3);
        box8 = new AABB2D(new Point2D(1, 1), 2, 4);
        
        listaCajas1.add(box0);
        listaCajas1.add(box1);
        listaCajas1.add(box2);
        
        listaCajas2.add(box3);
        listaCajas2.add(box4);
        listaCajas2.add(box5);

        listaCajas3.add(box6);
        listaCajas3.add(box7);
        listaCajas3.add(box8);
        
        AABB2D.sortFunction(listaCajas1);
        AABB2D.sortFunction(listaCajas2);
        AABB2D.sortFunction(listaCajas3);
        
        expResult1.add(box0);
        expResult1.add(box2);
        expResult1.add(box1);
        
        expResult2.add(box4);
        expResult2.add(box3);
        expResult2.add(box5);
        
        expResult3.add(box6);
        expResult3.add(box8);
        expResult3.add(box7);

        assertEquals(listaCajas1, expResult1);
        assertEquals(listaCajas2, expResult2);
        assertEquals(listaCajas3, expResult3);
        
        System.out.println("sortFunction");
        cajaUno = new AABB2D(new Point2D(-3.0, 4.0), 4, 2);
        cajaDos = new AABB2D(new Point2D(3.5, 3.0), 3, 4);
        cajaTres = new AABB2D(new Point2D(1.0, 1.0), 4, 4);
        AABB2D cajaCuatro = new AABB2D(new Point2D(3.0, -4.0), 2, 2);
        listaCajas1.clear();

        listaCajas1.add(cajaUno);
        listaCajas1.add(cajaDos);
        listaCajas1.add(cajaTres);
        listaCajas1.add(cajaCuatro);
        
        AABB2D.sortFunction(listaCajas1);
        assertTrue(listaCajas1.get(0).equals(cajaCuatro));  // por volumen?
        assertTrue(listaCajas1.get(1).equals(cajaTres));    
        assertTrue(listaCajas1.get(2).equals(cajaDos));
        assertTrue(listaCajas1.get(3).equals(cajaUno));
    }

    /**
     * Test of unwrap method, of class AABB2D.
     */
   /*@Test
    public void testUnwrap() {
        System.out.println("unwrap");
        //ingreso un hexagono regular
        listaPuntos.add(new Point2D(-2,-2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(2,-2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(4,0));
        listaPuntos.add(new Point2D(2,2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(-2,2*Math.sqrt(3)));
        listaPuntos.add(new Point2D(-4,0));

        BVNode nodo = new BVNode(new AABB2D(listaPuntos));
        AABB2D instance = new AABB2D();
        AABB2D expResult = new AABB2D(new Point2D(0, 0), 8, 4*Math.sqrt(3));
        AABB2D result = instance.unwrap(nodo);
        assertEquals(expResult, result);
    } */

    /**
     * Test of unionBV method, of class AABB2D.
     */
    @Test
    public void testUnionBV_BV() {
        System.out.println("unionBV");
        listaPuntos.add(new Point2D(-1, -1));
        listaPuntos.add(new Point2D(3, -1));
        listaPuntos.add(new Point2D(3, 3));
        listaPuntos.add(new Point2D(-1, 3));
        
        BV other = new AABB2D(listaPuntos);
        AABB2D instance = new AABB2D(new Point2D(3.5,3),3,4);
        BV expResult = new AABB2D(new Point2D(2,2),6,6);
        BV result = instance.unionBV(other);
        assertEquals(expResult, result);
    }

    /**
     * Test of intersects method, of class AABB2D.
     */
    @Test
    public void testIntersects_BV() {
        System.out.println("intersects");
        BV caja = new AABB2D(new Edge2D(new Point2D(1, 6), new Point2D(4, 2)), 0);
        listaPuntos.add(new Point2D(3, 4));
        listaPuntos.add(new Point2D(6, 4));
        listaPuntos.add(new Point2D(6, 7));
        listaPuntos.add(new Point2D(3, 7));
        AABB2D instance = new AABB2D(listaPuntos);
        boolean expResult = true;
        boolean result = instance.intersects(caja);
        assertEquals(expResult, result);
    }

    /**
     * Test of getElements method, of class AABB2D.
     */
    @Test
    public void testGetElements() {
        System.out.println("getElements");
        int dimension = 0;
        AABB2D instance = new AABB2D(new Point2D(4,-4), 2, 4);
        Pair<Element> expResult1 = new Pair<>(new Element(3, false), new Element(5, true));
        Pair<Element> expResult2 = new Pair<>(new Element(-6, false), new Element(-2, true));
        Pair<Element> result1 = instance.getElements(0);
        Pair<Element> result2 = instance.getElements(1);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of obtenerListaPares method, of class AABB2D.
     */
    @Test
    public void testObtenerListaPares() {
        System.out.println("obtenerListaPares");
        List<Point2D> l1 = new ArrayList<>();
        List<Point2D> l2 = new ArrayList<>();
        List<Point2D> l3 = new ArrayList<>();
        List<Point2D> l4 = new ArrayList<>();
        
        l1.add(new Point2D(2,6));
        l1.add(new Point2D(5,6));
        l1.add(new Point2D(5,7));
        l1.add(new Point2D(2,7));
        
        l2.add(new Point2D(5,2));
        l2.add(new Point2D(6,2));
        l2.add(new Point2D(6,3));
        l2.add(new Point2D(5,3));

        l3.add(new Point2D(4,5));
        l3.add(new Point2D(6,5));
        l3.add(new Point2D(6,8));
        l3.add(new Point2D(4,8));

        l4.add(new Point2D(3,4));
        l4.add(new Point2D(7,4));
        l4.add(new Point2D(7,9));
        l4.add(new Point2D(3,9));        
        
        cajaUno = new AABB2D(l1);
        cajaDos = new AABB2D(l2);
        cajaTres = new AABB2D(l3);
        AABB2D cajaCuatro = new AABB2D(l4);
        
        List<AABB2D> listaCajas = new ArrayList<>();
        listaCajas.add(cajaUno);
        listaCajas.add(cajaDos);
        listaCajas.add(cajaTres);
        listaCajas.add(cajaCuatro);
        
        List<Pair<Integer>> expResult0 = new ArrayList<>();
        expResult0.add(new Pair<>(0, 3));
        expResult0.add(new Pair<>(0, 2));
        expResult0.add(new Pair<>(0, 1));
        expResult0.add(new Pair<>(1, 3));
        expResult0.add(new Pair<>(1, 2));
        expResult0.add(new Pair<>(2, 3));
        
        List<Pair<Integer>> expResult1 = new ArrayList<>();
        expResult1.add(new Pair<>(0, 3));
        expResult1.add(new Pair<>(0, 2));
        expResult1.add(new Pair<>(2, 3));
           
        List<Pair<Integer>> result0 = AABB2D.obtenerListaPares(listaCajas, 0);
        List<Pair<Integer>> result1 = AABB2D.obtenerListaPares(listaCajas, 1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        cajaUno = new AABB2D(new Point2D(0,0), 10.0, 20.0);
        cajaDos = new AABB2D(new Point2D(0,0), 10.0, 20.0);
        cajaTres = new AABB2D(new Point2D(1,0), 10.0, 20.0);
        
        assertEquals(cajaUno, cajaDos);     
        //assertsTrue(cajaUno == cajaDos);
        assertTrue(cajaUno.equals(cajaDos));
        
        assertFalse(cajaUno == cajaTres);    
        assertFalse(cajaDos == cajaTres);    
        assertFalse(cajaUno.equals(cajaTres));   
        assertFalse(cajaDos.equals(cajaTres));
    }
    
    @Test
    public void testIntersects() {
        System.out.println("intersects");
        cajaUno = new AABB2D(new Point2D(-3,4),4.0,2.0);
        cajaDos = new AABB2D(new Point2D(3.5,3),3.0,4.0);
        cajaTres = new AABB2D(new Point2D(1,1),4.0,4.0);
        AABB2D cajaCuatro = new AABB2D(new Point2D(4,-4), 2,2);
        boolean result1 = cajaUno.intersects(cajaDos);
        boolean result2 = cajaUno.intersects(cajaTres);
        boolean result3 = cajaUno.intersects(cajaCuatro);
        boolean result4 = cajaDos.intersects(cajaTres);
        boolean result5 = cajaDos.intersects(cajaCuatro);
        boolean result6 = cajaTres.intersects(cajaCuatro);
        assertEquals(false, result1);
        assertEquals(true, result2);
        assertEquals(false, result3);
        assertEquals(true, result4);
        assertEquals(false, result5);
        assertEquals(false, result6);
    }

    @Test
    public void testUnionBV() {
        System.out.println("unionBV");
        List<Point2D> l1 = new ArrayList<>();
        List<Point2D> l2 = new ArrayList<>();
        List<Point2D> l3 = new ArrayList<>();
        List<Point2D> l4 = new ArrayList<>();
        
        l1.add(new Point2D(2,6));
        l1.add(new Point2D(5,6));
        l1.add(new Point2D(5,7));
        l1.add(new Point2D(2,7));
        
        l2.add(new Point2D(5,2));
        l2.add(new Point2D(6,2));
        l2.add(new Point2D(6,3));
        l2.add(new Point2D(5,3));

        l3.add(new Point2D(4,5));
        l3.add(new Point2D(6,5));
        l3.add(new Point2D(6,8));
        l3.add(new Point2D(4,8));

        l4.add(new Point2D(3,4));
        l4.add(new Point2D(7,4));
        l4.add(new Point2D(7,9));
        l4.add(new Point2D(3,9));        
        
        cajaUno = new AABB2D(l1);
        cajaDos = new AABB2D(l2);
        cajaTres = new AABB2D(l3);
        AABB2D cajaCuatro = new AABB2D(l4);

        AABB2D c1 = cajaUno.unionBV(cajaDos);
        AABB2D c2 = cajaUno.unionBV(cajaTres);
        AABB2D c3 = cajaUno.unionBV(cajaCuatro);
        AABB2D c4 = cajaDos.unionBV(cajaTres);
        AABB2D c5 = cajaDos.unionBV(cajaCuatro);
        AABB2D c6 = cajaTres.unionBV(cajaCuatro);
        
        assertEquals(c1, new AABB2D(new Point2D(4,4.5f),4,5));
        assertEquals(c2, new AABB2D(new Point2D(4,6.5f),4,3));
        assertEquals(c3, new AABB2D(new Point2D(4.5f,6.5f),5,5));
        assertEquals(c4, new AABB2D(new Point2D(5,5),2,6));
        assertEquals(c5, new AABB2D(new Point2D(5,5.5f),4,7));
        assertEquals(c6, new AABB2D(new Point2D(5,6.5f),4,5));
    }

    /**
     * Test of sortFunction method, of class AABB2D.
     */
    @Test
    public void testSortFunction1() {
        System.out.println("sortFunction_VarY");
        cajaUno = new AABB2D(new Point2D(-3, 4), 4, 2);
        cajaDos = new AABB2D(new Point2D(3.5f, 3), 3, 4);
        cajaTres = new AABB2D(new Point2D(1, 1), 4, 4);
        AABB2D cajaCuatro = new AABB2D(new Point2D(3, -4), 2, 2);
        List<AABB2D> listaCajas1 = new ArrayList<>();
        List<AABB2D> listaCajas2 = new ArrayList<>();
        
        listaCajas1.add(cajaUno);
        listaCajas1.add(cajaDos);
        listaCajas1.add(cajaTres);
        listaCajas1.add(cajaCuatro);
        
        listaCajas2.add(cajaCuatro);
        listaCajas2.add(cajaTres);
        listaCajas2.add(cajaDos);
        listaCajas2.add(cajaUno);

        // de acuerdo a la varianza se ve como se ordena
        // en este caso se debe al valor de la varianza de Y
        AABB2D.sortFunction(listaCajas1);
        
        assertEquals(listaCajas1, listaCajas2);
    }

    /**
     * Test of sortFunction method, of class AABB2D.
     */
    @Test
    public void testSortFunction2() {
        System.out.println("sortFunction_VarX");
        cajaUno = new AABB2D(new Point2D(-5, 0), 2, 2);
        cajaDos = new AABB2D(new Point2D(-1.5f, 0), 3, 3);
        cajaTres = new AABB2D(new Point2D(2, 0), 4, 4);
        AABB2D cajaCuatro = new AABB2D(new Point2D(7.5f, 0), 5, 5);
        List<AABB2D> listaCajas1 = new ArrayList<>();
        List<AABB2D> listaCajas2 = new ArrayList<>();
        
        listaCajas1.add(cajaCuatro);
        listaCajas1.add(cajaTres);
        listaCajas1.add(cajaDos);
        listaCajas1.add(cajaUno);
        
        listaCajas2.add(cajaUno);
        listaCajas2.add(cajaDos);
        listaCajas2.add(cajaTres);
        listaCajas2.add(cajaCuatro);

        // de acuerdo a la varianza se ve como se ordena
        // en este caso se debe a la varianza de X
        AABB2D.sortFunction(listaCajas1);
        
        assertEquals(listaCajas1, listaCajas2);
    }    
    
    /**
     * Test of sortFunction method, of class AABB2D.
     */
    @Test
    public void testSortFunction3() {
        System.out.println("sortFunction_VarianzasIguales");
        cajaUno = new AABB2D(new Point2D(4, 4), 2, 2);
        cajaDos = new AABB2D(new Point2D(-4, 4), 4, 4);
        cajaTres = new AABB2D(new Point2D(-4, -4), 6, 6);
        AABB2D cajaCuatro = new AABB2D(new Point2D(4, -4), 2, 4);
        List<AABB2D> listaCajas1 = new ArrayList<>();
        List<AABB2D> listaCajas2 = new ArrayList<>();
        
        listaCajas1.add(cajaUno);
        listaCajas1.add(cajaDos);
        listaCajas1.add(cajaTres);
        listaCajas1.add(cajaCuatro);
        
        listaCajas2.add(cajaUno);
        listaCajas2.add(cajaCuatro);
        listaCajas2.add(cajaDos);
        listaCajas2.add(cajaTres);

        // de acuerdo a la varianza se ve como se ordena
        // en este caso se debe al area ya que ambas varianzas son iguales
        AABB2D.sortFunction(listaCajas1);
        
        assertEquals(listaCajas1, listaCajas2);
    }        

    @Test
    public void testMove() {
        System.out.println("move");
        Point2D pto = new Point2D(2,1);
        AABB2D instance = new AABB2D(new Point2D(-5.0, 3.0), 3.0, 3.0);
        instance.move(pto);
        assertEquals(instance.getCentro(), new Point2D(-3.0, 4.0));
    }

    /**
     * Test of compareTo method, of class AABB2D.
     */
    @Test
    public void testCompareTo_AABB2D_1() {
        System.out.println("compareTo: X axis & with primitive");
        // caso 1: cuadros superpuestos
        AABB2D o00 = new AABB2D(new Edge2D(new Point2D(0,0), new Point2D(2,2)), 0);
        AABB2D o01 = new AABB2D(new Edge2D(new Point2D(0,2), new Point2D(2,0)), 1);
        // caso 2: cuadro desplazado a la derecha
        AABB2D o10 = new AABB2D(new Edge2D(new Point2D(0,0), new Point2D(3,2)), 0);
        AABB2D o11 = new AABB2D(new Edge2D(new Point2D(1,0), new Point2D(2,2)), 1);
        // caso 3: cuadro desplazado a la izquierda
        AABB2D o20 = new AABB2D(new Edge2D(new Point2D(0,0), new Point2D(3,2)), 0);
        AABB2D o21 = new AABB2D(new Edge2D(new Point2D(-1,2), new Point2D(1,1)), 1);
        
        assertEquals(o00.compareTo(o01), 0, 0.0);
        assertEquals(o10.compareTo(o11), -1, 0.0);
        assertEquals(o20.compareTo(o21), 1, 0.0);
    }

    /**
     * Test of compareTo method, of class AABB2D.
     */
    @Test
    public void testCompareTo_AABB2D_2() {
        System.out.println("compareTo: with primitive and selection of dimension");
        // caso 1: dos cuadros superpuestos
        AABB2D o00 = new AABB2D(new Edge2D(new Point2D(0,0), new Point2D(2,2)), 0);
        AABB2D o01 = new AABB2D(new Edge2D(new Point2D(0,2), new Point2D(2,0)), 1);
        assertEquals(o00.compareTo(o01, 0), 0, 0.0);
        assertEquals(o00.compareTo(o01, 1), 0, 0.0);
        // caso 2: segunda caja mas a la derecha
        AABB2D o10 = new AABB2D(new Edge2D(new Point2D(0,0), new Point2D(3,2)), 0);
        AABB2D o11 = new AABB2D(new Edge2D(new Point2D(1,0), new Point2D(2,2)), 1);
        assertEquals(o10.compareTo(o11, 0), -1, 0.0);
        assertEquals(o10.compareTo(o11, 1), 0, 0.0);
        // caso 3: segunda caja mas a la izquierda
        AABB2D o20 = new AABB2D(new Edge2D(new Point2D(0,0), new Point2D(3,2)), 0);
        AABB2D o21 = new AABB2D(new Edge2D(new Point2D(-1,1), new Point2D(1,2)), 1);
        assertEquals(o20.compareTo(o21, 0), 1, 0.0);
        assertEquals(o20.compareTo(o21, 1), -1, 0.0);
        // caso 4: misma componente X pero segunda caja mas abajo
        AABB2D o30 = new AABB2D(new Edge2D(new Point2D(0,0), new Point2D(2,2)), 0);
        AABB2D o31 = new AABB2D(new Edge2D(new Point2D(0,-1), new Point2D(1,-3)), 1);
        assertEquals(o30.compareTo(o31, 0), 0, 0.0);
        assertEquals(o30.compareTo(o31, 1), 1, 0.0);
        // caso 5: misma componente Y pero segunda caja mas a la derecha
        AABB2D o40 = new AABB2D(new Edge2D(new Point2D(0,1), new Point2D(2,0)), 0);
        AABB2D o41 = new AABB2D(new Edge2D(new Point2D(3,0), new Point2D(4,2)), 1);
        assertEquals(o40.compareTo(o41, 0), -1, 0.0);
        assertEquals(o40.compareTo(o41, 1), 0, 0.0);
        // caso 6: segunda caja mas a la derecha 
        AABB2D o50 = new AABB2D(new Edge2D(new Point2D(0,0), new Point2D(2,1)), 0);
        AABB2D o51 = new AABB2D(new Edge2D(new Point2D(-2,2), new Point2D(0,3)), 1);
        assertEquals(o50.compareTo(o51, 0), 1, 0.0);
        assertEquals(o50.compareTo(o51, 1), -1, 0.0);
    }
    
    /**
     * Test of compareTo method, of class AABB2D.
     */
    @Test
    public void testCompareTo_BV_int() {
        System.out.println("compareTo: BV with primitive and selection of dimension");
        // caso 1: dos cuadros superpuestos
        BV o00 = new AABB2D(new Edge2D(new Point2D(0,0), new Point2D(2,2)), 0);
        AABB2D o01 = new AABB2D(new Edge2D(new Point2D(0,2), new Point2D(2,0)), 1);
        assertEquals(o00.compareTo(o01, 0), 0, 0.0);
        assertEquals(o00.compareTo(o01, 1), 0, 0.0);
        // caso 2: segunda caja mas a la derecha
        BV o10 = new AABB2D(new Edge2D(new Point2D(0,0), new Point2D(3,2)), 0);
        AABB2D o11 = new AABB2D(new Edge2D(new Point2D(1,0), new Point2D(2,2)), 1);
        assertEquals(o10.compareTo(o11, 0), -1, 0.0);
        assertEquals(o10.compareTo(o11, 1), 0, 0.0);
        // caso 3: segunda caja mas a la izquierda
        BV o20 = new AABB2D(new Edge2D(new Point2D(0,0), new Point2D(3,2)), 0);
        AABB2D o21 = new AABB2D(new Edge2D(new Point2D(-1,1), new Point2D(1,2)), 1);
        assertEquals(o20.compareTo(o21, 0), 1, 0.0);
        assertEquals(o20.compareTo(o21, 1), -1, 0.0);
        // caso 4: misma componente X pero segunda caja mas abajo
        BV o30 = new AABB2D(new Edge2D(new Point2D(0,0), new Point2D(2,2)), 0);
        AABB2D o31 = new AABB2D(new Edge2D(new Point2D(0,-1), new Point2D(1,-3)), 1);
        assertEquals(o30.compareTo(o31, 0), 0, 0.0);
        assertEquals(o30.compareTo(o31, 1), 1, 0.0);
        // caso 5: misma componente Y pero segunda caja mas a la derecha
        BV o40 = new AABB2D(new Edge2D(new Point2D(0,1), new Point2D(2,0)), 0);
        AABB2D o41 = new AABB2D(new Edge2D(new Point2D(3,0), new Point2D(4,2)), 1);
        assertEquals(o40.compareTo(o41, 0), -1, 0.0);
        assertEquals(o40.compareTo(o41, 1), 0, 0.0);
        // caso 6: segunda caja mas a la derecha 
        BV o50 = new AABB2D(new Edge2D(new Point2D(0,0), new Point2D(2,1)), 0);
        AABB2D o51 = new AABB2D(new Edge2D(new Point2D(-2,2), new Point2D(0,3)), 1);
        assertEquals(o50.compareTo(o51, 0), 1, 0.0);
        assertEquals(o50.compareTo(o51, 1), -1, 0.0);
    }

    /**
     * Test of compareTo method, of class AABB2D.
     */
    @Test
    public void testCompareTo_AABB2D() {
        System.out.println("compareTo");
        
        AABB2D box0 = new AABB2D(new Point2D(1.5, 1.5), 1.0, 1.0);
        AABB2D box1 = new AABB2D(new Point2D(1.5, 2.0), 1.0, 2.0);
        AABB2D box2 = new AABB2D(new Point2D(2.0, 1.5), 2.0, 1.0);
        AABB2D box3 = new AABB2D(new Point2D(1.5, -2.5), 1.0, 1.0);
        AABB2D box4 = new AABB2D(new Point2D(2.0, -2.0), 2.0, 2.0);
        AABB2D box5 = new AABB2D(new Point2D(-1.5, 2.0), 1.0, 2.0);
        AABB2D box6 = new AABB2D(new Point2D(-2.5, 0), 1.0, 2.0);
        
        int expResult01 = 0;
        int expResult02 = 0;
        int expResult34 = 0;
        int expResult56 = 1;
        int result01 = box0.compareTo(box1);
        int result02 = box0.compareTo(box2);
        int result34 = box3.compareTo(box4);
        int result56 = box5.compareTo(box6);

        assertEquals(expResult01, result01);
        assertEquals(expResult02, result02);
        assertEquals(expResult34, result34);
        assertEquals(expResult56, result56);
    }

    /**
     * Test of compareTo method, of class AABB2D.
     */
    @Test
    public void testCompareTo_AABB2D_int() {
        System.out.println("compareTo");
        AABB2D box0 = new AABB2D(new Point2D(1.5, 1.5), 1.0, 1.0);
        AABB2D box1 = new AABB2D(new Point2D(1.5, 2.0), 1.0, 2.0);
        AABB2D box2 = new AABB2D(new Point2D(2.0, 1.5), 2.0, 1.0);
        AABB2D box3 = new AABB2D(new Point2D(1.5, -2.5), 1.0, 1.0);
        AABB2D box4 = new AABB2D(new Point2D(2.0, -2.0), 2.0, 2.0);
        AABB2D box5 = new AABB2D(new Point2D(-1.5, 2.0), 1.0, 2.0);
        AABB2D box6 = new AABB2D(new Point2D(-2.5, 0), 1.0, 2.0);
        
        int expResult001 = 0;
        int expResult002 = 0;
        int expResult034 = 0;
        int expResult056 = 1;
        int expResult101 = 0;
        int expResult102 = 0;
        int expResult134 = 0;
        int expResult156 = 1;

        int result001 = box0.compareTo(box1, 0);
        int result002 = box0.compareTo(box2, 0);
        int result034 = box3.compareTo(box4, 0);
        int result056 = box5.compareTo(box6, 0);

        int result101 = box0.compareTo(box1, 1);
        int result102 = box0.compareTo(box2, 1);
        int result134 = box3.compareTo(box4, 1);
        int result156 = box5.compareTo(box6, 1);

        assertEquals(expResult001, result001);
        assertEquals(expResult002, result002);
        assertEquals(expResult034, result034);
        assertEquals(expResult056, result056);
        assertEquals(expResult101, result101);
        assertEquals(expResult102, result102);
        assertEquals(expResult134, result134);
        assertEquals(expResult156, result156);
    }
}
