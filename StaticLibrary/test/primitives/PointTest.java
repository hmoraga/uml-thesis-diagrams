/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class PointTest {
    
    public PointTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetCoords() {
        System.out.println("getCoords");
        Point instance = new Point2D(-2.0, 3.0);
        double expResult0 = -2.0, expResult1 = 3.0;
        double result0 = instance.getCoords(0);
        double result1 = instance.getCoords(1);
        assertEquals(expResult0, result0, 0.0);
        assertEquals(expResult1, result1, 0.0);
    }

    @Test
    public void testDistanceTo() {
        System.out.println("distanceTo");
        Point p = new Point2D(3,5);
        Point instance = new Point2D(4,2);
        double expResult = Math.sqrt(10.0);
        double result = instance.distanceTo(p);
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testDistanceSquaredTo() {
        System.out.println("distanceSquaredTo");
        Point p = new Point2D(3,4);
        Point instance = new Point2D(0,0);
        double expResult = 25;
        double result = instance.distanceSquaredTo(p);
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        Point instance = new Point2D();
        String expResult = "(0.0,0.0)";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Point instance = new Point2D();
        int result = instance.hashCode();
        assertTrue(result!=0);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Point2D();
        Point instance0 = new Point2D(0.0, 0.0);
        Point instance1 = new Point2D(1.0, 1.0);
        boolean expResult0 = true;
        boolean expResult1 = false;
        boolean result0 = instance0.equals(obj);
        boolean result1 = instance1.equals(obj);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of add method, of class Point.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Point p = new Point2D(1,3);
        Point instance = new Point2D(2,1);
        instance.add(p);
        assertEquals(new Point2D(3,4), instance);
    }

    /**
     * Test of linearTransform method, of class Point.
     */
    @Test
    public void testLinearTransform() {
        System.out.println("linearTransform");
        double xmin = -10.0;
        double xmax = 10.0;
        double ymin = -10.0;
        double ymax = 10.0;
        double A = 800.0;
        double B = 600.0;
        Point instance = new Point2D(1,3);
        Point expResult = new Point2D(440, 210);
        Point result = instance.linearTransform(xmin, xmax, ymin, ymax, A, B);
        assertEquals(expResult, result);
    }

    public class PointImpl implements Point {

        public double getCoords(int dimension) {
            return 0.0;
        }

        public void add(Point p) {
        }

        public double distanceTo(Point p) {
            return 0.0;
        }

        public double distanceSquaredTo(Point p) {
            return 0.0;
        }

        public Point linearTransform(double xmin, double xmax, double ymin, double ymax, double A, double B) {
            return null;
        }
    }
}
