/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Point2DTest {
    private Point2D p0, p1, p2, q, r, s, instance;
    private List<Point2D> listaPuntos;
    
    public Point2DTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        listaPuntos = new ArrayList<>();
    }
    
    @After
    public void tearDown() {
        listaPuntos.clear();
    }

    /**
     * Test of equals method, of class Point2D.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Point2D(2.25, -1.75);
        p0 = new Point2D(2.25, -1.75);
        p1 = new Point2D(-2.25, -1.75);
        boolean expResult1 = true, expResult2 = false;
        boolean result1 = p0.equals(obj);
        boolean result2 = p1.equals(obj);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);        
    }

    /**
     * Test of hashCode method, of class Point2D.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        instance = new Point2D();
        int expResult = 0;
        int result = instance.hashCode();
        assertTrue(expResult != result);
    }

    /**
     * Test of getX method, of class Point2D.
     */
    @Test
    public void testGetX() {
        System.out.println("getX");
        instance = new Point2D(3.457, -2.278);
        double expResult = 3.457;
        double result = instance.getX();
        assertEquals(expResult, result, 0.0);        
    }

    /**
     * Test of getY method, of class Point2D.
     */
    @Test
    public void testGetY() {
        System.out.println("getY");
        instance = new Point2D(3.457, -2.278);
        double expResult = -2.278;
        double result = instance.getY();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of exists method, of class Point2D.
     */
    @Test
    public void testExists() {
        System.out.println("exists");
        listaPuntos.add(new Point2D(3, 4));
        listaPuntos.add(new Point2D(new double[]{-0.2, 4.3}));
        listaPuntos.add(new Point2D(-5.0, 0.23));
        
        Point2D instance1 = new Point2D(-5.0, 0.23);
        Point2D instance2 = new Point2D();
        boolean expResult1 = true, expResult2 = false;
        boolean result1 = instance1.exists(listaPuntos);
        boolean result2 = instance2.exists(listaPuntos);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);        
    }

    /**
     * Test of isInside method, of class Point2D.
     */
    @Test
    public void testIsInside() {
        System.out.println("isInside");
        p0 = new Point2D(5,10);
        p1 = new Point2D(-5,10);
        p2 = new Point2D(-5,-10);
        Point2D p3 = new Point2D(5,-10);
        
        listaPuntos.add(p0);
        listaPuntos.add(p1);
        listaPuntos.add(p2);
        listaPuntos.add(p3);
        AABB2D caja = new AABB2D(listaPuntos);
        Point2D inside = new Point2D(-1,-2);
        Point2D border = new Point2D(-5,-3);
        Point2D outside = new Point2D(6,7);
        
        boolean expResult1 = true, expResult2=true, expResult3=false;
        boolean result1 = inside.isInside(caja);
        boolean result2 = border.isInside(caja);
        boolean result3 = outside.isInside(caja);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
    }

    /**
     * Test of add method, of class Point2D.
     */
    @Test
    public void testAdd_Point2D() {
        System.out.println("add");
        p0 = new Point2D(3.5, 2.5);
        instance = new Point2D(2.0, -1.0);
        instance.add(p0);
        assertEquals(instance.getX(), 5.5, 0.00001);
        assertEquals(instance.getY(), 1.5, 0.00001);
    }

    /**
     * Test of add method, of class Point2D.
     */
    @Test
    public void testAdd_double_double() {
        System.out.println("add");
        double x = -1.0;
        double y = 3.0;
        instance = new Point2D(3.5, -2.3);
        instance.add(x, y);
        assertEquals(instance.distanceTo(new Point2D(2.5, 0.7)), 0 ,0.00001);
    }    
    
    /**
     * Test of distanceTo part of superclass Point.
     */
    @Test
    public void testDistanceTo(){
        System.out.println("DistanceTo");
        p0 = new Point2D(1,1);
        p1 = new Point2D(-1, -1);
        assertEquals(p1.distanceTo(p0), 2*Math.sqrt(2), 0.00001);
        assertEquals(p0.distanceTo(p1), 2*Math.sqrt(2), 0.00001);
    }
    
    /**
     * Test of distanceSquaredTo part of superclass Point.
     */
    @Test
    public void testDistanceSquaredTo(){
        System.out.println("DistanceSquaredTo");
        p0 = new Point2D(1,1);
        p1 = new Point2D(-1, -1);
        assertEquals(p1.distanceSquaredTo(p0), 8f, 0.00001);
        assertEquals(p0.distanceSquaredTo(p1), 8f, 0.00001);
    }
    
    /**
     * Test of equals method, of class Point2D.
     */
    @Test
    public void testEqualsFalse() {
        System.out.println("equals false");
        p0 = new Point2D(1,1);
        p1 = new Point2D(-1, -1);

        assertFalse(p0 == p1);
        assertFalse(p0.equals(p1));
        assertFalse(p1.equals(p0));
        assertNotSame(p0, p1);
    }
    
    /**
     * Test of equals method, of class Point2D.
     */
    @Test
    public void testEqualsTrue() {
        System.out.println("equals true");
        p0 = new Point2D(1,1);
        p1 = p0;

        assertTrue(p0.equals(p1));
        assertTrue(p1.equals(p0));
        assertTrue(p0 == p1);
        assertSame(p0, p1);
    }
    
    /**
     * Test of getCoords method, of class Point
     */
    @Test
    public void testGetCoords(){
        System.out.println("GetCoords");
        Point2D p = new Point2D(1,-1);
        assertEquals(p.getCoords(0), p.getX(), 0.0);
        assertEquals(p.getCoords(1), p.getY(), 0.0);
        assertEquals(p.getCoords(0), 1.0, 0.0);
        assertEquals(p.getCoords(1), -1.0, 0.0);
    }    

    /**
     * Test of toString method, of class Point2D.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Point2D instance1 = new Point2D();
        String expResult = "(0.0,0.0)";
        String result = instance1.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of add method, of class Point2D.
     */
    @Test
    public void testAdd_Point() {
        System.out.println("add");
        instance = new Point2D(2, -1);
        Point p = new Point2D(1, 3);
        instance.add(p);
        assertEquals(instance, new Point2D(3, 2));
    }

    /**
     * Test of rotateBy method, of class Point2D.
     */
    @Test
    public void testRotateBy() {
        System.out.println("rotateBy");
        double angle = Math.PI/2;
        instance = new Point2D(1,1);
        Point2D expResult = new Point2D(1,-1);
        Point2D result = instance.rotateBy(angle);
        assertEquals(expResult.getX(), result.getX(), 0.00001);
        assertEquals(expResult.getY(), result.getY(), 0.00001);
    }

    /**
     * Test of multipliedByScalar method, of class Point2D.
     */
    @Test
    public void testMultipliedByScalar() {
        System.out.println("multipliedByScalar");
        double scale = 2.0;
        instance = new Point2D(2.0, 3.0);
        Point2D expResult = new Point2D(4.0, 6.0);
        Point2D result = instance.multipliedByScalar(scale);
        assertEquals(expResult, result);
    }

    /**
     * Test of linearTransform method, of class Point2D.
     */
    @Test
    public void testLinearTransform() {
        System.out.println("linearTransform");
        double xmin = -10.0;
        double xmax = 10.0;
        double ymin = -10.0;
        double ymax = 10.0;
        double A = 800.0;
        double B = 600.0;
        instance = new Point2D(1, 3);
        Point expResult = new Point2D(440, 210);
        Point result = instance.linearTransform(xmin, xmax, ymin, ymax, A, B);
        assertEquals(expResult, result);
    }
}
