/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class PairTest {
    public List<Pair<Integer>> listaPares;
    
    public PairTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        listaPares = new ArrayList<>();
        listaPares.add(new Pair<>(1,2));
        listaPares.add(new Pair<>(3,4));
        listaPares.add(new Pair<>(4,1));
        listaPares.add(new Pair<>(1,6));
        listaPares.add(new Pair<>(4,5));
    }
    
    @After
    public void tearDown() {
        listaPares.clear();
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Pair<Integer> instance = new Pair<>(3, 5);
        int expResult = 0;
        int result = instance.hashCode();
        assertTrue(expResult != result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Pair<>(3, 5);
        Pair<Integer> instance0 = new Pair<>(3, 5);
        Pair<Integer> instance1 = new Pair<>(-3, 5);
        boolean expResult0 = true, expResult1 = false;
        boolean result0 = instance0.equals(obj), result1 = instance1.equals(obj);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);

        obj = new Pair<>(-2.5,4.5);
        Pair<Double> instance2 = new Pair<>(-2.5, 4.5);
        Pair<Double> instance3 = new Pair<>(4.5, -2.5);
        Pair<Double> instance4 = new Pair<>(-2.5, 1.5);
        Pair<Double> instance5 = new Pair<>(0.5, 4.5);
        
        boolean expResult2 = true, expResult3 = true, expResult4 = false, expResult5 = false;
        boolean result2 = instance2.equals(obj);
        boolean result3 = instance2.equals(instance3);  // pares invertidos tambien son true
        boolean result4 = instance2.equals(instance4);
        boolean result5 = instance2.equals(instance5);
        
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
        assertEquals(expResult5, result5);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        Pair<Integer> instance = new Pair<>(5,2);
        String expResult = "(5,2)";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    @Test
    public void testInverted() {
        System.out.println("inverted");
        Pair<Integer> instance0 = new Pair<>(5,2);
        Pair<Integer> instance1 = new Pair<>(2,5);
        Pair expResult0 = instance1;
        Pair result = instance0.inverted();
        assertEquals(expResult0, result);
    }

    @Test
    public void testExists() {
        System.out.println("exists");
        Pair<Integer> instance0 = new Pair<>(5,2);
        Pair<Integer> instance1 = new Pair<>(2,5);
        Pair<Integer> instance2 = new Pair<>(1,5);
        Pair<Integer> instance3 = new Pair<>(7,2);
        boolean expResult0 = true, expResult1 = false;
        
        listaPares.add(instance0);
        listaPares.add(instance2);
        
        boolean result0 = instance1.exists(listaPares);
        boolean result1 = instance3.exists(listaPares);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);

        Pair<Double> instance4 = new Pair<>(-2.5, 4.5);
        Pair<Double> instance5 = new Pair<>(4.5, -2.5);
        Pair<Double> instance6 = new Pair<>(-2.5, 1.5);
        Pair<Double> instance7 = new Pair<>(0.5, 4.5);
        
        List<Pair<Double>> listaPares2 = new ArrayList<>();
        
        listaPares2.add(instance4);
        listaPares2.add(instance5);
        listaPares2.add(instance6);
        listaPares2.add(instance7);
                
        assertEquals(true, instance5.exists(listaPares2));
        assertEquals(false, new Pair<>(1.0,1.0).exists(listaPares2));
    }

    @Test
    public void testExists_Integer() {
        System.out.println("exists");
        Pair<Integer> instance0 = new Pair<>(2,3);
        Pair<Integer> instance1 = new Pair<>(1,4);
        boolean expResult0 = false;
        boolean expResult1 = true;
        
        boolean result0 = instance0.exists(listaPares);
        boolean result1 = instance1.exists(listaPares);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of setFirst method, of class Pair.
     */
    @Test
    public void testSetFirst() {
        System.out.println("setFirst");
        Object first = 2;
        Pair<Integer> instance = new Pair<>(3,4);
        instance.setFirst((Integer)first);
        assertEquals(instance.first, 2, 0.00001);
    }

    /**
     * Test of setSecond method, of class Pair.
     */
    @Test
    public void testSetSecond() {
        System.out.println("setSecond");
        Object second = 4.0;
        Pair<Double> instance = new Pair<>(1.5, 2.5);
        instance.setSecond((Double)second);
        assertEquals(instance.second, 4.0, 0.00001);
    }
}
