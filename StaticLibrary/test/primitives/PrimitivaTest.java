/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class PrimitivaTest {
    
    public PrimitivaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getEdge method, of class Primitiva.
     */
    @Test
    public void testGetEdge() {
        System.out.println("getEdge");
        Primitiva instance = new Primitiva2D(new Edge2D(new Point2D(-1, -1), new Point2D(1, 1)), 2);
        Edge expResult = new Edge2D(new Point2D(-1, -1), new Point2D(1, 1));
        Edge result = instance.getEdge();
        assertEquals(expResult, result);
    }

    /**
     * Test of getIndice method, of class Primitiva.
     */
    @Test
    public void testGetIndice() {
        System.out.println("getIndice");
        Primitiva instance = new Primitiva2D(new Edge2D(new Point2D(-1,-1), new Point2D(1,1)), 1);
        int expResult = 1;
        int result = instance.getIndice();
        assertEquals(expResult, result);
    }

    public class PrimitivaImpl implements Primitiva {

        @Override
        public Edge getEdge() {
            return null;
        }

        @Override
        public int getIndice() {
            return 0;
        }
    }
}
