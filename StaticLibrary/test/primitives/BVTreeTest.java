/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class BVTreeTest {
    
    public BVTreeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getRoot method, of class BVTree.
     */
    @Test
    public void testGetRoot_1() {
        System.out.println("Crear Arbol Sin Nodos");
	BVTree arbol = new BVTree();

        assertNull(arbol.getRoot());
    }    
    
    /**
     * Test of getRoot method, of class BVTree.
     */
    @Test
    public void testGetRoot_2() {
        System.out.println("getRoot Null");
        BVTree instance = new BVTree();
        BVNode result = instance.getRoot();
        assertNotNull(instance);
        assertNull(result);
    }

    /**
     * Test of getRoot method, of class BVTree.
     */
    @Test
    public void testGetRoot_3() {
        System.out.println("Create Not Empty With Primitive");
        BVNode nodo = new BVNode(new AABB2D(new Edge2D(new Point2D(-5.0, 1.0), new Point2D(1.0, 9.0)), 0));
        BVTree arbol = new BVTree(nodo);
        
        assertNotNull(arbol);
        assertNotNull(arbol.getRoot());
        assertTrue(arbol.getRoot().isLeaf());
        assertEquals(arbol.getRoot(), nodo);
    }
    
    /**
     * Test of getRoot method, of class BVTree.
     */
    @Test
    public void testGetRoot_4() {
        System.out.println("Create Not Empty Without Primitive");
	BVNode nodo = new BVNode(new AABB2D(new Point2D(-2.0, 5.0), 6.0, 8.0));	// un nodo cualquiera
	BVTree arbol = new BVTree(nodo);
			
	assertNotNull(arbol);
	assertNotNull(arbol.getRoot());
	assertTrue(arbol.getRoot().isLeaf());
	assertEquals(arbol.getRoot(), nodo);
    }

    /**
     * Test of crearArbol method, of class BVTree.
     */
    @Test
    public void testCrearArbol_1() {
        System.out.println("crearArbol: Un nodo");
	List<AABB2D> listaCajas = new ArrayList<>();
	AABB2D q = new AABB2D(new Point2D(-5,-7),3, 4);

	listaCajas.add(q);
	BVTree arbol = new BVTree(listaCajas);

	BV p = arbol.getRoot().getData();

	assertEquals(((AABB2D)p).getCentro(), q.getCentro());
	assertEquals(((AABB2D)p).getLargoX(), q.getLargoX(), 0.0);
	assertEquals(((AABB2D)p).getLargoY(), q.getLargoY(), 0.0);

	assertTrue(arbol.getRoot().isLeaf());
	assertNull(arbol.getRoot().getLeft());
	assertNull(arbol.getRoot().getRight());
    }

    /**
     * Test of crearArbol method, of class BVTree.
     */
    @Test
    public void testCrearArbol_2() {
        System.out.println("crearArbol: Dos nodos");
	List<AABB2D> listaCajas = new ArrayList<>();
	AABB2D p0, p1, p2;

	listaCajas.add(new AABB2D(new Edge2D(new Point2D(-6.5,-9), new Point2D(-3.5,-5)), 0)); // leaf
	listaCajas.add(new AABB2D(new Edge2D(new Point2D(-4.5,-4), new Point2D(-1.5,0)), 1));  // leaf

	BVTree arbol = new BVTree(listaCajas);

	AABB2D proot = (AABB2D)arbol.getRoot().getData();
	AABB2D pleft = (AABB2D)arbol.getRoot().getLeft().getData();
	AABB2D pright = (AABB2D)arbol.getRoot().getRight().getData();

	p0 = new AABB2D(new Point2D(-4,-4.5),5,9);
	p1 = new AABB2D(new Point2D(-5,-7),3,4);
	p2 = new AABB2D(new Point2D(-3,-2),3,4);

	assertEquals(proot.getCentro(), p0.getCentro());
	assertEquals(proot.getLargoX(), p0.getLargoX(), 0.0);
	assertEquals(proot.getLargoY(), p0.getLargoY(), 0.0);

	assertEquals(pleft.getCentro(), p1.getCentro());
	assertEquals(pleft.getLargoX(), p1.getLargoX(), 0.0);
	assertEquals(pleft.getLargoY(), p1.getLargoY(), 0.0);

	assertEquals(pright.getCentro(), p2.getCentro());
	assertEquals(pright.getLargoX(), p2.getLargoX(), 0.0);
	assertEquals(pright.getLargoY(), p2.getLargoY(), 0.0);

	assertTrue(arbol.getRoot().getLeft().isLeaf());
	assertTrue(arbol.getRoot().getRight().isLeaf());
    }

    /**
     * Test of crearArbol method, of class BVTree.
     */
    @Test
    public void testCrearArbol_3() {
        System.out.println("crearArbol: tres nodos");
	List<AABB2D> listaCajas = new ArrayList<>();
	AABB2D p0, p1, p2, p3, p4;

	p0 = new AABB2D(new Point2D(-1,-2.5),11,13);
	p1 = new AABB2D(new Point2D(3,2),3,4);
	p2 = new AABB2D(new Point2D(-4,-4.5),5,9);
	p3 = new AABB2D(new Point2D(-5,-7),3,4);
	p4 = new AABB2D(new Point2D(-3,-2),3,4);

	listaCajas.add(new AABB2D(new Point2D(-5,-7),3,4));
	listaCajas.add(new AABB2D(new Point2D(-3,-2),3,4));
	listaCajas.add(new AABB2D(new Point2D(3,2),3,4));

	BVTree arbol = new BVTree(listaCajas);
	BVNode raiz = arbol.getRoot();
	BVNode izquierdo = raiz.getLeft();
	BVNode derecho = raiz.getRight();

	AABB2D boxRaiz = (AABB2D)raiz.getData();
	Point2D praiz = boxRaiz.getCentro();

	AABB2D boxIzq = (AABB2D)izquierdo.getData();
	Point2D pizq = boxIzq.getCentro();

	AABB2D boxDer = (AABB2D)derecho.getData();
	Point2D pder = boxDer.getCentro();

	AABB2D boxDerLeft = (AABB2D)derecho.getLeft().getData();
	Point2D pderizq = boxDerLeft.getCentro();

	AABB2D boxDerRight = (AABB2D)derecho.getRight().getData();
	Point2D pderder = boxDerRight.getCentro();

	assertEquals(praiz, p0.getCentro());
	assertEquals(boxRaiz.getLargoX(), p0.getLargoX(), 0.0);
	assertEquals(boxRaiz.getLargoY(), p0.getLargoY(), 0.0);

	assertEquals(pizq, p1.getCentro());
	assertEquals(boxIzq.getLargoX(), p1.getLargoX(), 0.0);
	assertEquals(boxIzq.getLargoY(), p1.getLargoY(), 0.0);

	assertEquals(pder, p2.getCentro());
	assertEquals(boxDer.getLargoX(), p2.getLargoX(), 0.0);
	assertEquals(boxDer.getLargoY(), p2.getLargoY(), 0.0);

	assertEquals(pderizq, p3.getCentro());
	assertEquals(boxDerLeft.getLargoX(), p3.getLargoX(), 0.0);
	assertEquals(boxDerLeft.getLargoY(), p3.getLargoY(), 0.0);

	assertEquals(pderder, p4.getCentro());
	assertEquals(boxDerRight.getLargoX(), p4.getLargoX(), 0.0);
	assertEquals(boxDerRight.getLargoY(), p4.getLargoY(), 0.0);

	assertTrue(izquierdo.isLeaf());
	assertFalse(derecho.isLeaf());
	assertTrue(derecho.getLeft().isLeaf());
	assertTrue(derecho.getRight().isLeaf());
    }

    /**
     * Test of crearArbol method, of class BVTree.
     */
    @Test
    public void testCrearArbol_4() {
        System.out.println("crearArbol: BoxesConcentricos");
	List<AABB2D> listaCajas = new ArrayList<>();
        
	AABB2D p0, p1, p2, p3;

	p0 = new AABB2D(new Point2D(0,0), 2.5, 2.5);
	p1 = new AABB2D(new Point2D(0,0), 1.5, 1.5);
	p2 = new AABB2D(new Point2D(0,0), 1, 1);
	p3 = new AABB2D(new Point2D(0,0), 2, 2);

	listaCajas.add(new AABB2D(new Point2D(0,0),1,1));
	listaCajas.add(new AABB2D(new Point2D(0,0),1.5,1.5));
	listaCajas.add(new AABB2D(new Point2D(0,0),2,2));
	listaCajas.add(new AABB2D(new Point2D(0,0),2.5,2.5));

	BVTree arbol = new BVTree(listaCajas);
	BVNode raiz = arbol.getRoot();
	BVNode izquierdo = raiz.getLeft();
	BVNode derecho = raiz.getRight();

	AABB2D boxRaiz = (AABB2D)raiz.getData();
	Point2D praiz = boxRaiz.getCentro();

	AABB2D boxIzq = (AABB2D)izquierdo.getData();
	Point2D pizq = boxIzq.getCentro();

	AABB2D boxDer = (AABB2D)derecho.getData();
	Point2D pder = boxDer.getCentro();

	AABB2D boxIzqLeft = (AABB2D)izquierdo.getLeft().getData();
	Point2D pizqizq = boxIzqLeft.getCentro();

	AABB2D boxIzqRight = (AABB2D)izquierdo.getRight().getData();
	Point2D pizqder = boxIzqRight.getCentro();

	AABB2D boxDerLeft = (AABB2D)derecho.getLeft().getData();
	Point2D pderizq = boxDerLeft.getCentro();

	AABB2D boxDerRight = (AABB2D)derecho.getRight().getData();
	Point2D pderder = boxDerRight.getCentro();

	assertEquals(praiz, p0.getCentro());
	assertEquals(boxRaiz.getLargoX(), p0.getLargoX(), 0.0);
	assertEquals(boxRaiz.getLargoY(), p0.getLargoY(), 0.0);

	assertEquals(pizq, p1.getCentro());
	assertEquals(boxIzq.getLargoX(), p1.getLargoX(), 0.0);
	assertEquals(boxIzq.getLargoY(), p1.getLargoY(), 0.0);

	assertEquals(pder, p0.getCentro());
	assertEquals(boxDer.getLargoX(), p0.getLargoX(), 0.0);
	assertEquals(boxDer.getLargoY(), p0.getLargoY(), 0.0);

	assertEquals(pizqizq, p2.getCentro());
	assertEquals(boxIzqLeft.getLargoX(), p2.getLargoX(), 0.0);
	assertEquals(boxIzqLeft.getLargoY(), p2.getLargoY(), 0.0);

	assertEquals(pizqder, p1.getCentro());
	assertEquals(boxIzqRight.getLargoX(), p1.getLargoX(), 0.0);
	assertEquals(boxIzqRight.getLargoY(), p1.getLargoY(), 0.0);

	assertEquals(pderizq, p3.getCentro());
	assertEquals(boxDerLeft.getLargoX(), p3.getLargoX(), 0.0);
	assertEquals(boxDerLeft.getLargoY(), p3.getLargoY(), 0.0);

	assertEquals(pderder, p0.getCentro());
	assertEquals(boxDerRight.getLargoX(), p0.getLargoX(), 0.0);
	assertEquals(boxDerRight.getLargoY(), p0.getLargoY(), 0.0);

	assertFalse(izquierdo.isLeaf());
	assertFalse(derecho.isLeaf());
	assertTrue(izquierdo.getRight().isLeaf());
	assertTrue(izquierdo.getLeft().isLeaf());
	assertTrue(derecho.getRight().isLeaf());
	assertTrue(derecho.getLeft().isLeaf());      
    }

    /**
     * Test of crearArbol method, of class BVTree.
     */
    @Test
    public void testCrearArbol_5() {
        System.out.println("crearArbol: BoxesTamanosDiferentesEquiespaciados");
	List<AABB2D> listaCajas = new ArrayList<>();

	AABB2D p0, p1, p2, p3, p4, p5, p6;

	p0 = new AABB2D(new Point2D(-40,0),10,10);
	p1 = new AABB2D(new Point2D(-17.5,0),15,15);
	p2 = new AABB2D(new Point2D(10,0),20,20);
	p3 = new AABB2D(new Point2D(42.5,0),25,25);

	p4 = new AABB2D(new Point2D(5,0), 100, 25);
	p5 = new AABB2D(new Point2D(-27.5,0), 35, 15);
	p6 = new AABB2D(new Point2D(27.5,0), 55, 25);

        listaCajas.add(p0);
	listaCajas.add(p1);
	listaCajas.add(p2);
	listaCajas.add(p3);

	BVTree arbol = new BVTree(listaCajas);
	BVNode raiz = arbol.getRoot();
	BVNode izquierdo = raiz.getLeft();
	BVNode derecho = raiz.getRight();

	AABB2D boxRaiz = (AABB2D)raiz.getData();
	Point2D praiz = boxRaiz.getCentro();

        AABB2D boxIzq = (AABB2D)izquierdo.getData();
	Point2D pizq = boxIzq.getCentro();

	AABB2D boxDer = (AABB2D)derecho.getData();
	Point2D pder = boxDer.getCentro();

	AABB2D boxIzqLeft = (AABB2D)izquierdo.getLeft().getData();
	Point2D pizqizq = boxIzqLeft.getCentro();

	AABB2D boxIzqRight = (AABB2D)izquierdo.getRight().getData();
	Point2D pizqder = boxIzqRight.getCentro();

	AABB2D boxDerLeft = (AABB2D)derecho.getLeft().getData();
	Point2D pderizq = boxDerLeft.getCentro();

	AABB2D boxDerRight = (AABB2D)derecho.getRight().getData();
	Point2D pderder = boxDerRight.getCentro();

	assertEquals(praiz, p4.getCentro());
	assertEquals(boxRaiz.getLargoX(), p4.getLargoX(), 0.0);
	assertEquals(boxRaiz.getLargoY(), p4.getLargoY(), 0.0);

	assertEquals(pizq, p5.getCentro());
	assertEquals(boxIzq.getLargoX(), p5.getLargoX(), 0.0);
	assertEquals(boxIzq.getLargoY(), p5.getLargoY(), 0.0);

	assertEquals(pder, p6.getCentro());
	assertEquals(boxDer.getLargoX(), p6.getLargoX(), 0.0);
	assertEquals(boxDer.getLargoY(), p6.getLargoY(), 0.0);

	assertEquals(pizqizq, p0.getCentro());
	assertEquals(boxIzqLeft.getLargoX(), p0.getLargoX(), 0.0);
	assertEquals(boxIzqLeft.getLargoY(), p0.getLargoY(), 0.0);

	assertEquals(pizqder, p1.getCentro());
	assertEquals(boxIzqRight.getLargoX(), p1.getLargoX(), 0.0);
	assertEquals(boxIzqRight.getLargoY(), p1.getLargoY(), 0.0);

	assertEquals(pderizq, p2.getCentro());
	assertEquals(boxDerLeft.getLargoX(), p2.getLargoX(), 0.0);
	assertEquals(boxDerLeft.getLargoY(), p2.getLargoY(), 0.0);

	assertEquals(pderder, p3.getCentro());
	assertEquals(boxDerRight.getLargoX(), p3.getLargoX(), 0.0);
	assertEquals(boxDerRight.getLargoY(), p3.getLargoY(), 0.0);
			
	assertTrue(izquierdo.getLeft().isLeaf());
	assertTrue(izquierdo.getRight().isLeaf());
	assertTrue(derecho.getLeft().isLeaf());
	assertTrue(derecho.getRight().isLeaf());
    }

    /**
     * Test of crearArbol method, of class BVTree.
     */
    @Test
    public void testCrearArbol_6() {
        System.out.println("crearArbol: Caso 6");
	List<AABB2D> listaCajas = new ArrayList<>();
    
	/* 4 cajas: 2 de tamaños iguales c/u. Asociadas las pequeñas con las 
            grandes a corta distancia, de manera que las cajas grandes no se
            juntan a la primera agrupacion */
	AABB2D p0, p1, p2, p3, p4, p5, p6;

	p0 = new AABB2D(new Point2D(-40,10),10,10);
	p1 = new AABB2D(new Point2D(30,0),10,10);
	p2 = new AABB2D(new Point2D(-30,0),20,20);
	p3 = new AABB2D(new Point2D(40,10),20,20);
	p4 = new AABB2D(new Point2D(2.5,5), 95, 30);
	p5 = new AABB2D(new Point2D(-32.5,2.5), 25, 25);
	p6 = new AABB2D(new Point2D(37.5,7.5), 25, 25);

	listaCajas.add(p0);
	listaCajas.add(p1);
	listaCajas.add(p2);
	listaCajas.add(p3);

	BVTree arbol = new BVTree(listaCajas);
	BVNode raiz = arbol.getRoot();
	BVNode izquierdo = raiz.getLeft();
	BVNode derecho = raiz.getRight();

	AABB2D boxRaiz = (AABB2D)raiz.getData();
	Point2D praiz = boxRaiz.getCentro();

        AABB2D boxIzq = (AABB2D)izquierdo.getData();
	Point2D pizq = boxIzq.getCentro();

	AABB2D boxDer = (AABB2D)derecho.getData();
	Point2D pder = boxDer.getCentro();

	AABB2D boxIzqLeft = (AABB2D)izquierdo.getLeft().getData();
	Point2D pizqizq = boxIzqLeft.getCentro();

	AABB2D boxIzqRight = (AABB2D)izquierdo.getRight().getData();
	Point2D pizqder = boxIzqRight.getCentro();

	AABB2D boxDerLeft = (AABB2D)derecho.getLeft().getData();
	Point2D pderizq = boxDerLeft.getCentro();

	AABB2D boxDerRight = (AABB2D)derecho.getRight().getData();
	Point2D pderder = boxDerRight.getCentro();
			
	assertEquals(praiz, p4.getCentro());
	assertEquals(boxRaiz.getLargoX(), p4.getLargoX(), 0.0);
	assertEquals(boxRaiz.getLargoY(), p4.getLargoY(), 0.0);

	assertEquals(pizq, p5.getCentro());
	assertEquals(boxIzq.getLargoX(), p5.getLargoX(), 0.0);
	assertEquals(boxIzq.getLargoY(), p5.getLargoY(), 0.0);

	assertEquals(pder, p6.getCentro());
	assertEquals(boxDer.getLargoX(), p6.getLargoX(), 0.0);
	assertEquals(boxDer.getLargoY(), p6.getLargoY(), 0.0);

	assertEquals(pizqizq, p0.getCentro());
	assertEquals(boxIzqLeft.getLargoX(), p0.getLargoX(), 0.0);
	assertEquals(boxIzqLeft.getLargoY(), p0.getLargoY(), 0.0);

	assertEquals(pizqder, p2.getCentro());
	assertEquals(boxIzqRight.getLargoX(), p2.getLargoX(), 0.0);
	assertEquals(boxIzqRight.getLargoY(), p2.getLargoY(), 0.0);

        assertEquals(pderizq, p1.getCentro());
	assertEquals(boxDerLeft.getLargoX(), p1.getLargoX(), 0.0);
	assertEquals(boxDerLeft.getLargoY(), p1.getLargoY(), 0.0);

        assertEquals(pderder, p3.getCentro());
	assertEquals(boxDerRight.getLargoX(), p3.getLargoX(), 0.0);
	assertEquals(boxDerRight.getLargoY(), p3.getLargoY(), 0.0);

	assertTrue(izquierdo.getLeft().isLeaf());
	assertTrue(izquierdo.getRight().isLeaf());
	assertTrue(derecho.getLeft().isLeaf());
	assertTrue(derecho.getRight().isLeaf());
    }

    /**
     * Test of crearArbol method, of class BVTree.
     */
    @Test
    public void testCrearArbol_7() {
        System.out.println("crearArbol: Caso 7");
	List<AABB2D> listaCajas = new ArrayList<>();
	/* 4 cajas de tamaños iguales, equiespaciadas entre si */
	AABB2D p0, p1, p2, p3, p4, p5, p6;

	p0 = new AABB2D(new Point2D(-10,10),10,10);
        p1 = new AABB2D(new Point2D(10,10),10,10);
	p2 = new AABB2D(new Point2D(10,-10),10,10);
	p3 = new AABB2D(new Point2D(-10,-10),10,10);
	p4 = new AABB2D(new Point2D(0,0), 30, 30);
	p5 = new AABB2D(new Point2D(-10,0), 10, 30);
	p6 = new AABB2D(new Point2D(10,0), 10, 30);

	listaCajas.add(p0);
	listaCajas.add(p1);
	listaCajas.add(p2);
	listaCajas.add(p3);

	BVTree arbol = new BVTree(listaCajas);
	BVNode raiz = arbol.getRoot();
	BVNode izquierdo = raiz.getLeft();
	BVNode derecho = raiz.getRight();

        AABB2D boxRaiz = (AABB2D)raiz.getData();
	Point2D praiz = boxRaiz.getCentro();

	AABB2D boxIzq = (AABB2D)izquierdo.getData();
	Point2D pizq = boxIzq.getCentro();

	AABB2D boxDer = (AABB2D)derecho.getData();
	Point2D pder = boxDer.getCentro();

	AABB2D boxIzqLeft = (AABB2D)izquierdo.getLeft().getData();
	Point2D pizqizq = boxIzqLeft.getCentro();

	AABB2D boxIzqRight = (AABB2D)izquierdo.getRight().getData();
	Point2D pizqder = boxIzqRight.getCentro();

	AABB2D boxDerLeft = (AABB2D)derecho.getLeft().getData();
	Point2D pderizq = boxDerLeft.getCentro();

	AABB2D boxDerRight = (AABB2D)derecho.getRight().getData();
	Point2D pderder = boxDerRight.getCentro();

	assertEquals(praiz, p4.getCentro());
	assertEquals(boxRaiz.getLargoX(), p4.getLargoX(), 0.0);
	assertEquals(boxRaiz.getLargoY(), p4.getLargoY(), 0.0);

	assertEquals(pizq, p5.getCentro());
	assertEquals(boxIzq.getLargoX(), p5.getLargoX(), 0.0);
	assertEquals(boxIzq.getLargoY(), p5.getLargoY(), 0.0);

        assertEquals(pder, p6.getCentro());
	assertEquals(boxDer.getLargoX(), p6.getLargoX(), 0.0);
	assertEquals(boxDer.getLargoY(), p6.getLargoY(), 0.0);

        assertEquals(pizqizq, p3.getCentro());
	assertEquals(boxIzqLeft.getLargoX(), p3.getLargoX(), 0.0);
	assertEquals(boxIzqLeft.getLargoY(), p3.getLargoY(), 0.0);

	assertEquals(pizqder, p0.getCentro());
	assertEquals(boxIzqRight.getLargoX(), p0.getLargoX(), 0.0);
	assertEquals(boxIzqRight.getLargoY(), p0.getLargoY(), 0.0);

	assertEquals(pderizq, p2.getCentro());
	assertEquals(boxDerLeft.getLargoX(), p2.getLargoX(), 0.0);
	assertEquals(boxDerLeft.getLargoY(), p2.getLargoY(), 0.0);

	assertEquals(pderder, p1.getCentro());
	assertEquals(boxDerRight.getLargoX(), p1.getLargoX(), 0.0);
	assertEquals(boxDerRight.getLargoY(), p1.getLargoY(), 0.0);

	assertTrue(izquierdo.getLeft().isLeaf());
	assertTrue(izquierdo.getRight().isLeaf());
	assertTrue(derecho.getLeft().isLeaf());
	assertTrue(derecho.getRight().isLeaf());
    }    
    
    /**
     * Test of getVariance method, of class BVTree.
     */
    @Test
    public void testGetVariance() {
        System.out.println("getVariance");
	// este método me pide un arreglo de Nodos y compara donde deberia ir un
        // nuevo nodo que deseo ingresar
        List<BVNode> listaNodos = new ArrayList<>();
	listaNodos.add(new BVNode(new AABB2D(new Point2D(-3,-5),3,4)));
        listaNodos.add(new BVNode(new AABB2D(new Point2D(-2,-3),3,4)));
	listaNodos.add(new BVNode(new AABB2D(new Point2D(2,-3),3,4)));
	listaNodos.add(new BVNode(new AABB2D(new Point2D(3,-5),3,4)));
        
	Pair<Double> varianza = BVTree.getVariance(listaNodos);

	assertEquals(varianza.first, 6.5, 0.00001);
	assertEquals(varianza.second, 1, 0.00001);
	assertTrue(varianza.first > varianza.second);
    }

    /**
     * Test of getMean method, of class BVTree.
     */
    @Test
    public void testGetMean() {
        System.out.println("getMean");
	// este método me pide un arreglo de Nodos y compara donde deberia ir un nuevo nodo
	// que deseo ingresar
	List<BVNode> listaNodos = new ArrayList<>();

        listaNodos.add(new BVNode(new AABB2D(new Point2D(-3,-5),3,4)));
	listaNodos.add(new BVNode(new AABB2D(new Point2D(-2,-3),3,4)));
	listaNodos.add(new BVNode(new AABB2D(new Point2D(2,-3),3,4)));
	listaNodos.add(new BVNode(new AABB2D(new Point2D(3,-5),3,4)));

	Pair<Double> promedio = BVTree.getMean(listaNodos);

	assertEquals(promedio.first, 0, 0.00001);
	assertEquals(promedio.second, -4, 0.00001);
    }

    /**
     * Test of obtenerExtremos method, of class BVTree.
     */
    @Test
    public void testObtenerExtremos() {
        System.out.println("obtenerExtremos");
        int dimension0 = 0, dimension1 = 1;

        List<AABB2D> listaCajas = new ArrayList<>();

        listaCajas.add(new AABB2D(new Point2D(-3,-5),3,4));
	listaCajas.add(new AABB2D(new Point2D(-2,-3),3,4));
	listaCajas.add(new AABB2D(new Point2D(2,-3),3,4));
	listaCajas.add(new AABB2D(new Point2D(3,-5),3,4));

        BVTree instance = new BVTree(listaCajas);
        
        Pair<Double> expResult0 = new Pair<>(-4.5, 4.5);
        Pair<Double> result0 = instance.obtenerExtremos(dimension0);
        
        Pair<Double> expResult1 = new Pair<>(-7.0, -1.0);
        Pair<Double> result1 = instance.obtenerExtremos(dimension1);
        
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of obtenerAristasColisionando method, of class BVTree.
     */
    @Test
    public void testObtenerAristasColisionando() {
        System.out.println("obtenerAristasColisionando");
	int i=0, j=0;

        List<AABB2D> listaCajasObjetoUno = new ArrayList<>();
	List<AABB2D> listaCajasObjetoDos = new ArrayList<>();
        List<Pair<Integer>> listaColisiones = new ArrayList<>();

	listaCajasObjetoUno.add(new AABB2D(new Edge2D(new Point2D(-2,-2), new Point2D(2,-2)), i++));
	listaCajasObjetoDos.add(new AABB2D(new Edge2D(new Point2D(-3,-1), new Point2D(1,-1)), j++));
	listaCajasObjetoUno.add(new AABB2D(new Edge2D(new Point2D(2,-2), new Point2D(2,2)), i++));
	listaCajasObjetoDos.add(new AABB2D(new Edge2D(new Point2D(1,-1), new Point2D(1,3)), j++));
	listaCajasObjetoUno.add(new AABB2D(new Edge2D(new Point2D(2,2), new Point2D(-2,2)), i++));
	listaCajasObjetoDos.add(new AABB2D(new Edge2D(new Point2D(1,3), new Point2D(-3,3)), j++));
	listaCajasObjetoUno.add(new AABB2D(new Edge2D(new Point2D(-2,2), new Point2D(-2,-2)), i++));
	listaCajasObjetoDos.add(new AABB2D(new Edge2D(new Point2D(-3,3), new Point2D(-3,-1)), j++));

	BVTree objetoUno = new BVTree(listaCajasObjetoUno);
	BVTree objetoDos = new BVTree(listaCajasObjetoDos);

	BVTree.obtenerAristasColisionando(listaColisiones, objetoUno, objetoDos);

	assertTrue(!listaColisiones.isEmpty());
    }    

    /**
     * Test of getRoot method, of class BVTree.
     */
    @Test
    public void testGetRoot() {
        System.out.println("getRoot");
        BVTree instance = new BVTree();

        AABB2D caja00, caja01, caja02, caja03;        
        AABB2D root0;
        
        BVNode nodo00, nodo01, nodo02, nodo03;       
        BVNode nodoRoot0;
        List<BVNode> listaNodos = new ArrayList<>();
        
        //objeto0
        List<Edge2D> listaAristas0 = new ArrayList<>();
        listaAristas0.add(new Edge2D(new Point2D(0,-1), new Point2D(1,0)));
        listaAristas0.add(new Edge2D(new Point2D(1,0), new Point2D(0,1)));
        listaAristas0.add(new Edge2D(new Point2D(0,1), new Point2D(0,-1)));
        
        caja00 = new AABB2D(listaAristas0.get(0), 0);
        caja01 = new AABB2D(listaAristas0.get(1), 1);
        caja02 = new AABB2D(listaAristas0.get(2), 2);
        
        nodo00 = new BVNode(caja00);
        nodo01 = new BVNode(caja01);
        nodo02 = new BVNode(caja02);
        
        caja03 = caja02.unionBV(caja00);
        nodo03 = new BVNode(caja03);
        nodo03.setLeft(nodo02);
        nodo03.setRight(nodo00);
        
        root0 = caja03.unionBV(caja01);
        nodoRoot0 = new BVNode(root0);
        nodoRoot0.setLeft(nodo01);
        nodoRoot0.setRight(nodo03);
        
        listaNodos.add(nodo00);
        listaNodos.add(nodo01);
        listaNodos.add(nodo02);
        
        instance.crearArbol(listaNodos);
        
        BVNode root = instance.getRoot();
        assertEquals(root, nodoRoot0);
    }

    /**
     * Test of crearArbol method, of class BVTree.
     */
    @Test
    public void testCrearArbol() {
        System.out.println("crearArbol");
        BVTree instance = new BVTree();

        AABB2D caja00, caja01, caja02, caja03;        
        AABB2D root0;
        
        BVNode nodo00, nodo01, nodo02, nodo03;       
        BVNode nodoRoot0;
        List<BVNode> listaNodos = new ArrayList<>();
        
        //objeto0
        List<Edge2D> listaAristas0 = new ArrayList<>();
        listaAristas0.add(new Edge2D(new Point2D(0,-1), new Point2D(1,0)));
        listaAristas0.add(new Edge2D(new Point2D(1,0), new Point2D(0,1)));
        listaAristas0.add(new Edge2D(new Point2D(0,1), new Point2D(0,-1)));
        
        caja00 = new AABB2D(listaAristas0.get(0), 0);
        caja01 = new AABB2D(listaAristas0.get(1), 1);
        caja02 = new AABB2D(listaAristas0.get(2), 2);
        
        nodo00 = new BVNode(caja00);
        nodo01 = new BVNode(caja01);
        nodo02 = new BVNode(caja02);
        
        caja03 = caja02.unionBV(caja00);
        nodo03 = new BVNode(caja03);
        nodo03.setLeft(nodo02);
        nodo03.setRight(nodo00);
        
        root0 = caja03.unionBV(caja01);
        nodoRoot0 = new BVNode(root0);
        nodoRoot0.setLeft(nodo01);
        nodoRoot0.setRight(nodo03);
        
        listaNodos.add(nodo00);
        listaNodos.add(nodo01);
        listaNodos.add(nodo02);
        
        instance.crearArbol(listaNodos);
        assertEquals(instance.getRoot(), nodoRoot0);
        assertEquals(instance.getRoot().getLeft(), nodo01);
        assertEquals(instance.getRoot().getRight(), nodo03);
        assertEquals(instance.getRoot().getRight().getLeft(), nodo02);
        assertEquals(instance.getRoot().getRight().getRight(), nodo00);
    }
}