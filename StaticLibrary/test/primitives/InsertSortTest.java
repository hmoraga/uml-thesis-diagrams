/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

import comparators.AABB2DComparator;
import java.util.Comparator;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class InsertSortTest {
    AABB2D[] listaCajas;
    AABB2D c1, c2, c3, c4;
    
    public InsertSortTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        c1 = new AABB2D(new Point2D(3, 5), 2, 2);
        c2 = new AABB2D(new Point2D(4, 6), 2, 2);
        c3 = new AABB2D(new Point2D(4, 2), 4, 2);
        c4 = new AABB2D(new Point2D(4.5, 2), 1, 4);
        
        listaCajas = new AABB2D[]{c1, c2, c3, c4};
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of sort method, of class InsertSort.
     */
    @Test
    public void testSort() {
        System.out.println("sort");
        Comparable[] a = listaCajas.clone(), b = listaCajas.clone();
        
        Comparator compX = new AABB2DComparator();
        Comparator compY = new AABB2DComparator(1);
        int sortType = 0;   // ordenamiento estable
        Comparable[] aComp = new AABB2D[]{c1, c3, c2, c4}, bComp = new AABB2D[]{c4, c3, c1, c2};
        InsertSort.sort(a, compX, sortType);
        assertArrayEquals(a, aComp);
        
        InsertSort.sort(b, compY, sortType);
        assertArrayEquals(b, bComp);
    }
    
}
