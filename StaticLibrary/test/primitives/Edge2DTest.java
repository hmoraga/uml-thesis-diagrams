/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Edge2DTest {
    
    public Edge2DTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getPoints method, of class Edge2D.
     */
    @Test
    public void testGetPoints() {
        System.out.println("getPoints");
        Edge2D instance = new Edge2D(new Point2D(-1.5,2.3), new Point2D(0.87, 1.03));
        Pair<Point2D> expResult = new Pair<>(new Point2D(-1.5,2.3), new Point2D(0.87, 1.03));
        assertEquals(expResult, instance.getPoints());
    }

    /**
     * Test of signed2DTriArea method, of class Edge2D.
     */
    @Test
    public void testSigned2DTriArea_Point2D() {
        System.out.println("signed2DTriArea");
        Point2D c1 = new Point2D(4, 10);
        Point2D c2 = new Point2D(5, 6);
        Point2D c3 = new Point2D(7, 8.2f);
        
        Edge2D instance = new Edge2D(new Point2D(3,5), new Point2D(8,9));
        double expResult1 = 21.0, expResult2 = -3, expResult3 = 0.0;
        double result1 = instance.signed2DTriArea(c1);
        double result2 = instance.signed2DTriArea(c2);
        double result3 = instance.signed2DTriArea(c3);
        
        assertEquals(expResult1, result1, 0.0);
        assertEquals(expResult2, result2, 0.0);
        assertEquals(expResult3, result3, 0.00001);
    }

    /**
     * Test of signed2DTriArea method, of class Edge2D.
     */
    @Test
    public void testSigned2DTriArea_Point() {
        System.out.println("signed2DTriArea");
        Point c1 = new Point2D(4, 10);
        Point c2 = new Point2D(5, 6);
        Point c3 = new Point2D(7, 8.2f);
        
        Edge2D instance = new Edge2D(new Point2D(3,5), new Point2D(8,9));
        double expResult1 = 21.0, expResult2 = -3, expResult3 = 0.0;
        double result1 = instance.signed2DTriArea(c1);
        double result2 = instance.signed2DTriArea(c2);
        double result3 = instance.signed2DTriArea(c3);
        
        assertEquals(expResult1, result1, 0.0);
        assertEquals(expResult2, result2, 0.0);
        assertEquals(expResult3, result3, 0.00001);
    }

    /**
     * Test of intersects method, of class Edge2D.
     */
    @Test
    public void testIntersects_Edge2D() {
        System.out.println("intersects");
        Edge2D arista = new Edge2D(new Point2D(-5, 2), new Point2D(-1, 2));
        Edge2D instance1 = new Edge2D(new Point2D(-1, 2), new Point2D(-1, -2));
        Edge2D instance2 = new Edge2D(new Point2D(-3, 3), new Point2D(-2, 1));
        Edge2D instance3 = new Edge2D(new Point2D(-2, 5), new Point2D(0, 5));
        boolean expResult1 = true, expResult2 = true, expResult3 = false;
        boolean result1 = instance1.intersects(arista), result2 = instance2.intersects(arista), result3 = instance3.intersects(arista);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
    }

    /**
     * Test of intersects method, of class Edge2D.
     */
    @Test
    public void testIntersects_Edge() {
        System.out.println("intersects");
        Edge arista = new Edge2D(new Point2D(-1, 2), new Point2D(-1, -2));
        Edge2D instance1 = new Edge2D(new Point2D(-2, 1), new Point2D(0, 1));
        Edge2D instance2 = new Edge2D(new Point2D(-3, 3), new Point2D(-2, 5));
        boolean expResult1 = true, expResult2 = false;
        boolean result1 = instance1.intersects(arista);
        boolean result2 = instance2.intersects(arista);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);        
    }

    /**
     * Test of getDistance method, of class Edge2D.
     */
    @Test
    public void testGetDistance() {
        System.out.println("getDistance");
        Edge2D instance = new Edge2D(new Point2D(1, 1), new Point2D(-1, -1));
        double expResult = 2*Math.sqrt(2);
        double result = instance.getDistance();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of toString method, of class Edge2D.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Edge2D instance = new Edge2D(new Point2D(-1,0), new Point2D());
        String expResult = "Edge2D{" + "a=(-1.0,0.0), b=(0.0,0.0)}";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Edge2D.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Edge2D(new Point2D(-1,1), new Point2D(1,-1));
        Edge2D instance1 = new Edge2D(new Point2D(-1,1), new Point2D(1,-1));
        Edge2D instance2 = new Edge2D(new Point2D(1,1), new Point2D(-1,-1));
        boolean expResult1 = true, expResult2 = false;
        boolean result1 = instance1.equals(obj), result2 = instance2.equals(obj);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of hashCode method, of class Edge2D.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Edge2D instance = new Edge2D(new Point2D(-1,-1), new Point2D(1,1));
        int expResult = 0;
        int result = instance.hashCode();
        assertTrue(expResult != result);
    }    

    @Test
    public void testAdd() {
        System.out.println("add");
        Point2D p = new Point2D(3, 2);
        Edge2D instance = new Edge2D(new Point2D(-1,0), new Point2D(1,0));
        instance.add(p);
        Pair<Point2D> listaPuntos = new Pair<>(new Point2D(2, 2), new Point2D(4, 2));
        assertEquals(instance.getPoints(), listaPuntos);
    }

    /**
     * Test of clone method, of class Edge2D.
     */
    @Test
    public void testClone() throws Exception {
        System.out.println("clone");
        Edge2D instance = new Edge2D(new Point2D(-1,0), new Point2D(1,0));
        Object expResult = new Edge2D(new Point2D(-1,0), new Point2D(1,0));
        Object result = instance.clone();
        assertEquals(expResult, result);
    }
}
