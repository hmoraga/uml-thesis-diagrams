/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Primitiva2DTest {
    
    public Primitiva2DTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of toString method, of class Primitiva.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Primitiva instance = new Primitiva2D(new Edge2D(new Point2D(1, 3), new Point2D(3, 1)), 2);
        String expResult = "Primitiva{objeto=Edge2D{a=(1.0,3.0), b=(3.0,1.0)}, indice=2}";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Primitiva.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Primitiva instance = new Primitiva2D(new Edge2D(new Point2D(), new Point2D(0.1f, 0.1f)), 0);
        int expResult = 0;
        int result = instance.hashCode();
        assertTrue(expResult != result);
    }

    /**
     * Test of equals method, of class Primitiva.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Primitiva2D(new Edge2D(new Point2D(), new Point2D(0.1, 0.1)), 0);
        Primitiva instance1 = new Primitiva2D(new Edge2D(new Point2D(), new Point2D(0.1, 0.1)), 0);
        Primitiva instance2 = new Primitiva2D(new Edge2D(new Point2D(), new Point2D(0.1, 0.1)), 1);
        Primitiva instance3 = new Primitiva2D(new Edge2D(new Point2D(0,0), new Point2D(0.1, 0.2)), 0);
        boolean expResult1 = true, expResult2 = false, expResult3 = false;
        boolean result1 = instance1.equals(obj);
        boolean result2 = instance2.equals(obj);
        boolean result3 = instance3.equals(obj);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
    }

    /**
     * Test of getEdge method, of class Primitiva2D.
     */
    @Test
    public void testGetEdge() {
        System.out.println("getEdge");
        Primitiva2D instance = new Primitiva2D(new Edge2D(new Point2D(), new Point2D(0.1, 0.1)), 0);
        Edge2D expResult = new Edge2D(new Point2D(), new Point2D(0.1, 0.1));
        Edge2D result = instance.getEdge();
        assertEquals(expResult, result);
    }

    /**
     * Test of getIndice method, of class Primitiva2D.
     */
    @Test
    public void testGetIndice() {
        System.out.println("getIndice");
        Primitiva2D instance = new Primitiva2D(new Edge2D(new Point2D(), new Point2D(0.1, 0.1)), 1);
        int expResult = 1;
        int result = instance.getIndice();
        assertEquals(expResult, result);
    }

    /**
     * Test of clone method, of class Primitiva2D.
     * @throws java.lang.Exception
     */
    @Test
    public void testClone() throws Exception {
        System.out.println("clone");
        Primitiva2D instance = new Primitiva2D(new Edge2D(new Point2D(), new Point2D(0.1, 0.1)), 0);
        Object expResult = new Primitiva2D(new Edge2D(new Point2D(), new Point2D(0.1, 0.1)), 0);
        Object result = instance.clone();
        assertEquals(expResult, result);
    }
}
