/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparators;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import primitives.AABB2D;
import primitives.Point2D;
import sap.BoxWrap;

/**
 *
 * @author hmoraga
 */
public class BoxWrapComparatorTest {
    private BoxWrapComparator cmp0, cmp1, cmp2;
    private BoxWrap boxWp1, boxWp2, boxWp3;
            
    public BoxWrapComparatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        boxWp1 = new BoxWrap(new AABB2D(new Point2D(-1.5, 0), 1.0, 4.0), 0);
        boxWp2 = new BoxWrap(new AABB2D(new Point2D(3.5, 1.0), 3.0, 2.0), 1);
        boxWp3 = new BoxWrap(new AABB2D(new Point2D(2.0, -2.0), 2.0, 2.0), 2);
        cmp0 = new BoxWrapComparator();
        cmp1 = new BoxWrapComparator(0);
        cmp2 = new BoxWrapComparator(1);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class BoxWrapComparator.
     */
    @Test
    public void testCompare() {
        System.out.println("compare");
        int expResult00 = -1, expResult01 = -1, expResult02 = 1;
        int expResult10 = -1, expResult11 = -1, expResult12 = 1;
        int expResult20 = -1, expResult21 = 1, expResult22 = 1;
        
        int result00 = cmp0.compare(boxWp1, boxWp2);
        int result01 = cmp0.compare(boxWp1, boxWp3);
        int result02 = cmp0.compare(boxWp2, boxWp3);
        
        int result10 = cmp1.compare(boxWp1, boxWp2);
        int result11 = cmp1.compare(boxWp1, boxWp3);
        int result12 = cmp1.compare(boxWp2, boxWp3);
        
        int result20 = cmp2.compare(boxWp1, boxWp2);
        int result21 = cmp2.compare(boxWp1, boxWp3);
        int result22 = cmp2.compare(boxWp2, boxWp3);
        
        assertEquals(expResult00, result00);
        assertEquals(expResult01, result01);
        assertEquals(expResult02, result02);

        assertEquals(expResult10, result10);
        assertEquals(expResult11, result11);
        assertEquals(expResult12, result12);

        assertEquals(expResult20, result20);
        assertEquals(expResult21, result21);
        assertEquals(expResult22, result22);        
    }
}
