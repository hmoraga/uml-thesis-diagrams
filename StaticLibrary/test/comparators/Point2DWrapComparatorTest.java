/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparators;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import primitives.Pair;
import primitives.Point2D;
import sap.Point2DWrap;

/**
 *
 * @author hmoraga
 */
public class Point2DWrapComparatorTest {
    private Point2D p0, p1, p2, p3;
    private Point2DWrap pw0, pw1, pw2, pw3;
    private Pair<Double> v0, v1, v2, v3;
    
    public Point2DWrapComparatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        p0 = new Point2D(3, 1);
        p1 = new Point2D(1, 3);
        p2 = new Point2D(5, -1);
        p3 = new Point2D(3, 1);
        
        v0 = new Pair<>(1.0,1.0);
        v1 = new Pair<>(1.0,1.0);
        v2 = new Pair<>(-1.0,-1.0);
        v3 = new Pair<>(-1.0,-1.0);
                      
        pw0 = new Point2DWrap(p0, v0, 0, false);
        pw1 = new Point2DWrap(p1, v1, 1, false);
        pw2 = new Point2DWrap(p2, v2, 2, false);
        pw3 = new Point2DWrap(p3, v3, 3, true);        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class Point2DWrapComparator.
     */
    @Test
    public void testCompare() {
        System.out.println("compare");
        Point2DWrapComparator instance0 = new Point2DWrapComparator(0, 0); // tiempo=0, dim=0
        
        int expResult01time0Dim0 = 1;
        int expResult02time0Dim0 = -1;
        int expResult03time0Dim0 = 1;
        int expResult12time0Dim0 = -1;
        int expResult13time0Dim0 = -1;
        int expResult23time0Dim0 = 1;

        int result01time0Dim0 = instance0.compare(pw0, pw1);
        int result02time0Dim0 = instance0.compare(pw0, pw2);
        int result03time0Dim0 = instance0.compare(pw0, pw3);
        int result12time0Dim0 = instance0.compare(pw1, pw2);
        int result13time0Dim0 = instance0.compare(pw1, pw3);
        int result23time0Dim0 = instance0.compare(pw2, pw3);

        assertEquals(expResult01time0Dim0, result01time0Dim0);
        assertEquals(expResult02time0Dim0, result02time0Dim0);
        assertEquals(expResult03time0Dim0, result03time0Dim0);
        assertEquals(expResult12time0Dim0, result12time0Dim0);
        assertEquals(expResult13time0Dim0, result13time0Dim0);
        assertEquals(expResult23time0Dim0, result23time0Dim0);

        Point2DWrapComparator instance1 = new Point2DWrapComparator(0, 1); // tiempo=0, dim=1

        int expResult01time0Dim1 = -1;
        int expResult02time0Dim1 = 1;
        int expResult03time0Dim1 = 1; // usando desempate delta
        int expResult12time0Dim1 = 1;
        int expResult13time0Dim1 = 1;
        int expResult23time0Dim1 = -1;
        
        int result01time0Dim1 = instance1.compare(pw0, pw1);
        int result02time0Dim1 = instance1.compare(pw0, pw2);
        int result03time0Dim1 = instance1.compare(pw0, pw3);
        int result12time0Dim1 = instance1.compare(pw1, pw2);
        int result13time0Dim1 = instance1.compare(pw1, pw3);
        int result23time0Dim1 = instance1.compare(pw2, pw3);

        assertEquals(expResult01time0Dim1, result01time0Dim1);
        assertEquals(expResult02time0Dim1, result02time0Dim1);
        assertEquals(expResult03time0Dim1, result03time0Dim1);
        assertEquals(expResult12time0Dim1, result12time0Dim1);
        assertEquals(expResult13time0Dim1, result13time0Dim1);
        assertEquals(expResult23time0Dim1, result23time0Dim1);
                
        Point2DWrapComparator instance2 = new Point2DWrapComparator(1.0, 0); // tiempo=1.0, dim=0
        
        int expResult01time1Dim0 = 1;
        int expResult02time1Dim0 = 1;   // desempate delta
        int expResult03time1Dim0 = 1;
        int expResult12time1Dim0 = -1;
        int expResult13time1Dim0 = 1;
        int expResult23time1Dim0 = 1;

        int result01time1Dim0 = instance2.compare(pw0, pw1);
        int result02time1Dim0 = instance2.compare(pw0, pw2);
        int result03time1Dim0 = instance2.compare(pw0, pw3);
        int result12time1Dim0 = instance2.compare(pw1, pw2);
        int result13time1Dim0 = instance2.compare(pw1, pw3);
        int result23time1Dim0 = instance2.compare(pw2, pw3);

        assertEquals(expResult01time1Dim0, result01time1Dim0);
        assertEquals(expResult02time1Dim0, result02time1Dim0);
        assertEquals(expResult03time1Dim0, result03time1Dim0);
        assertEquals(expResult12time1Dim0, result12time1Dim0);
        assertEquals(expResult13time1Dim0, result13time1Dim0);
        assertEquals(expResult23time1Dim0, result23time1Dim0);        
        
        Point2DWrapComparator instance3 = new Point2DWrapComparator(1.0, 1); // tiempo=1.0, dim=1
 
        int expResult01time1Dim1 = -1;
        int expResult02time1Dim1 = 1; // desempate delta
        int expResult03time1Dim1 = 1;
        int expResult12time1Dim1 = 1;
        int expResult13time1Dim1 = 1; // desempate delta
        int expResult23time1Dim1 = -1;
        
        int result01time1Dim1 = instance3.compare(pw0, pw1);
        int result02time1Dim1 = instance3.compare(pw0, pw2);
        int result03time1Dim1 = instance3.compare(pw0, pw3);
        int result12time1Dim1 = instance3.compare(pw1, pw2);
        int result13time1Dim1 = instance3.compare(pw1, pw3);
        int result23time1Dim1 = instance3.compare(pw2, pw3);

        assertEquals(expResult01time1Dim1, result01time1Dim1);
        assertEquals(expResult02time1Dim1, result02time1Dim1);
        assertEquals(expResult03time1Dim1, result03time1Dim1);
        assertEquals(expResult12time1Dim1, result12time1Dim1);
        assertEquals(expResult13time1Dim1, result13time1Dim1);
        assertEquals(expResult23time1Dim1, result23time1Dim1);
        
        Point2DWrapComparator instance4 = new Point2DWrapComparator(2.0, 0); // tiempo=2.0, dim=0
        
        int expResult01time2Dim0 = 1;
        int expResult02time2Dim0 = 1;
        int expResult03time2Dim0 = 1;
        int expResult12time2Dim0 = 1;
        int expResult13time2Dim0 = 1;
        int expResult23time2Dim0 = 1;

        int result01time2Dim0 = instance4.compare(pw0, pw1);
        int result02time2Dim0 = instance4.compare(pw0, pw2);
        int result03time2Dim0 = instance4.compare(pw0, pw3);
        int result12time2Dim0 = instance4.compare(pw1, pw2);
        int result13time2Dim0 = instance4.compare(pw1, pw3);
        int result23time2Dim0 = instance4.compare(pw2, pw3);

        assertEquals(expResult01time2Dim0, result01time2Dim0);
        assertEquals(expResult02time2Dim0, result02time2Dim0);
        assertEquals(expResult03time2Dim0, result03time2Dim0);
        assertEquals(expResult12time2Dim0, result12time2Dim0); // desempate delta
        assertEquals(expResult13time2Dim0, result13time2Dim0);
        assertEquals(expResult23time2Dim0, result23time2Dim0);        
        
        Point2DWrapComparator instance5 = new Point2DWrapComparator(2.0, 1); // tiempo=2.0, dim=1

        int expResult01time2Dim1 = -1;
        int expResult02time2Dim1 = 1;
        int expResult03time2Dim1 = 1;
        int expResult12time2Dim1 = 1;  // desempate delta
        int expResult13time2Dim1 = 1;
        int expResult23time2Dim1 = -1;

        int result01time2Dim1 = instance5.compare(pw0, pw1);
        int result02time2Dim1 = instance5.compare(pw0, pw2);
        int result03time2Dim1 = instance5.compare(pw0, pw3);
        int result12time2Dim1 = instance5.compare(pw1, pw2);
        int result13time2Dim1 = instance5.compare(pw1, pw3);
        int result23time2Dim1 = instance5.compare(pw2, pw3);

        assertEquals(expResult01time2Dim1, result01time2Dim1);
        assertEquals(expResult02time2Dim1, result02time2Dim1);
        assertEquals(expResult03time2Dim1, result03time2Dim1);
        assertEquals(expResult12time2Dim1, result12time2Dim1);
        assertEquals(expResult13time2Dim1, result13time2Dim1);
        assertEquals(expResult23time2Dim1, result23time2Dim1);        
    }
}
