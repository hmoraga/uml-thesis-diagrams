/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparators;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import primitives.Point2D;
import sap.Point2DStaticWrap;

/**
 *
 * @author hmoraga
 */
public class Point2DStaticWrapComparatorTest {
    
    public Point2DStaticWrapComparatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class Point2DStaticWrapComparator.
     */
    @Test
    public void testCompare() {
        System.out.println("compare");
        Point2DStaticWrap o0 = new Point2DStaticWrap(new Point2D(3,1), 0, false);
        Point2DStaticWrap o1 = new Point2DStaticWrap(new Point2D(1,3), 1, false);
        Point2DStaticWrap o2 = new Point2DStaticWrap(new Point2D(5,-1), 2, false);
        Point2DStaticWrap o3 = new Point2DStaticWrap(new Point2D(3,1), 0, true);
                
        Point2DStaticWrapComparator instance0 = new Point2DStaticWrapComparator(0);
        Point2DStaticWrapComparator instance1 = new Point2DStaticWrapComparator(1);
        
        int expResult01 = 1;
        int expResult02 = -1;
        int expResult03 = -1;
        int expResult12 = -1;
        int expResult13 = -1;
        int expResult23 = 1;
        
        int result01 = instance0.compare(o0, o1);
        int result02 = instance0.compare(o0, o2);
        int result03 = instance0.compare(o0, o3); // primero son los inicios
        int result12 = instance0.compare(o1, o2);
        int result13 = instance0.compare(o1, o3);
        int result23 = instance0.compare(o2, o3);
        
        assertEquals(expResult01, result01);
        assertEquals(expResult02, result02);
        assertEquals(expResult03, result03);
        assertEquals(expResult12, result12);
        assertEquals(expResult13, result13);
        assertEquals(expResult23, result23);

        // comparaciones en Y
        expResult01 = -1;
        expResult02 = 1;
        expResult03 = -1; // el que inicia es menor al que termina
        expResult12 = 1;
        expResult13 = 1;
        expResult23 = -1;
        
        result01 = instance1.compare(o0, o1);
        result02 = instance1.compare(o0, o2);
        result03 = instance1.compare(o0, o3);
        result12 = instance1.compare(o1, o2);
        result13 = instance1.compare(o1, o3);
        result23 = instance1.compare(o2, o3);
        
        assertEquals(expResult01, result01);
        assertEquals(expResult02, result02);
        assertEquals(expResult03, result03);
        assertEquals(expResult12, result12);
        assertEquals(expResult13, result13);
        assertEquals(expResult23, result23);
    }
}
