/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparators;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author hmoraga
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({AABB2DComparatorTest.class, Point2DStaticWrapComparatorTest.class, Point2DWrapComparatorTest.class, AABB2DComparatorAreaTest.class, ElementComparatorTest.class, AABB2DComparatorYAxisByCenterTest.class, Point2DComparatorXaxisTest.class, BoxWrapComparatorTest.class, Point2DComparatorYaxisTest.class, BVNodeComparatorTest.class, AABB2DWrapperComparatorTest.class, AABB2DComparatorXAxisByCenterTest.class})
public class ComparatorsSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
