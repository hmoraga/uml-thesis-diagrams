/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparators;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import primitives.AABB2D;
import primitives.Point2D;
import wrappers.AABB2DWrapper;

/**
 *
 * @author hmoraga
 */
public class AABB2DWrapperComparatorTest {
    
    public AABB2DWrapperComparatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class AABB2DWrapperComparator.
     */
    @Test
    public void testCompare() {
        System.out.println("compare");
        List<Point2D> listaPuntos0, listaPuntos1, listaPuntos2;
        listaPuntos0 = new ArrayList<>();
        listaPuntos0.add(new Point2D(1,1));
        listaPuntos0.add(new Point2D(4,1));
        listaPuntos0.add(new Point2D(4,4));
        listaPuntos0.add(new Point2D(1,4));
        
        listaPuntos1 = new ArrayList<>();
        listaPuntos1.add(new Point2D(-2,5));
        listaPuntos1.add(new Point2D(2,5));
        listaPuntos1.add(new Point2D(2,8));
        listaPuntos1.add(new Point2D(-2,8));
        
        listaPuntos2 = new ArrayList<>();
        listaPuntos2.add(new Point2D(1,1));
        listaPuntos2.add(new Point2D(4,1));
        listaPuntos2.add(new Point2D(4,4));
        listaPuntos2.add(new Point2D(1,4));

        AABB2D box1 = new AABB2D(listaPuntos0);
        AABB2D box2 = new AABB2D(listaPuntos1);
        AABB2D box3 = new AABB2D(listaPuntos2);
        
        AABB2DWrapper o0 = new AABB2DWrapper(box1, 0);
        AABB2DWrapper o1 = new AABB2DWrapper(box2, 1);
        AABB2DWrapper o2 = new AABB2DWrapper(box3, 2);
        
        // identico a comparar con dimension 0
        AABB2DWrapperComparator instanceDefault = new AABB2DWrapperComparator(); 
        AABB2DWrapperComparator instanceX = new AABB2DWrapperComparator(0);
        AABB2DWrapperComparator instanceY = new AABB2DWrapperComparator(1);
                
        int expResult01 = 1, expResult02 = 0, expResult12 = -1;  
        int result01 = instanceDefault.compare(o0, o1);
        int result02 = instanceDefault.compare(o0, o2);
        int result12 = instanceDefault.compare(o1, o2);
        
        assertEquals(expResult01, result01);
        assertEquals(expResult02, result02);
        assertEquals(expResult12, result12);
        
        expResult01 = 1;
        expResult02 = 0;
        expResult12 = -1;
        result01 = instanceX.compare(o0, o1);
        result02 = instanceX.compare(o0, o2);
        result12 = instanceX.compare(o1, o2);
        
        assertEquals(expResult01, result01);
        assertEquals(expResult02, result02);
        assertEquals(expResult12, result12);

        expResult01 = -1;
        expResult02 = 0;
        expResult12 = 1;  
        result01 = instanceY.compare(o0, o1);
        result02 = instanceY.compare(o0, o2);
        result12 = instanceY.compare(o1, o2);
        
        assertEquals(expResult01, result01);
        assertEquals(expResult02, result02);
        assertEquals(expResult12, result12);        
    }
    
}
