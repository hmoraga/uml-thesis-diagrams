/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import primitives.AABB2D;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class AABB2DComparatorAreaTest {
    
    public AABB2DComparatorAreaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class AABB2DComparatorArea.
     */
    @Test
    public void testCompare() {
        System.out.println("sortByArea");
        AABB2DComparatorArea result = new AABB2DComparatorArea();
        //primer escenario areas diferentes
        AABB2D caja1 = new AABB2D(new Point2D(1, 1), 2, 2);
        AABB2D caja2 = new AABB2D(new Point2D(1, 1), 4, 4);
        //segundo escenario areas iguales dimensiones diferentes
        AABB2D caja3 = new AABB2D(new Point2D(0.5, 0.5), 3, 4);
        AABB2D caja4 = new AABB2D(new Point2D(1, 1), 4, 3);
        //tercer escenario areas iguales dimensiones iguales posiciones diferentes
        AABB2D caja5 = new AABB2D(new Point2D(3, 2), 2, 2);
        AABB2D caja6 = new AABB2D(new Point2D(-3, 5), 2, 2);
        
        assertEquals(result.compare(caja1, caja2),-1);
        assertEquals(result.compare(caja3, caja4),0);
        assertEquals(result.compare(caja5, caja6),0);

        List<AABB2D> listaCajas = new ArrayList<>();

        AABB2D cajaUno = new AABB2D(new Point2D(-3,4),4.0,2.0);
        AABB2D cajaDos = new AABB2D(new Point2D(3.5,3),3.0,4.0);
        AABB2D cajaTres = new AABB2D(new Point2D(1,1),4.0,4.0);
        AABB2D cajaCuatro = new AABB2D(new Point2D(4.0,-4.0), 2.0,2.0);

        listaCajas.add(cajaUno);
        listaCajas.add(cajaDos);
        listaCajas.add(cajaTres);
        listaCajas.add(cajaCuatro);
        
        Collections.sort(listaCajas, result);
        assertEquals(cajaCuatro, listaCajas.get(0));
        assertEquals(cajaUno, listaCajas.get(1));
        assertEquals(cajaDos, listaCajas.get(2));
        assertEquals(cajaTres, listaCajas.get(3));
    }
}
