/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparators;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class Point2DComparatorYaxisTest {
    private Point2D p0, p1, p2;
    
    public Point2DComparatorYaxisTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class Point2DComparatorYaxis.
     */
    @Test
    public void testCompare() {
        System.out.println("compare YAxis");
        p0 = new Point2D(2,3);
        p1 = new Point2D(2.1,3.1);
        p2 = new Point2D(2,3);

        Point2DComparatorYaxis instance = new Point2DComparatorYaxis();
        
        assertEquals(instance.compare(p0, p1), -1);
        assertEquals(instance.compare(p1, p0), 1);
        assertEquals(instance.compare(p0, p2), 0);
    }
    /**
     * Comparator eje Y
     */
    @Test
    public void testComparatorY(){
        System.out.println("Comparator Y axis");
        Point2D p = new Point2D(0.5, -1.0);
        Point2D q = new Point2D(0.5, 1.0);
        Point2D r = new Point2D(0.5, -1.0);

        Point2DComparatorYaxis instance = new Point2DComparatorYaxis();

        assertTrue(instance.compare(p, q)== -1);
        assertTrue(instance.compare(q, p)== 1);
        assertTrue(instance.compare(p, r) == 0);
    }
    
}
