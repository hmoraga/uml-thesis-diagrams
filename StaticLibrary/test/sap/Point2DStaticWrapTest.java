/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sap;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class Point2DStaticWrapTest {
    
    public Point2DStaticWrapTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getPunto method, of class Point2DStaticWrap.
     */
    @Test
    public void testGetPunto() {
        System.out.println("getPunto");
        Point2DStaticWrap instance = new Point2DStaticWrap(new Point2D(), 0, true);
        Point2D expResult = new Point2D();
        Point2D result = instance.getPunto();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPunto method, of class Point2DStaticWrap.
     */
    @Test
    public void testSetPunto() {
        System.out.println("setPunto");
        Point2D punto = new Point2D(2, -1);
        Point2DStaticWrap instance = new Point2DStaticWrap(new Point2D(), 1, false);
        assertNotEquals(punto, instance.getPunto());
        instance.setPunto(punto);
        assertEquals(punto, instance.getPunto());
    }

    /**
     * Test of getId method, of class Point2DStaticWrap.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Point2DStaticWrap instance = new Point2DStaticWrap(new Point2D(), 1, false);
        int expResult = 1;
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Point2DStaticWrap.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        int id = 3;
        Point2DStaticWrap instance = new Point2DStaticWrap(new Point2D(), 1, false);
        assertEquals(1,instance.getId());
        instance.setId(id);
        assertEquals(id,instance.getId());
    }

    /**
     * Test of isFin method, of class Point2DStaticWrap.
     */
    @Test
    public void testIsFin() {
        System.out.println("isFin");
        Point2DStaticWrap instance0 = new Point2DStaticWrap(new Point2D(), 1, false);
        Point2DStaticWrap instance1 = new Point2DStaticWrap(new Point2D(3,4), 2, true);
        boolean expResult0 = false, expResult1 = true;
        boolean result0 = instance0.isFin();
        boolean result1 = instance1.isFin();
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of setFin method, of class Point2DStaticWrap.
     */
    @Test
    public void testSetFin() {
        System.out.println("setFin");
        boolean fin = true;
        Point2DStaticWrap instance = new Point2DStaticWrap(new Point2D(), 1, false);
        assertFalse(instance.isFin());
        instance.setFin(fin);
        assertTrue(instance.isFin());
    }
    
}
