/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sap;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import primitives.AABB2D;
import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class DirectSweepAndPrune2DTest {
    private List<List<Point2D>> listaObjetos;
    List<Point2D> lista0, lista1, lista2, lista3, lista4;
    List<AABB2D> listaCajas;
    private List<Point2DStaticWrap> listaX, listaY;
    private DirectSweepAndPrune2D sp2D;
            
    public DirectSweepAndPrune2DTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        lista0 = new ArrayList<>(3);
        lista0.add(new Point2D(-1, -1));
        lista0.add(new Point2D(1, 1));
        lista0.add(new Point2D(-1, 1));

        lista1 = new ArrayList<>(3);
        lista1.add(new Point2D(0, 0));
        lista1.add(new Point2D(0, 2));
        lista1.add(new Point2D(-2, 2));

        lista2 = new ArrayList<>(3);
        lista2.add(new Point2D(2, -2));
        lista2.add(new Point2D(2, 0));
        lista2.add(new Point2D(0, 0));

        lista3 = new ArrayList<>(3);
        lista3.add(new Point2D(0.5, 0.5));
        lista3.add(new Point2D(1.5, 0.5));
        lista3.add(new Point2D(0.5, 1.5));

        lista4 = new ArrayList<>(3);
        lista4.add(new Point2D(-0.5, -1.5));
        lista4.add(new Point2D(-0.5, -0.5));
        lista4.add(new Point2D(-1.5, -0.5));
                
        listaX = new ArrayList<>(10);

        listaX.add(new Point2DStaticWrap(new Point2D(-2, 0), 1, false));
        listaX.add(new Point2DStaticWrap(new Point2D(-1.5, -1.5), 4, false));
        listaX.add(new Point2DStaticWrap(new Point2D(-1, -1), 0, false));
        listaX.add(new Point2DStaticWrap(new Point2D(-0.5, -0.5), 4, true));
        listaX.add(new Point2DStaticWrap(new Point2D(0, -2), 2, false));
        listaX.add(new Point2DStaticWrap(new Point2D(0, 2), 1, true));
        listaX.add(new Point2DStaticWrap(new Point2D(0.5, 0.5), 3, false));
        listaX.add(new Point2DStaticWrap(new Point2D(1, 1), 0, true));
        listaX.add(new Point2DStaticWrap(new Point2D(1.5, 1.5), 3, true));
        listaX.add(new Point2DStaticWrap(new Point2D(2, 0), 2, true));

        listaY = new ArrayList<>(10);
                
        listaY.add(new Point2DStaticWrap(new Point2D(0, -2), 2, false));
        listaY.add(new Point2DStaticWrap(new Point2D(-1.5, -1.5), 4, false));
        listaY.add(new Point2DStaticWrap(new Point2D(-1, -1), 0, false));
        listaY.add(new Point2DStaticWrap(new Point2D(-0.5, -0.5), 4, true));
        listaY.add(new Point2DStaticWrap(new Point2D(-2, 0), 1, false));
        listaY.add(new Point2DStaticWrap(new Point2D(2, 0), 2, true));
        listaY.add(new Point2DStaticWrap(new Point2D(0.5, 0.5), 3, false));
        listaY.add(new Point2DStaticWrap(new Point2D(1, 1), 0, true));
        listaY.add(new Point2DStaticWrap(new Point2D(1.5, 1.5), 3, true));
        listaY.add(new Point2DStaticWrap(new Point2D(0, 2), 1, true));
        
        listaObjetos = new ArrayList<>();
        
        listaObjetos.add(lista0);
        listaObjetos.add(lista1);
        listaObjetos.add(lista2);
        listaObjetos.add(lista3);
        listaObjetos.add(lista4);
        
        
        sp2D = new DirectSweepAndPrune2D();
        listaCajas = new ArrayList<>();
        
        for (List<Point2D> obj : listaObjetos) {
            listaCajas.add(new AABB2D(obj));
        }
        
        sp2D.inicializarEstructuras(listaCajas, 0);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListaPuntosCajasPorDimension method, of class SAP2Dv2.
     */
    @Test
    public void testGetListaPuntosCajasPorDimension() {
        System.out.println("getListaPuntosCajasPorDimension");
        DirectSweepAndPrune2D instance = sp2D;
        
        List<List<Point2DStaticWrap>> expResult = new ArrayList<>(2);
        expResult.add(listaX);
        expResult.add(listaY);
        
        List<List<Point2DStaticWrap>> result = instance.getListaPuntosCajasPorDimension();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPosicionesPorObjetoEnListaPorDimension method, of class SAP2Dv2.
     */
    @Test
    public void testGetPosicionesPorObjetoEnListaPorDimension() {
        System.out.println("getPosicionesPorObjetoEnListaPorDimension");
        DirectSweepAndPrune2D instance = sp2D;
                
        instance.inicializarEstructuras(listaCajas, 0);
        
        List<List<Pair<Integer>>> expResult = new ArrayList<>(2);
        List<Pair<Integer>> listaAux1 = new ArrayList<>();
        List<Pair<Integer>> listaAux2 = new ArrayList<>();
        listaAux1.add(new Pair<>(2,7));
        listaAux1.add(new Pair<>(0,5));
        listaAux1.add(new Pair<>(4,9));
        listaAux1.add(new Pair<>(6,8));
        listaAux1.add(new Pair<>(1,3));
        
        listaAux2.add(new Pair<>(2,7));
        listaAux2.add(new Pair<>(4,9));
        listaAux2.add(new Pair<>(0,5));
        listaAux2.add(new Pair<>(6,8));
        listaAux2.add(new Pair<>(1,3));
        expResult.add(listaAux1);
        expResult.add(listaAux2);
        
        List<List<Pair<Integer>>> result = instance.getPosicionesPorObjetoEnListaPorDimension();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPosicionesPorObjetoEnListaPorDimension method, of class SAP2Dv2.
     */
    @Test
    public void testSetPosicionesPorObjetoEnListaPorDimension() {
        System.out.println("setPosicionesPorObjetoEnListaPorDimension");
        List<List<Pair<Integer>>> posicionesPorObjetoEnListaPorDimension = new ArrayList<>();
        DirectSweepAndPrune2D instance = new DirectSweepAndPrune2D();
        instance.setPosicionesPorObjetoEnListaPorDimension(posicionesPorObjetoEnListaPorDimension);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaParesEfectivosColisionando method, of class SAP2Dv2.
     */
    @Test
    public void testGetListaParesEfectivosColisionando() {
        System.out.println("getListaParesEfectivosColisionando");
        DirectSweepAndPrune2D instance = sp2D;
        
        List<Pair<Integer>> expResult = new ArrayList<>();
        expResult.add(new Pair<>(0,1));
        expResult.add(new Pair<>(0,2));
        expResult.add(new Pair<>(0,3));
        expResult.add(new Pair<>(0,4));
        expResult.add(new Pair<>(1,2));
        
        List<Pair<Integer>> result = instance.getListaParesEfectivosColisionando();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaParesEfectivosColisionando method, of class SAP2Dv2.
     */
    @Test
    public void testSetListaParesEfectivosColisionando() {
        System.out.println("setListaParesEfectivosColisionando");
        List<Pair<Integer>> listaParesEfectivosColisionando = null;
        DirectSweepAndPrune2D instance = new DirectSweepAndPrune2D();
        instance.setListaParesEfectivosColisionando(listaParesEfectivosColisionando);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of inicializarEstructuras method, of class SAP2Dv2.
     */
    @Test
    public void testInicializarEstructuras() {
        System.out.println("inicializarEstructuras");
        List<AABB2D> listaCajas = null;
        int frameNumber = 0;
        DirectSweepAndPrune2D instance = new DirectSweepAndPrune2D();
        instance.inicializarEstructuras(listaCajas, frameNumber);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTextoEstadisticas method, of class SAP2Dv2.
     */
    /**
     * Test of obtenerListaDeParesColisionando method, of class SAP2Dv2.
     */
    @Test
    public void testObtenerListaDeParesColisionando() {
        System.out.println("obtenerListaDeParesColisionando");
        DirectSweepAndPrune2D instance = sp2D;
        List<Pair<Integer>> expResult = new ArrayList<>();
        expResult.add(new Pair<>(0,1));
        expResult.add(new Pair<>(0,2));
        expResult.add(new Pair<>(0,3));
        expResult.add(new Pair<>(0,4));
        expResult.add(new Pair<>(1,2));
        
        List<Pair<Integer>> result = instance.obtenerListaDeParesColisionando();
        assertEquals(expResult, result);
    }
}
