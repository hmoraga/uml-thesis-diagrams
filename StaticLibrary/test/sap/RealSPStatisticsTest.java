/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sap;

import java.nio.file.Path;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import primitives.Pair;

/**
 *
 * @author hmoraga
 */
public class RealSPStatisticsTest {
    
    public RealSPStatisticsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getAreaTotal method, of class RealSPStatistics.
     */
    @Test
    public void testGetAreaTotal() {
        System.out.println("getAreaTotal");
        RealSPStatistics instance = new RealSPStatistics(5);
        double expResult = 0.0;
        double result = instance.getAreaTotal();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getTiempoTotalSimulado method, of class RealSPStatistics.
     */
    @Test
    public void testGetTiempoTotalSimulado() {
        System.out.println("getTiempoTotalSimulado");
        RealSPStatistics instance = new RealSPStatistics(5);
        double expResult = 0.0;
        double result = instance.getTiempoTotalSimulado();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getAreaOcupadaInicial method, of class RealSPStatistics.
     */
    @Test
    public void testGetAreaOcupadaInicial() {
        System.out.println("getAreaOcupadaInicial");
        RealSPStatistics instance = new RealSPStatistics(5);
        double expResult = 0.0;
        double result = instance.getAreaOcupadaInicial();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setAreaTotal method, of class RealSPStatistics.
     */
    @Test
    public void testSetAreaTotal() {
        System.out.println("setAreaTotal");
        double areaTotal = 0.0;
        RealSPStatistics instance = new RealSPStatistics(5);
        instance.setAreaTotal(areaTotal);
    }

    /**
     * Test of setTiempoTotalSimulado method, of class RealSPStatistics.
     */
    @Test
    public void testSetTiempoTotalSimulado() {
        System.out.println("setTiempoTotalSimulado");
        Pair<Double> IntervaloTiempo = null;
        RealSPStatistics instance = new RealSPStatistics(5);
        instance.setTiempoTotalSimulado(IntervaloTiempo);
    }

    /**
     * Test of setAreaOcupadaInicial method, of class RealSPStatistics.
     */
    @Test
    public void testSetAreaOcupadaInicial() {
        System.out.println("setAreaOcupadaInicial");
        double areaOcupadaInicial = 0.0;
        RealSPStatistics instance = new RealSPStatistics(5);
        instance.setAreaOcupadaInicial(areaOcupadaInicial);
    }

//    /**
//     * Test of getText method, of class RealSPStatistics.
//     */
//    @Test
//    public void testGetText() {
//        System.out.println("getText");
//        RealSPStatistics instance = new RealSPStatistics(5);
//        String expResult = "";
//        String result = instance.getText();
//        assertEquals(expResult, result);
//    }
//
//    /**
//     * Test of escribirArchivo method, of class RealSPStatistics.
//     */
//    @Test
//    public void testEscribirArchivo() throws Exception {
//        System.out.println("escribirArchivo");
//        Path arch = null;
//        RealSPStatistics instance = new RealSPStatistics(5);
//        instance.escribirArchivo(arch);
//    }
//
//    /**
//     * Test of calcularEstadisticas method, of class RealSPStatistics.
//     */
//    @Test
//    public void testCalcularEstadisticas() {
//        System.out.println("calcularEstadisticas");
//        List<Pair<Integer>> colisiones = null;
//        RealSPStatistics instance = new RealSPStatistics(5);
//        instance.calcularEstadisticas(colisiones);
//    }
//
//    /**
//     * Test of addTexto method, of class RealSPStatistics.
//     */
//    @Test
//    public void testAddTexto() {
//        System.out.println("addTexto");
//        int frameNumber = 0;
//        int totalColisionesSimples = 0;
//        int totalColisionesMultiples = 0;
//        RealSPStatistics instance = new RealSPStatistics(5);
//        instance.addTexto(frameNumber, totalColisionesSimples, totalColisionesMultiples);
//    }
}
