/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sap;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import poligons.AnyPolygon2D;
import primitives.AABB2D;
import primitives.Pair;
import primitives.Point2D;
import wrappers.AABB2DWrapper;

/**
 *
 * @author hmoraga
 */
public class IncrementalSweepAndPruneTest {
    private List<Pair<Double>> listaVelocidades;
    private IncrementalSweepAndPrune rsp;
    private List<List<Point2D>> listaPoligonos;
    private List<Point2D> listaPuntos0, listaPuntos1, listaPuntos2;
    
    public IncrementalSweepAndPruneTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        listaPoligonos = new ArrayList<>();
        
        listaPuntos0 = new ArrayList<>();
        listaPuntos0.add(new Point2D(1, 0));
        listaPuntos0.add(new Point2D(2, 1));
        listaPuntos0.add(new Point2D(1, 3));
        listaPuntos0.add(new Point2D(0, 3));

        listaPuntos1 = new ArrayList<>();
        listaPuntos1.add(new Point2D(4, 4));
        listaPuntos1.add(new Point2D(5, 6));
        listaPuntos1.add(new Point2D(3, 6));

        listaPuntos2 = new ArrayList<>();
        listaPuntos2.add(new Point2D(4, -3));
        listaPuntos2.add(new Point2D(3, -1));
        listaPuntos2.add(new Point2D(1, -2));

        listaPoligonos.add(listaPuntos0);
        listaPoligonos.add(listaPuntos1);
        listaPoligonos.add(listaPuntos2);       
        
        listaVelocidades = new ArrayList<>();
        listaVelocidades.add(new Pair<>(1.0,-1.0));
        listaVelocidades.add(new Pair<>(0.0,-1.0));
        listaVelocidades.add(new Pair<>(-1.0,1.0));

        rsp = new IncrementalSweepAndPrune(listaPoligonos, listaVelocidades);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListaPuntosCajasPorDimension method, of class RealSP.
     */
    @Test
    public void testGetListaPuntosCajasPorDimension() {
        System.out.println("getListaPuntosCajasPorDimension");
        IncrementalSweepAndPrune instance = rsp;

        List<List<Point2DStaticWrap>> expResult = new ArrayList<>();
        expResult.add(new ArrayList<>());
        expResult.add(new ArrayList<>());
        
        expResult.get(0).add(new Point2DStaticWrap(new Point2D(0, 0), 0, false));
        expResult.get(0).add(new Point2DStaticWrap(new Point2D(1, -3), 2, false));
        expResult.get(0).add(new Point2DStaticWrap(new Point2D(2, 3), 0, true));
        expResult.get(0).add(new Point2DStaticWrap(new Point2D(3, 4), 1, false));
        expResult.get(0).add(new Point2DStaticWrap(new Point2D(4, -1), 2, true));
        expResult.get(0).add(new Point2DStaticWrap(new Point2D(5, 6), 1, true));

        expResult.get(1).add(new Point2DStaticWrap(new Point2D(1, -3), 2, false));
        expResult.get(1).add(new Point2DStaticWrap(new Point2D(4, -1), 2, true));       
        expResult.get(1).add(new Point2DStaticWrap(new Point2D(0, 0), 0, false));
        expResult.get(1).add(new Point2DStaticWrap(new Point2D(2, 3), 0, true));
        expResult.get(1).add(new Point2DStaticWrap(new Point2D(3, 4), 1, false));
        expResult.get(1).add(new Point2DStaticWrap(new Point2D(5, 6), 1, true));
        
        List<List<Point2DStaticWrap>> result = instance.getListaPuntosCajasPorDimension();

        assertEquals(expResult.get(0).get(0), result.get(0).get(0));
        assertEquals(expResult.get(0).get(1), result.get(0).get(1));
        assertEquals(expResult.get(0).get(2), result.get(0).get(2));
        assertEquals(expResult.get(0).get(3), result.get(0).get(3));
        assertEquals(expResult.get(0).get(4), result.get(0).get(4));
        assertEquals(expResult.get(0).get(5), result.get(0).get(5));        
    }

    /**
     * Test of getListaParesEfectivosColisionando method, of class RealSP.
     */
    @Test
    public void testGetListaParesEfectivosColisionando() {
        System.out.println("getListaParesEfectivosColisionando");
        List<Point2D> listaPuntos0, listaPuntos1, listaPuntos2, listaPuntos3, listaPuntos4;
        List<List<Point2D>> listaObjetos = new ArrayList<>();
        
        listaPuntos0 = new ArrayList<>();
        listaPuntos1 = new ArrayList<>();
        listaPuntos2 = new ArrayList<>();
        listaPuntos3 = new ArrayList<>();
        listaPuntos4 = new ArrayList<>();
        
        listaPuntos0.add(new Point2D(-1, -1));
        listaPuntos0.add(new Point2D(1, 1));
        listaPuntos0.add(new Point2D(-1, 1));
        listaObjetos.add(listaPuntos0);
        
        listaPuntos1.add(new Point2D(-2, 2));
        listaPuntos1.add(new Point2D(0, 0));
        listaPuntos1.add(new Point2D(0, 2));
        listaObjetos.add(listaPuntos1);
        
        listaPuntos2.add(new Point2D(0, 0));
        listaPuntos2.add(new Point2D(2, -2));
        listaPuntos2.add(new Point2D(2, 0));
        listaObjetos.add(listaPuntos2);

        listaPuntos3.add(new Point2D(0.5, 1.5));
        listaPuntos3.add(new Point2D(1.5, 0.5));
        listaPuntos3.add(new Point2D(0.5, 0.5));
        listaObjetos.add(listaPuntos3);

        listaPuntos4.add(new Point2D(-1.5, -0.5));
        listaPuntos4.add(new Point2D(-0.5, -1.5));
        listaPuntos4.add(new Point2D(-0.5, -0.5));
        listaObjetos.add(listaPuntos4);
 
        IncrementalSweepAndPrune instance = new IncrementalSweepAndPrune(listaObjetos, listaVelocidades);
        List<Pair<Integer>> expResult = new ArrayList<>();
        expResult.add(new Pair<>(0,4));
        expResult.add(new Pair<>(0,2));
        expResult.add(new Pair<>(1,2));
        expResult.add(new Pair<>(0,1));
        expResult.add(new Pair<>(0,3));
        
        List<Pair<Integer>> result = instance.getListaParesEfectivosColisionando();

        assertTrue(!result.isEmpty());
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaCajasPorDimension method, of class RealSP.
     */
    @Test
    public void testGetListaCajasPorDimension() {
        System.out.println("getListaCajasPorDimension");
        IncrementalSweepAndPrune instance = rsp;
        List<List<AABB2DWrapper>> expResult = new ArrayList<>();
        expResult.add(new ArrayList<>());
        expResult.add(new ArrayList<>());
        // en X
        expResult.get(0).add(new AABB2DWrapper(new AnyPolygon2D(listaPuntos0, false).getBox(), 0));
        expResult.get(0).add(new AABB2DWrapper(new AnyPolygon2D(listaPuntos2, false).getBox(), 2));
        expResult.get(0).add(new AABB2DWrapper(new AnyPolygon2D(listaPuntos1, false).getBox(), 1));
        // en Y
        expResult.get(1).add(new AABB2DWrapper(new AnyPolygon2D(listaPuntos2, false).getBox(), 2));
        expResult.get(1).add(new AABB2DWrapper(new AnyPolygon2D(listaPuntos0, false).getBox(), 0));
        expResult.get(1).add(new AABB2DWrapper(new AnyPolygon2D(listaPuntos1, false).getBox(), 1));
        
        List<List<AABB2DWrapper>> result = instance.getListaCajasPorDimension();
        assertEquals(expResult, result);
    }

    /**
     * Test of insertionSort method, of class RealSP.
     */
    @Test
    public void testInsertionSort() {
        System.out.println("insertionSort");
        IncrementalSweepAndPrune instance = rsp;
        instance.insertionSort();
    }
}
