/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sap;

import comparators.ElementComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import primitives.Pair;

/**
 *
 * @author hmoraga
 */
public class ElementTest {
    public Element e0, e1, e2, e3, e4, e5;
    public List<Element> listaElementos;
    
    public ElementTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        e0 = new Element(-2.0, 0, false);
        e1 = new Element(-1.0, 1, false);
        e2 = new Element(0.0, 2, false);
        e3 = new Element(1.0, 0, true);
        e4 = new Element(2.0, 1, true);
        e5 = new Element(3.0, 2, true);
        listaElementos = new ArrayList<>();
        listaElementos.add(e0);
        listaElementos.add(e1);
        listaElementos.add(e2);
        listaElementos.add(e3);
        listaElementos.add(e4);
        listaElementos.add(e5);
        Collections.sort(listaElementos, new ElementComparator());
    }
    
    @After
    public void tearDown() {
        listaElementos.clear();
    }

    @Test
    public void testGetValue() {
        System.out.println("getValue");
        Element instance0 = new Element(2.5, 0, false);
        Element instance1 = new Element(4.5, 0, true);
        double expResult0 = 2.5, expResult1 = 4.5;
        double result0 = instance0.getValue(), result1 = instance1.getValue();
        assertEquals(expResult0, result0, 0.0);
        assertEquals(expResult1, result1, 0.0);
    }

    @Test
    public void testSetValue() {
        System.out.println("setValue");
        double value = 3.0;
        Element instance = new Element(2.5, 0, false);
        instance.setValue(value);
        assertEquals(instance.getValue(), value, 0.0);
    }

    @Test
    public void testGetId() {
        System.out.println("getId");
        Element instance = new Element(2.5, 3, false);
        int expResult = 3;
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetId() {
        System.out.println("setId");
        int id = 1;
        Element instance = new Element(2.5, 0, false);
        instance.setId(id);
        assertEquals(instance.getId(),id);
    }

    @Test
    public void testIsFin() {
        System.out.println("isFin");
        Element instance0 = new Element(2.5, 0, false);
        Element instance1 = new Element(2.5, 0, true);
        boolean expResult0 = false;
        boolean expResult1 = true;
        boolean result0 = instance0.isFin();
        boolean result1 = instance1.isFin();
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    @Test
    public void testSetFin() {
        System.out.println("setFin");
        boolean fin0 = true, fin1 = false;
        Element instance0 = new Element(2.5, 0, false);
        Element instance1 = new Element(-4.58, 12, true);
        instance0.setFin(fin0);
        instance1.setFin(fin1);
        assertTrue(instance0.isFin());
        assertFalse(instance1.isFin());
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        Element instance0 = new Element(2.5, 0, false);
        String expResult0 = "Element{value=2.5, id=0, fin=false}";
        String result0 = instance0.toString();
        Element instance1 = new Element(7.32, 1, true);
        String expResult1 = "Element{value=7.32, id=1, fin=true}";
        String result1 = instance1.toString();
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Element instance = new Element(2.5, 0, false);
        int expResult = 0;
        int result = instance.hashCode();
        assertTrue(expResult != result); 
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Element(2.5, 0, false);
        Element instance0 = new Element(2.5, 0, false);
        Element instance1 = new Element(2.5, 0, true);
        Element instance2 = new Element(2.5, 1, false);
        Element instance3 = new Element(1.5, 0, false);
        boolean expResult0 = true, expResult1 = false, expResult2 = false, expResult3 = false;
        boolean result0 = instance0.equals(obj), result1 = instance1.equals(obj), result2 = instance2.equals(obj), result3 = instance3.equals(obj);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
    }

    @Test
    public void testObtenerListaPares() {
        System.out.println("obtenerListaPares");
        e1 = new Element(-5, 1, false);
        e2 = new Element(-1, 1, true);
        e3 = new Element(-1, 3, false);
        e4 = new Element(0, 4, false);
        e5 = new Element(2, 4, true);
        Element e6 = new Element(2, 2, false);
        Element e7 = new Element(3, 3, true);
        Element e8 = new Element(5, 2, true);
        
        List<Element> listaElems = new ArrayList<>();
        listaElems.add(e8);
        listaElems.add(e2);
        listaElems.add(e6);
        listaElems.add(e4);
        listaElems.add(e5);
        listaElems.add(e3);
        listaElems.add(e7);
        listaElems.add(e1);
        Collections.sort(listaElems, new ElementComparator());
        
        List<Pair<Integer>> expResult = new ArrayList<>();
        expResult.add(new Pair<>(1,3));
        expResult.add(new Pair<>(4,3));
        expResult.add(new Pair<>(4,2));
        expResult.add(new Pair<>(3,2));
        
        List<Pair<Integer>> result = Element.obtenerListaPares(listaElems);
        assertEquals(expResult, result);
    }

    @Test
    public void testBuscarIndiceElemInicial() {
        // donde esta en el arreglo el elemento con indice x
        System.out.println("buscarIndiceElemInicial");
        e1 = new Element(-5, 1, false);
        e2 = new Element(-1, 1, true);
        e3 = new Element(-1, 3, false);
        e4 = new Element(0, 4, false);
        e5 = new Element(2, 4, true);
        Element e6 = new Element(2, 2, false);
        Element e7 = new Element(3, 3, true);
        Element e8 = new Element(5, 2, true);
        
        List<Element> listaElems = new ArrayList<>();
        listaElems.add(e1);
        listaElems.add(e2);
        listaElems.add(e3);
        listaElems.add(e4);
        listaElems.add(e5);
        listaElems.add(e6);
        listaElems.add(e7);
        listaElems.add(e8);
        
        int elemId = 3;
        int expResult = 2;
        int result = Element.buscarIndiceElemInicial(listaElems, elemId);
        assertEquals(expResult, result);
    }
}
