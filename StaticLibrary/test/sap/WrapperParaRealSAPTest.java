/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sap;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import primitives.AABB2D;
import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class WrapperParaRealSAPTest {
    private WrapperParaRealSAP wrsp;    
    private FileWriter fw;
    
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    public WrapperParaRealSAPTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws IOException {
        File output = temporaryFolder.newFile("output.txt");
        fw = new FileWriter(output.getPath(), true);

        fw.write("//frame:0"+'\n');
        fw.write("//time:0.0"+'\n');
        fw.write("//marco:(-5.0,5.0),(-5.0,5.0)"+'\n');
        fw.write("//dimensiones:2"+'\n');
        fw.write("3 1 -1 2 0 1 1"+'\n');
        fw.write("3 4 0 4 3 3 2"+'\n');
        fw.write("3 3 -3 4 -2 3 0"+'\n');
        fw.close();
        
        wrsp = new WrapperParaRealSAP(output.getPath(), true);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getFrameNumber method, of class WrapperParaRealSAP.
     */
    @Test
    public void testGetFrameNumber() {
        System.out.println("getFrameNumber");
        WrapperParaRealSAP instance = wrsp;
        int expResult = 0;
        int result = instance.getFrameNumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFrameNumber method, of class WrapperParaRealSAP.
     */
    @Test
    public void testSetFrameNumber() {
        System.out.println("setFrameNumber");
        int frameNumber = 2;
        WrapperParaRealSAP instance = wrsp;
        instance.setFrameNumber(frameNumber);
        assertEquals(frameNumber, instance.getFrameNumber());
    }

    /**
     * Test of getNumDims method, of class WrapperParaRealSAP.
     */
    @Test
    public void testGetNumDims() {
        System.out.println("getNumDims");
        WrapperParaRealSAP instance = wrsp;
        int expResult = 2;
        int result = instance.getNumDims();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNumDims method, of class WrapperParaRealSAP.
     */
    @Test
    public void testSetNumDims() {
        System.out.println("setNumDims");
        int numDims = 3;
        WrapperParaRealSAP instance = wrsp;
        instance.setNumDims(numDims);
        assertEquals(numDims, instance.getNumDims());
    }

    /**
     * Test of getListaCajas method, of class WrapperParaRealSAP.
     */
    @Test
    public void testGetListaCajas() {
        System.out.println("getListaCajas");
        WrapperParaRealSAP instance = wrsp;
        List<AABB2D> expResult = new ArrayList<>(3);
        List<Point2D> lista0, lista1, lista2;
        lista0 = new ArrayList<>(3);
        lista0.add(new Point2D(1, -1));
        lista0.add(new Point2D(2, 0));
        lista0.add(new Point2D(1, 1));

        lista1 = new ArrayList<>(3);
        lista1.add(new Point2D(4, 0));
        lista1.add(new Point2D(4, 3));
        lista1.add(new Point2D(3, 2));

        lista2 = new ArrayList<>(3);
        lista2.add(new Point2D(3, -3));
        lista2.add(new Point2D(4, -2));
        lista2.add(new Point2D(3, 0));
        
        expResult.add(new AABB2D(lista0));
        expResult.add(new AABB2D(lista1));
        expResult.add(new AABB2D(lista2));
        
        List<AABB2D> result = instance.getListaCajas();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaCajas method, of class WrapperParaRealSAP.
     */
    @Test
    public void testSetListaCajas() {
        System.out.println("setListaCajas");
List<AABB2D> expResult = new ArrayList<>(3);
        List<Point2D> lista0, lista1, lista2;
        lista0 = new ArrayList<>(3);
        lista0.add(new Point2D(2, -1));
        lista0.add(new Point2D(3, 0));
        lista0.add(new Point2D(2, 1));

        lista1 = new ArrayList<>(3);
        lista1.add(new Point2D(4, -1));
        lista1.add(new Point2D(4, 2));
        lista1.add(new Point2D(3, 1));

        lista2 = new ArrayList<>(3);
        lista2.add(new Point2D(3, -2));
        lista2.add(new Point2D(4, -1));
        lista2.add(new Point2D(3, 1));
        
        expResult.add(new AABB2D(lista0));
        expResult.add(new AABB2D(lista1));
        expResult.add(new AABB2D(lista2));
        
        WrapperParaRealSAP instance = wrsp;
        instance.setListaCajas(expResult);
        assertEquals(expResult, instance.getListaCajas());
    }

    /**
     * Test of getAreaObjetos method, of class WrapperParaRealSAP.
     */
    @Test
    public void testGetAreaObjetos() {
        System.out.println("getAreaObjetos");
        WrapperParaRealSAP instance = wrsp;
        double expResult = 4.0;
        double result = instance.getAreaObjetos();
        assertEquals(expResult, result, 0.00001);
    }

    /**
     * Test of setAreaObjetos method, of class WrapperParaRealSAP.
     */
    @Test
    public void testSetAreaObjetos() {
        System.out.println("setAreaObjetos");
        List<List<Point2D>> listaPuntosObjetos = new ArrayList<>();
        WrapperParaRealSAP instance = wrsp;
        instance.setAreaObjetos(listaPuntosObjetos);
        assertEquals(0.0, instance.getAreaObjetos(), 0.0);
    }

    /**
     * Test of getAreaSimulacion method, of class WrapperParaRealSAP.
     */
    @Test
    public void testGetAreaSimulacion() {
        System.out.println("getAreaSimulacion");
        WrapperParaRealSAP instance = wrsp;
        double expResult = 100.0;
        double result = instance.getAreaSimulacion();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setAreaSimulacion method, of class WrapperParaRealSAP.
     */
    @Test
    public void testSetAreaSimulacion() {
        System.out.println("setAreaSimulacion");
        List<Pair<Double>> dimensiones = new ArrayList<>(2);
        dimensiones.add(new Pair<>(0.0, 5.0));
        dimensiones.add(new Pair<>(-5.0, 5.0));
        WrapperParaRealSAP instance = wrsp;
        instance.setAreaSimulacion(dimensiones);
        double result = instance.getAreaSimulacion();
        assertEquals(50.0, result, 0.0);
    }
}
