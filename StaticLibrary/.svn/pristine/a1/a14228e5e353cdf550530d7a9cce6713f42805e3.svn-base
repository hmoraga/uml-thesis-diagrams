/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/**
 *
 * @author hmoraga
 */
public class Point2D implements Point{
    private double[] coords;
    
    public Point2D(){
        coords = new double[2];
        Arrays.fill(coords, 0);
    }

    public Point2D(double x, double y) {
        coords = new double[2];
        coords[0] = x;
        coords[1] = y;
    }

    public Point2D(double[] coords) {
        this.coords = new double[2];
        
        assert(coords.length==2);
        
        this.coords = coords;
    }
    
    public Point2D(Point2D p){
        this.coords = new double[2];
        assert(coords.length==2);
        coords[0] = p.getX();
        coords[1] = p.getY();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final Point2D other = (Point2D) obj;
        
        return ((this.getCoords(0) == other.getCoords(0)) && (this.getCoords(1) == other.getCoords(1)));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Arrays.hashCode(this.coords);
        return hash;
    }
    
    public double getX(){
        return getCoords(0);
    }
    
    public double getY(){
        return getCoords(1);
    }
    
    @Override
    public double getCoords(int dimension)
    {
        assert (dimension>=0 && dimension<2);
        return coords[dimension];
    }
    
    @Override
    public double distanceTo(Point p){
        try{
            return Math.sqrt(distanceSquaredTo(p));
        } catch (ArithmeticException e){
            return -1.0;
        }
    }
    
    @Override
    public double distanceSquaredTo(Point p){
        if (p == null) 
            return -1.0;
        if (getClass() != p.getClass())
            return -1.0;
        else {
            double sum = 0.0;

            for (int i = 0; i < coords.length; i++) {
                double x = Math.pow(coords[i] - p.getCoords(i), 2.0);
                sum +=x;
            }
        
            return sum;
        }
    }

    @Override
    public String toString() {
        String s = "(";
        if (coords.length>0){
            for (int i = 0; i < coords.length-1; i++) {
                s = s + coords[i] + ",";            
            }
            s = s + coords[coords.length-1] + ")";
            return s;
        } else return null;
    }

    public boolean exists(ArrayList<Point2D> listaPuntos){
        return listaPuntos.contains(this);
    }
    
    public boolean isInside(AABB2D box){
        return ((box.getMinX() <= this.getX()) &&
                (box.getMaxX() >= this.getX()) &&
                (box.getMinY() <= this.getY()) &&
                (box.getMaxY() >= this.getY()));
    }
    
    public void add(Point2D p){
        coords[0] += p.getX();
        coords[1] += p.getY();
    }
    
    public void add(double x, double y){
        coords[0] += x;
        coords[1] += y;
    }

    @Override
    public void add(Point p) {
        Point2D pto = (Point2D)p;
        
        if (pto!=null)
            this.add(pto);
    }

    @Override
    public int sizeOf() {
        return 2*Double.BYTES;
    }
}
