/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author hmoraga
 */
public class Point2D implements Point{
    private double[] coords;
    
    public Point2D(){
        coords = new double[2];
        Arrays.fill(coords, 0);
    }

    public Point2D(double x, double y) {
        coords = new double[2];
        coords[0] = x;
        coords[1] = y;
    }

    public Point2D(double[] coords) {
        this.coords = new double[2];
        
        assert(coords.length==2);
        
        this.coords = coords;
    }
    
    public Point2D(Point2D p){
        this.coords = new double[2];
        assert(coords.length==2);
        coords[0] = p.getX();
        coords[1] = p.getY();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final Point2D other = (Point2D) obj;
        
        return ((this.getCoords(0) == other.getCoords(0)) && (this.getCoords(1) == other.getCoords(1)));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Arrays.hashCode(this.coords);
        return hash;
    }
    
    public double getX(){
        return getCoords(0);
    }
    
    public double getY(){
        return getCoords(1);
    }
    
    @Override
    public double getCoords(int dimension)
    {
        assert (dimension>=0 && dimension<2);
        return coords[dimension];
    }
    
    @Override
    public double distanceTo(Point p){
        try{
            return Math.sqrt(distanceSquaredTo(p));
        } catch (ArithmeticException e){
            return -1.0;
        }
    }
    
    @Override
    public double distanceSquaredTo(Point p){
        if (p == null) 
            return -1.0;
        if (getClass() != p.getClass())
            return -1.0;
        else {
            double sum = 0.0;

            for (int i = 0; i < coords.length; i++) {
                double x = Math.pow(coords[i] - p.getCoords(i), 2.0);
                sum +=x;
            }
        
            return sum;
        }
    }

    @Override
    public String toString() {
        String s = "(";
        if (coords.length>0){
            for (int i = 0; i < coords.length-1; i++) {
                s = s + coords[i] + ",";            
            }
            s = s + coords[coords.length-1] + ")";
            return s;
        } else return null;
    }

    public boolean exists(ArrayList<Point2D> listaPuntos){
        return listaPuntos.contains(this);
    }
    
    public boolean isInside(AABB2D box){
        return ((box.getMinX() <= this.getX()) &&
                (box.getMaxX() >= this.getX()) &&
                (box.getMinY() <= this.getY()) &&
                (box.getMaxY() >= this.getY()));
    }
    
    public void add(Point2D p){
        coords[0] += p.getX();
        coords[1] += p.getY();
    }
    
    public void add(double x, double y){
        coords[0] += x;
        coords[1] += y;
    }

    @Override
    public void add(Point p) {
        Point2D pto = (Point2D)p;
        
        if (pto!=null)
            this.add(pto);
    }

    /**
     * Me devuelve las coordenadas del punto pero si se hubiera
     * rotado el eje coordenado en un angulo angle
     * @param angle en radianes
     * @return 
     */
    public Point2D rotateBy(double angle){
        if (angle!=0 && angle!=2*Math.PI){
            double x = getX()*Math.cos(angle)+getY()*Math.sin(angle);
            double y = -getX()*Math.sin(angle)+getY()*Math.cos(angle);
        
            return new Point2D(x, y);
        }
        return this;
    }
    
    /**
     * Me multiplica el punto por un valor escalar
     * @param scale valor escalar a multiplicar el punto
     * @return punto escalado
     */
    public Point2D multipliedByScalar(double scale){
        return (scale!=1.0)?new Point2D(getX()*scale, getY()*scale):this;
    }

    @Override
    public Point linearTransform(double xmin, double xmax, double ymin, double ymax, double A, double B) {
        double xp = A*(this.getX()-xmin)/(xmax-xmin);
        double yp = -B*(this.getY()-ymax)/(ymax-ymin);
        
        return new Point2D(xp, yp);
    }
}
