/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligons;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import primitives.AABB2D;
import primitives.Edge2D;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class Polygon2DTest {
    private Polygon2D poly1, poly2, poly3;
    private ArrayList<Point2D> listaPuntos;

    public Polygon2DTest() {
        listaPuntos = new ArrayList<>();
        
        listaPuntos.add(new Point2D(1.0, 0.0));
        listaPuntos.add(new Point2D(2.0, 0.0));
        listaPuntos.add(new Point2D(1.0, 1.0));
        listaPuntos.add(new Point2D(-1.0, 1.0));
        listaPuntos.add(new Point2D(-2.0, 0.0));
        listaPuntos.add(new Point2D(-2.0, -2.0));
        listaPuntos.add(new Point2D(0.0, -1.0));
        listaPuntos.add(new Point2D(-1.0, -3.0));
        listaPuntos.add(new Point2D(1.0, -3.0));
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        // poligono 1: lista de puntos cualquiera (polígono no convexo)
        poly1 = new Polygon2D(listaPuntos, 1.0, false);
        // poligono 2: triangulo rotado irregular
        poly2 = new Polygon2D(new double[]{0,5*Math.PI/6,4*Math.PI/3}, 5, false);
        // poligono 3: cuadrado sin rotar
        poly3 = new Polygon2D(4, 2.0, true, false);
    }
    
    @After
    public void tearDown() {
        poly1.clear();
        poly2.clear();
        poly3.clear();
    }

    @Test
    public void testGetVertices() {
        System.out.println("getVertices");

        Polygon2D instance = poly1;
        ArrayList<Point2D> expResult = listaPuntos;
        ArrayList<Point2D> result = instance.getVertices();
        assertEquals(expResult, result);
    }

    @Test
    public void testDesplazamiento_double_double() {
        System.out.println("desplazamiento");
        ArrayList<Point2D> listaVertices = new ArrayList<>();
        listaVertices.add(new Point2D(3.0, 2.0));
        listaVertices.add(new Point2D(1.0, 4.0));
        listaVertices.add(new Point2D(-1.0, 2.0));
        listaVertices.add(new Point2D(1.0, 0.0));
        
        double x = 1.0;
        double y = 2.0;
        Polygon2D instance = poly3;
        instance.desplazamiento(x, y);
        int i=0;
        
        for (Point2D p : instance) {
            assertTrue(p.distanceSquaredTo(listaVertices.get(i++))<0.00001);
        }
        
    }

    @Test
    public void testDesplazamiento_Point2D() {
        System.out.println("desplazamiento");
        double x = 1.0;
        double y = 2.0;
        poly1.desplazamiento(new Point2D(x, y));
        int i=0;
        
        for (Point2D p : poly1) {
            listaPuntos.get(i).add(new Point2D(x, y));
            assertTrue(p.distanceSquaredTo(listaPuntos.get(i++))<0.00001);
        }
    }

    @Test
    public void testGetEdgeList() {
        System.out.println("getEdgeList");
        Polygon2D instance = poly3;
        ArrayList<Edge2D> expResult = new ArrayList<>(poly3.size());
        
        for (int i = 0; i < poly3.size(); i++) {
            expResult.add(new Edge2D(poly3.get(i), poly3.get((i+1)%poly3.size())));
        }
                
        ArrayList<Edge2D> result = instance.getEdgeList();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetBox() {
        System.out.println("getBox");
        Polygon2D instance = poly1;
        AABB2D expResult = new AABB2D(listaPuntos);
        AABB2D result = instance.getBox();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetArea() {
        System.out.println("getArea");
        Polygon2D instance = poly1;
        double expResult = 10.0;
        double result = instance.getArea();
        assertEquals(expResult, result, 0.00001);
    }    
}
