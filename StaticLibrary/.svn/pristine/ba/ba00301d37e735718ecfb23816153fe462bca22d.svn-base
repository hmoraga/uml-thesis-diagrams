/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitives;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import poligons.Polygon2D;
import wrappers.ListAABB2D;
import wrappers.ListPuntos2D;

/**
 *
 * @author hmoraga
 */
public class BVTree {
    private BVNode root;
    
    public BVTree(){
	root = null;
    }

    public BVTree(Polygon2D objeto){
        switch (objeto.getEdgeList().size())
	{
            case 0:
		root = null;
		break;
            case 1:
		root = new BVNode(new AABB2D(objeto.getEdgeList().get(0), 0));
		break;
            default:
                ListAABB2D listaCajas = new ListAABB2D();
                ArrayList<BVNode> listaNodos = new ArrayList<>();
                int i=0;
        
                for (Edge2D arista : objeto.getEdgeList())
                    listaCajas.add(new AABB2D(arista, i++));
        
                //ordeno la lista de AABB2D
                AABB2D.sortFunction(listaCajas);
        
                for (AABB2D boundingVolume: listaCajas) {
                    BVNode nodo = new BVNode(boundingVolume);
                    listaNodos.add(nodo);
                }
		
		crearArbol(listaNodos);
		break;
	}
    }
    
    public BVTree(ListPuntos2D listaPuntos){
        this(new Polygon2D((List<Point2D>)listaPuntos, 1.0, false));
    }
    
    public BVTree(BVNode raiz){
	this.root = raiz;
    }

    public BVTree(ArrayList<? extends BV> listaBVs){
        switch (listaBVs.size())
	{
            case 0:
		root = null;
		break;
            case 1:
		root = new BVNode(listaBVs.get(0));
		break;
            default:
                ArrayList<BVNode> listaNodos = new ArrayList<>();
		// se supone que si el vector no viene ordenado hago el orden de acuerdo a la posicion primero
		// y despues respecto a su volumen
		
                if (listaBVs.get(0) instanceof AABB2D){
                    AABB2D.sortFunction((ArrayList<AABB2D>)listaBVs);
                }

                for (BVNode nodo : listaNodos) {
                    listaNodos.add(nodo);
                }
                
		crearArbol(listaNodos);
		break;
	}
    }

    public BVNode getRoot(){
        return root;
    }

    protected final void crearArbol(ArrayList<BVNode> listaNodos){
        // construyo el arbol bottom-up
	switch (listaNodos.size()){
            case 0:
                root = null;
                break;
            case 1:
                root = new BVNode(listaNodos.get(0));
		break;
            default:
                do {
                    List<BVNode> listaNodosTmp = new ArrayList<>();
                    
                    //averiguo el tamaño de nodos de la lista
                    int n = listaNodos.size();

                    //si el numero es par debo hacer n/2 veces la siguiente operacion
                    for (int i=0; i<n/2; i++){
                        BVNode nodoNuevo = BVNode.obtenerNodoCombinadoMinimaArea(listaNodos);
			listaNodosTmp.add(nodoNuevo);
                    }

                    //en el caso que n es impar quedaba un nodo extra
                    if (n%2 != 0)
                        listaNodosTmp.add(listaNodos.get(0));

                    listaNodos = new ArrayList(listaNodosTmp);
		} while (listaNodos.size() > 1);
			
		root = new BVNode(listaNodos.get(0));			
		break;
	}
    }

    public static Pair<Double> getVariance(ArrayList<BVNode> listaNodos){
	Pair<Double> mean = BVTree.getMean(listaNodos); 
	double varX=0, varY=0;
        Iterator<BVNode> it = listaNodos.iterator();
        
        while (it.hasNext()){
            BV p = it.next().unwrap();

            double ptoX = p.getMaximum(0)-p.getMinimum(0);
            double ptoY = p.getMaximum(1)-p.getMinimum(1);
            
            varX += Math.pow(ptoX,2.0);
            varY += Math.pow(ptoY,2.0);
        }

        varX /= listaNodos.size();
	varY /= listaNodos.size();

	varX -= Math.pow(mean.first,2);
	varY -= Math.pow(mean.second,2);

	return new Pair<>(varX, varY);
    }

    public static Pair<Double> getMean(ArrayList<BVNode> listaNodos){
	double meanX=0, meanY=0;
        
        Iterator<BVNode> it = listaNodos.iterator();
        
        while (it.hasNext()){
            BV p = it.next().unwrap();
            
            double ptoX = p.getMaximum(0)-p.getMinimum(0);
            double ptoY = p.getMaximum(1)-p.getMinimum(1);

            meanX+=ptoX;
            meanY+=ptoY;
	}

	meanX/=listaNodos.size();
	meanY/=listaNodos.size();

	return new Pair<>(meanX, meanY);
    }

    public Pair<Double> obtenerExtremos(int dimension){
        BV datosRaiz = getRoot().getData();

	return new Pair<>(datosRaiz.getMinimum(dimension), datosRaiz.getMaximum(dimension));
    }
    
    public static void obtenerAristasColisionando(ArrayList<Pair<Integer>> listaColisiones, BVTree objetoUno, BVTree objetoDos){
	BVNode.obtenerAristasColisionando(listaColisiones, objetoUno.getRoot(), objetoDos.getRoot());
    }
}