/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sap;

import comparators.AABB2DWrapperComparator;
import comparators.Point2DStaticWrapComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import primitives.AABB2D;
import primitives.Pair;
import primitives.Point2D;
import wrappers.AABB2DWrapper;

/**
 * Suposicion INICIAL: tendremos los poligonos iniciales, los cuales tendrán 
 * asociado un índice (dado por el orden en que aparecen en la lista) y sus
 * velocidades de desplazamiento (por segundo de simulacion), se necesita además
 * el intervalo de simulación y la cantidad de imágenes que tendrá la simulación
 * (o en su defecto los FPS).
 * 
 * Como atributos estará la lista de cajas que representan a cada polígono,
 * el intervalo de simulacion, el timestep.
 * 
 * Como atributos auxiliares están: la lista de valores extremos de cada caja 
 * (por cada coordenada) y la lista de posiciones que tiene cada objeto en la
 * lista (por cada coordenada).
 * 
 * @author hmoraga
 */
public class RealSP {
    private List<List<AABB2DWrapper>> listaCajasPorDimension;
    // primer elemento del par es el orden de lectura de los objetos 
    // (segun archivo) y el segundo es el orden en que quedaran en la
    // lista una vez ordenados.
    private List<List<Pair<Integer>>> ordenListaCajas; 
    
    public RealSP(List<AABB2D> listaCajas) {
        List<AABB2DWrapper> listaCajasWrapper = new ArrayList<>(listaCajas.size());

        for (int j = 0; j < listaCajas.size(); j++) {
            AABB2D box = listaCajas.get(j);
            listaCajasWrapper.add(new AABB2DWrapper(box, j));
        }
        
        listaCajasPorDimension = new ArrayList<>(2);
        ordenListaCajas = new ArrayList<>(2);
        
        for (int i=0; i<2; i++){
            // una copia de la lista por dimension
            listaCajasPorDimension.add(new ArrayList<>(listaCajasWrapper));
            ordenListaCajas.add(new ArrayList<>(listaCajas.size()));
            // se ordena la lista O(n log n)
            Collections.sort(listaCajasPorDimension.get(i), new AABB2DWrapperComparator(i));
            // recorro la lista para llenar el orden original
            for (int j = 0; j < listaCajasPorDimension.get(i).size(); j++) {
                AABB2DWrapper boxWrapper = listaCajasPorDimension.get(i).get(j);
                ordenListaCajas.get(i).add(new Pair<>(boxWrapper.getId(), j));
            }
        }
    }

    public List<List<Point2DStaticWrap>> getListaPuntosCajasPorDimension() {
        List<List<Point2DStaticWrap>> listaPuntosCajasPorDimension = new ArrayList<>(2);
        listaPuntosCajasPorDimension.add(new ArrayList<>(2*listaCajasPorDimension.get(0).size()));
        listaPuntosCajasPorDimension.add(new ArrayList<>(2*listaCajasPorDimension.get(1).size()));
        
        // se calculan de la lista de cajas, la lista de puntosCajasPorDimension
        for (int i = 0; i < listaCajasPorDimension.size(); i++) {
            List<AABB2DWrapper> listaCajasWrapper = listaCajasPorDimension.get(i);
            
            for (int j = 0; j < listaCajasWrapper.size(); j++) {
                AABB2D box = listaCajasWrapper.get(j).getBox();
                List<Point2D> listaPuntosAux = box.getPoints();
                
                listaPuntosCajasPorDimension.get(i).add(new Point2DStaticWrap(listaPuntosAux.get(0), j, false));
                listaPuntosCajasPorDimension.get(i).add(new Point2DStaticWrap(listaPuntosAux.get(2), j, true));
            }
        }
        
        // ordeno las listas de puntos de cajas por dimension
        for (int i = 0; i < 2; i++) {
            Collections.sort(listaPuntosCajasPorDimension.get(i), new Point2DStaticWrapComparator(i));
        }
       
        return listaPuntosCajasPorDimension;
    }

    public List<List<Pair<Integer>>> getPosicionesPorObjetoEnListaPorDimension() {
        List<List<Pair<Integer>>> listaParesPorDimension = new ArrayList<>(2);
        listaParesPorDimension.add(new ArrayList<>());
        listaParesPorDimension.add(new ArrayList<>());
        List<List<Point2DStaticWrap>> listaPuntosWrap = getListaPuntosCajasPorDimension();
        
        List<Integer> listaAuxiliar = new ArrayList<>();
        for (int i = 0; i < listaPuntosWrap.size(); i++) {
            listaAuxiliar.clear();
            List<Point2DStaticWrap> listaP2Dsw = listaPuntosWrap.get(i);
            
            for (int j = 0; j < listaP2Dsw.size(); j++) {
                Point2DStaticWrap p2Dsw = listaP2Dsw.get(j);
                
                if (!p2Dsw.isFin())
                    listaAuxiliar.add(p2Dsw.getId());
                else {
                    if (p2Dsw.isFin() && (listaAuxiliar.get(0) == p2Dsw.getId())){
                        Integer idAux = listaAuxiliar.remove(0);
                        
                        for (int k = 0; k < listaAuxiliar.size(); k++) {
                            Integer y = listaAuxiliar.get(k);
                            
                            listaParesPorDimension.get(i).add(new Pair<>(Math.min(idAux, y),Math.max(idAux, y)));
                        }
                    }
                }
            }
        }
        
        return listaParesPorDimension;
    }

    public List<Pair<Integer>> getListaParesEfectivosColisionando() {
        List<Pair<Integer>> resultado = new ArrayList<>();
        List<List<Pair<Integer>>> listaParesAux = getPosicionesPorObjetoEnListaPorDimension();
        
        if (listaParesAux.get(0).size() < listaParesAux.get(1).size()){
            for (Pair<Integer> par : listaParesAux.get(0)) {
                if (listaParesAux.get(1).contains(par) || listaParesAux.get(1).contains(par.inverted()))
                    resultado.add(par);
            }
        } else {
            for (Pair<Integer> par : listaParesAux.get(1)) {
                if (listaParesAux.get(0).contains(par) || listaParesAux.get(0).contains(par.inverted()))
                    resultado.add(par);
            }            
        }
        return resultado;
    }

    /**
     * Retorna la lista de cajas de acuerdo al orden interno.
     * @return lista de cajas para cada coordenada
     */
    public List<List<AABB2DWrapper>> getListaCajasPorDimension() {
        return listaCajasPorDimension;
    }

    public void setListaCajasPorDimension(List<List<AABB2DWrapper>> listaCajasPorDimension) {
        this.listaCajasPorDimension = listaCajasPorDimension;
    }
    /**
     * 
     * @param listaCajas
     * @return 
     */
    public boolean validarListaCajas(List<List<AABB2DWrapper>> listaCajas){
        boolean resultado = true;
       
        for (int i = 0; i < listaCajas.size(); i++) {
            for (int j = 0; j < listaCajas.get(i).size(); j++) {
                AABB2DWrapper box = listaCajas.get(i).get(j);
                
                resultado = resultado && (box.getId() == listaCajasPorDimension.get(i).get(j).getId());
                
                if (!resultado)
                    return false;
            }
        }
        return true;
    }
    
    /**
     * 
     */
    public void insertionSort(){
        int i, j;
        AABB2DWrapper newValue;

        for (int dim=0; dim < listaCajasPorDimension.size(); dim++){
            AABB2DWrapperComparator wrComp = new AABB2DWrapperComparator(dim);
            
            for (i = 1; i < listaCajasPorDimension.get(dim).size(); i++) {
                newValue = listaCajasPorDimension.get(dim).get(i);
                j = i;
                
                while (j > 0 && wrComp.compare(listaCajasPorDimension.get(dim).get(j - 1), newValue)==1) {
                    listaCajasPorDimension.get(dim).set(j, listaCajasPorDimension.get(dim).get(j - 1));
                    j--;
                }
                
                listaCajasPorDimension.get(dim).set(j, newValue);
            }
        }
    }
    
    /*private List<List<AABB2DWrapper>> ordenarSegunAtributoInterno(List<List<AABB2DWrapper>> listaBoxes){
        List<List<AABB2DWrapper>> resultado = new ArrayList<>(listaBoxes.size());
        
        // relleno el espacio del arrayList
        for (int i = 0; i < listaBoxes.size(); i++) {
            resultado.add(new ArrayList<>());
            for (int j=0; j< listaBoxes.get(i).size(); j++) {
                resultado.get(i).add(null);
            }
        }
        
        // ejecuto el llenado de acuerdo al ordenListaCajas
        for (int i = 0; i < listaBoxes.size(); i++) {
            for (int j = 0; j < listaBoxes.get(i).size(); j++) {
                AABB2DWrapper box = listaBoxes.get(i).get(j); // de aqui leo el indice
                Pair<Integer> par = ordenListaCajas.get(i).get(j); // (objeto, posicion)
                
                resultado.get(i).set(par.second, box);
                assert(par.first == box.getId());
            }
        }
        return resultado;
    }

    private boolean testOrder(List<List<AABB2DWrapper>> listaBoxWrappers) {
        boolean resultado = true;
        
        for (int i=0; i< listaBoxWrappers.size(); i++){
            AABB2DWrapperComparator cmp = new AABB2DWrapperComparator(i);
            
            for (int j = 0; j < listaBoxWrappers.get(i).size()-1; j++) {
                if (cmp.compare(listaBoxWrappers.get(i).get(j), listaBoxWrappers.get(i).get(j+1))>=0){
                    resultado = false;
                    break;
                }
            }
        }
        
        return resultado;
    }*/
}