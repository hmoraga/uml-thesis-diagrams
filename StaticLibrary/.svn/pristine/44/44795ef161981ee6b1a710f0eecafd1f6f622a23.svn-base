/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import primitives.Pair;

/**
 *
 * @author hmoraga
 */
public class Element{
    /** Dada una lista de elementos, obtener la lista de pares que colisionan
     * se asume que la lista de Elem esta ordenada.
     * @param listaElementos
     * @return
     **/
    public static List<Pair<Integer>> obtenerListaPares(List<Element> listaElementos){
        List<Integer> pool = new ArrayList<>(listaElementos.size()/2);
        List<Pair<Integer>> listaPares = new ArrayList<>(listaElementos.size()/2);
        List<Pair<Integer>> listaParesTmp;
        
        // reglas para ingresar o salir del pool:
        // - cada vez que hay un elemento que no es fin de intervalo se agrega, segun las reglas de
        //   comparacion de Elem
        // - para los pares: (x,y) = (y,x)
        // - cuando es sacado un elemento se hace una lista temporal (con la combinacion con el resto)
        //   y se revisa todas las combinaciones de esta lista y si alguna no está se agrega a la lista de pares.
        
        for (Element elem : listaElementos) {
            if (!elem.isFin())
                pool.add(elem.getId());
            else {
                pool.remove(new Integer(elem.getId()));
                
                if (pool.size()>=1){
                    listaParesTmp = elem.crearListaParesTemporal(pool);
                    
                    if (!listaParesTmp.isEmpty()){
                        Iterator<Pair<Integer>> it = listaParesTmp.iterator();
                        
                        while (it.hasNext()){
                            Pair<Integer> par = it.next();
                            if (!listaPares.contains(par))
                                listaPares.add(par);
                        }
                    }
                    listaParesTmp.clear();
                }
            }
        }
        return listaPares;
    }
    public static int buscarIndiceElemInicial(ArrayList<Element> listaElems, int elemId){
        for (int i = 0; i < listaElems.size(); i++) {
            Element get = listaElems.get(i);
            
            if ((get.getId() == elemId) && !get.isFin())
                return i;
        }
        
        return -1;	// posicion del elemento no encontrada
    }
    private double value;
    private int id;
    private boolean fin;

    public Element(){
        value = 0.0;
	id = -1;
	fin = false;
    }
    
    public Element(double value, int id, boolean fin){
        assert(id>=0);
        this.value = value;
        this.id = id;
        this.fin = fin;
    }

    public Element(double value, boolean fin){
        this.value = value;
        this.fin = fin;
    }    
    
    public double getValue(){
        return value;
    }

    public void setValue (double value){
        this.value = value;
    }
    
    public int getId(){
        return id;
    }
    
    public void setId(int id){
        this.id = id;
    }

    public boolean isFin() {
        return fin;
    }

    public void setFin(boolean fin) {
        this.fin = fin;
    }

    @Override
    public String toString() {
        return "Element{" + "value=" + value + ", id=" + id + ", fin=" + fin + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Objects.hashCode(this.value);
        hash = 73 * hash + this.id;
        hash = 73 * hash + (this.fin ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Element other = (Element) obj;
        if (Double.doubleToLongBits(this.value) != Double.doubleToLongBits(other.value)) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        return this.fin == other.fin;
    }
    
    /**
     * Ordeno de acuerdo a lo siguiente:
       - valores disferentes, primero va el de menor valor
       - en caso contrario:
        - si existe uno que es fin de intervalo y otro que no, primero va el que abre el intervalo
	- si AMBOS son inicio de intervalo, va primero el que tiene menor Id
	- si AMBOS son fin de intervalo, va primero el que tiene mayor Id
     * @return 
    */ 
    private List<Pair<Integer>> crearListaParesTemporal(List<Integer> listaElems){
	List<Pair<Integer>> pares = new ArrayList<>();
	
        if (!listaElems.isEmpty()){
            for (int numeroId : listaElems) {
                if (this.getId() != numeroId)
                    pares.add(new Pair<Integer>(this.getId(), numeroId));
            }            
        }

        return pares;
    }
    
}
