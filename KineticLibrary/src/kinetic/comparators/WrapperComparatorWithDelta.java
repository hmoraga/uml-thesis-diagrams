/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.comparators;

import java.util.Comparator;
import kinetic.Wrapper;

/**
 *
 * @author hmoraga
 */
public class WrapperComparatorWithDelta implements Comparator<Wrapper>{
    private final double time, delta; //delta se usa en caso de multiples eventos para desempate
    
    public WrapperComparatorWithDelta(double time, double delta) {
        this.time = time;
        this.delta = delta;
    }

    @Override
    public int compare(Wrapper i1, Wrapper i2) {
        DimensionKineticComparatorWithDelta dkc = new DimensionKineticComparatorWithDelta(time, delta);
        
        return dkc.compare(i1.getDimensionKinetic(), i2.getDimensionKinetic());
    }    
}
