/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.comparators;

import java.util.Comparator;
import kinetic.DimensionKinetic;

/**
 *
 * @author hmoraga
 */
public class DimensionKineticComparator implements Comparator<DimensionKinetic>{
    private final double time;
    
    public DimensionKineticComparator(double time) {
        this.time = time;
    }

    @Override
    public int compare(DimensionKinetic i1, DimensionKinetic i2) {
        if (((i1.getV()-i2.getV())*time+(i1.getK()-i2.getK()))<0)
            return -1;
        else if (((i1.getV()-i2.getV())*time+(i1.getK()-i2.getK()))>0)
            return 1;
        else return 0;
    }    
}