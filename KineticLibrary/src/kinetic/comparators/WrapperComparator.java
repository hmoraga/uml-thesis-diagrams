/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.comparators;

import java.util.Comparator;
import kinetic.Wrapper;

/**
 *
 * @author hmoraga
 */
public class WrapperComparator implements Comparator<Wrapper>{
    private final double time; //para ordenar
    
    public WrapperComparator(double time) {
        this.time = time;
    }

    @Override
    public int compare(Wrapper i1, Wrapper i2) {
        DimensionKineticComparator dkc = new DimensionKineticComparator(time);
        
        return dkc.compare(i1.getDimensionKinetic(), i2.getDimensionKinetic());
    }    
}
