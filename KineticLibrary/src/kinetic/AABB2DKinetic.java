package kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.List;
import primitives.AABB2D;
import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class AABB2DKinetic {
    private Pair<DimensionKinetic> p, q;

    public AABB2DKinetic(List<Point2D> listaPuntos, Pair<Double> velocidades){
        assert (listaPuntos.size()>0);
        double minX=listaPuntos.get(0).getX(), minY=listaPuntos.get(0).getY();
        double maxX=listaPuntos.get(0).getX(), maxY=listaPuntos.get(0).getY();
        
        // dada la lista de puntos estaticos genero el AABB2DKinetico del objeto
        for (Point2D punto : listaPuntos) {
            if (punto.getX() < minX)
                minX = punto.getX();
            
            if (punto.getX() > maxX)
                maxX = punto.getX();
            
            if (punto.getY() < minY)
                minY = punto.getY();
            
            if (punto.getY() > maxY)
                maxY = punto.getY();            
        }

        // busco el punto inferior izq y el superior derecho de la caja
        p = new Pair<>(new DimensionKinetic(velocidades.first, minX),
                new DimensionKinetic(velocidades.second, minY));
        
        q = new Pair<>(new DimensionKinetic(velocidades.first, maxX),
                new DimensionKinetic(velocidades.second, maxY));
        
        assert(p.first.getK() < q.first.getK());
        assert(p.second.getK() < q.second.getK());
    }
    
    public double getVx() {
        return p.first.getV();
    }

    public void setVx(double vx) {
        p = new Pair<>(new DimensionKinetic(vx, p.first.getK()),
                new DimensionKinetic(p.second.getV(), p.second.getK()));
        
        q = new Pair<>(new DimensionKinetic(vx, q.first.getK()),
                new DimensionKinetic(q.second.getV(), q.second.getK()));
    }

    public double getVy() {
        return p.second.getV();
    }

    public void setVy(double vy) {
        p = new Pair<>(new DimensionKinetic(p.first.getV(), p.first.getK()),
                new DimensionKinetic(vy, p.second.getK()));
        
        q = new Pair<>(new DimensionKinetic(q.first.getV(), q.first.getK()),
                new DimensionKinetic(vy, q.second.getK()));
    }
    
    public AABB2D getBox(double time){
        // consulto el valor de p y q en e tiempo time y de ahi calculo la caja
        Point2D pt, qt;
        
        pt = new Point2D(p.first.getPosition(time), p.second.getPosition(time));
        qt = new Point2D(q.first.getPosition(time), q.second.getPosition(time));
        
        return new AABB2D(new Point2D((pt.getX()+qt.getX())/2, (pt.getY()+qt.getY())/2),
                Math.abs(pt.getX()-qt.getX()), Math.abs(pt.getY()-qt.getY()));
    }
    
    public Interval getInterval(int dimension){
        return (dimension==0)?new Interval(p.first, q.first):
                new Interval(p.second, q.second);
    }
    
    public Pair<DimensionKinetic> getP(){
        return p;
    }
    
    public Pair<DimensionKinetic> getQ(){
        return q;    
    }
}
