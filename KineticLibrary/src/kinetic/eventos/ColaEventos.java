/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.eventos;

import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.PriorityQueue;
import primitives.Pair;

/**
 *
 * @author hmoraga
 */
public class ColaEventos extends Observable{
    private PriorityQueue<Evento> colaEventos;
    private double tpo_inicial, tpo_final, tpo_actual;
    private boolean verbose;
    private double minimalTimeStep;
    /**
     * 
     * @param tiempos un par [t0, tfin] de la simulacion
     * @param verbose si true imprime mensajes por pantalla
     */
    public ColaEventos(Pair<Double> tiempos, boolean verbose){
        this(tiempos, 11, verbose);
    }

    public ColaEventos(Pair<Double> tiempos, int cantidadInicial, boolean verbose){
        this.tpo_inicial = tiempos.first;
        this.tpo_actual = tiempos.first;
        this.tpo_final = tiempos.second;
        this.colaEventos = new PriorityQueue<>(cantidadInicial);
        this.verbose = verbose;
        this.minimalTimeStep = Double.POSITIVE_INFINITY;
    }
    
    public void calcularEventosIniciales(){
        super.setChanged();
        super.notifyObservers(EnumerateEvents.CALCULAREVENTOSINICIALES);
    }
    
    /**
     * método para calcular eventos desde el tiempo actual hasta el tfinal. Se
     * usa para cuando invalido la listaKSL y la de colisiones.
     * @param includeCurrentTime true si se debe incluir el tiempo actual o no
     */
    public void calcularEventosActuales(boolean includeCurrentTime){
        super.setChanged();
        super.notifyObservers(EnumerateEvents.CALCULAREVENTOSACTUALES);   
    }
    
    public double getTpo_inicial() {
        return tpo_inicial;
    }

    public double getTpo_final() {
        return tpo_final;
    }

    public double getTpo_actual() {
        return tpo_actual;
    }

    public void setTpo_actual(double tpo_actual) {
        this.tpo_actual = tpo_actual;
    }

    public double getMinimalTimeStep() {
        return minimalTimeStep;
    }

    public void setMinimalTimeStep(double minimalTimeStep) {
        this.minimalTimeStep = minimalTimeStep;
    }

    public boolean add(Evento e) {
        return colaEventos.add(e);
    }

    public Evento peek() {
        return colaEventos.peek();
    }

    public Evento poll() {
        return colaEventos.poll();
    }

    /**
     * metodo para procesar los eventos de la cola
     */
    public void procesarEventos(){
        int contador = 0;
        // se supone que existe una lista de colisiones inicial, 
        while (!colaEventos.isEmpty()){
            if (verbose)
                System.out.println("Evento " + (++contador));

            // asumo que todos los eventos son simples (a menos que el actuador diga
            // lo contrario)
            super.setChanged();
            super.notifyObservers(EnumerateEvents.EVENTOSIMPLE);
        }
    }
    
    /*
     * Revisa si al sacar el elemento al tope de la pila, la lista
     * cinética tiene 3 o más objetos en la misma posicion al mismo tiempo.
     * 
     * @param listaKSL lista de KineticSortedList
     * @return retorna true si existen eventos multiples y false en caso
     * contrario.
     *
     *public boolean existenEventosMultiples(ListaKSL listaKSL){
        // reviso el evento al tope de la pila y reviso si en la ksl hay 
        // valores multiples (mas de 2) por lo que ahí le pido resolver al actuador
        int dimension = peek().getDimension();
        
        KineticSortedList ksl = listaKSL.get(dimension);
        
        return  (ksl.testIfMultiplesEventos(peek()));
    }*/

    /**
     * 
     * @param o 
     */
    public void setObserver(Observer o){
        super.addObserver(o);
    }    
    
    /**
     * 
     * @param masEventos
     * @param dimension 
     */
    public void addAll(List<SubEventoSwap> masEventos, int dimension) {
        masEventos.stream().filter((subEv) -> (subEv.getTime()> tpo_actual && subEv.getTime()<tpo_final)).forEach((subEv) -> {
            colaEventos.add(new EventoSwap(subEv.getId0(), subEv.getId1(),subEv.getTime(), dimension));
        });
    }
    
    public boolean isEmpty(){
        return colaEventos.isEmpty();
    }
    
    /**
     * 
     * @param time
     * @param idMin
     * @param idMax
     * @param dimension 
     */
    public void eliminarMultiplesEventos(double time, int idMin, int idMax, int dimension){
        Iterator<Evento> it = colaEventos.iterator();
        
        while (it.hasNext()){
            EventoSwap ev = (EventoSwap)it.next();
            
            if ((ev.getTime() == time) && (ev.getDimension() == dimension)){
                if ((idMin<=ev.getId0() && ev.getId0()<=idMax) ||
                        (idMin<=ev.getId1() && ev.getId1()<=idMax))
                    it.remove();
            } else if (ev.getTime() > time)
                break;
        }        
    }

    public void clear() {
        super.setChanged();
        super.notifyObservers(EnumerateEvents.CLEARALL);
    }
}