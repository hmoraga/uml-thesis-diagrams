/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.eventos;

/**
 *
 * @author hmoraga
 */
public class EventoSwap extends Evento{
    private final int id0, id1; // ids corresponden a LAS POSICIONES en la KSL

    public EventoSwap(int id0, int id1, double time, int dimension) {
        super(time, dimension);
        this.id0 = id0;
        this.id1 = id1;
    }

    public int getId0() {
        return id0;
    }

    public int getId1() {
        return id1;
    }
    
    @Override
    public int compareTo(Evento o) throws NullPointerException{
        EventoSwap ev = (EventoSwap)o;

        if (ev == null) throw new NullPointerException("No es evento Swap");
        
        if ((this.time<o.time) || 
                ((this.time==o.time) && (this.dimension < o.dimension)) ||
                ((this.time==o.time) && (this.dimension == o.dimension) &&
                (Math.min(this.id0, this.id1) < Math.min(ev.getId0(), ev.getId1()))))
        {
            return -1;
        } else if ((this.time>o.time) || 
                ((this.time==o.time) && (this.dimension > o.dimension)) ||
                ((this.time==o.time) && (this.dimension == o.dimension) &&
                (Math.min(this.id0, this.id1) > Math.min(ev.getId0(), ev.getId1()))))
        {
            return 1;
        } else return 0;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + this.id0;
        hash = 43 * hash + this.id1;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final EventoSwap other = (EventoSwap) obj;
        
        return (this.time == other.time) && (this.dimension == other.dimension) &&
                (((this.id0 == other.id0) && (this.id1 == other.id1)) ||
                ((this.id0 == other.id1) && (this.id1 == other.id0)));
    }

    @Override
    public String toString() {
        return "EV(t=" + time +")" + id0 + "_" + id1 + ((dimension==0)?"X":"Y");
    }
}
