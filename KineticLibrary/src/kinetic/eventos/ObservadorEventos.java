/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.eventos;

import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import kinetic.ListaIntersecciones;
import kinetic.ListaKSL;
import kinetic.actuators.Actuador;
import kinetic.actuators.KineticStatistics;

/**
 *
 * @author hmoraga
 */
public class ObservadorEventos implements Observer{
    private ListaKSL listaKSL;
    private ListaIntersecciones colisiones;
    private ColaEventos colaEventos;
    private KineticStatistics estadisticas;
    private boolean verbose;
    
    /**
     * 
     * @param listaKSL
     * @param colisionesPorDimension
     * @param colaEventos
     * @param estadisticas
     * @param verbose 
     */
    public ObservadorEventos(ListaKSL listaKSL, ListaIntersecciones colisionesPorDimension, ColaEventos colaEventos, 
            KineticStatistics estadisticas, boolean verbose) {
        this.listaKSL = listaKSL;
        this.colisiones = colisionesPorDimension;
        this.colaEventos = colaEventos;
        this.estadisticas = estadisticas;
        this.verbose = verbose;
    }
    
    /**
     * 
     * @param o objeto observable
     * @param arg 
     */
    @Override
    public void update(Observable o, Object arg) {
        if (arg.equals(EnumerateEvents.CALCULAREVENTOSINICIALES)){
            Actuador.inicializarEstructuras(colaEventos, listaKSL, colisiones, estadisticas, verbose);
        } else if (arg.equals(EnumerateEvents.EVENTOSIMPLE)){
            // reviso si la listaKSL tiene eventos simples o son multiples
            // para eso pregunto por el evento de la cola y reviso si la
            // listaKSL tiene un evento multiple
            Evento ev = colaEventos.peek();
            
            if (!listaKSL.existeMultiplesEventos(ev)){
                // saco el evento de la cola y hago el swap respectivo en la KSL
                Actuador.resolverEventoSimple(colaEventos, listaKSL, colisiones, estadisticas, verbose);
            } else {
                try {
                    //evento multiple
                    Actuador.resolverEventoMultiple(colaEventos, listaKSL, colisiones, estadisticas, verbose);
                } catch (Exception ex) {
                    Logger.getLogger(ObservadorEventos.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
