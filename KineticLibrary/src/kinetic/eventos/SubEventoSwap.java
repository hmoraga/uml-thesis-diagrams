/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.eventos;

/**
 *
 * @author hmoraga
 */
public class SubEventoSwap {
    public double time;
    public int id0, id1;

    public SubEventoSwap(double time, int id0, int id1) {
        this.time = time;
        this.id0 = id0;
        this.id1 = id1;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public int getId0() {
        return id0;
    }

    public void setId0(int id0) {
        this.id0 = id0;
    }

    public int getId1() {
        return id1;
    }

    public void setId1(int id1) {
        this.id1 = id1;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.time) ^ (Double.doubleToLongBits(this.time) >>> 32));
        hash = 23 * hash + this.id0;
        hash = 23 * hash + this.id1;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SubEventoSwap other = (SubEventoSwap) obj;
        if (Double.doubleToLongBits(this.time) != Double.doubleToLongBits(other.time)) {
            return false;
        }
        if (this.id0 != other.id0) {
            return false;
        }
        if (this.id1 != other.id1) {
            return false;
        }
        return true;
    }
}
