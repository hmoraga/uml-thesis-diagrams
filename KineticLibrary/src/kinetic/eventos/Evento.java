/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.eventos;

/**
 *
 * @author hmoraga
 */
public abstract class Evento implements Comparable<Evento>{
    protected double time;
    protected int dimension;

    public Evento(double time, int dimension) {
        this.time = time;
        this.dimension = dimension;
    }

    public double getTime() {
        return time;
    }

    public int getDimension() {
        return dimension;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }
}
