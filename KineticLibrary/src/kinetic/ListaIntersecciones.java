package kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import primitives.Pair;

/**
 *
 * @author hmoraga
 */
public class ListaIntersecciones extends Observable{
    private List<Intersecciones> lista;
    
    public ListaIntersecciones() {
        lista = new LinkedList<>();
    }

    public ListaIntersecciones(Collection<? extends Intersecciones> c) {
        lista = new LinkedList<>(c);
    }

    public void clear() {
        lista.clear(); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean add(Intersecciones e) {
        return lista.add(e); //To change body of generated methods, choose Tools | Templates.
    }

    public Intersecciones getColisiones(){
        int menor = 0;
        
        //encontrar lista con menos elementos
        for (int i=0; i < lista.size(); i++){
            if (lista.get(i).size() < lista.get(menor).size())
                menor = i;
        }
        
        // separo momentaneamente la lista de menos elementos del resto
        Intersecciones listaMenor = lista.remove(menor);
        Intersecciones salida = new Intersecciones();
        
        // reviso el resto de las listas con la seleccionada
        for (Intersecciones otras:lista) {
            for (Pair<Integer> par : listaMenor) {
                if (otras.contains(par) || otras.contains(par.inverted()))
                    salida.add(par);
            }
        }

        // vuelvo this al esto original
        lista.add(menor, listaMenor);
        
        return salida;
    }

    public void setObserver(Observer o) {
        super.addObserver(o);
    }

    public Intersecciones get(int dimension) {
        return lista.get(dimension);
    }

    public int size() {
        return lista.size();
    }

    @Override
    public String toString() {
        return lista.toString();
    }
    
    
}
