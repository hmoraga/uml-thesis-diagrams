package kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import kinetic.eventos.Evento;

/**
 *
 * @author hmoraga
 */
public class ListaKSL extends Observable{
    private List<KineticSortedList> listaKSL;
    
    public ListaKSL() {
        listaKSL = new ArrayList<>();
    }

    public ListaKSL(Collection<? extends KineticSortedList> c) {
        listaKSL = new ArrayList<>(c);
    }

    public ListaKSL(ListaKSL lista) {
        this.listaKSL = lista.listaKSL;
    }

    public void clear() {
        listaKSL.clear();
    }

    public boolean add(KineticSortedList e) {
        return listaKSL.add(e);
    }
    
    public Intersecciones obtenerIntersecciones(){
        ListaIntersecciones listaTemp = new ListaIntersecciones();
        
        for (KineticSortedList ksl: listaKSL)
            listaTemp.add(ksl.obtenerIntersecciones());
        
        return listaTemp.getColisiones();
    }

    public boolean isEmpty() {
        return listaKSL.isEmpty();
    }

    public int size() {
        return listaKSL.size();
    }

    public KineticSortedList get(int i) {
        return listaKSL.get(i);
    }

    public void set(int i, KineticSortedList KSL) {
        listaKSL.set(i, KSL);
    }

    public List<KineticSortedList> getListaKSL() {
        return listaKSL;
    }
    
    public void setObserver(Observer o){
        super.addObserver(o);
    }
    
    public void swap(int idx0, int idx1, int dimension){
        KineticSortedList ksl = listaKSL.get(dimension);
        Wrapper aux = ksl.get(idx0);
        ksl.set(idx0, ksl.get(idx1));
        ksl.set(idx1, aux);
    }
    
    public boolean existeMultiplesEventos(Evento ev){
        return listaKSL.get(ev.getDimension()).testIfMultiplesEventos(ev);
    }
}
