package kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Objects;

/**
 *
 * @author hmoraga
 */
public class Wrapper {
    private DimensionKinetic pos;   // posicion 
    private int objectId;
    private boolean maximum;  // whether the point is the box extrema or not

    public Wrapper(DimensionKinetic p, int objectId, boolean isMaximum) {
        this.pos = p;
        this.objectId = objectId;
        this.maximum = isMaximum;
    }

    public DimensionKinetic getDimensionKinetic() {
        return pos;
    }

    public void setDimensionKinetic(DimensionKinetic p) {
        this.pos = p;
    }

    public int getObjectId() {
        return objectId;
    }

    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }

    public boolean isMaximum() {
        return maximum;
    }

    public void setIsMaximum(boolean maximum) {
        this.maximum = maximum;
    }

    @Override
    public String toString() {
        return "Wrapper{" + "pos=" + pos.toString() + ", objectId=" + objectId + ", isMaximum=" + maximum + '}';
    }
    
    public double getEventTime(Wrapper other){
        return (pos.getIntersection(other.getDimensionKinetic()));
    }
    
    /*    public double getBorderTime(double border){
        return getDimensionKinetic().getBorderTime(border);
    } */

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.pos);
        hash = 47 * hash + this.objectId;
        hash = 47 * hash + (this.maximum ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Wrapper other = (Wrapper) obj;
        if (!Objects.equals(this.pos, other.pos)) {
            return false;
        }
        if (this.objectId != other.objectId) {
            return false;
        }
        if (this.maximum != other.maximum) {
            return false;
        }
        return true;
    }
}
