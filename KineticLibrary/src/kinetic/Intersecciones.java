package kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Collection;
import java.util.LinkedList;
import primitives.Pair;

/**
 *
 * @author hmoraga
 */
public class Intersecciones extends LinkedList<Pair<Integer>>{

    public Intersecciones() {
        super();
    }

    public Intersecciones(Collection<? extends Pair<Integer>> c) {
        super(c);
    }

    @Override
    public boolean add(Pair<Integer> par){
        if (!contains(par) && !contains(par.inverted()))
            return super.add(par);
        return false;
    }
}
