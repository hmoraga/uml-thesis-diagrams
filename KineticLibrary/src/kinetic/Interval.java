package kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Objects;

/**
 *
 * @author hmoraga
 */
public class Interval{    
    public DimensionKinetic minimum, maximum;

    public Interval(DimensionKinetic min, DimensionKinetic max) {
        this.minimum = min;
        this.maximum = max;
    }

    public double getMinimum(double time) {
        return minimum.getPosition(time);
    }

    public void setMinimum(DimensionKinetic minimum) {
        this.minimum = minimum;
    }

    public double getMaximum(double time) {
        return maximum.getPosition(time);
    }

    public void setMaximum(DimensionKinetic maximum) {
        this.maximum = maximum;
    }

    public double getLength(double time){
        return (maximum.getV()-minimum.getV())*time+(maximum.getK()-minimum.getK()); 
    }
    
    public double getCenter(double time){
        return getMinimum(time)+getLength(time)/2;
    }
    
    @Override
    public String toString() {
        return "Interval{" + "minimum=" + minimum.toString() + ", maximum=" + maximum.toString() + '}';
    }
    
    public boolean intersects(Interval other, double time){
        //calculo el largo de ambos intervalos
        double miLargo = getLength(time);
        double otherLargo = other.getLength(time);
        // calculo los centros de ambos intervalos
        double miCentro = getCenter(time);
        double otherCentro = other.getCenter(time);
        
        return ((2*Math.abs(miCentro-otherCentro)-(miLargo+otherLargo))<=0);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.minimum);
        hash = 37 * hash + Objects.hashCode(this.maximum);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Interval other = (Interval) obj;
        if (!Objects.equals(this.minimum, other.minimum)) {
            return false;
        }
        if (!Objects.equals(this.maximum, other.maximum)) {
            return false;
        }
        return true;
    }
}