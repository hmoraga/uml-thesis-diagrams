package kinetic.actuators;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import primitives.Pair;
import sap.Statistics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hmoraga
 */
public class KineticStatistics extends Observable implements Statistics{
    private double areaTotal, areaOcupadaInicial;
    private Pair<Double> intervaloSimulacion;
    // especifico de la simulacion cinetica
    private int eventosSimples, eventosMultiples;
    private int[] colisionesPorObjeto;
    
    /**
     * Constructor con parametros
     * @param totalObjetos cantidad total de objetos 
     */
    public KineticStatistics(int totalObjetos) {
        // ingreso cantidad total de objetos
        colisionesPorObjeto = new int[totalObjetos];
    }

    @Override
    public double getAreaTotal() {
        return areaTotal;
    }

    @Override
    public void setAreaTotal(double areaTotal) {
        this.areaTotal = areaTotal;
    }

    @Override
    public double getTiempoTotalSimulado() {
        return intervaloSimulacion.second-intervaloSimulacion.first;
    }

    @Override
    public void setTiempoTotalSimulado(Pair<Double> intervaloSimulacion) {
        this.intervaloSimulacion = intervaloSimulacion;
    }

    @Override
    public double getAreaOcupadaInicial() {
        return areaOcupadaInicial;
    }

    @Override
    public void setAreaOcupadaInicial(double areaOcupadaInicial) {
        this.areaOcupadaInicial = areaOcupadaInicial;
    }

    public int getEventosSimples() {
        return eventosSimples;
    }

    public void setEventosSimples(int eventosSimples) {
        this.eventosSimples = eventosSimples;
    }

    public int getEventosMultiples() {
        return eventosMultiples;
    }

    public void setEventosMultiples(int eventosMultiples) {
        this.eventosMultiples = eventosMultiples;
    }
    
    /**
     * Ingreso el par de objetos que involucran el evento simple
     * @param par para de valores que corresponden a los objetos originales que 
     * colisionan
     */
    public void addEventoSimple(Pair<Integer> par){
        colisionesPorObjeto[par.first]++;
        colisionesPorObjeto[par.second]++;
        this.eventosSimples++;
    }
    
    public void addEventoMultiple(List<Integer> objectsIdList){
        objectsIdList.stream().forEach((i) -> {
            colisionesPorObjeto[i]++;
        });
        this.eventosMultiples++;
    }
    
    @Override
    public List<String> getText(){
        List<String> s = new ArrayList<>();
        
        s.add("areaTotal:"+areaTotal);
        s.add("tiempoTotalSimulado:"+(intervaloSimulacion.second-intervaloSimulacion.first));
        s.add("areaOcupadaInicial:"+areaOcupadaInicial);
        s.add("eventosSimples:"+eventosSimples);
        s.add("eventosMultiples:"+eventosMultiples);
        s.add("colisionesPorObjeto:"+Arrays.toString(colisionesPorObjeto));
        
        return s;
    }
    
    public void setObserver(Observer o) {
        super.addObserver(o);
    }
    
    public boolean hasMultipleEvents(){
        return (this.eventosMultiples!=0);
    }
    
    public void escribirArchivo(Path arch) throws FileNotFoundException{
        try (Formatter output = new Formatter(arch.toFile())) {
            output.format("%s%n", getText());
        }
    }

    public void calcularEstadisticas(List<Pair<Integer>> colisiones) {
        colisiones.stream().map((par) -> {
            eventosSimples++;
            return par;
        }).map((par) -> {
            colisionesPorObjeto[par.first]++;
            return par;
        }).forEach((par) -> {
            colisionesPorObjeto[par.second]++;
        });
    }
}
