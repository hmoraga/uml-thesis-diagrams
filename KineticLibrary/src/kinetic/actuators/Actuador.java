/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.actuators;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import kinetic.Intersecciones;
import kinetic.KineticSortedList;
import kinetic.ListaIntersecciones;
import kinetic.ListaKSL;
import kinetic.Wrapper;
import kinetic.eventos.ColaEventos;
import kinetic.eventos.EventoSwap;
import kinetic.eventos.SubEventoSwap;
import primitives.Pair;

/**
 *
 * @author hmoraga
 */
public class Actuador {
    private static double actualTime;

/**
 * 
 * @param colaEventos
 * @param listaKSL
 * @param colisiones
 * @param estadisticas
 * @param verbose 
 */    
    public static void inicializarEstructuras(ColaEventos colaEventos, ListaKSL listaKSL, ListaIntersecciones colisiones, 
            KineticStatistics estadisticas, boolean verbose){
        actualTime = colaEventos.getTpo_inicial();
        
        // ordeno los KSL para el valor inicial de la simulacion
        listaKSL.getListaKSL().stream().forEach((ksl) -> {
            ksl.sort(colaEventos.getTpo_inicial(), 0.00001);
        });
         
        for (int dim=0; dim < listaKSL.size(); dim++) {
            List<SubEventoSwap> listaEventosDimSwap = listaKSL.get(dim).calcularEventosSwap(colaEventos.getTpo_inicial(), true);

            //agrego lista de eventos a la cola de eventos
            for (SubEventoSwap subEv : listaEventosDimSwap){
                double time = subEv.getTime();
                        
                // debo revisar si los eventos estan entre ]t_inicial;t_final]
                if ((time>colaEventos.getTpo_inicial()) && (time<colaEventos.getTpo_final())){
                    if (Math.abs(time-actualTime)> colaEventos.getMinimalTimeStep() &&
                            (time!=actualTime)){
                        colaEventos.setMinimalTimeStep(time-actualTime);
                    }
                    colaEventos.add(new EventoSwap(subEv.getId0(), subEv.getId1(), time, dim));
                }
            }
        }
            
        // por cada dimension genero una lista de intersecciones
        listaKSL.getListaKSL().stream().forEach((ksl) -> {
            colisiones.add(ksl.obtenerIntersecciones());
        });
        
        // reviso dentro de la lista de colisiones por dimension si es que
        // existe una colision verdadera
        colisiones.getColisiones().stream().forEach((par) -> {
            estadisticas.addEventoSimple(par);
        });
        
        colaEventos.setMinimalTimeStep(Double.POSITIVE_INFINITY);
    }
    
    /** por defecto todos los eventos son considerados simples y se evalua si el
     *  certificado es multiple.
     * @param colaEventos
     * @param listaKSL
     * @param colisiones
     * @param estadisticas
     * @param verbose
     */  
    public static void resolverEventoSimple(ColaEventos colaEventos, ListaKSL listaKSL, ListaIntersecciones colisiones, 
            KineticStatistics estadisticas, boolean verbose){
        EventoSwap ev = (EventoSwap)colaEventos.poll(); //saco el evento de la cola
        
        if ((Math.abs(ev.getTime()-actualTime) < colaEventos.getMinimalTimeStep()) &&
                (actualTime!=ev.getTime())){
            colaEventos.setMinimalTimeStep(Math.abs(ev.getTime()-actualTime));
        }
        
        actualTime = ev.getTime();
        int dimension = ev.getDimension();
        
        // reviso si el evento es un evento multiple o simple
        if (listaKSL.existeMultiplesEventos(ev)){
            try {
                colaEventos.add(ev);
                Actuador.resolverEventoMultiple(colaEventos, listaKSL, colisiones, estadisticas, false);
            } catch (java.lang.Exception ex) {
                Logger.getLogger(Actuador.class.getName()).log(Level.SEVERE, "Problema en metodo resolver Evento Simple!", ex);
            }
        } else {
            if (verbose){
                System.out.println(ev.toString());
                System.out.println("Resolviendo evento simple");
            }

            // obtengo los OBJECT_ID relacionados al evento
            Wrapper w0 = listaKSL.get(dimension).get(ev.getId0()); 
            Wrapper w1 = listaKSL.get(dimension).get(ev.getId1());              
            int o0 = w0.getObjectId();
            int o1 = w1.getObjectId();
            estadisticas.addEventoSimple(new Pair<>(o0,o1));
        
            // si un punto es minimo de un objeto y el otro es maximo,
            // estamos en presencia de: o se agrega un nuevo par de colisiones
            // o se elimina una colision existente
            if ((w0.isMaximum() && !w1.isMaximum()) || (!w0.isMaximum() && w1.isMaximum())){
                // variable auxiliar
                if (verbose)
                    System.out.println("Colisiones:" + colisiones.toString());
                
                Intersecciones aux = colisiones.get(dimension);

                if (aux.contains(new Pair<>(o0, o1)))
                    aux.remove(new Pair<>(o0, o1)); // si el par de objetos existe, se borra de la lista
                else if (aux.contains(new Pair<>(o1, o0)))
                    aux.remove(new Pair<>(o1, o0)); // si el par de objetos existe, se borra de la lista
                else {
                    aux.add(new Pair<>(Math.min(o0, o1), Math.max(o0, o1))); // agregar el par

                    // reviso dentro de la lista de colisiones por dimension si es que
                    // existe una colision verdadera
                    //Intersecciones colisionesReales = colisiones.getColisiones();
                }
                aux.clear();    // limpio
            }
            
            // realizar el swap
            listaKSL.swap(ev.getId0(), ev.getId1(), dimension);

            // actualizo el tiempo actual de la cola de eventos y calculo una nueva
            colaEventos.setTpo_actual(ev.getTime());
        
            // lista de eventos de los vecinos (nuevos eventos)
            List<SubEventoSwap> masEventos=listaKSL.get(dimension).calcularEventosVecinos(ev.getId0(), ev.getId1());
            colaEventos.addAll(masEventos, dimension);
            
            masEventos.clear(); // limpiar la coleccion
        }
    }
    
    /**
     * 
     * @param colaEventos
     * @param listaKSL
     * @param colisiones
     * @param estadisticas
     * @param verbose
     * @throws Exception 
     */
    public static void resolverEventoMultiple(ColaEventos colaEventos, ListaKSL listaKSL, ListaIntersecciones colisiones, 
            KineticStatistics estadisticas, boolean verbose)
    throws Exception {
        if (verbose)
            System.out.println("Resolviendo evento multiple");
        // se invalida la KSL respectiva y los eventos asociados a ella
        // obtener el rango de indices de los eventos a eliminar
        EventoSwap ev = (EventoSwap)colaEventos.poll();
        
        int dimension = ev.getDimension();
        double time = ev.getTime();
        
        KineticSortedList ksl = listaKSL.get(dimension);

        // obtengo el rango de elementos a eliminar
        Pair<Integer> rangeToDelete = ksl.obtenerRangoEliminar(time);
        estadisticas.addEventoMultiple(Actuador.rango(ksl, rangeToDelete.first,rangeToDelete.second));
        
        // elimino los elementos de la cola de eventos que estan dentro del rango
        colaEventos.eliminarMultiplesEventos(time, rangeToDelete.first, rangeToDelete.second, dimension);
        
        // Resuelvo en la ksl respectiva el orden de los objetos cineticos despues
        // del evento multiple. Para eso se evalua con un delta muy pequeño, el
        // nuevo orden.
        ksl.insertionSort(rangeToDelete.first,rangeToDelete.second, time, 0.00001);
        
        // reviso si la ksl actual es valida ahora
        if (!ksl.validarCertificado(time)){
            throw new Exception("KSL inválida!");
        }
        
        // calculo los nuevos eventos entre los extremos del intervalo
        List<SubEventoSwap> eventosNuevos = ksl.calcularEventosVecinos(rangeToDelete.first, rangeToDelete.second);
        
        // actualizo el tiempo en la cola de eventos
        colaEventos.setTpo_actual(time);
        
        // agrego esos eventos nuevos a la cola de eventos
        colaEventos.addAll(eventosNuevos, dimension);
        
        eventosNuevos.clear(); // limpio la coleccion
    }
    
    /**
     * Del rango de elementos de la KSL, se deben obtener los objectId
     * y esa lista es la que se debe devolver!!!
     * @param ksl
     * @param inicio posicion de inicio en la KSL
     * @param fin posicion de final en la KSL
     * @return lista de ObjectId
     */
    public static List<Integer> rango(KineticSortedList ksl, int inicio, int fin){
        List<Integer> resultado = new ArrayList<>();
        
        for (int i = inicio; i <= fin; i++) {
            Integer objId = ksl.get(i).getObjectId();
            
            if (!resultado.contains(objId))
                resultado.add(objId);
        }
        
        return resultado;
    }
}
