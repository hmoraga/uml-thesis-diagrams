package kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import java.util.List;
import poligons.AnyPolygon2D;
import poligons.Polygon2D;
import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class Object2D {
    private final int id;
    private List<Point2D> listaPuntos;
    private Pair<Double> velocidad;
    private double actualTime;
    private int idMinX, idMinY, idMaxX, idMaxY;

    /**
     * 
     * @param id
     * @param listaPuntos
     * @param velocidad 
     * @param time 
     */
    public Object2D(int id, List<Point2D> listaPuntos, 
            Pair<Double> velocidad, double time) {        
        if (time<0) throw new IllegalArgumentException("Tiempo debe ser mayor o igual a 0!");
        idMinX = 0;
        idMinY = 0;
        idMaxX = 0;
        idMaxY = 0;
        
        this.id = id;
        this.actualTime = time;
        
        this.listaPuntos = new ArrayList<>(listaPuntos);
        this.velocidad = velocidad;
        
        for (int i=0; i< listaPuntos.size(); i++) {
            Point2D punto = listaPuntos.get(i);
            
            if (punto.getX() < listaPuntos.get(idMinX).getX())
                idMinX = i;
            
            if (punto.getX() > listaPuntos.get(idMaxX).getX())
                idMaxX = i;
            
            if (punto.getY() < listaPuntos.get(idMinY).getY())
                idMinY = i;
            
            if (punto.getY() > listaPuntos.get(idMaxY).getY())
                idMaxY = i;
        }
    }

    public Object2D(int id, List<Point2D> listaPuntos, 
            Pair<Double> velocidad) {
        this(id, listaPuntos, velocidad, 0.0);
    }
    
    /** me devuelve la lista de puntos en la posicion actual
     * 
     * @return 
     */
    public List<Point2D> getListaPuntos() {
        return listaPuntos;
    }

    public Pair<Double> getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(Pair<Double> velocidad) {
        this.velocidad = velocidad;
    }

    public int getObjId() {
        return id;
    }
    
    public List<Wrapper> getWrappers(int dim){
        List<Wrapper> temp = new ArrayList<>();
        
        Wrapper p, q;
        
        if (dim==0){
            p = new Wrapper(new DimensionKinetic(velocidad.first, listaPuntos.get(idMinX).getX()), id, false);
            q = new Wrapper(new DimensionKinetic(velocidad.first, listaPuntos.get(idMaxX).getX()), id, true);
        } else {
            p = new Wrapper(new DimensionKinetic(velocidad.second, listaPuntos.get(idMinY).getY()), id, false);
            q = new Wrapper(new DimensionKinetic(velocidad.second, listaPuntos.get(idMaxY).getY()), id, true);
        }

        temp.add(p);
        temp.add(q);
        
        return temp;
    }

    public List<Point2D> getPosition(double time) {
        List<Point2D> resultado = new ArrayList<>(listaPuntos.size());
        
        for (Point2D pto : listaPuntos) {
            Point2D p = new Point2D(pto.getX(), pto.getY());
            p.add(velocidad.first*(time-actualTime),velocidad.second*(time-actualTime));
            resultado.add(p);
        }
        
        return resultado;
    }
    
    public void setActualTime(double time){
        this.actualTime = time;
    }
    
    public void close(){
        listaPuntos.clear();
        velocidad = null;
    }
    
    public double getArea(){
        Polygon2D poly = new AnyPolygon2D(listaPuntos, false);
        
        return poly.getArea();
    }
}
