package kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import primitives.Pair;
import primitives.Point2D;


/**
 *
 * @author hmoraga
 */
public class Point2DKinetic {
    private DimensionKinetic[] kinetics;

    public Point2DKinetic(double vx, double kx, double vy, double ky) {
        kinetics = new DimensionKinetic[]{new DimensionKinetic(vx, kx), new DimensionKinetic(vy, ky)};
    }
    
    public Point2DKinetic(DimensionKinetic dimX, DimensionKinetic dimY) {
        kinetics = new DimensionKinetic[]{dimX, dimY};
    }

    public Point2D getPosition(double time){
        return new Point2D(kinetics[0].getPosition(time), kinetics[1].getPosition(time));
    }

    public void setPosition(double time){
        for (DimensionKinetic kin : kinetics) {
            kin.setK(kin.getPosition(time));
        }
    }

    public void setVelocidad(Pair<Double> velocidades){
        kinetics[0].setV(velocidades.first);
        kinetics[1].setV(velocidades.second);
    }
    
    public Pair<Double> getVelocidad(){
        return new Pair<>(kinetics[0].getV(), kinetics[1].getV());
    }
}
