package kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Collection;
import java.util.LinkedList;

/**
 *
 * @author hmoraga
 */
public class IntervalList extends LinkedList<Interval>{

    public IntervalList() {
        super();
    }

    public IntervalList(Collection<? extends Interval> c) {
        super(c);
    }
    
    public void insertionSort(double time){
        for (int i=1;i<this.size(); i++){
            Interval x = this.get(i);
            int j = i;
            
            while ((j>0) && (this.get(j-1).getCenter(time)> x.getCenter(time))){
                set(j, get(j-1));
                j--;
            }
            
            this.set(j,x);
        }
    }
}

