package kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author hmoraga
 */
public class DimensionKinetic{
    private double v, k;    // x(t) = v*t+k, v = velocidad, k = pos inicial en t0

    public DimensionKinetic(double v, double k) {
        this.v = v;
        this.k = k;
    }
    
    public double getPosition(double t){
        return v*t+k;
    }

    public double getV() {
        return v;
    }

    public void setV(double v) {
        this.v = v;
    }

    public double getK() {
        return k;
    }

    public void setK(double k) {
        this.k = k;
    }
    
    public double getIntersection(DimensionKinetic other){
        return (this.v == other.getV())?Double.POSITIVE_INFINITY:
                (k-other.getK())/(other.getV()-v);
    }

    @Override
    public String toString() {
        return "(v=" + v + ", k=" + k + ')';
    }
    
    public double getBorderTime(double border){
        if (this.v!=0){
            return (border-this.k)/this.v;
        } else {
            return Double.POSITIVE_INFINITY;
        }
    }
 
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.v) ^ (Double.doubleToLongBits(this.v) >>> 32));
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.k) ^ (Double.doubleToLongBits(this.k) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DimensionKinetic other = (DimensionKinetic) obj;
        if (Double.doubleToLongBits(this.v) != Double.doubleToLongBits(other.v)) {
            return false;
        }
        return Double.doubleToLongBits(this.k) == Double.doubleToLongBits(other.k);
    }
}
