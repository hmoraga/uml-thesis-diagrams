/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class WrapperTest {
    
    public WrapperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetObjectId() {
        System.out.println("getObjectId");
        Wrapper instance = new Wrapper(new DimensionKinetic(1.5, 2.3), 1, false);
        int expResult = 1;
        int result = instance.getObjectId();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetObjectId() {
        System.out.println("setObjectId");
        int objectId = 0;
        Wrapper instance = new Wrapper(new DimensionKinetic(1.5, 2.3), 1, false);
        instance.setObjectId(objectId);
        assertEquals(instance.getObjectId(), objectId);
    }

    @Test
    public void testIsMaximum() {
        System.out.println("isMaximum");
        Wrapper instance = new Wrapper(new DimensionKinetic(1.5, 2.3), 1, false);
        boolean expResult = false;
        boolean result = instance.isMaximum();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetMaximum() {
        System.out.println("setMaximum");
        boolean isMaximum = true;
        Wrapper instance = new Wrapper(new DimensionKinetic(1.5, 2.3), 1, false);
        instance.setIsMaximum(isMaximum);
        assertEquals(instance.isMaximum(), isMaximum);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        Wrapper instance = new Wrapper(new DimensionKinetic(1.5, 2.3), 1, false);
        String expResult = "Wrapper{pos=(v=1.5, k=2.3), objectId=1, isMaximum=false}";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetEventTime() {
        System.out.println("getEventTime");
        Wrapper other = new Wrapper(new DimensionKinetic(1.5, 2.3), 1, false);
        Wrapper instance = new Wrapper(new DimensionKinetic(-2.5, 3.7), 0, true);
        double expResult = 0.35;
        double result = instance.getEventTime(other);
        assertEquals(expResult, result, 0.00001);
    }

    @Test
    public void testGetDimensionKinetic() {
        System.out.println("getDimensionKinetic");
        Wrapper instance = new Wrapper(new DimensionKinetic(3.0, 5.0), 0, true);
        DimensionKinetic expResult = new DimensionKinetic(3.0, 5.0);
        DimensionKinetic result = instance.getDimensionKinetic();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetDimensionKinetic() {
        System.out.println("setDimensionKinetic");
        DimensionKinetic p = new DimensionKinetic(0.7, -2.3);
        Wrapper instance = new Wrapper(new DimensionKinetic(-0.7, 12.3), 0, false);
        instance.setDimensionKinetic(p);
        assertTrue(instance.getDimensionKinetic().equals(p));
    }

    @Test
    public void testSetIsMaximum() {
        System.out.println("setIsMaximum");
        boolean maximum = false;
        Wrapper instance = new Wrapper(new DimensionKinetic(-3.4, 5.7), 1, true);
        instance.setIsMaximum(maximum);
        assertFalse(instance.isMaximum());
    }

/*    @Test
    public void testGetBorderTime() {
        System.out.println("getBorderTime");
        double border = 0.0;
        Wrapper instance = new Wrapper(new DimensionKinetic(1.0, -4.0), 0, false);
        double expResult = 4.0;
        double result = instance.getBorderTime(border);
        assertEquals(expResult, result, 0.0);
    } */

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Wrapper instance = new Wrapper(new DimensionKinetic(1.0, -4.0), 0, false);
        int expResult = 0;
        int result = instance.hashCode();
        assertFalse(expResult==result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Wrapper(new DimensionKinetic(1.0, -4.0), 0, false);
        Wrapper instance0 = new Wrapper(new DimensionKinetic(1.0, -4.0), 0, false);
        Wrapper instance1 = new Wrapper(new DimensionKinetic(1.0, -4.0), 0, true);
        Wrapper instance2 = new Wrapper(new DimensionKinetic(1.0, -4.0), 1, false);
        Wrapper instance3 = new Wrapper(new DimensionKinetic(1.0, 4.0), 0, false);
        boolean expResult0 = true;
        boolean expResult1 = false;
        boolean expResult2 = false;
        boolean expResult3 = false;
        boolean result0 = instance0.equals(obj);
        boolean result1 = instance1.equals(obj);
        boolean result2 = instance2.equals(obj);
        boolean result3 = instance3.equals(obj);
        
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);        
    }
}
