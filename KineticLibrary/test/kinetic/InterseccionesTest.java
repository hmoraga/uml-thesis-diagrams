/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import primitives.Pair;

/**
 *
 * @author hmoraga
 */
public class InterseccionesTest {
    public Pair<Integer> par0, par1, par2, par3, par4;
    public Intersecciones listaX, listaY, listaZ;
    
    
    public InterseccionesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        par0 = new Pair<Integer>(0,1);
        par1 = new Pair<Integer>(0,2);
        par2 = new Pair<Integer>(1,2);
        par3 = new Pair<Integer>(2,0);
        par4 = new Pair<Integer>(1,0);
        
        listaX = new Intersecciones();
        listaY = new Intersecciones();
        listaZ = new Intersecciones();
    }
    
    @After
    public void tearDown() {
        listaX.clear();
        listaY.clear();
        listaZ.clear();
    }

    @Test
    public void testAdd() {
        System.out.println("add");
        listaX.add(par0);   // (0,1)
        listaX.add(par1);   // (0,2)
        
        listaY.add(par0);   // (0,1)
        listaY.add(par2);   // (1,2)
        listaY.add(par3);   // (2,0)

        assertFalse(listaX.add(par3));
        assertTrue(listaX.add(par2));
    }
}
