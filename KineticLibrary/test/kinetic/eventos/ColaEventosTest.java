/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.eventos;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;
import kinetic.DimensionKinetic;
import kinetic.Intersecciones;
import kinetic.KineticSortedList;
import kinetic.ListaIntersecciones;
import kinetic.ListaKSL;
import kinetic.Object2D;
import kinetic.Wrapper;
import kinetic.actuators.KineticStatistics;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class ColaEventosTest {
    ListaKSL KSL;
    ListaIntersecciones listaColisiones;
    Intersecciones colisiones;
    KineticStatistics estadisticas;
    DimensionKinetic px, qx, py, qy;
    double time0, timeF;
    List<Pair<Double>> dims;
    ColaEventos cola;
    ObservadorEventos observer;
    
    
    
    public ColaEventosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        listaColisiones = new ListaIntersecciones();
        colisiones = new Intersecciones();
        
        KSL = new ListaKSL();
        KineticSortedList kslx = new KineticSortedList();
        KineticSortedList ksly = new KineticSortedList();
        kslx.add(new Wrapper(new DimensionKinetic(1.0, -5.0), 0, false));
        kslx.add(new Wrapper(new DimensionKinetic(1.0, -1.0), 0, true));
        kslx.add(new Wrapper(new DimensionKinetic(-2.0, 1.0), 1, false));
        kslx.add(new Wrapper(new DimensionKinetic(-2.0, 3.0), 1, true));
        kslx.add(new Wrapper(new DimensionKinetic(-1.0, -3.0), 2, false));
        kslx.add(new Wrapper(new DimensionKinetic(-1.0, -1.0), 2, true));
        kslx.sort(0.0, 0.00001);
        
        ksly.add(new Wrapper(new DimensionKinetic(-1.0, 1.0), 0, false));
        ksly.add(new Wrapper(new DimensionKinetic(-1.0, 2.0), 0, true));
        ksly.add(new Wrapper(new DimensionKinetic(0.0, -2.5), 1, false));
        ksly.add(new Wrapper(new DimensionKinetic(0.0, 1.5), 1, true));
        ksly.add(new Wrapper(new DimensionKinetic(2.0, -4.0), 2, false));
        ksly.add(new Wrapper(new DimensionKinetic(2.0, -2.0), 2, true));
        ksly.sort(0.0, 0.00001);
        
        KSL.add(kslx);
        KSL.add(ksly);
        
        dims = new ArrayList<>();
        dims.add(new Pair<>(-10.0, 10.0));  // [-10,10] en X
        dims.add(new Pair<>(-10.0, 10.0));  // [-10,10] en Y
        time0 = 0.0;
        timeF = 10.0;

        estadisticas = new KineticStatistics(KSL.get(0).size()/2);
        cola = new ColaEventos(new Pair<>(time0, timeF), false);
        
        observer = new ObservadorEventos(KSL, listaColisiones, cola, estadisticas, false);
        
        KSL.addObserver(observer);
        cola.addObserver(observer);
        listaColisiones.addObserver(observer);
    }
    
    @After
    public void tearDown() {
        KSL.clear();
    }

    @Test
    public void testCalcularEventos() {
        System.out.println("calcularEventos_Mixtos");
        ColaEventos instance = cola;
        instance.calcularEventosIniciales();
        
        Evento ev0 = instance.poll();
        assertTrue(new EventoSwap(4, 5, 0.5, 1).equals(ev0));
        Evento ev1 = instance.poll();
        assertTrue(new EventoSwap(3, 4, 2.0/3.0, 0).equals(ev1));
        Evento ev2 = instance.poll();
        assertTrue(new EventoSwap(0, 1, 0.75, 1).equals(ev2));
        Evento ev3 = instance.poll();
        assertTrue(new EventoSwap(0, 1, 1.0, 0).equals(ev3));
        Evento ev4 = instance.poll();
        assertTrue(new EventoSwap(2, 3, 1.0, 1).equals(ev4));
        assertTrue(instance.isEmpty());
    }

    @Test
    public void testCalcularEventosIniciales() {
        System.out.println("calcularEventosIniciales");
        cola.calcularEventosIniciales();
        
        assertTrue(!cola.isEmpty());
    }

    @Test
    public void testCalcularEventosActuales() {
        System.out.println("calcularEventosActuales");
        boolean includeCurrentTime = true;
        assertTrue(cola.isEmpty());
        
        cola.calcularEventosIniciales();        
        assertFalse(cola.isEmpty());

        cola.setTpo_actual(1.0);
        cola.calcularEventosActuales(includeCurrentTime);
        assertTrue(true);
    }

    @Test
    public void testGetTpo_inicial() {
        System.out.println("getTpo_inicial");
        double expResult = 0.0;
        double result = cola.getTpo_inicial();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testGetTpo_final() {
        System.out.println("getTpo_final");
        double expResult = 10.0;
        double result = cola.getTpo_final();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testGetTpo_actual() {
        System.out.println("getTpo_actual");
        double expResult = 0.0;
        double result = cola.getTpo_actual();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testSetTpo_actual() {
        System.out.println("setTpo_actual");
        double tpo_actual = 1.0;
        cola.setTpo_actual(tpo_actual);
        assertTrue(cola.getTpo_actual()==tpo_actual);
    }

    @Test
    public void testAdd() {
        System.out.println("add");
        Evento e = new EventoSwap(0, 1, 5.3, 1);
        ColaEventos instance = cola;
        boolean expResult = true;
        boolean result = instance.add(e);
        assertEquals(expResult, result);
    }

    @Test
    public void testPeek() {
        System.out.println("peek");
        ColaEventos instance = cola;
        cola.calcularEventosIniciales();
        
        Evento expResult = new EventoSwap(4, 5, 0.5, 1);
        Evento result = instance.peek();
        assertEquals(expResult, result);
    }

    @Test
    public void testPoll() {
        System.out.println("poll");
        ColaEventos instance = cola;
        cola.calcularEventosIniciales();

        Evento expResult = new EventoSwap(4, 5, 0.5, 1);
        Evento result = instance.poll();
        assertEquals(expResult, result);
    }

    /*@Test
    public void testExistenEventosMultiples() {
        System.out.println("existenEventosMultiples");
        cola.calcularEventosIniciales();
        boolean expResult = false;
        boolean result = cola.existenEventosMultiples(KSL);
        assertEquals(expResult, result);
    }*/

    @Test
    public void testSetObserver() {
        System.out.println("setObserver");
        Observer o = new ObservadorEventos(KSL, listaColisiones, cola, estadisticas, false);
        cola.setObserver(o);
    }

    @Test
    public void testAddAll() {
        System.out.println("addAll");
        List<SubEventoSwap> masEventos = new ArrayList<>();
        masEventos.add(new SubEventoSwap(3.5, 1, 4));
        int dimension = 0;
        ColaEventos instance = cola;
        cola.calcularEventosIniciales();
        instance.addAll(masEventos, dimension);
        
        assertEquals(new EventoSwap(4, 5, 0.5, 1), cola.poll());
        assertEquals(new EventoSwap(3, 4, 2.0/3, 0), cola.poll());
        assertEquals(new EventoSwap(0, 1, 0.75, 1), cola.poll());
        assertEquals(new EventoSwap(0, 1, 1.0, 0), cola.poll());
        assertEquals(new EventoSwap(2, 3, 1.0, 1), cola.poll());
        assertEquals(new EventoSwap(1, 4, 3.5, 0), cola.poll());
        assertTrue(cola.isEmpty());
    }

    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty");
        
        assertTrue(cola.isEmpty());
        
        cola.calcularEventosIniciales();
        
        assertFalse(cola.isEmpty());
    }

    /**
     * Test of procesarEventos method, of class ColaEventos.
     */
    @Test
    public void testProcesarEventos() {
        System.out.println("procesarEventos");
        cola.calcularEventosIniciales();
        cola.procesarEventos();
        
        assertTrue(cola.isEmpty());
    }

    /**
     * Test of eliminarMultiplesEventos method, of class ColaEventos.
     */
    @Test
    public void testEliminarMultiplesEventos() {
        System.out.println("eliminarMultiplesEventos");
        Object2D o0, o1, o2;
        
        List<Point2D> listaPuntos0, listaPuntos1, listaPuntos2;
        Pair<Double> veloc0, veloc1, veloc2;

        listaPuntos0 = new ArrayList<>();
        listaPuntos0.add(new Point2D(-3, 1));
        listaPuntos0.add(new Point2D(-1, 1));
        listaPuntos0.add(new Point2D(-1, 3));
        listaPuntos0.add(new Point2D(-3, 3));

        veloc0 = new Pair<>(1.0,-1.0);
        
        o0 = new Object2D(0, listaPuntos0, veloc0);

        listaPuntos1 = new ArrayList<>();
        listaPuntos1.add(new Point2D(1, 2));
        listaPuntos1.add(new Point2D(4, 2));
        listaPuntos1.add(new Point2D(4, 4));
        listaPuntos1.add(new Point2D(1, 4));

        veloc1 = new Pair<>(-1.0,-2.0);

        o1 = new Object2D(0, listaPuntos1, veloc1);

        listaPuntos2 = new ArrayList<>();
        listaPuntos2.add(new Point2D(-2, -4));
        listaPuntos2.add(new Point2D(-1, -4));
        listaPuntos2.add(new Point2D(-1, -1));
        listaPuntos2.add(new Point2D(-2, -1));

        veloc2 = new Pair<>(1.0,12.0);

        o2 = new Object2D(0, listaPuntos2, veloc2);
        
        KineticSortedList kslx = new KineticSortedList();
        KineticSortedList ksly = new KineticSortedList();
        
        kslx.add(o0, 0);
        kslx.add(o1, 0);
        kslx.add(o2, 0);
        
        ksly.add(o0, 1);
        ksly.add(o1, 1);
        ksly.add(o2, 1);
        
        KSL.clear();
        KSL.add(kslx);
        KSL.add(ksly);
        
        Pair<Double> tiempos = new Pair<>(0.0, 2.0);

        dims = new ArrayList<>();
        dims.add(new Pair<>(-10.0, 10.0));  // [-10,10] en X
        dims.add(new Pair<>(-10.0, 10.0));  // [-10,10] en Y

        estadisticas = new KineticStatistics(KSL.get(0).size()/2);
        cola = new ColaEventos(tiempos, false);
        
        observer = new ObservadorEventos(KSL, listaColisiones, cola, estadisticas, false);
        
        KSL.addObserver(observer);
        cola.addObserver(observer);
        listaColisiones.addObserver(observer);
        estadisticas.addObserver(observer);
        
        double time = 1.0;
        cola.eliminarMultiplesEventos(time, 0, 2, 0);
        
        assertTrue(cola.isEmpty());
    }
}
