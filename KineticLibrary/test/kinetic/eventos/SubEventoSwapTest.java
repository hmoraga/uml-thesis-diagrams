/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.eventos;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class SubEventoSwapTest {
    
    public SubEventoSwapTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetTime() {
        System.out.println("getTime");
        SubEventoSwap instance = new SubEventoSwap(1.5, 3, 4);
        double expResult = 1.5;
        double result = instance.getTime();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testSetTime() {
        System.out.println("setTime");
        double time = 3.6;
        SubEventoSwap instance = new SubEventoSwap(2.7, 1, 2);
        instance.setTime(time);
        assertEquals(instance.getTime(),time, 0.0);
    }

    @Test
    public void testGetId0() {
        System.out.println("getId0");
        SubEventoSwap instance = new SubEventoSwap(1.5, 3, 4);
        int expResult = 3;
        int result = instance.getId0();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetId0() {
        System.out.println("setId0");
        int id0 = 3;
        SubEventoSwap instance = new SubEventoSwap(1.5, 2, 4);
        instance.setId0(id0);
        assertTrue(instance.getId0()==id0);
    }

    @Test
    public void testGetId1() {
        System.out.println("getId1");
        SubEventoSwap instance = new SubEventoSwap(1.5, 3, 4);
        int expResult = 4;
        int result = instance.getId1();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetId1() {
        System.out.println("setId1");
        int id1 = 3;
        SubEventoSwap instance = new SubEventoSwap(1.5, 2, 4);
        instance.setId1(id1);
        assertTrue(instance.getId1()==id1);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        SubEventoSwap instance = new SubEventoSwap(0.4566666, 2, 3);
        int expResult = 0;
        int result = instance.hashCode();
        assertTrue(expResult!=result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new SubEventoSwap(1.5, 4, 3);
        SubEventoSwap instance = new SubEventoSwap(1.5, 3, 4);
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
}
