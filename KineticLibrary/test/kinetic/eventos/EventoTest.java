/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.eventos;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class EventoTest {
    
    public EventoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetTime() {
        System.out.println("getTime");
        Evento instance2 = new EventoSwap(0, 2, 5.678, 0);
        double expResult2 = 5.678;
        double result2 = instance2.getTime();
        
        assertEquals(expResult2, result2, 0.0);
    }

    @Test
    public void testGetDimension() {
        System.out.println("getDimension");
        Evento instance1 = new EventoSwap(1, 3, 2.75, 0);
        int expResult1 = 0;
        int result1 = instance1.getDimension();
        assertEquals(expResult1, result1);
    }

    @Test
    public void testSetTime() {
        System.out.println("setTime");
        double time = 1.0;
        Evento instance = new EventoSwap(0, 1, 0.8, 1);
        instance.setTime(time);
        assertEquals(instance.getTime(), time, 0.0);
    }

    @Test
    public void testSetDimension() {
        System.out.println("setDimension");
        int dimension = 0;
        Evento instance = new EventoSwap(0, 1, 0.8, 1);
        instance.setDimension(dimension);
        assertTrue(instance.getDimension() == dimension);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Evento instance = new EventoSwap(0, 1, 0.8, 1);
        int expResult = 0;
        int result = instance.hashCode();
        assertFalse(expResult==result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj0 = new EventoSwap(0, 1, 0.8, 0);
        Object obj1 = new EventoSwap(0, 1, 0.8, 1);
        Object obj2 = new EventoSwap(0, 1, 0.7, 1);
        Evento instance = new EventoSwap(0, 1, 0.8, 1);
        
        boolean expResult0 = false, expResult1 = true, expResult2 = false;
        boolean result0 = instance.equals(obj0);
        boolean result1 = instance.equals(obj1);
        boolean result2 = instance.equals(obj2);

        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Evento o = new EventoSwap(0, 1, 0.8, 1);
        Evento instance0 = new EventoSwap(0, 1, 0.8, 0);
        Evento instance1 = new EventoSwap(0, 1, 0.9, 1);
        
        int expResult0 = -1, expResult1 = 1;
        int result0 = instance0.compareTo(o);
        int result1 = instance1.compareTo(o);
        
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }
}
