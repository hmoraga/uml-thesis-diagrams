/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.actuators;

import java.util.ArrayList;
import java.util.List;
import kinetic.KineticSortedList;
import kinetic.ListaIntersecciones;
import kinetic.ListaKSL;
import kinetic.Object2D;
import kinetic.eventos.ColaEventos;
import kinetic.eventos.ObservadorEventos;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class ActuadorTest {
    private ColaEventos colaEventos;
    private ListaKSL listaKSL;
    private ListaIntersecciones colisiones;
    private KineticStatistics estadisticas;
    
    public ActuadorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Object2D obj1, obj2, obj3;
        listaKSL = new ListaKSL();
        estadisticas = new KineticStatistics(3);

        List<Point2D> list1= new ArrayList<>(), list2= new ArrayList<>(), list3= new ArrayList<>();
        
        list1.add(new Point2D(3,1));
        list1.add(new Point2D(5,5));
        list1.add(new Point2D(1,4));
        
        list2.add(new Point2D(-4,-3));
        list2.add(new Point2D(-2,-3));
        list2.add(new Point2D(-3,-1));
        
        list3.add(new Point2D(2,-3.5));
        list3.add(new Point2D(5,-5));
        list3.add(new Point2D(5,-2));
        
        obj1 = new Object2D(0, list1, new Pair<>(-1.0,-1.0));
        obj2 = new Object2D(1, list2, new Pair<>(2.0,1.0));
        obj3 = new Object2D(2, list3, new Pair<>(-2.0,1.0));
        
        KineticSortedList kslx = new KineticSortedList();
        KineticSortedList ksly = new KineticSortedList();
        
        kslx.addAll(obj1.getWrappers(0));
        kslx.addAll(obj2.getWrappers(0));
        kslx.addAll(obj3.getWrappers(0));
        kslx.sort(0, 0.00001);
        
        ksly.addAll(obj1.getWrappers(1));
        ksly.addAll(obj2.getWrappers(1));
        ksly.addAll(obj3.getWrappers(1));
        ksly.sort(0, 0.00001);
        
        listaKSL.add(kslx);
        listaKSL.add(ksly);
        
        colaEventos = new ColaEventos(new Pair<>(0.0, 10.0), true);
        
        colisiones = new ListaIntersecciones();

        // inicializo el observer
        ObservadorEventos observador = new ObservadorEventos(listaKSL, colisiones, colaEventos, estadisticas, false);

        // agrego el observer a todos los objetos necesarios
        listaKSL.setObserver(observador);
        colaEventos.setObserver(observador);
        colisiones.setObserver(observador);
        estadisticas.setObserver(observador);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of inicializarEstructuras method, of class Actuador.
     */
    @Test
    public void testInicializarEstructuras() {
        System.out.println("inicializarEstructuras");

        Actuador.inicializarEstructuras(colaEventos, listaKSL, colisiones, estadisticas, false);
        assertTrue(!colaEventos.isEmpty());
        assertTrue(colisiones.size()!=0);
    }

    /**
     * Test of rango method, of class Actuador.
     */
    @Test
    public void testRango() {
        System.out.println("rango");
        KineticSortedList ksl = listaKSL.get(0);
        int inicio = 0;
        int fin = listaKSL.get(0).size()-1;
        List<Integer> expResult = new ArrayList();
        
        // el orden de los objetos ordenados en t=0, en X
        expResult.add(new Integer(1));
        expResult.add(new Integer(0));
        expResult.add(new Integer(2));
        
        List<Integer> result = Actuador.rango(ksl, inicio, fin);
        assertEquals(expResult, result);
    }
    
}
