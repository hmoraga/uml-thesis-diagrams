/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class Point2DKineticTest {
    
    public Point2DKineticTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetPosition() {
        System.out.println("getPoint2D");
        double time = 1.0;
        Point2DKinetic instance = new Point2DKinetic(3.0, 1.0, 1.0, 5.0);
        Point2D expResult = new Point2D(4.0, 6.0);
        Point2D result = instance.getPosition(time);
        assertEquals(expResult, result);
    }

    @Test
    public void testSetPosition() {
        System.out.println("setPosition");
        double time = 2.0;
        Point2DKinetic instance = new Point2DKinetic(3.0, 1.0, -1.0, 5.0);
        instance.setPosition(time);
        // el tiempo (0,0) desde que me movi a la nueva posicion
        assertEquals(instance.getPosition(0.0), new Point2D(7.0,3.0)); 
    }

    @Test
    public void testSetVelocidad() {
        System.out.println("setVelocidad");
        Pair<Double> velocidades = new Pair<Double>(1.5, -2.5);
        Point2DKinetic instance = new Point2DKinetic(3.0, 1.0, -1.0, 5.0);
        instance.setVelocidad(velocidades);
        assertEquals(instance.getVelocidad(), velocidades);
    }

    @Test
    public void testGetVelocidad() {
        System.out.println("getVelocidad");
        Point2DKinetic instance = new Point2DKinetic(3.0, 1.0, -1.0, 5.0);
        Pair<Double> expResult = new Pair<Double>(3.0, -1.0);
        Pair<Double> result = instance.getVelocidad();
        assertEquals(expResult, result);
    }
    
}
