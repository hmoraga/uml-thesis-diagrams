/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class IntervalTest {
    Interval i1, i2, i3, i4, i5, i6;
    
    public IntervalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        i1 = new Interval(new DimensionKinetic(0.0, 1), new DimensionKinetic(0.0, 5));
        i2 = new Interval(new DimensionKinetic(0.0, 3), new DimensionKinetic(0.0, 8));
        i3 = new Interval(new DimensionKinetic(0.0, 8), new DimensionKinetic(0.0, 11));
        i4 = new Interval(new DimensionKinetic(0.0, 1), new DimensionKinetic(0.0, 4));
        i5 = new Interval(new DimensionKinetic(0.0, 7), new DimensionKinetic(0.0, 11));
        i6 = new Interval(new DimensionKinetic(0.0, 4), new DimensionKinetic(0.0, 8));
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetMinimum() {
        System.out.println("getMinimum");
        Interval instance = i1;
        double expResult = 1.0;
        double result = instance.getMinimum(0.0);
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testSetMinimum() {
        System.out.println("setMinimum");
        DimensionKinetic minimum = new DimensionKinetic(2,-3);
        Interval instance = i1;
        instance.setMinimum(minimum);
        double time = 0.0;
        assertEquals(instance.getMinimum(time), minimum.getPosition(time), 0.0);
    }

    @Test
    public void testGetMaximum() {
        System.out.println("getMaximum");
        Interval instance = i2;
        double expResult = 8.0;
        double time = 0.0;
        double result = instance.getMaximum(time);
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testSetMaximum() {
        System.out.println("setMaximum");
        DimensionKinetic maximum = new DimensionKinetic(2,9);
        Interval instance = i2;
        instance.setMaximum(maximum);
        double time = 0.0;
        assertEquals(instance.getMaximum(time), maximum.getPosition(time), 0.0);
    }

    @Test
    public void testGetLength() {
        System.out.println("getLength");
        Interval instance = i3;
        double expResult = 3.0;
        double result = instance.getLength(0.0);
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testGetCenter() {
        System.out.println("getCenter");
        Interval instance = i4;
        double expResult = 2.5;
        double result = instance.getCenter(0.0);
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        Interval instance = i5;
        String expResult = "Interval{minimum=(v=0.0, k=7.0), maximum=(v=0.0, k=11.0)}";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    @Test
    public void testIntersects() {
        System.out.println("intersects");
        boolean expResult1 = true, expResult2 = false, expResult3 = false, expResult4 = true, expResult5 = true;
        boolean result1 = i1.intersects(i2,0.0);
        boolean result2 = i1.intersects(i3,0.0);
        boolean result3 = i4.intersects(i5,0.0);
        boolean result4 = i4.intersects(i1,0.0);
        boolean result5 = i4.intersects(i6,0.0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
        assertEquals(expResult5, result5);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Interval instance = new Interval(new DimensionKinetic(1, 3), new DimensionKinetic(1, 6.5));
        int expResult = 0;
        int result = instance.hashCode();
        assertTrue(expResult!=result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Interval(new DimensionKinetic(1.2, 3.4), new DimensionKinetic(1.2, 4.7));
        Interval instance = new Interval(new DimensionKinetic(1.2, 3.4), new DimensionKinetic(1.2, 4.7));
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
}
