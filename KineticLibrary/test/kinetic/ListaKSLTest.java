/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;
import kinetic.eventos.ObservadorEventos;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import primitives.Pair;

/**
 *
 * @author hmoraga
 */
public class ListaKSLTest {
    public Wrapper p0x, q0x, p0y, q0y, p1x, q1x, p1y, q1y, p2x, q2x, p2y, q2y;
    public KineticSortedList lista0, lista1;
    public ListaKSL listaKSL;
    
    public ListaKSLTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        double t0 = 0.0;
        lista0 = new KineticSortedList();
        lista1 = new KineticSortedList();
        listaKSL = new ListaKSL();
        listaKSL.add(lista0);
        listaKSL.add(lista1);
        
        p0x = new Wrapper(new DimensionKinetic(1.0, -5.0), 0, false);
        q0x = new Wrapper(new DimensionKinetic(1.0, -1.0), 0, true);

        p0y = new Wrapper(new DimensionKinetic(-1.0, 1.0), 0, false);
        q0y = new Wrapper(new DimensionKinetic(-1.0, 2.0), 0, true);
        
        p1x = new Wrapper(new DimensionKinetic(-2.0, 1.0), 1, false);
        q1x = new Wrapper(new DimensionKinetic(-2.0, 3.0), 1, true);
        
        p1y = new Wrapper(new DimensionKinetic(0.0, -2.0), 1, false);
        q1y = new Wrapper(new DimensionKinetic(0.0, 2.0), 1, true);
        
        p2x = new Wrapper(new DimensionKinetic(-1.0, -3.0), 2, false);
        q2x = new Wrapper(new DimensionKinetic(-1.0, -1.0), 2, true);
        
        p2y = new Wrapper(new DimensionKinetic(2.0, -4.0), 2, false);
        q2y = new Wrapper(new DimensionKinetic(2.0, -2.0), 2, true);
        
        lista0.add(p0x);
        lista0.add(q0x);
        lista0.add(p1x);
        lista0.add(q1x);
        lista0.add(p2x);
        lista0.add(q2x);
        
        lista1.add(p0y);
        lista1.add(q0y);
        lista1.add(p1y);
        lista1.add(q1y);
        lista1.add(p2y);
        lista1.add(q2y);

        //sort de ambas listas en t0=0
        lista0.insertionSort(0, lista0.size()-1, t0, 0.00001);
        lista1.insertionSort(0, lista1.size()-1, t0, 0.00001);
        
    }
    
    @After
    public void tearDown() {
        lista0.clear();
        lista1.clear();
        listaKSL.clear();
    }

    @Test
    public void testClear() {
        System.out.println("clear");
        ListaKSL instance = listaKSL;
        instance.clear();
        assertTrue(instance.isEmpty());
    }

    @Test
    public void testAdd() {
        System.out.println("add");
        KineticSortedList e = lista1;
        ListaKSL instance = new ListaKSL();
        boolean expResult = true;
        boolean result = instance.add(e);
        assertEquals(expResult, result);
    }

    @Test
    public void testObtenerIntersecciones() {
        System.out.println("obtenerIntersecciones");
        lista0.insertionSort(0, lista0.size()-1, 1.0, 0.00001);
        lista1.insertionSort(0, lista1.size()-1, 1.0, 0.00001);

        ListaKSL instance = new ListaKSL();
        instance.add(lista0);
        instance.add(lista1);
        
        Intersecciones expResult = new Intersecciones();
        expResult.add(new Pair<>(2,0));
        expResult.add(new Pair<>(0,1));
        Intersecciones result = instance.obtenerIntersecciones();
        
        assertEquals(expResult, result);
    }

    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty");
        ListaKSL instance = new ListaKSL();
        boolean expResult = true;
        boolean result = instance.isEmpty();
        assertEquals(expResult, result);
        assertEquals(false, listaKSL.isEmpty());
    }

    @Test
    public void testSize() {
        System.out.println("size");
        int expResult = 2;
        int result = listaKSL.size();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet() {
        System.out.println("get");
        int i = 0;
        KineticSortedList expResult = lista0;
        KineticSortedList result = listaKSL.get(i);
        assertEquals(expResult, result);
    }

    @Test
    public void testSet() {
        System.out.println("set");
        int i = 0;
        KineticSortedList KSL = listaKSL.get(0);
        ListaKSL instance = new ListaKSL();
        instance.add(new KineticSortedList());
        instance.set(i, KSL);
        assertFalse(instance.get(i).isEmpty());
    }

    @Test
    public void testGetListaKSL() {
        System.out.println("getListaKSL");
        List<KineticSortedList> expResult = new ArrayList<>();
        expResult.add(lista0);
        expResult.add(lista1);
        List<KineticSortedList> result = listaKSL.getListaKSL();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetObserver() {
        System.out.println("setObserver");
        Observer o = new ObservadorEventos(listaKSL, null, null, null, false);
        listaKSL.setObserver(o);
    }

    @Test
    public void testSwap() {
        System.out.println("swap");
        int idx0 = 0;
        int idx1 = 1;
        int dimension = 0;
        ListaKSL instance = listaKSL;
        instance.swap(idx0, idx1, dimension);
        assertEquals(instance.get(0).get(0),p2x);
        assertEquals(instance.get(0).get(1),p0x);
    }
}
