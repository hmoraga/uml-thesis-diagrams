/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.comparators;

import kinetic.DimensionKinetic;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class DimensionKineticComparatorTest {
    private DimensionKinetic dk1, dk2, dk3;
    private DimensionKineticComparator i1, i2, i3;
    
    public DimensionKineticComparatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        dk1 = new DimensionKinetic(3.0, 2.0);
        dk2 = new DimensionKinetic(-3.0, 2.0);
        dk3 = new DimensionKinetic(5.0, 2.0);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCompare() {
        System.out.println("compare");
        DimensionKineticComparator instance0 = new DimensionKineticComparator(0.0);
        DimensionKineticComparator instance1 = new DimensionKineticComparator(0.1);
        
        int expResult001 = 0, expResult002 = 0, expResult012 = 0;
        int expResult101 = 1, expResult102 = -1, expResult112 = -1;
        
        int result001 = instance0.compare(dk1, dk2);
        int result002 = instance0.compare(dk1, dk3);
        int result012 = instance0.compare(dk2, dk3);
        int result101 = instance1.compare(dk1, dk2);
        int result102 = instance1.compare(dk1, dk3);
        int result112 = instance1.compare(dk2, dk3);
        
        assertEquals(expResult001, result001);
        assertEquals(expResult002, result002);
        assertEquals(expResult012, result012);
        assertEquals(expResult101, result101);
        assertEquals(expResult102, result102);
        assertEquals(expResult112, result112);        
    }

    @Test
    public void testCompareWithDelta() {
        System.out.println("compareWithDelta");
        double delta = 0.00001;
        DimensionKineticComparatorWithDelta instance0 = new DimensionKineticComparatorWithDelta(0.0, delta);
        
        int expResult01 = 1, expResult02 = -1, expResult12 = -1;
        
        int result01 = instance0.compare(dk1, dk2);
        int result02 = instance0.compare(dk1, dk3);
        int result12 = instance0.compare(dk2, dk3);
        
        assertEquals(expResult01, result01);
        assertEquals(expResult02, result02);
        assertEquals(expResult12, result12);
    }
}
