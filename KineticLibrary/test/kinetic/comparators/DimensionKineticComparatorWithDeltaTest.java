/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.comparators;

import kinetic.DimensionKinetic;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class DimensionKineticComparatorWithDeltaTest {
    
    public DimensionKineticComparatorWithDeltaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCompare() {
        System.out.println("compare");
        double time = 0.0;
        double delta = 0.00001;
        
        DimensionKinetic i1 = new DimensionKinetic(1.0, 2.0);
        DimensionKinetic i2 = new DimensionKinetic(1.1, 2.0);
        DimensionKinetic i3 = new DimensionKinetic(-2.0, 2.0);
        DimensionKinetic i4 = new DimensionKinetic(-2.0, 2.1);
        
        DimensionKineticComparatorWithDelta instance = new DimensionKineticComparatorWithDelta(time, delta);
        int expResult0 = -1, expResult1 = 1, expResult2 = -1, expResult3 = 1, expResult4 = -1, expResult5 = -1;
        
        int result0 = instance.compare(i1, i2);
        int result1 = instance.compare(i1, i3);
        int result2 = instance.compare(i1, i4);
        int result3 = instance.compare(i2, i3);
        int result4 = instance.compare(i2, i4);
        int result5 = instance.compare(i3, i4);
        
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
        assertEquals(expResult5, result5);        
    }
    
}
