/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class IntervalListTest {
    Interval i0, i1, i2, i3, i4;
    IntervalList lista;
    
    public IntervalListTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        i0 = new Interval(new DimensionKinetic(2.0, -10), new DimensionKinetic(2.0, -6));
        i1 = new Interval(new DimensionKinetic(-1.0, -10), new DimensionKinetic(-1.0, -5));
        i2 = new Interval(new DimensionKinetic(1.5, -5), new DimensionKinetic(1.5, -2));
        i3 = new Interval(new DimensionKinetic(-1.5, -5), new DimensionKinetic(-1.5, -2));
        i4 = new Interval(new DimensionKinetic(-2, -7), new DimensionKinetic(-2, -1));
        
        lista = new IntervalList();
    }
    
    @After
    public void tearDown() {
        lista.clear();
    }

    @Test
    public void testInsertionSort() {
        System.out.println("insertionSort");
        lista.add(i0);
        lista.add(i1);
        lista.add(i2);
        lista.add(i3);
        lista.add(i4);
        lista.insertionSort(0.0);

        assertEquals(lista.get(0), i0);
        assertEquals(lista.get(1), i1);
        assertEquals(lista.get(2), i4);
        assertEquals(lista.get(3), i2);
        assertEquals(lista.get(4), i3);
    }
}
