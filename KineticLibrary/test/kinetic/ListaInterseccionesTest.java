/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic;

import java.util.Observer;
import kinetic.eventos.ObservadorEventos;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import primitives.Pair;

/**
 *
 * @author hmoraga
 */
public class ListaInterseccionesTest {
    public Pair<Integer> par0, par1, par2, par3, par4;
    public ListaIntersecciones lista;
    public Intersecciones listaX, listaY;
    public ListaIntersecciones listaColisiones;
    
    public ListaInterseccionesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        par0 = new Pair<Integer>(0,1);
        par1 = new Pair<Integer>(0,2);
        par2 = new Pair<Integer>(1,2);
        par3 = new Pair<Integer>(2,0);
        par4 = new Pair<Integer>(1,0);
        
        listaX = new Intersecciones();
        listaY = new Intersecciones();
        lista = new ListaIntersecciones();
        listaColisiones = new ListaIntersecciones();
    }
    
    @After
    public void tearDown() {
        listaX.clear();
        listaY.clear();
        lista.clear();
        listaColisiones.clear();
    }

    @Test
    public void testGetColisiones() {
        System.out.println("getColisiones");
        Intersecciones listaZ = new Intersecciones();
        
        listaX.add(par0);   // (0,1)
        listaX.add(par1);   // (0,2)
        
        listaY.add(par0);   // (0,1)
        listaY.add(par2);   // (1,2)
        listaY.add(par3);   // (2,0)
        
        listaZ.add(par4);   // (1,0)
        listaZ.add(par1);   // (0,2);       
        
        lista.add(listaX);
        lista.add(listaY);
        lista.add(listaZ);

        Intersecciones result = lista.getColisiones();
        Intersecciones expResult = new Intersecciones();
        expResult.addAll(listaX);
        
        assertTrue(result.size() == expResult.size());
        assertTrue(result.get(0).equals(expResult.get(0)));
        assertTrue(result.get(1).equals(expResult.get(1)));
    }

    @Test
    public void testClear() {
        System.out.println("clear");
        ListaIntersecciones instance = this.lista;
        instance.clear();
        assertTrue(instance.size()==0);
    }

    @Test
    public void testAdd() {
        System.out.println("add");
        Intersecciones e = new Intersecciones();
        e.add(par0);
        ListaIntersecciones instance = new ListaIntersecciones();
        boolean expResult = true;
        boolean result = instance.add(e);
        assertEquals(expResult, result);
    }

    @Test
    public void testSetObserver() {
        System.out.println("setObserver");
        Observer o = new ObservadorEventos(null, listaColisiones, null, null, false);
        ListaIntersecciones instance = new ListaIntersecciones();
        instance.setObserver(o);
    }

    @Test
    public void testGet() {
        System.out.println("get");
        int dimension = 0;

        listaX.add(par0);   // (0,1)
        listaX.add(par1);   // (0,2)
        
        listaY.add(par0);   // (0,1)
        listaY.add(par2);   // (1,2)
        listaY.add(par3);   // (2,0)
        
        lista.add(listaX);
        lista.add(listaY);

        Intersecciones result = lista.get(dimension);
        Intersecciones expResult = new Intersecciones();
        expResult.addAll(listaX);
        
        assertTrue(result.size() == expResult.size());
        assertTrue(result.get(0).equals(expResult.get(0)));
        assertTrue(result.get(1).equals(expResult.get(1)));
    }

    @Test
    public void testSize() {
        System.out.println("size");
        ListaIntersecciones instance = new ListaIntersecciones();
        int expResult = 0;
        int result = instance.size();
        assertEquals(expResult, result);
    }
}
