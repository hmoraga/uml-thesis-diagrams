package kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import primitives.AABB2D;
import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class AABB2DKineticTest {
    List<Point2D> listaPuntos = new ArrayList<>();
    Point2D p, q;
    double vx, vy;    
    AABB2DKinetic cajaMovil;

    public AABB2DKineticTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        p = new Point2D(1.75, 3.25);
        q = new Point2D(4.25, 6.75);
        listaPuntos.add(p);
        listaPuntos.add(q);
        vx = 3.0;
        vy = 1.0;
        cajaMovil = new AABB2DKinetic(listaPuntos, new Pair<>(vx, vy));
    }
    
    @After
    public void tearDown() {
        listaPuntos.clear();
    }

    @Test
    public void testGetVx() {
        System.out.println("getVx");
        AABB2DKinetic instance = cajaMovil;
        double expResult = 3.0;
        double result = instance.getVx();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testSetVx() {
        System.out.println("setVx");
        double v = 2.0;
        AABB2DKinetic instance = cajaMovil;
        instance.setVx(v);
        assertEquals(v, instance.getVx(),0.0);
    }

    @Test
    public void testGetVy() {
        System.out.println("getVy");
        AABB2DKinetic instance = cajaMovil;
        double expResult = 1.0;
        double result = instance.getVy();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testSetVy() {
        System.out.println("setVy");
        double v = 2.0;
        AABB2DKinetic instance = cajaMovil;
        instance.setVy(v);
        assertEquals(v, instance.getVy(),0.0);
    }

    @Test
    public void testGetBox() {
        System.out.println("getBox");
        double time = 0.0;
        AABB2DKinetic instance = cajaMovil;
        AABB2D expResult = new AABB2D(new Point2D(3, 5), 2.5, 3.5);
        AABB2D result = instance.getBox(time);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetInterval() {
        System.out.println("getInterval");
        int dimension0 = 0, dimension1 = 1;
        AABB2DKinetic instance = cajaMovil;
        Interval expResult0 = new Interval(new DimensionKinetic(3.0, 1.75), new DimensionKinetic(3.0, 4.25));
        Interval result0 = instance.getInterval(dimension0);
        Interval expResult1 = new Interval(new DimensionKinetic(1.0, 3.25), new DimensionKinetic(1.0, 6.75));
        Interval result1 = instance.getInterval(dimension1);
        assertEquals(expResult0.maximum, result0.maximum);
        assertEquals(expResult0.minimum, result0.minimum);
        assertEquals(expResult1.maximum, result1.maximum);
        assertEquals(expResult1.minimum, result1.minimum);
    }

    @Test
    public void testGetP() {
        System.out.println("getP");
        Pair<DimensionKinetic> expResult = new Pair<>(new DimensionKinetic(3, 1.75), new DimensionKinetic(1, 3.25));
        Pair<DimensionKinetic> result = cajaMovil.getP();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetQ() {
        System.out.println("getQ");
        Pair<DimensionKinetic> expResult = new Pair<>(new DimensionKinetic(3, 4.25), new DimensionKinetic(1, 6.75));
        Pair<DimensionKinetic> result = cajaMovil.getQ();
        assertEquals(expResult, result);
    }
}
