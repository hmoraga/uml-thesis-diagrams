/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import kinetic.eventos.EventoSwap;
import kinetic.eventos.SubEventoSwap;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class KineticSortedListTest {
    KineticSortedList lista;
    
    public KineticSortedListTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        lista = new KineticSortedList();
    }
    
    @After
    public void tearDown() {
        lista.clear();
    }

    @Test
    public void testAddAll() {
        System.out.println("addAll");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);
        
        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);
        
        KineticSortedList instance = lista;
        boolean expResult = true;
        boolean result = instance.addAll(listaPuntos);
        assertEquals(listaPuntos.get(0), instance.get(0));
        assertEquals(expResult, result);
    }

    @Test
    public void testAdd() {
        System.out.println("add");
        Wrapper puntoCinetico = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, false);
        KineticSortedList instance = lista;
        boolean expResult = true;
        boolean result = instance.add(puntoCinetico);
        assertEquals(expResult, result);
    }

    @Test
    public void testValidarCertificado() {
        System.out.println("validarCertificado");
        double time = 0.0;
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);
        
        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);
        
        KineticSortedList instance = lista;
        instance.addAll(listaPuntos);
        boolean result = instance.validarCertificado(time);
        boolean expResult = false;
        assertEquals(expResult, result);
    }

    @Test
    public void testInsertionSort() {
        System.out.println("insertionSort");
        double time = 0.0;
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);
        
        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);
        
        KineticSortedList instance = lista;
        instance.addAll(listaPuntos);
        
        instance.insertionSort(0, instance.size()-1, time, 0.00001);
        assertTrue(instance.get(0) == p1);
        assertTrue(instance.get(1) == p2);
        assertTrue(instance.get(2) == p0);
        assertTrue(instance.get(3) == p3);
    }

    @Test
    public void testSwap() {
        System.out.println("swap");
        double time = 0.0;
        
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);
        
        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);
        
        KineticSortedList instance = lista;
        instance.addAll(listaPuntos);
        
        instance.insertionSort(0, instance.size()-1, time, 0.00001);
        instance.swap(0, 1);
        
        assertEquals(instance.get(0), p2);
        assertEquals(instance.get(1), p1);
    }

    @Test
    public void testCalcularEventosVecinos() {
        System.out.println("calcularEventosVecinos");
        double time = 0.0;
        
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);
        
        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);
        
        KineticSortedList instance = lista;
        instance.addAll(listaPuntos);
        instance.insertionSort(0, instance.size()-1, time, 0.00001);
        
        List<SubEventoSwap> listaEventos=instance.calcularEventosVecinos(1, 2);
        
        assertTrue(listaEventos.isEmpty());
    }

    @Test
    public void testCalcularEventos() {
        System.out.println("calcularEventos");
        double time = 0.0;
        boolean include = false;
        KineticSortedList instance = new KineticSortedList();
        instance.add(new Wrapper(new DimensionKinetic(1.0, 2.0), 0, false));
        instance.add(new Wrapper(new DimensionKinetic(1.0, 5.0), 0, true));
        instance.add(new Wrapper(new DimensionKinetic(0.5, 3.0), 1, false));
        instance.add(new Wrapper(new DimensionKinetic(0.5, 6.0), 1, true));
        instance.add(new Wrapper(new DimensionKinetic(-2.0, 4.0), 2, false));
        instance.add(new Wrapper(new DimensionKinetic(-2.0, 7.0), 2, true));
        
        List<SubEventoSwap> expResult = new ArrayList<>();
        SubEventoSwap ev0, ev1, ev2, ev3;
        
        ev0 = new SubEventoSwap(2.0, 0, 1);
        ev1 = new SubEventoSwap(2.0/5.0, 1, 2);
        ev2 = new SubEventoSwap(2.0, 3, 4);
        ev3 = new SubEventoSwap(2.0/5.0, 4, 5);
        
        expResult.add(ev0);
        expResult.add(ev1);
        expResult.add(ev2);
        expResult.add(ev3);
        
        instance.insertionSort(0, instance.size()-1, time, 0.00001);
        List<SubEventoSwap> result = instance.calcularEventosSwap(time, include);
        
        int i=0;
        for (SubEventoSwap evento : result) {
            assertEquals(evento, expResult.get(i++));
        }
    }

    @Test
    public void testTestIfMultiplesEventos() {
        System.out.println("testIfMultiplesEventos");
        double time = 0.0;
        
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(-1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 3.0), 1, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.9), 2, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(-2.0, 3.0), 3, true);
        
        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);
        
        KineticSortedList instance = lista;
        instance.addAll(listaPuntos);
        EventoSwap ev = new EventoSwap(1, 2, 0, 0);
        assertTrue(instance.testIfMultiplesEventos(ev));
    }

    @Test
    public void testClear() {
        System.out.println("clear");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);
        
        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);
        
        KineticSortedList instance = lista;
        instance.addAll(listaPuntos);
        assertTrue(instance.size()==4);
        instance.clear();
        assertTrue(instance.isEmpty());
    }

    @Test
    public void testGet() {
        System.out.println("get");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);
        
        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);
        
        KineticSortedList instance = lista;
        instance.addAll(listaPuntos);

        assertEquals(instance.get(0), p0);
        assertEquals(instance.get(1), p3);
    }

    @Test
    public void testSize() {
        System.out.println("size");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);
        
        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);
        
        KineticSortedList instance = lista;
        instance.addAll(listaPuntos);
        int expResult = 4;
        
        int result = instance.size();
        assertEquals(expResult, result);
    }

    @Test
    public void testObtenerIntersecciones() {
        System.out.println("obtenerIntersecciones");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);
        
        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);
        
        KineticSortedList instance = lista;
        instance.addAll(listaPuntos);
        instance.insertionSort(0, instance.size()-1, 0.0, 0.00001);
        
        Intersecciones expResult = new Intersecciones();
        expResult.add(new Pair<>(0,1));
        
        Intersecciones result = instance.obtenerIntersecciones();
        assertEquals(expResult, result);
    }

    @Test
    public void testCalcularEventosVecinos_int_int() {
        System.out.println("calcularEventosVecinos");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);        
        Wrapper q0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper q1 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);
        
        listaPuntos.add(p0);
        listaPuntos.add(q0);
        listaPuntos.add(p1);
        listaPuntos.add(q1);
        
        KineticSortedList instance = lista;
        instance.addAll(listaPuntos);
        instance.insertionSort(0, instance.size()-1, 0.0, 0.00001);

        List<SubEventoSwap> expResult0 = new ArrayList<>();
        expResult0.add(new SubEventoSwap(1.0, 1, 2));
        List<SubEventoSwap> expResult1 = new ArrayList<>();
        List<SubEventoSwap> expResult2 = new ArrayList<>();
        expResult2.add(new SubEventoSwap(1.0, 1, 2));
        
        List<SubEventoSwap> result0 = instance.calcularEventosVecinos(0, 1);
        List<SubEventoSwap> result1 = instance.calcularEventosVecinos(1, 2);
        List<SubEventoSwap> result2 = instance.calcularEventosVecinos(2, 3);
        
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    @Test
    public void testCalcularEventosVecinos_int() {
        System.out.println("calcularEventosVecinos");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);        
        Wrapper p1 = new Wrapper(new DimensionKinetic(-1.0, 3.0), 1, false);
        Wrapper q0 = new Wrapper(new DimensionKinetic(1.0, 2.0), 0, true);
        Wrapper q1 = new Wrapper(new DimensionKinetic(-1.0, 4.0), 1, true);
        
        listaPuntos.add(p0);
        listaPuntos.add(p1);
        listaPuntos.add(q0);
        listaPuntos.add(q1);
        
        lista.addAll(listaPuntos);
        lista.insertionSort(0, lista.size()-1, 0.0, 0.00001);

        int pos = 1;
        List<SubEventoSwap> expResult = new ArrayList<>();
        expResult.add(new SubEventoSwap(Double.POSITIVE_INFINITY, 0, 1));
        expResult.add(new SubEventoSwap(0.5, 1, 2));
        List<SubEventoSwap> result = lista.calcularEventosVecinos(pos);
        assertEquals(expResult, result);
    }

    @Test
    public void testCalcularEventosSwap_double_boolean() {
        System.out.println("calcularEventosSwap");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);        
        Wrapper q1 = new Wrapper(new DimensionKinetic(-1.0, 3.5), 1, true);
        Wrapper q0 = new Wrapper(new DimensionKinetic(1.0, 2.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(-1.0, 1.5), 1, false);
        
        listaPuntos.add(p0);
        listaPuntos.add(q1);
        listaPuntos.add(q0);
        listaPuntos.add(p1);
        
        lista.addAll(listaPuntos);
        lista.insertionSort(0, lista.size()-1, 0.0, 0.00001);

        double time = 0.0;
        boolean include = false;
        
        List<SubEventoSwap> expResult = new ArrayList<>();
        expResult.add(new SubEventoSwap(0.25, 0, 1));
        expResult.add(new SubEventoSwap(0.75, 2, 3));
        // son solo dos eventos porque no actualizamos la ksl,
        // son solo los eventos iniciales
        List<SubEventoSwap> result = lista.calcularEventosSwap(time, include);
        assertEquals(expResult, result);
    }

    @Test
    public void testCalcularEventosSwap_4args() {
        System.out.println("calcularEventosSwap");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);        
        Wrapper p1 = new Wrapper(new DimensionKinetic(-1.0, 3.0), 1, false);
        Wrapper q0 = new Wrapper(new DimensionKinetic(1.0, 2.0), 0, true);
        Wrapper q1 = new Wrapper(new DimensionKinetic(-1.0, 4.0), 1, true);
        
        listaPuntos.add(p0);
        listaPuntos.add(q0);
        listaPuntos.add(p1);
        listaPuntos.add(q1);
        
        KineticSortedList instance = lista;
        instance.addAll(listaPuntos);
        instance.insertionSort(0, instance.size()-1, 0.5, 0.00001);
        
        int idInicio = 0;
        int idFinal = 3;
        double time = 1.1;
        boolean include = false;
        List<SubEventoSwap> expResult = new ArrayList<>();
        List<SubEventoSwap> result = instance.calcularEventosSwap(idInicio, idFinal, time, include);
        assertEquals(expResult, result);
    }

    @Test
    public void testObtenerRangoEliminar() {
        System.out.println("obtenerRangoEliminar");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);        
        Wrapper q0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(2.0, 3.0), 1, false);
        Wrapper q1 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);
        Wrapper p2 = new Wrapper(new DimensionKinetic(1.5, 3.0), 2, false);
        Wrapper q2 = new Wrapper(new DimensionKinetic(1.5, 4.0), 2, true);
        
        listaPuntos.add(p0);
        listaPuntos.add(q0);
        listaPuntos.add(p1);
        listaPuntos.add(q1);
        listaPuntos.add(p2);
        listaPuntos.add(q2);
        
        KineticSortedList instance = lista;
        instance.addAll(listaPuntos);
        instance.insertionSort(0, instance.size()-1, 0.0, 0.00001);
        
        double time = 0.0;
        Pair<Integer> expResult = new Pair<>(1, 3);    //posiciones en la KSL ordenada
        Pair<Integer> result = instance.obtenerRangoEliminar(time);
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);        
        Wrapper q0 = new Wrapper(new DimensionKinetic(-1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper q1 = new Wrapper(new DimensionKinetic(-2.0, 4.0), 1, true);
        
        listaPuntos.add(p0);
        listaPuntos.add(q0);
        listaPuntos.add(p1);
        listaPuntos.add(q1);
        
        KineticSortedList instance = lista;
        instance.addAll(listaPuntos);
        instance.insertionSort(0, instance.size()-1, 0.0, 0.00001);

        String expResult = "p0 p1 q0 q1 ";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    @Test
    public void testAdd_Object2D_int() {
        System.out.println("add");
        Pair<Double> velocidades = new Pair<>(1.0, -2.0);
        List<Point2D> listaPtos = new ArrayList<>();
        
        listaPtos.add(new Point2D(2.0, -1.0));
        listaPtos.add(new Point2D(3.0, 0.0));
        listaPtos.add(new Point2D(1.0, 1.0));
        listaPtos.add(new Point2D(0.0, 0.0));
        listaPtos.add(new Point2D(0.0, -2.0));
        listaPtos.add(new Point2D(1.0, -3.0));
        listaPtos.add(new Point2D(3.0, -2.0));
        
        Object2D objeto = new Object2D(0, listaPtos, velocidades);
        KineticSortedList instance0 = new KineticSortedList();
        KineticSortedList instance1 = new KineticSortedList();
        boolean expResult = true;
        boolean result0 = instance0.add(objeto, 0);
        boolean result1 = instance1.add(objeto, 1);
        assertEquals(expResult, result0);
        assertEquals(expResult, result1);
        assertTrue(instance0.size() == 2);
        assertTrue(instance1.size() == 2);
    }

    @Test
    public void testAdd_Wrapper() {
        System.out.println("add");
        Wrapper puntoCinetico = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, true);
        KineticSortedList instance = new KineticSortedList();
        boolean expResult = true;
        boolean result = instance.add(puntoCinetico);
        assertEquals(expResult, result);
        assertTrue(instance.size() == 1);
    }

/*    @Test
    public void testCheckValidity() {
        System.out.println("checkValidity");
        KineticSortedList instance = new KineticSortedList();
        instance.checkValidity();
    } */

    @Test
    public void testFindIds() {
        System.out.println("findIds");
        int objectId = 0;
        lista.add(new Wrapper(new DimensionKinetic(1.5, -2.0), 0, false));
        lista.add(new Wrapper(new DimensionKinetic(1.0, -1.0), 1, false));
        lista.add(new Wrapper(new DimensionKinetic(0.7, 0.0), 2, false));
        lista.add(new Wrapper(new DimensionKinetic(0.7, 1.0), 2, true));
        lista.add(new Wrapper(new DimensionKinetic(1.5, 2.0), 0, true));
        lista.add(new Wrapper(new DimensionKinetic(1.0, 3.0), 1, true));

        Pair<Integer> expResult = new Pair<>(0,4);
        Pair<Integer> result = lista.findIds(objectId);
        assertEquals(expResult, result);
    }

    @Test
    public void testObtenerListaCompletaPares() {
        System.out.println("obtenerListaCompletaPares");
        Integer first = 2;
        Integer second = 4;
        
        lista.add(new Wrapper(new DimensionKinetic(1.5, -2.0), 0, false));
        lista.add(new Wrapper(new DimensionKinetic(1.0, -1.0), 1, false));
        lista.add(new Wrapper(new DimensionKinetic(0.7, 0.0), 2, false));
        lista.add(new Wrapper(new DimensionKinetic(0.7, 1.0), 2, true));
        lista.add(new Wrapper(new DimensionKinetic(1.5, 2.0), 0, true));
        lista.add(new Wrapper(new DimensionKinetic(1.0, 3.0), 1, true));
        
        List<Pair<Integer>> expResult = new ArrayList<>();
        expResult.add(new Pair<>(2, 0));
        
        List<Pair<Integer>> result = lista.obtenerListaCompletaPares(first, second);
        assertEquals(expResult, result);
    }
}