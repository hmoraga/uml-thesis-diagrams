/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic;

import kinetic.Object2D;
import java.util.ArrayList;
import java.util.List;
import kinetic.DimensionKinetic;
import kinetic.Wrapper;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import poligons.AnyPolygon2D;
import poligons.Polygon2D;
import primitives.Pair;
import primitives.Pair;
import primitives.Point2D;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class Object2DTest {
    public Object2D byPair, byPolygon2D;
    public Polygon2D poligono;
    public List<Point2D> listaPuntos;
    public Pair<Double> velocidades;
    public List<Pair<Double>> dimensiones;
    private List<Point2D> puntos;
    
    public Object2DTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        puntos = new ArrayList<>();
        puntos.add(new Point2D(-2,-2));
        puntos.add(new Point2D(2,-2));
        puntos.add(new Point2D(2,2));
        puntos.add(new Point2D(-2,2));
        
        listaPuntos = new ArrayList<>(4);
        listaPuntos.add(new Point2D(-2.0,-2.0));
        listaPuntos.add(new Point2D(2.0,-2.0));
        listaPuntos.add(new Point2D(2.0,2.0));
        listaPuntos.add(new Point2D(-2.0,2.0));
        
        poligono = new AnyPolygon2D(puntos, false);
        dimensiones = new ArrayList<>(2);
        dimensiones.add(new Pair<>(-5.0,5.0));
        dimensiones.add(new Pair<>(-5.0,5.0));
        velocidades = new Pair<>(3.0, -1.0);
        
        byPolygon2D = new Object2D(0, poligono.getVertices(), velocidades, 0);
        byPair = new Object2D(1, listaPuntos, velocidades);
    }
    
    @After
    public void tearDown() {
        listaPuntos.clear();
    }

    @Test
    public void testGetListaPuntos() {
        System.out.println("getListaPuntos");
        Object2D instance0 = byPair;
        Object2D instance1 = byPolygon2D;
        
        List<Point2D> expResult0 = new ArrayList<>();
        expResult0.add(new Point2D(-2.0,-2.0));
        expResult0.add(new Point2D(2.0,-2.0));
        expResult0.add(new Point2D(2.0,2.0));
        expResult0.add(new Point2D(-2.0,2.0));
        
        List<Point2D> result0 = instance0.getListaPuntos();
        List<Point2D> result1 = instance1.getListaPuntos();
        assertEquals(expResult0, result0);
        assertEquals(expResult0, result1);
    }

    @Test
    public void testGetVelocidad() {
        System.out.println("getVelocidad");
        Object2D instance0 = byPair, instance1 = byPolygon2D;
        Pair<Double> expResult = new Pair<>(3.0, -1.0);
        Pair<Double> result0 = instance0.getVelocidad();
        Pair<Double> result1 = instance1.getVelocidad();
        assertEquals(expResult, result0);
        assertEquals(expResult, result1);
    }

    @Test
    public void testSetVelocidad() {
        System.out.println("setVelocidad");
        Pair<Double> velocidad = new Pair<>(-3.0, 1.0);
        Object2D instance = byPolygon2D;
        instance.setVelocidad(velocidad);
        assertEquals(instance.getVelocidad(), velocidad);
    }

   /* @Test
    public void testGetTimeBorderEvent() {
        System.out.println("getTimeBorderEvent");
        int dimension0 = 0, dimension1 = 1;
        List<PairDouble> limites = new ArrayList<>(2);
        limites.add(new Pair<>(-6.0, 6.0));
        limites.add(new Pair<>(-6.0, 6.0));
        Object2D instance = byPair;
        double expResult0 = 4.0/3.0, expResult1 = 4.0;
        double result0 = instance.getTimeBorder(limites, dimension0);
        double result1 = instance.getTimeBorder(limites, dimension1);
        
        assertEquals(expResult0, result0, 0.0);
        assertEquals(expResult1, result1, 0.0);        
    } */

    @Test
    public void testSetSpeed() {
        System.out.println("setSpeed");
        double vx = -3.0, vy = 1.0;
        Object2D instance = byPolygon2D;
        
        instance.setVelocidad(new Pair<>(vx, vy));
        assertEquals(instance.getVelocidad(), new Pair<>(-3.0, 1.0));
    }

    @Test
    public void testGetObjId() {
        System.out.println("getObjId");
        Object2D instance0 = byPair, instance1 = byPolygon2D;
        int result0 = instance0.getObjId();
        int result1 = instance1.getObjId();
        assertEquals(1, result0);
        assertEquals(0, result1);
    }

/*    @Test
    public void testExcedeBorde() {
        System.out.println("excedeBorde");
        double timeStep = 0.1;
        Object2D instance0 = byPair, instance1 = byPolygon2D;
        boolean expResult0 = false, expResult1 = false;
        boolean result0 = instance0.excedeBorde(dimensiones, timeStep);
        boolean result1 = instance1.excedeBorde(dimensiones, timeStep);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);        
    } */

/*    @Test
    public void testGetTimeBorder() {
        System.out.println("getTimeBorder");
        int dim0 = 0, dim1 = 1;
        Object2D instance0 = byPolygon2D, instance1 = byPair;
        double expResult0 = 1.0, expResult1 = 3.0;
        double result0 = instance0.getTimeBorder(dimensiones, dim0);
        double result1 = instance1.getTimeBorder(dimensiones, dim1);        
        assertEquals(expResult0, result0, 0.0);
        assertEquals(expResult1, result1, 0.0);
    } */

    @Test
    public void testGetWrappers() {
        System.out.println("getWrappers");
        int dim0 = 0, dim1 = 1;
        Object2D instance = byPair;
        List<Wrapper> expResult0 = new ArrayList<>(), expResult1 = new ArrayList<>();
        Wrapper px = new Wrapper(new DimensionKinetic(3.0, -2), 1, false);
        Wrapper qx = new Wrapper(new DimensionKinetic(3.0, 2), 1, true);
        Wrapper py = new Wrapper(new DimensionKinetic(-1.0, -2), 1, false);
        Wrapper qy = new Wrapper(new DimensionKinetic(-1.0, 2), 1, true);
        expResult0.add(px);
        expResult0.add(qx);
        expResult1.add(py);
        expResult1.add(qy);
        
        List<Wrapper> result0 = instance.getWrappers(dim0);
        List<Wrapper> result1 = instance.getWrappers(dim1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

/*    @Test
    public void testSetPosition() {
        System.out.println("setPosition");
        double time = 2.0;
        Object2D instance = byPair;
        instance.setPosition(dimensiones, time);
        List<Point2D> ptos = instance.getListaPuntos();
        assertEquals(ptos, listaPuntos);
    }

    @Test
    public void testSetPosition1() {
        System.out.println("setPosition_1");
        double time = 1.0;
        Object2D instance = byPolygon2D;
        List<Point2D> expResult = new ArrayList<>();
        expResult.add(new Point2D(1.0,-3.0));
        expResult.add(new Point2D(5.0,-3.0));
        expResult.add(new Point2D(5.0,1.0));
        expResult.add(new Point2D(1.0,1.0));
        
        instance.setPosition(dimensiones, time);
        List<Point2D> result = instance.getListaPuntos();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetPosition2() {
        System.out.println("setPosition_2");
        double time = 3.0;
        Object2D instance = byPolygon2D;
        instance.setPosition(dimensiones, time);
        List<Point2D> listaPtos = new ArrayList<>();
        listaPtos.add(new Point2D(-5.0,-5.0));
        listaPtos.add(new Point2D(-1.0,-5.0));
        listaPtos.add(new Point2D(-1.0,-1.0));
        listaPtos.add(new Point2D(-5.0,-1.0));
        assertEquals(instance.getListaPuntos(), listaPtos);
    } */

    @Test
    public void testGetPosition() {
        System.out.println("getPosition");
        double time0 = 3.0, time1 = 5.0;
        Object2D instance = new Object2D(0, poligono.getVertices(), velocidades, 0.0);

        // comparando en t=3.0
        List<Point2D> result0 = instance.getPosition(time0);
        List<Point2D> expResult0 = new ArrayList<>();
        
        expResult0.add(new Point2D(7, -5));
        expResult0.add(new Point2D(11, -5));
        expResult0.add(new Point2D(11, -1));
        expResult0.add(new Point2D(7, -1));

        assertEquals(expResult0, result0);

        // comparando en t=5.0
        // el problema aqui es que asume su t_actual como el ultimo
        // ingresado, el cual me causa problemas en la actualizacion
        // se debe pedir un setTiempoActual manual!!!
        List<Point2D> result1 = instance.getPosition(time1);
        List<Point2D> expResult1 = new ArrayList<>();
        
        expResult1.add(new Point2D(13, -7));
        expResult1.add(new Point2D(17, -7));
        expResult1.add(new Point2D(17, -3));
        expResult1.add(new Point2D(13, -3));
        
        assertEquals(expResult1, result1);
    }

    /*@Test
    public void testUpdatePosition() throws CloneNotSupportedException {
        System.out.println("updatePosition");
        double time0 = 3.0, time1= 5.0;
        Object2D instance = new Object2D(0, listaPuntos, velocidades);
        
        instance. updatePosition(time0);
        List<Point2D> result0 = instance.getListaPuntos();
        List<Point2D> expResult0 = new ArrayList<>();
        expResult0.add(new Point2D(7, -5));
        expResult0.add(new Point2D(11, -5));
        expResult0.add(new Point2D(11, -1));
        expResult0.add(new Point2D(7, -1));

        assertEquals(expResult0, result0);
        
        instance.updatePosition(time1);
        List<Point2D> result1 = instance.getListaPuntos();
        List<Point2D> expResult1 = new ArrayList<>();
        expResult1.add(new Point2D(13, -7));
        expResult1.add(new Point2D(17, -7));
        expResult1.add(new Point2D(17, -3));
        expResult1.add(new Point2D(13, -3));
                
        assertEquals(expResult1, result1);        
    }*/

    /**
     * Test of close method, of class Object2D.
     */
    @Test
    public void testClose() {
        System.out.println("close");
        Object2D instance = byPair;
        instance.close();
        
        assertTrue(byPair.getListaPuntos().isEmpty());
        assertNull(byPair.getVelocidad());
    }
}
