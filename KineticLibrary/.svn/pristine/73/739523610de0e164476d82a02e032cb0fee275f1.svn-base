/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kinetic.actuators;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import primitives.Pair;

/**
 *
 * @author hmoraga
 */
public class KineticStatisticsTest {
    private KineticStatistics estadisticas = new KineticStatistics(2);
    private KineticStatistics estadisticas1 = new KineticStatistics(3);
    
    public KineticStatisticsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        estadisticas.setAreaTotal(100);
        estadisticas.setAreaOcupadaInicial(10);
        estadisticas.setTiempoTotalSimulado(new Pair<>(0.0, 10.0));

        estadisticas1.setAreaTotal(100);
        estadisticas1.setAreaOcupadaInicial(10);
        estadisticas1.setTiempoTotalSimulado(new Pair<>(0.0, 10.0));
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getTiempoTotalSimulado method, of class Statistics.
     */
    @Test
    public void testGetTiempoTotalSimulado() {
        System.out.println("getTiempoTotalSimulado");
        KineticStatistics instance = estadisticas;
        double expResult = 10.0;
        double result = instance.getTiempoTotalSimulado();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setTiempoTotalSimulado method, of class Statistics.
     */
    @Test
    public void testSetTiempoTotalSimulado() {
        System.out.println("setTiempoTotalSimulado");
        double tiempoTotalSimulado = 8.0;
        KineticStatistics instance = estadisticas;
        instance.setTiempoTotalSimulado(new Pair<>(0.0, tiempoTotalSimulado));
        assertEquals(tiempoTotalSimulado, instance.getTiempoTotalSimulado(), 0.0);
    }

    /**
     * Test of getAreaOcupadaInicial method, of class Statistics.
     */
    @Test
    public void testGetAreaOcupadaInicial() {
        System.out.println("getAreaOcupadaInicial");
        KineticStatistics instance = estadisticas;
        double expResult = 10.0;
        double result = instance.getAreaOcupadaInicial();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setAreaOcupadaInicial method, of class Statistics.
     */
    @Test
    public void testSetAreaOcupadaInicial() {
        System.out.println("setAreaOcupadaInicial");
        double areaOcupadaInicial = 80.0;
        KineticStatistics instance = estadisticas;
        instance.setAreaOcupadaInicial(areaOcupadaInicial);
        assertEquals(areaOcupadaInicial, instance.getAreaOcupadaInicial(), 0.0);
    }

    /**
     * Test of addEventoSimple method, of class Statistics.
     */
    @Test
    public void testAddEventoSimple() {
        System.out.println("addEventoSimple");
        Pair<Integer> par = new Pair<>(0,1);
        KineticStatistics instance = estadisticas;
        instance.addEventoSimple(par);
        
        List<String> expResult = new ArrayList<>();
        
        expResult.add("areaTotal:100.0");
        expResult.add("tiempoTotalSimulado:10.0");
        expResult.add("areaOcupadaInicial:10.0");
        expResult.add("eventosSimples:1");
        expResult.add("eventosMultiples:0");
        expResult.add("colisionesPorObjeto:[1, 1]");
        expResult.add("minimalTimeStep:0.0");

        assertEquals(expResult, instance.getText());
    }

    /**
     * Test of addEventoMultiple method, of class Statistics.
     */
    @Test
    public void testAddEventoMultiple() {
        System.out.println("addEventoMultiple");
        List<Integer> objectsIdList = new ArrayList<>();
        objectsIdList.add(0);
        objectsIdList.add(1);
        objectsIdList.add(2);
        
        KineticStatistics instance = estadisticas1;
        instance.addEventoMultiple(objectsIdList);
        
        List<String> expResult = new ArrayList<>();
        
        expResult.add("areaTotal:100.0");
        expResult.add("tiempoTotalSimulado:10.0");
        expResult.add("areaOcupadaInicial:10.0");
        expResult.add("eventosSimples:0");
        expResult.add("eventosMultiples:1");
        expResult.add("colisionesPorObjeto:[1, 1, 1]");
        expResult.add("minimalTimeStep:0.0");        
        
        assertEquals(expResult, instance.getText());
    }

    /**
     * Test of getText method, of class Statistics.
     */
    @Test
    public void testGetText() {
        System.out.println("getText");
        KineticStatistics instance = estadisticas;
        
        StringBuilder expResult = new StringBuilder();
        
        expResult.append("areaTotal:100.0").append('\n');
        expResult.append("tiempoTotalSimulado:10.0").append('\n');
        expResult.append("areaOcupadaInicial:10.0").append('\n');
        expResult.append("eventosSimples:0").append('\n');
        expResult.append("eventosMultiples:0").append('\n');
        expResult.append("colisionesPorObjeto:[0, 0]").append('\n');
        expResult.append("minimalTimeStep:0.0").append('\n');

        String result = instance.getText();
        assertEquals(expResult.toString(), result);
    }

    /**
     * Test of calcularEstaticas method, of class Statistics.
     */
    @Test
    public void testCalcularEstaticas() {
        System.out.println("calcularEstaticas");
        List<Pair<Integer>> listaColisiones = new ArrayList<>();
        listaColisiones.add(new Pair<>(0,1));
        listaColisiones.add(new Pair<>(1,2));
        listaColisiones.add(new Pair<>(0,2));
        
        KineticStatistics instance = estadisticas1;
        
        instance.calcularEstadisticas(listaColisiones);

        StringBuilder expResult = new StringBuilder();

        expResult.append("areaTotal:100.0").append('\n');
        expResult.append("tiempoTotalSimulado:10.0").append('\n');
        expResult.append("areaOcupadaInicial:10.0").append('\n');
        expResult.append("eventosSimples:3").append('\n');
        expResult.append("eventosMultiples:0").append('\n');
        expResult.append("colisionesPorObjeto:[2, 2, 2]").append('\n');
        expResult.append("minimalTimeStep:0.0").append('\n');

        String result = instance.getText();
        assertEquals(expResult.toString(), result);
    }
}
