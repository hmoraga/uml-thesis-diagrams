package kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import kinetic.comparators.DimensionKineticComparator;
import kinetic.comparators.WrapperComparatorWithDelta;
import kinetic.eventos.Evento;
import kinetic.eventos.EventoSwap;
import kinetic.eventos.SubEventoSwap;
import primitives.Pair;

/**
 *
 * @author hmoraga
 */
public class KineticSortedList extends LinkedList<Wrapper>{
    private ArrayList<Pair<Integer>> listaPosiciones;
    /**
     * me da el orden inicial 
     */
    public KineticSortedList() {
        super();
        //llenar la lista de posiciones de p_i y q_i en la ksl para cada objeto i
        llenarListaPosiciones();
    }

    public boolean add(Object2D objeto, int dimension){
        return addAll(objeto.getWrappers(dimension));
    }
    
    @Override
    public boolean addAll(Collection<? extends Wrapper> c) {
        return super.addAll(c);
    }
    
    public void swap(int i, int j){
        Wrapper aux = super.get(i);
        super.set(i, super.get(j));
        super.set(j, aux);
    }
    
    public ArrayList<SubEventoSwap> calcularEventosVecinos(int posi, int posj){
        ArrayList<SubEventoSwap> listaNuevosEventos = new ArrayList<>();
        
        if (size()>=3){
            if ((posi==0) && (posj==1)){
                Wrapper vecino = get(2);
                if (get(posj).getEventTime(vecino)>0){
                    SubEventoSwap subev = new SubEventoSwap(get(posj).getEventTime(vecino), posj, 2);
                    listaNuevosEventos.add(subev);
                }
            } else if ((posi==this.size()-2) && (posj==this.size()-1)){
                Wrapper vecino = get(this.size()-3);
                if (get(posi).getEventTime(vecino)>0){
                    SubEventoSwap subev = new SubEventoSwap(get(posi).getEventTime(vecino), this.size()-3, posi);
                    listaNuevosEventos.add(subev);
                }
            } else {
                Wrapper vecino1 = get(posi-1);
                Wrapper vecino2 = get(posj+1);
                if (get(posi).getEventTime(vecino1)>0){
                    SubEventoSwap subev1 = new SubEventoSwap(get(posi).getEventTime(vecino1), posi-1, posi);
                    listaNuevosEventos.add(subev1);
                }
                
                if (get(posj).getEventTime(vecino2)>0){
                    SubEventoSwap subev2 = new SubEventoSwap(get(posj).getEventTime(vecino2), posj, posj+1);
                    listaNuevosEventos.add(subev2);
                }
            }
        }
        
        return listaNuevosEventos;
    }

    public ArrayList<SubEventoSwap> calcularEventosVecinos(int pos){
        ArrayList<SubEventoSwap> listaNuevosEventos = new ArrayList<>();
        
        if (size()>=2){
            if (pos==0){
                Wrapper vecino = get(1);
                if (get(pos).getEventTime(vecino)>0){
                    SubEventoSwap subev = new SubEventoSwap(get(pos).getEventTime(vecino), pos, 1);
                    listaNuevosEventos.add(subev);
                }
            } else if (pos==this.size()-1){
                Wrapper vecino = get(this.size()-2);
                if (get(pos).getEventTime(vecino)>0){
                    SubEventoSwap subev = new SubEventoSwap(get(pos).getEventTime(vecino), this.size()-2, pos);
                    listaNuevosEventos.add(subev);
                }
            } else {
                Wrapper vecino1 = get(pos-1);
                Wrapper vecino2 = get(pos+1);
                if (get(pos).getEventTime(vecino1)>0){
                    SubEventoSwap subev1 = new SubEventoSwap(get(pos).getEventTime(vecino1), pos-1, pos);
                    listaNuevosEventos.add(subev1);
                }
                
                if (get(pos).getEventTime(vecino2)>0){
                    SubEventoSwap subev2 = new SubEventoSwap(get(pos).getEventTime(vecino2), pos, pos+1);
                    listaNuevosEventos.add(subev2);
                }
            }
        }
        
        return listaNuevosEventos;
    }    
    
    @Override
    public boolean add(Wrapper puntoCinetico){
        return super.add(puntoCinetico);
    }

    /**
     * 
     * @param time
     * @param include if include time in event search
     * @return event list
     */
    public ArrayList<SubEventoSwap> calcularEventosSwap(double time, boolean include){
        ArrayList<SubEventoSwap> listaEventos = new ArrayList<>();
        
        if (this.size()>=2){
            for (int i = 1; i < this.size(); i++) {
                double timeEvent = get(i-1).getEventTime(get(i));
                
                if (include){
                    if (timeEvent>=time && timeEvent!=Double.POSITIVE_INFINITY)
                        listaEventos.add(new SubEventoSwap(timeEvent, i-1, i));
                } else {
                    if (timeEvent>time && timeEvent!=Double.POSITIVE_INFINITY)
                        listaEventos.add(new SubEventoSwap(timeEvent, i-1, i));
                }
            }
            return listaEventos;
        } else return new ArrayList<>();
    }
    
    /**
     * 
     * @param idInicio id dentro de la ksl (inclusive)
     * @param idFinal id dentro de la ksl (inclusive)
     * @param time
     * @param include if include time in event search
     * @return event list
     */
    public ArrayList<SubEventoSwap> calcularEventosSwap(int idInicio, int idFinal, double time, boolean include){
        ArrayList<SubEventoSwap> listaEventos = new ArrayList<>();
        
        if (this.size()>=2){
            if (idInicio!=0){
                double timeEvent = this.get(idInicio-1).getEventTime(get(idInicio));
                SubEventoSwap sev = new SubEventoSwap(timeEvent, idInicio-1, idInicio);
                listaEventos.add(sev);
            }
            
            if (idFinal!=size()-1){
                double timeEvent = this.get(idFinal).getEventTime(get(idFinal+1));
                SubEventoSwap sev = new SubEventoSwap(timeEvent, idFinal, idFinal+1);
                listaEventos.add(sev);
            }
            
            return listaEventos;
        } else return null;
    }    
    
    public boolean testIfMultiplesEventos(Evento ev){
        double time = ev.getTime();
        int id0=((EventoSwap)ev).getId0();
        int id1=((EventoSwap)ev).getId1();
        
        if (this.size()>2){
            if ((id0>0) && (id1<size()-1))
                return (((get(id0-1).getDimensionKinetic().getPosition(time) == get(id0).getDimensionKinetic().getPosition(time)) &&
                    (get(id0).getDimensionKinetic().getPosition(time) == get(id1).getDimensionKinetic().getPosition(time))) || 
                    ((get(id0).getDimensionKinetic().getPosition(time) == get(id1).getDimensionKinetic().getPosition(time)) &&
                    (get(id1).getDimensionKinetic().getPosition(time) == get(id1+1).getDimensionKinetic().getPosition(time))));
            else if (id0==0)
                return ((get(id0).getDimensionKinetic().getPosition(time) == get(id1).getDimensionKinetic().getPosition(time)) &&
                    (get(id1).getDimensionKinetic().getPosition(time) == get(id1+1).getDimensionKinetic().getPosition(time)));
            else if (id1==size()-1)
                return ((get(id0-1).getDimensionKinetic().getPosition(time) == get(id0).getDimensionKinetic().getPosition(time)) &&
                    (get(id0).getDimensionKinetic().getPosition(time) == get(id1).getDimensionKinetic().getPosition(time)));
        }
        return false;
    }
    
    /**
     * Metodo que revisa si el certificado es válido.
     * <P>
     * Un certificado es válido si para todos los puntos p_i de la lista
     * cinetica, con valores x_i son estrictamente mayores.
     * @param time tiempo. Un valor mayor que cero.
     * @return true si el certificado es verdadero o false en caso contrario.
     */
    public boolean validarCertificado(double time){
        boolean res = true;

        switch (super.size()){
            case 0:
            case 1:
                break;
            default:
                DimensionKineticComparator dkc = new DimensionKineticComparator(time);

                for (int i=1; i < super.size(); i++) {
                    DimensionKinetic pto0 = super.get(i-1).getDimensionKinetic();
                    DimensionKinetic pto1 = super.get(i).getDimensionKinetic();
            
                    res = res && (dkc.compare(pto0, pto1) <= 0);
                
                    if (!res)
                        break;
                }
        }
        return res;
    }
    
    /**
     * Método para ordenar la lista cinética dado un tiempo y un delta
     * @param idInicial indice inicial, inclusive
     * @param idFinal indice final, inclusive
     * @param time tiempo para ordenar la lista cinética 
     * @param delta tiempo para resolver las colisiones múltiples
     */
    public void insertionSort(int idInicial, int idFinal, double time, double delta){
        Collections.sort(this.subList(idInicial, idFinal+1), new WrapperComparatorWithDelta(time, delta));
    }

    /**
     * Método para ordenar la lista cinética dado un tiempo y un delta
     * @param time tiempo para ordenar la lista cinética 
     * @param delta tiempo para resolver las colisiones múltiples
     */
    public void sort(double time, double delta){
        Collections.sort(this, new WrapperComparatorWithDelta(time, delta));        
    }

    
    @Override
    public void clear(){
        super.clear();
    }
    
    @Override
    public Wrapper get(int pos){
        return super.get(pos);
    }
    
    @Override
    public int size(){
        return super.size();
    }
    
    public Intersecciones obtenerIntersecciones(){
        Intersecciones interseccs = new Intersecciones();
        ArrayList<Integer> aux = new ArrayList<>();
        
        for (Wrapper aThi : this) {
            if (!aThi.isMaximum()) {
                aux.add(aThi.getObjectId());
            } else {
                int pos = aux.indexOf(aThi.getObjectId());
                aux.remove(pos);
                for (Integer value : aux) {
                    int obj1 = Math.min(aThi.getObjectId(), value);
                    int obj2 = Math.max(aThi.getObjectId(), value);
                    interseccs.add(new Pair<>(obj1, obj2));
                }
            }
        }
        
        return interseccs;
    }
    
    /**
     * Se usa solo cuando se encuentran multiples eventos dentro de la lista 
     * cinetica. Me retorna el rango de indices, dentro de la actual lista
     * cinetica, la que será usada por la cola de eventos para borrar los eventos
     * cuyos indices se encuentran dentro del rango retornado por este metodo, 
     * incluidos los extremos.
     * 
     * @param time momento donde se evaluará la KSL
     * @return Par de indices de la lista [inicio, fin] que me indican 
     */
    public Pair<Integer> obtenerRangoEliminar(double time){
        // evaluo la ksl en el tiempo ingresado como parametro y dado que está
        // ordenada se revisa entre que indices se enuentran en la misma posicion
        // en el mismo tiempo.
        int minima = -1;
        int maxima = -1;
        
        if (this.size()>=3){
            for (int i = 0; i <= this.size()-3; i++) {
                double posxi = get(i).getDimensionKinetic().getPosition(time);
                double posximas1 = get(i+1).getDimensionKinetic().getPosition(time);
                double posximas2 = get(i+2).getDimensionKinetic().getPosition(time);
                
                if ((posxi==posximas1) && (posxi==posximas2)){
                    minima=i;                    
                    int j=2;
                    
                    while ((j<this.size()) && 
                            (posxi == get(i+j).getDimensionKinetic().getPosition(time))){
                        maxima=i+j;
                        j++;
                    }
                }
            }
                
            if ((minima==-1) || (maxima==-1))
                return null;
            else
                return new Pair<>(minima, maxima);
        }
        return null;
    }

    @Override
    public String toString() {
        String s = "";
        
        for (Wrapper wrapper : this) {
            String obj;
            if (wrapper.isMaximum()){
                obj = "q";
            } else {
                obj = "p";
            }
            s = s + obj + wrapper.getObjectId() + " ";
        }
        
        return s;
    }
    
    /**
     * Dado un objectId se debe encontrar el par de indices de la KSL
     * @param objectId objectId del objeto
     * @return par de índices dentro de la lista donde se encuentran los
     * elementos del objectId
     */
    public Pair<Integer> findIds(int objectId){
        int minId=-1, maxId=-1;
        
        for (int i = 0; i < this.size(); i++) {
            Wrapper wrap = this.get(i);
            
            if (wrap.getObjectId()==objectId){
                if (!wrap.isMaximum())
                    minId = i;
                else
                    maxId = i;
            }
        }
                
        return new Pair<>(minId, maxId);
    }

    public List<Pair<Integer>> obtenerListaCompletaPares(Integer first, Integer second) {
        List<Pair<Integer>> lista = new ArrayList<>();
        
        for (int i=first; i<=second-1;i++){
            for (int j=i+1; j<=second; j++){
                int idx0 = this.get(i).getObjectId();
                int idx1 = this.get(j).getObjectId();
                if ((idx0!=idx1) && 
                        (!(lista.contains(new Pair<>(idx0, idx1)) || lista.contains(new Pair<>(idx1, idx0)))))
                    lista.add(new Pair<>(idx0, idx1));
            }
        }
        return lista;
    }

    private void llenarListaPosiciones() {
        List<Integer> listaTemp = new ArrayList<>();
        
        int i=0;
        for (Wrapper wrap : this) {
            if (wrap.isMaximum()){
                listaTemp.set(2*wrap.getObjectId()+1, i++);
            } else {
                listaTemp.set(2*wrap.getObjectId(), i++);
            }
        }
        
        for (int j=0; j<listaTemp.size()/2; j++){
            listaPosiciones.add(new Pair<>(listaTemp.get(j), listaTemp.get(j+1)));
        }
    }
}