/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loader;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class LoadModel {
    private List<List<Point2D>> listaObjetos;
    private List<Pair<Double>> listaVelocidades;
    private List<Pair<Double>> marco;
    private int framesPerSecond;
    private double tinicial, tfinal;

    public LoadModel(List<String> listaLineas) {
        String patternString;
        Pattern pattern;
        Matcher matcher;

        // linea 0
        patternString = "marco:\\(\\((\\d+?.\\d+),(\\d+?.\\d+)\\),\\((\\d+?.\\d+),(\\d+?.\\d+)\\)\\)";
        pattern = Pattern.compile(patternString);
        matcher = pattern.matcher(listaLineas.get(0));

        if (matcher.find()) {
            double minX = Double.parseDouble(matcher.group(1));
            double maxX = Double.parseDouble(matcher.group(2));
            double minY = Double.parseDouble(matcher.group(3));
            double maxY = Double.parseDouble(matcher.group(4));

            marco = new ArrayList<>(2);
            marco.add(new Pair<>(minX, maxX));
            marco.add(new Pair<>(minY, maxY));
        } else {
            marco = new ArrayList<>(2);
            marco.add(new Pair<>(-10.0, 10.0));
            marco.add(new Pair<>(-10.0, 10.0));
        }
        matcher.reset();
        
        //linea 1
        patternString = "timeIntervals:(\\d+.?\\d+)-(\\d+.?\\d+)";
        pattern = Pattern.compile(patternString);
        matcher = pattern.matcher(listaLineas.get(1));

        if (matcher.find()) {
            tinicial = Double.parseDouble(matcher.group(1));
            tfinal = Double.parseDouble(matcher.group(2));
        } else {
            tinicial = 0.0;
            tfinal = 1.0;
        }
        matcher.reset();
        
        //linea 2
        patternString = "framesPerSecond:(\\d+)";
        pattern = Pattern.compile(patternString);
        matcher = pattern.matcher(listaLineas.get(2));

        if (matcher.find()) {
            framesPerSecond = Integer.parseInt(matcher.group(1));
        } else {
            framesPerSecond = 10;
        }
        matcher.reset();

        listaVelocidades = new ArrayList<>();
        listaObjetos = new ArrayList<>();
        
        for (int i = 3; i < listaLineas.size(); i++) {
            String line = listaLineas.get(i);
            
            // leo los objetos uno a uno
            // estará la linea de la velocidades primero
            // luego la de los puntos
            if (i % 2==1){
                patternString = "(\\d+.\\d+)\\s+(\\d+.\\d+)";
                pattern = Pattern.compile(patternString);
                matcher = pattern.matcher(line);
                        
                double velocidadX = Double.parseDouble(matcher.group(1));
                double velocidadY = Double.parseDouble(matcher.group(2));
                        
                listaVelocidades.add(new Pair<>(velocidadX, velocidadY));
            } else {
                String[] split = line.split("\\s+");
                int cantPuntos = Integer.parseInt(split[0]);
                List<Point2D> listaPuntosObj = new ArrayList<>(cantPuntos);
                        
                for (int j=1; j < split.length; j+=2){
                    Point2D p = new Point2D(Double.parseDouble(split[j]), Double.parseDouble(split[j+1]));
                    listaPuntosObj.add(p);
                }
                
                listaObjetos.add(listaPuntosObj);
            }
            matcher.reset();
        }
    }

    public List<List<Point2D>> getListaObjetos() {
        return listaObjetos;
    }

    public void setListaObjetos(List<List<Point2D>> listaObjetos) {
        this.listaObjetos = listaObjetos;
    }

    public List<Pair<Double>> getListaVelocidades() {
        return listaVelocidades;
    }

    public void setListaVelocidades(List<Pair<Double>> listaVelocidades) {
        this.listaVelocidades = listaVelocidades;
    }

    public List<Pair<Double>> getMarco() {
        return marco;
    }

    public void setMarco(List<Pair<Double>> marco) {
        this.marco = marco;
    }

    public int getFramesPerSecond() {
        return framesPerSecond;
    }

    public void setFramesPerSecond(int framesPerSecond) {
        this.framesPerSecond = framesPerSecond;
    }

    public double getTinicial() {
        return tinicial;
    }

    public void setTinicial(double tinicial) {
        this.tinicial = tinicial;
    }

    public double getTfinal() {
        return tfinal;
    }

    public void setTfinal(double tfinal) {
        this.tfinal = tfinal;
    }
    
    public void updatePositions(double time){
        for (int i = 0; i < listaObjetos.size(); i++) {
            List<Point2D> obj = listaObjetos.get(i);
            
            for (int j = 0; j < obj.size(); j++) {
                Point2D p = obj.get(j);
                
                p.add(listaVelocidades.get(i).first*framesPerSecond, listaVelocidades.get(i).second*framesPerSecond);
            }
        }
    } 
}
