package scenarios;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import kinetic.actuators.Statistics;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import kinetic.eventos.ColaEventos;
import kinetic.eventos.ObservadorEventos;
import kinetic.primitives.KineticSortedList;
import kinetic.primitives.ListaIntersecciones;
import kinetic.primitives.ListaKSL;
import kinetic.primitives.Object2D;
import kinetic.primitives.Wrapper;
import primitives.Pair;

/**
 *
 * @author hmoraga
 */
public class SimulationKinetic implements Simulation{
    private ArrayList<Object2D> listaObjetos;
    private double tinicial, tfinal;
    private ArrayList<Pair<Double>> dimensiones;
    private Statistics estadisticas;
    
    /**
     * método para simulación de modelo cinético
     * @param listaObjetos arreglo con la lista de objetos
     * @param tinicial tiempo inicial
     * @param tfinal tiempo final
     * @param dimensiones dimesnsiones [a,b]x[c,d]
     */
    public SimulationKinetic(ArrayList<Object2D> listaObjetos, double tinicial, double tfinal, ArrayList<Pair<Double>> dimensiones) {
        this.tinicial = tinicial;
        this.tfinal = tfinal;
        this.dimensiones = dimensiones;
        this.listaObjetos = listaObjetos;
        estadisticas = new Statistics(listaObjetos.size());
    }
    
    @Override
    public void exec(){
        ListaKSL listaKSL = new ListaKSL();
        
        // se supone que tengo 2 dimensiones, por lo que debo crear dos KSL temporales
        // genero una KSL por cada dimension
        KineticSortedList KSLx = new KineticSortedList(), KSLy  = new KineticSortedList();
        
        // de todos los objetos debo generar los DimensionKinetic
        for (Object2D listaObjeto : listaObjetos) {
            ArrayList<Wrapper> wrappersX = listaObjeto.getWrappers(0);
            ArrayList<Wrapper> wrappersY = listaObjeto.getWrappers(1);            
            // de cada DimensionKinetic genero su Wrapper
            // y agrego los Wrapper a cada KSL
            KSLx.add(wrappersX.get(0));
            KSLx.add(wrappersX.get(1));
            KSLy.add(wrappersY.get(0));
            KSLy.add(wrappersY.get(1));
        }

        // ingreso los valores en la lista SIN ORDENAR
        // seran ordenados al calcular los eventos iniciales
        listaKSL.add(KSLx);
        listaKSL.add(KSLy);

        // colaEventos inicial
        ColaEventos colaEventos = new ColaEventos(new Pair<>(tinicial, tfinal), false);
        
        // inicializo la lista de colisiones
        ListaIntersecciones colisiones = new ListaIntersecciones();
        
        // inicializo el observer
        ObservadorEventos observador = new ObservadorEventos(listaKSL, colisiones, colaEventos);
        
        // agrego el observer a todos los objetos necesarios
        listaKSL.setObserver(observador);
        colaEventos.setObserver(observador);
        colisiones.setObserver(observador);
        
        // calculo los eventos iniciales
        colaEventos.calcularEventosIniciales();
        
        // procesar todos los eventos hasta que la cola de eventos esté vacia
        colaEventos.procesarEventos();
    }

    @Override
    public void writeStatisticsToFile(String filePath, String fileName) {
        try {
            String dirName = filePath;
            File theDir = new File(dirName);
            Charset charset = Charset.forName("UTF-8");
            
            if (!theDir.exists()) {
                System.out.println("creating directory: " + dirName);
                theDir.mkdirs();  
                System.out.println("DIRECTORIES created");
            }
            
            Path arch = Paths.get(dirName+"\\"+fileName);
            Files.write(arch,estadisticas.getText(),charset);
        } catch (IOException ex) {
            Logger.getLogger(SimulationKinetic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
