package scenarios;

import java.util.ArrayList;
import java.util.List;
import poligons.*;
import primitives.BVTree;
import primitives.Point2D;
import primitives.Pair;
import sap.SAP2D;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hmoraga
 */
public class TestSAPOnly {
    public static void main(String[] args) {
        System.out.println("Caso 1: Cuadrado versus hexagono regular");
        //caso 1: un cuadrado y un hexagono regular que no intersectan sus AABB2D
        Polygon2D c = new Cuadrado(4, false);
        c.desplazar(new Point2D(-3,0));
        
        Polygon2D h = new Hexagono(6, false);
        h.desplazar(new Point2D(-1,3));
        
        //reviso deteccion de colisiones
        List<List<Point2D>> listaObjs = new ArrayList<>();
        listaObjs.add(c.getVertices());
        listaObjs.add(h.getVertices());
        
        SAP2D sap = new SAP2D(listaObjs);
        
        List<Pair<Integer>> listaColisiones=sap.getListaColisiones();
        
        for (Pair<Integer> colision : listaColisiones) {
            System.out.println(colision.toString());
        }
        sap.close();
        
        //caso 2: un triangulo y un cuadrado que SI intersectan sus AABB2D
        System.out.println("Caso 2: Triangulo versus Cuadrado");

        listaObjs.clear();
        Triangulo tr = new Triangulo(27*Math.sqrt(3)/16, false);
        Cuadrado c1 = new Cuadrado(25/2, false);
        c1.desplazar(2.5, 2.5);
        listaObjs.add(tr.getVertices());
        listaObjs.add(c1.getVertices());
        sap = new SAP2D(listaObjs);
        
        //demostrar que los AABB intersectan
        if (sap.getListaColisiones()!=null){
            System.out.println("Colisiones detectadas");
            BVTree arbolTriangulo = new BVTree(new AnyPolygon2D(tr.getVertices(),false));
            BVTree arbolCuadrado = new BVTree(new AnyPolygon2D(c1.getVertices(),false));
            
            listaColisiones = new ArrayList<>();
            BVTree.obtenerAristasColisionando(listaColisiones, arbolTriangulo, arbolCuadrado);
            
            if (listaColisiones.isEmpty())
                System.out.println("No existe colision entre aristas");
            else {
                for (Pair<Integer> colision : listaColisiones)
                    System.out.println("Colision entre arista " + colision.first.toString() + " y " + colision.second.toString());
            }
        } else {
            System.out.println("No existe colision");
        }
        sap.close();
        
        //caso 3: dos objetos no convexos (estrella de 6 puntas y una de 4) cuyos AABB2D
        // SI chocan y sus aristas tambien 
        System.out.println("Caso 3: Estrella de 4 puntas versus Estrella de 6 puntas");
        Estrella4Puntas e1 = new Estrella4Puntas(35/2, false);
        Estrella6Puntas e2 = new Estrella6Puntas(16*Math.sqrt(3), false);
       
        e1.desplazar(3,4);
        listaObjs.clear();
        listaObjs.add(e1.getVertices());
        listaObjs.add(e2.getVertices());
        sap = new SAP2D(listaObjs);

        for (Pair<Integer> colision : sap.getListaColisiones()) {
            System.out.println(colision.toString());
        }

        //demostrar que los AABB intersectan
        if (sap.getListaColisiones()!=null){
            System.out.println("Colisiones detectadas");
            BVTree arbolEstrella1 = new BVTree(new AnyPolygon2D(e1.getVertices(),false));
            BVTree arbolEstrella2 = new BVTree(new AnyPolygon2D(e2.getVertices(),false));
            
            listaColisiones = new ArrayList<>();
            BVTree.obtenerAristasColisionando(listaColisiones, arbolEstrella1, arbolEstrella2);
            
            if (listaColisiones.isEmpty())
                System.out.println("No existe colision entre aristas");
            else {
                for (Pair<Integer> colision : listaColisiones) {
                    System.out.println("Colision entre arista " + colision.first.toString() + " y " + colision.second.toString());
                }
            }
        } else {
            System.out.println("No existe colision");
        }
        sap.close();
        
        //caso 4: dos objetos: uno concavo y el otro convexo donde si chocan
        // sus aristas
        System.out.println("Caso 4: Boomerang versus Rectangulo");
        Polygon2D boomerang = new Boomerang(3, false);
        Polygon2D rectangulo = new Rectangulo(4, 5, false);

        listaObjs.clear();
        listaObjs.add(boomerang.getBox().getPoints());
        listaObjs.add(rectangulo.getBox().getPoints());
        sap = new SAP2D(listaObjs);
        
        for (Pair<Integer> colision : sap.getListaColisiones()) {
            System.out.println(colision.toString());
        }

        //demostrar que los AABB intersectan
        if (sap.getListaColisiones()!=null){
            System.out.println("Colisiones detectadas");
            BVTree arbolBoomerang = new BVTree(new AnyPolygon2D(boomerang.getVertices(),false));
            BVTree arbolRectangulo = new BVTree(new AnyPolygon2D(rectangulo.getVertices(),false));
            
            listaColisiones = new ArrayList<>();
            BVTree.obtenerAristasColisionando(listaColisiones, arbolBoomerang, arbolRectangulo);
            
            if (listaColisiones.isEmpty())
                System.out.println("No existe colision entre aristas");
            else {
                for (Pair<Integer> colision : listaColisiones) {
                    System.out.println("Colision entre arista " + colision.first.toString() + " y " + colision.second.toString());
                }
            }
        } else {
            System.out.println("No existe colision");
        }
        sap.close();
    }
}