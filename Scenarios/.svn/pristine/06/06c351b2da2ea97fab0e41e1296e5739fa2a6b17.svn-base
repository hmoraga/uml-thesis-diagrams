package kinetic.actuators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import primitives.Pair;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hmoraga
 */
public class Statistics extends Observable{
    private double areaTotal, tiempoTotalSimulado, areaOcupadaInicial;
    private int eventosSimples, eventosMultiples;
    private int[] colisionesPorObjeto;
    private double minimalTimeStep;
    
    /**
     * Constructor con parametros
     * @param totalObjetos cantidad total de objetos 
     */
    public Statistics(int totalObjetos) {
        // ingreso cantidad total de objetos
        colisionesPorObjeto = new int[totalObjetos];
    }

    public double getAreaTotal() {
        return areaTotal;
    }

    public void setAreaTotal(double areaTotal) {
        this.areaTotal = areaTotal;
    }

    public double getTiempoTotalSimulado() {
        return tiempoTotalSimulado;
    }

    public void setTiempoTotalSimulado(double tiempoTotalSimulado) {
        this.tiempoTotalSimulado = tiempoTotalSimulado;
    }

    public double getAreaOcupadaInicial() {
        return areaOcupadaInicial;
    }

    public void setAreaOcupadaInicial(double areaOcupadaInicial) {
        this.areaOcupadaInicial = areaOcupadaInicial;
    }

    public int getEventosSimples() {
        return eventosSimples;
    }

    public void setEventosSimples(int eventosSimples) {
        this.eventosSimples = eventosSimples;
    }

    public int getEventosMultiples() {
        return eventosMultiples;
    }

    public void setEventosMultiples(int eventosMultiples) {
        this.eventosMultiples = eventosMultiples;
    }

    public double getMinimalTimeStep() {
        return minimalTimeStep;
    }

    public void setMinimalTimeStep(double minimalTimeStep) {
        this.minimalTimeStep = minimalTimeStep;
    }
    
    /**
     * Ingreso el par de objetos que involucran el evento simple
     * @param par para de valores que corresponden a los objetos originales que 
     * colisionan
     */
    public void addEventoSimple(Pair<Integer> par){
        colisionesPorObjeto[par.first]++;
        colisionesPorObjeto[par.second]++;
        this.eventosSimples++;
    }
    
    public void addEventoMultiple(List<Integer> objectsIdList){
        for (Integer i : objectsIdList) {
            colisionesPorObjeto[i]++;
        }
        this.eventosMultiples++;
    }
    
    public List<String> getText(){
        List<String> s =  new ArrayList<>();
        
        s.add("areaTotal:" + areaTotal);
        s.add("tiempoTotalSimulado:" + tiempoTotalSimulado);
        s.add("areaOcupadaInicial:" + areaOcupadaInicial);
        s.add("eventosSimples:" + eventosSimples);
        s.add("eventosMultiples:" + eventosMultiples);
        s.add("colisionesPorObjeto:" + Arrays.toString(colisionesPorObjeto));
        s.add("minimalTimeStep:" + minimalTimeStep);
        
        return s;
    }
    
    /**
     * Metodo util para las colisiones estáticas
     * @param listaColisiones 
     */
    public void calcularEstaticas(List<Pair<Integer>> listaColisiones){
        for (Pair<Integer> parColision : listaColisiones) {
            addEventoSimple(parColision);
        }
    }

    public void setObserver(Observer o) {
        super.addObserver(o);
    }
    
    public boolean hasMultipleEvents(){
        return (this.eventosMultiples!=0);
    }
}
