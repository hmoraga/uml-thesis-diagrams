/*
 * Based on Example of Java SE 8 for programmers of Paul & Harvey Deitel. 
 * Ch. 15.4 Sequencial-Access Text Files 
 */
package apps;

import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.NoSuchElementException;

/**
 *
 * @author hmoraga
 */
public class CreateTextFile {
    private Formatter output; // output text to a file
    private String fileName, texto;
    
    // constructor privado que elimina la posibilidad de llamar directamente a
    // construir mas de un objeto
    public CreateTextFile(String fileName, String texto){
        this.fileName = fileName;
        this.texto = texto;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
    
    public void create(){
        openFile(fileName);
        addRecords(texto);
        closeFile();
    }
        
    // open file
    private void openFile(String filename){
        try {
            output = new Formatter(filename);
        } catch (SecurityException securityException){
            System.err.println("Write permission denied. Terminating.");
            System.exit(1);
        } catch (FileNotFoundException fileNotFoundException){
            System.err.println("Error opening file. Terminating.");
            System.exit(1);
        }
    }
    
    private void addRecords(String record){
        try {
            output.format("%s", record);
        } catch (FormatterClosedException formatterClosedException){
            System.err.println("Error writing to file. Terminating.");  
        } catch (NoSuchElementException noSuchElementException){
            System.err.println("Invalid input.");  
        }
    }
    
    private void closeFile(){
        if (output !=null){
            output.close();
        }
    }
}
