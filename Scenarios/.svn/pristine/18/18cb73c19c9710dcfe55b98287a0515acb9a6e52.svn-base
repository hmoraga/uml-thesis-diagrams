package scenarios;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import kinetic.Object2D;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import primitives.Pair;
import primitives.Point2D;

/**
 * Clase que genera modelos. En rigor el model file generator me genera los 
 * objetos dinámicos y dadas sus velocidades, el tiempo de simulación y el paso
 * temporal, me va generando las fotos de los objetos estáticos, los que son 
 * guardados en una carpeta, con el numero del experimento, y los archivos
 * fileXXXX con la numeracion en orden consecutivo de los archivos generados.
 * <P>
 * Como segundo paso me genera el archivo cinético, dentro de su propia carpeta. 
 * 
 * @author hmoraga
 */
public final class ModelFilesWriter {    
    private final List<Pair<Double>> velocidades;    // lista de velocidades, un par por cada objeto
    private final Pair<Pair<Double>> limites;   // dimensiones limite: [a,b]x[c,d]
    private final List<List<Point2D>> listaPuntosObjetos;  // lista de puntos por cada objeto
    private final double tinicial, tfinal;
    private final int timeStepInFPS;
    private final String dirName;

    /**
     * Constructor de la clase para cuando se ingresan los objetos y sus velocidades
     * iniciales.
     * @param listaPuntosObjetos
     * @param dimensiones
     * @param velocidades
     * @param tinic
     * @param tfinal
     * @param timeStepInFPS
     * @param dirName
     * @throws java.io.IOException
     */
    public ModelFilesWriter(List<List<Point2D>> listaPuntosObjetos, Pair<Pair<Double>> dimensiones,
            List<Pair<Double>> velocidades, double tinic, double tfinal, int timeStepInFPS,
            String dirName) throws IOException{
        assert (tinic < tfinal);
        
        this.tinicial = tinic;
        this.tfinal = tfinal;
        this.timeStepInFPS = timeStepInFPS;
        this.limites = dimensiones;
        this.velocidades = velocidades;
        this.dirName = dirName;
        this.listaPuntosObjetos = listaPuntosObjetos;
    }

    public ModelFilesWriter(List<List<Point2D>> listaPuntosObjetos, Pair<Pair<Double>> dimensiones,
            List<Pair<Double>> velocidades, double tinic, double tfinal, String dirName)  throws IOException{
        this(listaPuntosObjetos, dimensiones, velocidades, tinic, tfinal, 0, dirName);
    }
    
    /**
     * 
     * @throws java.io.IOException
     */
    public void generarArchivosEstaticos() throws IOException, SecurityException{
        List<Object2D> listaObjetos = new ArrayList<>(listaPuntosObjetos.size());
        
        // creacion de los objetos dados como dato
        for (int i = 0; i < listaPuntosObjetos.size(); i++) {
            listaObjetos.add(new Object2D(i, listaPuntosObjetos.get(i), velocidades.get(i)));
        }

        File theDir = new File(dirName + "\\static\\inputs");
        Charset charset = Charset.forName("UTF-8");
        DecimalFormat myFormatter = new DecimalFormat("00000");
        int cantFrames = (int)((tfinal-tinicial)*timeStepInFPS);
                
        if (!theDir.exists()) {
            System.out.println("creating directory: " + theDir.toString());
            theDir.mkdirs();
            System.out.println("DIRECTORIES created");  
        }
            
        List<String> listaTexto = new ArrayList<>();
        String filePath = theDir.getCanonicalPath();
        
        for (int i=0; i<cantFrames; i++){
            double time = tinicial+(double)i/timeStepInFPS;
            System.out.println("t="+time);
            
            // genero un archivo por cada timeStep
            String num = myFormatter.format(i);
            String fileName = filePath + "\\" + "file" + num +".txt";
            
            // escribo el contenido del archivo
            // primero los datos del frame en cuestion
            // despues cada uno de los objetos
            listaTexto.add("//frame:"+i);
            listaTexto.add("//time:"+time);
            listaTexto.add("//marco:"+limites.toString());
            listaTexto.add("//dimensiones:2");
            
            // indico cada objeto como una lista con el formato
            // cantPuntos cantDimensiones punto_0x punto_0y ...
            for (Object2D obj : listaObjetos) {
                // cada objeto es una lista de //kinetic 
                ArrayList<Point2D> listaTempPuntos = new ArrayList(obj.getPosition(time));
                String linea = listaTempPuntos.size()+" ";
                
                for (Point2D p : listaTempPuntos)
                    linea = linea.concat(p.getX()+" "+p.getY()+" ");
                
                linea = linea.substring(0, linea.length()-1);
                listaTexto.add(linea);
            }
            
            Files.write(Paths.get(fileName), listaTexto, charset);
            System.out.println("Archivo " + fileName + " creado");
            
            listaTexto.clear();
        }
    }
        
    /**
     * Genero un archivo de texto con los datos que necesito serán leidos
     * tanto por el modelo cinetico como el estático
     * 
     * @throws IOException si no se puede crear el directorio 
     */
    public void generarArchivoCinetico() throws IOException{
        List<String> listaTexto = new ArrayList<>();
        List<Object2D> listaObjetos = new ArrayList<>(listaPuntosObjetos.size());    // lista de objetos2D

        // creacion de los objetos dados como dato
        for (int i = 0; i < listaPuntosObjetos.size(); i++) {
            listaObjetos.add(new Object2D(i, listaPuntosObjetos.get(i), velocidades.get(i)));
        }

        // genero un solo archivo
        File theDir = new File(dirName + "\\kinetic\\input");
        Charset charset = Charset.forName("UTF-8");
                
        if (!theDir.exists()) {
            System.out.println("creating directory: " + theDir);
            theDir.mkdirs();
            System.out.println("DIRECTORIES created");  
        }
            
        // escribo el contenido del archivo
        // primero los datos de tiempo marco y dimensiones de la simulacion
        // despues cada uno de los objetos
        listaTexto.add("//time:["+tinicial+","+tfinal+"]");
        listaTexto.add("//marco:"+limites.toString());
        listaTexto.add("//dimensiones:2");
        
        // finalmente dos lineas por objeto donde: la primera linea con los dos
        // primeros valores que son velocidades (en x e y), 
        // la segunda linea es la cantidad de vertices del
        // objeto, seguido por los x,y del objeto partiendo por un borde
        // cualquiera.
        
        for (Object2D obj : listaObjetos) {
            Pair<Double> velocidad = obj.getVelocidad();
            String auxText = String.valueOf(velocidad.first)+" "+String.valueOf(velocidad.second);
            listaTexto.add(auxText);
            
            auxText = "";
            List<Point2D> listaPuntos = obj.getListaPuntos();
            auxText += (listaPuntos.size() + " ");
            
            for (Point2D punto : listaPuntos)
                auxText += (punto.getX()+" "+punto.getY()+ " ");
            
            auxText = auxText.substring(0, auxText.length()-1);
            listaTexto.add(auxText);
        }

        Path arch = Paths.get(theDir.toString() + "\\kinetic.txt");        
        Files.write(arch, listaTexto, charset);
        System.out.println("File cinetico creado");
        
        listaTexto.clear();
    }    
}
