package scenarios;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import poligons.Polygon2D;
import poligons.PolygonType;
import poligons.PolygonFactory;
import primitives.Pair;
import primitives.Point2D;

/**
 * Clase util solo para los objetos cineticos
 * @author hmoraga
 */
public class ObjectRandomGenerator {
    private List<Point2D> listaPuntos;
    private final Pair<Double> velocidades;

    /**
     * 
     * @param dimensiones dimensiones en largo y ancho
     * @param percentOfTotalArea porcentaje del area total
     * @param numberObjects cantidad de objetos de la simulación
     * @param speedAsPercentOfScreen
     * @param timeInterval
     * @param rnd 
     */
    public ObjectRandomGenerator(Pair<Pair<Double>> dimensiones,
            double percentOfTotalArea, double speedAsPercentOfScreen,
            double timeInterval, Random rnd){
        listaPuntos = new ArrayList<>();
        
        double dimX = dimensiones.first.second-dimensiones.first.first;
        double dimY = dimensiones.second.second-dimensiones.second.first;
        double areaTotal = dimX*dimY;
        
        // obtencion de un polygono regular al azar
        PolygonType polyType = PolygonType.values()[rnd.nextInt(PolygonType.values().length)];
        
        // area Individual de cada objeto
        double areaIndividual = areaTotal*percentOfTotalArea;

       // creo el objeto
        Polygon2D poli = PolygonFactory.getPolygon(polyType, areaIndividual, true);
        
        double posX = obtenerAzarPorRango(dimensiones.first.first, dimensiones.first.second, rnd);
        double posY = obtenerAzarPorRango(dimensiones.second.first, dimensiones.second.second, rnd);
 
        // calculo el min y max de cada coordenada
        double minX = poli.getBox().getMinX();
        double maxX = poli.getBox().getMaxX();
        double minY = poli.getBox().getMinY();
        double maxY = poli.getBox().getMaxY();
        
        if (posX-minX < dimensiones.first.first)
            posX += Math.abs(minX-dimensiones.first.first);
            
        if (posX+maxX > dimensiones.first.second)
            posX -= Math.abs(maxX-dimensiones.first.second);
            
        if (posY-minY < dimensiones.second.first)
            posY += Math.abs(minY-dimensiones.second.first);
            
        if (posY+maxY > dimensiones.second.second)
            posY -= Math.abs(maxY-dimensiones.second.second);
        
        poli.desplazar(posX, posY);
        
        listaPuntos = poli.getVertices();
            
        // obtencion de su velocidad al azar
        double vx = obtenerAzarPorRango(-dimX*speedAsPercentOfScreen, dimX*speedAsPercentOfScreen, rnd);
        double vy = obtenerAzarPorRango(-dimY*speedAsPercentOfScreen, dimY*speedAsPercentOfScreen, rnd);
            
        velocidades = new Pair<>(vx, vy);
    }

    public List<Point2D> getListaPuntos() {
        return listaPuntos;
    }

    public Pair<Double> getVelocidades() {
        return velocidades;
    }
    
    private double obtenerAzarPorRango(double minimo, double maximo, Random rnd) {
        return rnd.nextDouble()*(maximo-minimo)+minimo;
    }    
}
