package scenarios.frameStudies;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import apps.ModelFilesReader;
import apps.ModelFilesWriter;
import apps.ObjectRandomGenerator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import poligons.AnyPolygon2D;
import primitives.Pair;
import primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class FrameStudies {
    public static void main(String[] args) throws IOException {
        long timestamp = System.currentTimeMillis();
        Random rnd = new Random(timestamp);

        boolean verbose = false;

        // cantidad de objetos en la simulación
        // valores posibles: 0, 1, 2, 5, 10, 20, 30, 50, 100, 200, 500, 1000...
        int cantObjetos = Integer.parseInt(args[0]);
        //int cantObjetos = 1000;

        // Se elige un marco de simulacion
        // valores posibles (-3.0,3.0)-(-5.0,5.0)-(-10.0,10.0)-(-20.0,20.0)-(-50.0,50.0)
        Pair<Pair<Double>> dimensiones = new Pair<>(new Pair<>(-10.0, 10.0), new Pair<>(-10.0, 10.0));
        // tamaño máximo de un objeto (como porcentaje de las dimensiones)
        double objectMaxSize = Double.parseDouble(args[1]) / cantObjetos;
        //double objectMaxSize = 0.01 / cantObjetos;

        // Se elige un tiempo de simulacion: desde 0 al valor elegido
        // valores posibles 1.0, 2.0, 5.0, 10.0, 20.0
        double simulationTime = 1.0;

        // Se elige un timeStep (en FPS), util solo para las simulaciones estáticas
        int[] timeStep = new int[]{1, 2, 5, 10, 20, 50, 100, 200, 500, 1000};

        // Se elige un valor de lista de velocidades, basados en un porcentaje del radio
        // del objeto. Esta lista de velocidades es un par de valores para cada objeto, 
        // representando la velocidad por cada eje
        // se generan pares aleatorios de velocidades con los siguientes 
        // valores POSIBLES: 0.0,0.1,0.3,0.5,1.0,1.5
        Pair<Double> velocidad = new Pair<>(0.3, 1.5);

        for (int i = 0; i < timeStep.length; i++) {
            int ts = timeStep[i];
            String path = "D:\\experimentos\\experimento_" + String.valueOf(timestamp) + "_frames_" + ts;

            if (verbose) {
                System.out.println("Generando experimento " + path);
            }
            
            // area de la simulacion
            double areaSimulacion, areaTotalObjetos = 0;
            
            List<List<Point2D>> objetos = new ArrayList<>(cantObjetos);
            List<Pair<Double>> velocidadesPorObjeto = new ArrayList<>(cantObjetos);
            
            for (int j = 0; j < cantObjetos; j++) {
                // generador de objetos al azar
                ObjectRandomGenerator org = new ObjectRandomGenerator(dimensiones, objectMaxSize, velocidad, simulationTime, rnd);
                // agrego a la lista de velocidades
                velocidadesPorObjeto.add(org.getVelocidades());
                // agrego a la lista de puntos por objeto
                objetos.add(org.getListaPuntos());
            }

            for (List<Point2D> objeto : objetos) {
                areaTotalObjetos += new AnyPolygon2D(objeto, false).getArea();
            }

            areaSimulacion = (dimensiones.first.second - dimensiones.first.first) * (dimensiones.second.second - dimensiones.second.first);
            if (verbose) {
                System.out.println("Area del frame original:" + areaSimulacion);
                System.out.println("Area de los poligonos iniciales:" + areaTotalObjetos);
            }

            //llamo al ModelFilesWriter
            ModelFilesWriter mfw = new ModelFilesWriter(objetos, dimensiones, velocidadesPorObjeto, 0.0, simulationTime, ts, path, verbose);

            // genero el archivo cinetico
            //mfw.generarArchivoCinetico();
            
            // genero los archivos estaticos
            mfw.generarArchivosEstaticos();

            // garbage collector
            velocidadesPorObjeto.clear();
            objetos.clear();

            ////////////////////////////////////////////////////////////
            if (verbose) {
                System.out.println("Leyendo modelo " + path); // lectura de los archivos
            }
            // leyendo archivos estaticos
            ModelFilesReader mfr = new ModelFilesReader(path, "static", verbose);
            mfr.exec(); // ejecuto la lectura de los archivos

            // leyendo archivos cineticos
            //mfr = new ModelFilesReader(path, "kinetic", verbose);
            //mfr.exec(); // ejecuto la lectura de los archivos
        }

        System.out.println("Fin de la simulacion");
        System.gc();
    }
}
