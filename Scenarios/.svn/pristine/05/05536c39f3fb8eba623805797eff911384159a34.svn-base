package apps;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import poligons.Polygon2D;
import poligons.PolygonFactory;
import poligons.PolygonType;
import primitives.Pair;
import primitives.Point2D;

/**
 * Clase util solo para los objetos cineticos
 * @author hmoraga
 */
public class ObjectRandomGenerator {
    private List<Point2D> listaPuntos;
    private final Pair<Double> velocidades;

    /**
     * 
     * @param dimensiones dimensiones en largo y ancho
     * @param percentOfTotalArea porcentaje del area total
     * @param timeInterval intervalo de tiempo (partiendo de 0.0)
     * @param rnd 
     */
    public ObjectRandomGenerator(Pair<Pair<Double>> dimensiones,
            double percentOfTotalArea, double timeInterval, Random rnd){
        listaPuntos = new ArrayList<>();
        
        double dimX = dimensiones.first.second-dimensiones.first.first;
        double dimY = dimensiones.second.second-dimensiones.second.first;
        double areaTotal = dimX*dimY;
        
        // obtencion de un polygono regular al azar
        PolygonType polyType = PolygonType.values()[rnd.nextInt(PolygonType.values().length)];
        //System.out.println("poligono elegido: " + polyType.toString());
        
        // area Individual de cada objeto
        double areaIndividual = areaTotal*percentOfTotalArea;

       // creo el objeto
        Polygon2D poli = PolygonFactory.getPolygon(polyType, areaIndividual, true);
        //if (poli.getVertices().get(0).getX() == Double.NaN){
        //    System.out.println("polgono invalido");
        //}
        
        double posX = obtenerAzarPorRango(dimensiones.first.first, dimensiones.first.second, rnd);
        double posY = obtenerAzarPorRango(dimensiones.second.first, dimensiones.second.second, rnd);
 
        // calculo el min y max de cada coordenada
        double minX = poli.getBox().getMinX();
        double maxX = poli.getBox().getMaxX();
        double minY = poli.getBox().getMinY();
        double maxY = poli.getBox().getMaxY();
        
        if (posX+minX < dimensiones.first.first)
            posX = dimensiones.first.first - minX;
        else if (posX+maxX > dimensiones.first.second)
            posX = dimensiones.first.second - maxX;
            
        if (posY+minY < dimensiones.second.first)
            posY = dimensiones.second.first - minY;
        else if (posY+maxY > dimensiones.second.second)
            posY = dimensiones.second.second - maxY;
        
        poli.desplazar(posX, posY);
        
        listaPuntos = poli.getVertices();
            
        // obtencion de su velocidad al azar
        // se elije la mayor diferencia entre anchos en X y en Y
        double ancho = Math.abs(poli.getBox().getMaxX()-poli.getBox().getMinX());
        double alto = Math.abs(poli.getBox().getMaxY()-poli.getBox().getMinY());
        double valor = obtenerAzarPorRango(-3.5*Math.max(ancho, alto), 3.5*Math.max(ancho, alto), rnd); // 3.5 veces el ancho del objeto (por segundo)
        double anguloAzar = obtenerAzarPorRango(0, 2*Math.PI, rnd);
        double vx = valor*Math.cos(anguloAzar);
        double vy = valor*Math.sin(anguloAzar);
        // se crea el vector velocidad
        velocidades = new Pair<>(vx, vy);
    }

    /**
     * 
     * @param dimensiones dimensiones en largo y ancho
     * @param percentOfTotalArea porcentaje del area total
     * @param objectSpeedRanges par de valores de velocidades minimo y maximo como porcentaje del radio de un objeto
     * @param timeInterval intervalo de tiempo (partiendo de 0.0)
     * @param rnd 
     */
    public ObjectRandomGenerator(Pair<Pair<Double>> dimensiones,
            double percentOfTotalArea, Pair<Double> objectSpeedRanges,
            double timeInterval, Random rnd){
        listaPuntos = new ArrayList<>();
        
        double dimX = dimensiones.first.second-dimensiones.first.first;
        double dimY = dimensiones.second.second-dimensiones.second.first;
        double areaTotal = dimX*dimY;
        
        // obtencion de un polygono regular al azar
        PolygonType polyType = PolygonType.values()[rnd.nextInt(PolygonType.values().length)];
        //System.out.println("poligono elegido: " + polyType.toString());
        
        // area Individual de cada objeto
        double areaIndividual = areaTotal*percentOfTotalArea;

       // creo el objeto
        Polygon2D poli = PolygonFactory.getPolygon(polyType, areaIndividual, true);
        //if (poli.getVertices().get(0).getX() == Double.NaN){
        //    System.out.println("polgono invalido");
        //}
        
        double posX = obtenerAzarPorRango(dimensiones.first.first, dimensiones.first.second, rnd);
        double posY = obtenerAzarPorRango(dimensiones.second.first, dimensiones.second.second, rnd);
 
        // calculo el min y max de cada coordenada
        double minX = poli.getBox().getMinX();
        double maxX = poli.getBox().getMaxX();
        double minY = poli.getBox().getMinY();
        double maxY = poli.getBox().getMaxY();
        
        if (posX+minX < dimensiones.first.first)
            posX = dimensiones.first.first - minX;
        else if (posX+maxX > dimensiones.first.second)
            posX = dimensiones.first.second - maxX;
            
        if (posY+minY < dimensiones.second.first)
            posY = dimensiones.second.first - minY;
        else if (posY+maxY > dimensiones.second.second)
            posY = dimensiones.second.second - maxY;
        
        poli.desplazar(posX, posY);
        
        listaPuntos = poli.getVertices();
            
        // obtencion de su velocidad al azar
        // se elije la mayor diferencia entre anchos en X y en Y
        double ancho = Math.abs(poli.getBox().getMaxX()-poli.getBox().getMinX());
        double alto = Math.abs(poli.getBox().getMaxY()-poli.getBox().getMinY());
        double valor = Math.max(ancho, alto)*obtenerAzarPorRango(objectSpeedRanges.first, objectSpeedRanges.second, rnd);
        double anguloAzar = obtenerAzarPorRango(0, 2*Math.PI, rnd);
        double vx = valor*Math.cos(anguloAzar);
        double vy = valor*Math.sin(anguloAzar);
        // se crea el vector velocidad
        velocidades = new Pair<>(vx, vy);
    }

    public List<Point2D> getListaPuntos() {
        return listaPuntos;
    }

    public Pair<Double> getVelocidades() {
        return velocidades;
    }
    
    private double obtenerAzarPorRango(double minimo, double maximo, Random rnd) {
        return rnd.nextDouble()*(maximo-minimo)+minimo;
    }    
}
