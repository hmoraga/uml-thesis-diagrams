
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rammodels.testscene;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import kinetic.Object2D;
import primitives.Pair;
import primitives.Point2D;
import poligons.Polygon2D;
import poligons.AnyPolygon2D;
import rammodels.apps.Simulation;
import rammodels.apps.SimulationKinetic;
import rammodels.apps.SimulationStaticDirectSweepAndPrune;
import rammodels.apps.SimulationStaticIncrementalSweepAndPrune;

/**
 *
 * @author hmoraga
 */
public class TestTresObjetos {
    public static void main(String[] args) throws IOException {
        // distintos marcos de simulación
        List<Pair<Double>> dimensiones = new ArrayList<>(2);
        dimensiones.add(new Pair<>(-10.0,10.0));
        dimensiones.add(new Pair<>(-10.0,10.0));
        
        List<Point2D> listaPuntos = new ArrayList<>();
        List<List<Point2D>> listaObjetos = new ArrayList<>();
        List<Pair<Double>> listaVelocidades = new ArrayList<>();
        double areaTotalObjetos = 0;
        
        //int timeStep = Integer.parseInt(args[0]);  // in FPS
        // solo para debug
        int timeStep = 10; // en FPS
        
        double tiempoSimulacion = 10.0;
        //String dirName = "D:\\experimentos\\tresObjetos-"+timeStep;
        
        // area inicial
        double areaSimulacion = (dimensiones.get(0).second-dimensiones.get(0).first)*(dimensiones.get(1).second-dimensiones.get(1).first);
        
        // velocidades (por segundo)
        listaVelocidades.add(new Pair<>(1.0, -1.0));
        listaVelocidades.add(new Pair<>(-2.0, 0.0));
        listaVelocidades.add(new Pair<>(-1.0, 2.0));

        // puntos del primer objeto
        listaPuntos.add(new Point2D(-3.0, 1.0));
        listaPuntos.add(new Point2D(-1.0, 1.5));
        listaPuntos.add(new Point2D(-5.0, 2.0));
        listaPuntos.add(new Point2D(-5.0, 1.5));

        // se agrega a la lista de poligonos y objetos
        Polygon2D polyAuxiliar = new AnyPolygon2D(listaPuntos, false);
        //listaPoligonos.add(polyAuxiliar);
        areaTotalObjetos+=(polyAuxiliar.getArea());
        listaObjetos.add(listaPuntos);
        //listaBoxes.add(polyAuxiliar.getBox());
        
        // segundo objeto
        listaPuntos = new ArrayList<>();
        listaPuntos.add(new Point2D(3.0, -2.0));
        listaPuntos.add(new Point2D(2.0, 2.0));
        listaPuntos.add(new Point2D(1.0, -2.0));
        
        // se agrega a la lista de objetos
        polyAuxiliar = new AnyPolygon2D(listaPuntos, false);
        //listaPoligonos.add(polyAuxiliar);
        areaTotalObjetos+=(polyAuxiliar.getArea());
        listaObjetos.add(listaPuntos);
        //listaBoxes.add(polyAuxiliar.getBox());

        // tercer objeto
        listaPuntos = new ArrayList<>();
        listaPuntos.add(new Point2D(-3.0, -4.0));
        listaPuntos.add(new Point2D(-1.0, -3.0));
        listaPuntos.add(new Point2D(-2.0, -2.0));        
        listaPuntos.add(new Point2D(-3.0, -2.0));

        // genero la lista de poligonos
        polyAuxiliar = new AnyPolygon2D(listaPuntos, false);
        //listaPoligonos.add(polyAuxiliar);
        areaTotalObjetos+=(polyAuxiliar.getArea());
        listaObjetos.add(listaPuntos);
        //listaBoxes.add(polyAuxiliar.getBox());

        System.out.println("Area del frame original:" + areaSimulacion);
        System.out.println("Area de los poligonos iniciales:" + areaTotalObjetos);
        System.out.println("Simulacion Swep and Prune 1 Frame");
        
        assert(timeStep>0);
        Simulation simulaSpOneFrame = new SimulationStaticDirectSweepAndPrune(listaObjetos, dimensiones, listaVelocidades, 0.0, tiempoSimulacion, timeStep, true);
        simulaSpOneFrame.exec();
        
        // simulacion del Sweep and prune verdadero
        System.out.println("Simulacion Swep and Prune Real");
        Simulation simulaRealSp = new SimulationStaticIncrementalSweepAndPrune(listaObjetos, listaVelocidades, timeStep, 0.0, tiempoSimulacion, false);
        simulaRealSp.exec();
        
        // KSP
        // creo lista de Object2D necesarios para la simulacion cinética
        List<Object2D> listaObject2D = new ArrayList<>();
        
        for (int i=0; i< listaObjetos.size(); i++) {
            Object2D obj = new Object2D(i, listaObjetos.get(i), listaVelocidades.get(i));
            listaObject2D.add(obj);
        }
        
        Simulation simula = new SimulationKinetic(listaObject2D, 0.0, tiempoSimulacion, dimensiones, true);
        simula.exec();
    }
//        // leo archivo y finalmente se utiliza el metodo exec
//        System.out.println("Leyendo archivos para RealSAP");
//        mfr = new ModelFilesReader(dirName, "realSAP", false);
//        mfr.exec();
//        
//        // leo archivo y finalmente se utiliza el metodo exec
//        mfr = new ModelFilesReader(dirName, "kinetic", false);
//        mfr.exec();
//        
//        // clear
//        listaObjetos.clear();
//        listaVelocidades.clear();
//        listaPuntos.clear();
//    }
}
