package rammodels.testscene;

import java.util.ArrayList;
import java.util.List;
import primitives.Pair;
import primitives.Point2D;
import rammodels.apps.SimulationStaticIncrementalSweepAndPrune;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hmoraga
 */
public class TestSPReal {
    public static void main(String[] args) {
        // SOLO DEBUG
        //int cantCajas = 10;
        int cantCajas = Integer.valueOf(args[0]);
        
        List<List<Point2D>> listaObjetos = new ArrayList<>(cantCajas);
        List<Pair<Double>> listaVelocidades = new ArrayList<>(cantCajas);
        int timeStepInFPS = 10;
        
        for (int i = 1; i <= cantCajas; i++) {
            List<Point2D> objeto = new ArrayList<>();
            objeto.add(new Point2D(0, -i));
            objeto.add(new Point2D(i, 0));
            objeto.add(new Point2D(0, i));
            objeto.add(new Point2D(-i, 0));
            
            listaObjetos.add(objeto);
        }
        
        for (int i = 0; i < cantCajas; i++) {
            listaVelocidades.add(new Pair<>((cantCajas-i)*Math.pow(-1, i), 0.0));
        }

        SimulationStaticIncrementalSweepAndPrune rsp = new SimulationStaticIncrementalSweepAndPrune(listaObjetos, listaVelocidades, timeStepInFPS, 0.0, 1.0, true);
        rsp.exec();        
    }
}
