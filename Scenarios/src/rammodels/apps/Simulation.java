package rammodels.apps;

import java.io.IOException;
import sap.Statistics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Clase que genera simulacion
 * @author hmoraga
 */
public interface Simulation {
    void exec();
    void writeStatisticsToFile(String filePath, String fileName) throws IOException;
    Statistics getStatistics();
}
