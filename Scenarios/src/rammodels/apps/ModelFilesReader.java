package rammodels.apps;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kinetic.Object2D;
import primitives.Pair;
import primitives.Point2D;

/**
 * Clase que lee el modelo desde un archivo de texto
 * @author hmoraga
 */
public class ModelFilesReader {
    private final String filePath;    //filePath del directorio inicial a leer
    private final String type;    // tipo de metodo a ejecutar o crear
    //private double tinicial, tfinal;
    private final boolean verbose;
    
    /**
     * 
     * @param filePath dir del experimento a leer (la ruta completa)
     * @param simulationType tipo de simulacion: static, kinetic, bruteForce
     * @param verbose
     */
    public ModelFilesReader(String filePath, String simulationType, boolean verbose) {
        this.filePath = filePath;
        // inicializo objetos
        this.type = simulationType;
        this.verbose = verbose;
    }

//    public void exec() throws IOException{
//        /*switch (type) {
//            case "static":
//            {
//                // leer el total de archivos del directorio
//                File folder = new File(this.filePath+"\\static\\inputs");
//                if (verbose)
//                    System.out.println("Leyendo archivos en la ruta " + folder.toString());
//                File[] temp = folder.listFiles();
//                    
//                for (File arch : temp) {
//                    if (arch.isFile()){
//                        Simulation simula = readStaticFile(arch.getCanonicalPath());
//                        // ejecuto la simulacion
//                        simula.exec();
//                        // grabo las estadisticas a un archivo
//                        try {
//                            simula.writeStatisticsToFile(this.filePath+"\\static", arch.getName());
//                        } catch (IOException ex){
//                            Logger.getLogger(ModelFilesReader.class.getName()).log(Level.SEVERE, "problema al leer archivo", ex);
//                        }
//                    }
//                }
//                break;
//            }
//            
//            case "kinetic":
//            {
//                // leer archivo unico
//                try {
//                    String s= this.filePath+"\\kinetic\\input\\kinetic.txt";
//                    Simulation simula = readKineticFile(s);
//                    // ejecuto la simulacion
//                    simula.exec();
//                    
//                    if (verbose)
//                        System.out.println(simula.getStatistics().getText()); // imprimo las estadisticas
//                    // grabo las estadisticas a un archivo
//                    simula.writeStatisticsToFile(this.filePath+"\\kinetic\\output", "kinetic.txt");
//                } catch (IOException ex) {
//                    Logger.getLogger(ModelFilesReader.class.getName()).log(Level.SEVERE, "problema al grabar archivo", ex);
//                }
//                
//                break;
//            }
//            
//            case "realSAP":
//            {
//                // leer el total de archivos del directorio
//                File folder = new File(this.filePath+"\\static\\inputs");
//                if (verbose)
//                    System.out.println("Leyendo archivos en la ruta " + folder.toString());
//                File[] fileList = folder.listFiles();
//                
//                // el primer archivo debe ser el que inicialize todas las
//                // estructuras, escencialmente es ejecutar una vez el SAP estatico
//                // el resto de archivos, lo que se debe armar es la lista de 
//                // indices de los objetos (sus extremos) y compararla con la que
//                // existe inicialmente. Si hay cambios se recalcula la lista de
//                // colisiones.
//                // esto se logra internamente con la construccion de un objeto
//                // RealSP que es singleton, el cual al inicio genera todas las 
//                // estructuras internas necesarias y despues lo único que necesita
//                // es actualizarse.
//                SimulationStaticRealSAP srSAP = new SimulationStaticRealSAP(fileList, verbose);
//                
//                srSAP.exec();
//                
//                srSAP.writeStatisticsToFile(this.filePath+"\\RealSP", "output.txt");
//                break;
//            }
//        }*/
//    }
//    
//    /**
//     * Método me debe generar todas las estructuras necesarias para leer UN 
//     * archivo estático y luego el método exec me ejecutará el metodo respectivo.
//     * @param fileName archivo estático a leer
//     * @throws java.io.IOException si el archivo no se encuentra
//     */
//    private SimulationStatic readStaticFile(String fileName) throws IOException {
//        int numDims;
//        int frameNumber;
//        Pattern pattern;
//        Matcher matcher;
//        
//        if (verbose)
//            System.out.println("Leyendo archivo " + fileName); // leo los datos del archivo
//
//        Charset utf8 = Charset.forName("UTF-8");        
//        List<String> lista = Files.readAllLines(Paths.get(fileName), utf8);
//        
//        // frame, es crucial para la creacion del archivo de salida 
//        pattern = Pattern.compile(".*(\\d+)");
//        matcher = pattern.matcher(lista.get(0));
//
//        frameNumber = (matcher.find())?Integer.valueOf(matcher.group(1)):0;            
//        matcher.reset();
//        
//        pattern = Pattern.compile(".*\\((-?\\d+.\\d+),(-?\\d+.\\d+)\\),\\((-?\\d+.\\d+),(-?\\d+.\\d+)\\)");
//        matcher = pattern.matcher(lista.get(2));
//        
//        List<Pair<Double>> limitesLocal = new ArrayList<>(2);
//        
//        if (matcher.find()){
//            String grupo1 = matcher.group(1);
//            String grupo2 = matcher.group(2);
//            String grupo3 = matcher.group(3);
//            String grupo4 = matcher.group(4);
//            
//            // dimensiones en x
//            limitesLocal.add(new Pair<>(Double.valueOf(grupo1), Double.valueOf(grupo2)));
//            // dimensiones en y
//            limitesLocal.add(new Pair<>(Double.valueOf(grupo3), Double.valueOf(grupo4)));
//        } else {
//            // dimensiones en x
//            limitesLocal.add(new Pair<>(-10.0, 10.0));
//            // dimensiones en y
//            limitesLocal.add(new Pair<>(-10.0, 10.0));            
//        }
//        matcher.reset();
//        
//        pattern = Pattern.compile(".*(\\d+)");
//        matcher = pattern.matcher(lista.get(3));
//        
//        if (matcher.find()){
//            String grupo1 = matcher.group(1);
//            numDims = Integer.valueOf(grupo1);
//        } else {
//            numDims = 2;
//        }
//        matcher.reset();
//
//        List<List<Point2D>> listaPuntosObjetosLocal = new ArrayList<>((lista.size()-4)/2); // arreglo de arreglo de puntos
//        // proceso los datos que se repiten en todos los archivos
//        for (String s : lista.subList(4, lista.size())) {
//            // primer dato es la cantidad de puntos del objeto
//            pattern = Pattern.compile("^(\\d+)\\s+(.*)");
//            matcher = pattern.matcher(s);
//            
//            if (matcher.find()){
//                int cantPuntos = Integer.valueOf(matcher.group(1));
//                List<Point2D> listaPuntos = new ArrayList<>(cantPuntos);
//                String[] aux = matcher.group(2).split("\\s+");
//                
//                assert(aux.length == numDims*cantPuntos);
//                
//                if (numDims==2){
//                    for (int i=0; i<cantPuntos; i++){
//                        double posX = Double.valueOf(aux[2*i]);
//                        double posY = Double.valueOf(aux[2*i+1]);
//                    
//                        listaPuntos.add(new Point2D(posX, posY));
//                    }
//                
//                    listaPuntosObjetosLocal.add(listaPuntos);
//                }
//            }
//            matcher.reset();
//        }
//        return new SimulationStatic(listaPuntosObjetosLocal, limitesLocal, frameNumber, verbose);
//    }
//
//    /**
//     * Método me debe generar todas las estructuras necesarias para leer un 
//     * archivo dinámico y luego el método exec me ejecutará el metodo respectivo.
//     * @param fileName archivo estático a leer
//     * @throws java.io.IOException si el archivo no se encuentra
//     */    
//    private SimulationKinetic readKineticFile(String fileName) throws IOException{
//        int numDims;
//        List<Pair<Double>> limitesLocal = new ArrayList<>(2);
//        Pair<Double> tiemposSimulacion;
//        Pattern pattern;
//        Matcher matcher;
//
//        if (verbose)
//            System.out.println("Leyendo archivo " + fileName); // leo los datos del archivo
//        Charset utf8 = Charset.forName("UTF-8");
//        List<String> lista = Files.readAllLines(Paths.get(fileName), utf8);
//        
//        pattern = Pattern.compile(".*(\\d+.\\d+),(\\d+.\\d+)");
//        matcher = pattern.matcher(lista.get(0));
//        
//        if (matcher.find()){
//            String grupo1 = matcher.group(1);
//            String grupo2 = matcher.group(2);
//            
//            tiemposSimulacion = new Pair<>(Double.valueOf(grupo1), Double.valueOf(grupo2));
//        } else {
//            tiemposSimulacion = new Pair<>(0.0, 10.0);
//        }    
//        matcher.reset();
//
//        pattern = Pattern.compile(".*\\((-?\\d+.\\d+),(-?\\d+.\\d+)\\),\\((-?\\d+.\\d+),(-?\\d+.\\d+)\\)");
//        matcher = pattern.matcher(lista.get(1));
//
//        if (matcher.find()){
//            String grupo1 = matcher.group(1);
//            String grupo2 = matcher.group(2);        
//            String grupo3 = matcher.group(3);
//            String grupo4 = matcher.group(4);        
//        
//            // dimensiones en x
//            limitesLocal.add(new Pair<>(Double.valueOf(grupo1), Double.valueOf(grupo2)));
//            // dimensiones en y
//            limitesLocal.add(new Pair<>(Double.valueOf(grupo3), Double.valueOf(grupo4)));
//        } else {
//            // dimensiones en x
//            limitesLocal.add(new Pair<>(-10.0, 10.0));
//            // dimensiones en y
//            limitesLocal.add(new Pair<>(-10.0, 10.0));            
//        }
//        matcher.reset();
//        
//        pattern = Pattern.compile(".*(\\d+)");
//        matcher = pattern.matcher(lista.get(2));
//      
//        if (matcher.find()){
//            String grupo1 = matcher.group(1);
//            numDims = Integer.valueOf(grupo1);
//        } else {
//            numDims = 2;
//        }
//        matcher.reset();
//        
//        int cont=0;
//        List<Object2D> listaObjetosLocal = new ArrayList<>((lista.size()-4)/2); // arreglo de objetos2D
//
//        // proceso los datos que se repiten
//        for (int i=3; i<lista.size(); i=i+2) {
//            Pair<Double> velocObjeto;
//
//            // primer dato es las velocidades del objeto
//            pattern = Pattern.compile("^(-?\\d+.\\d+)\\s+(-?\\d+.\\d+)");
//            matcher = pattern.matcher(lista.get(i));
//
//            if (matcher.find()){
//                String grupo1 = matcher.group(1);
//                String grupo2 = matcher.group(2);
//
//                velocObjeto = new Pair<>(Double.valueOf(grupo1), Double.valueOf(grupo2));
//            } else {
//                velocObjeto = new Pair<>(0.0, 0.0);
//            }
//            matcher.reset();
//            
//            // la otra linea
//            // primer dato es la cantidad de puntos del objeto
//            // el resto son las coordenadas de todos los puntos
//            pattern = Pattern.compile("^(\\d+)\\s+(.*)");
//            matcher = pattern.matcher(lista.get(i+1));
//            List<Point2D> listaPuntosObjeto = new ArrayList<>((lista.size()-4)/2);
//            
//            if (matcher.find()){
//                int cantPuntos = Integer.valueOf(matcher.group(1));
//                String[] aux = matcher.group(2).split("\\s+");
//                
//                if (numDims==2){
//                    for (int j=0; j<cantPuntos; j++){
//                        double posX = Double.valueOf(aux[2*j]);
//                        double posY = Double.valueOf(aux[2*j+1]);
//                    
//                        listaPuntosObjeto.add(new Point2D(posX, posY));
//                    }
//                }
//            }
//            matcher.reset();
//            listaObjetosLocal.add(new Object2D(cont++, listaPuntosObjeto, velocObjeto));
//        }
//        
//        return new SimulationKinetic(listaObjetosLocal, tiemposSimulacion.first, tiemposSimulacion.second, limitesLocal, verbose);
//    }
//
///*    private SimulationStaticRealSAP readRealSAP(String fileName) throws IOException{
//        int numDims, frameNumber;
//        Pattern pattern;
//        Matcher matcher;
//        
//        if (verbose)
//            System.out.println("Leyendo archivo " + fileName); // leo los datos del archivo
//        Charset utf8 = Charset.forName("UTF-8");
//        List<String> lista = Files.readAllLines(Paths.get(fileName), utf8);
//        
//        // frame, es crucial para la creacion del archivo de salida 
//        pattern = Pattern.compile(".*(\\d+)");
//        matcher = pattern.matcher(lista.get(0));
//
//        frameNumber = (matcher.find())?Integer.valueOf(matcher.group(1)):0;            
//        matcher.reset();
//        
//        pattern = Pattern.compile(".*\\((-?\\d+.\\d+),(-?\\d+.\\d+)\\),\\((-?\\d+.\\d+),(-?\\d+.\\d+)\\)");
//        matcher = pattern.matcher(lista.get(2));
// 
//        List<Pair<Double>> limitesLocal = new ArrayList<>(2);
// 
//        if (matcher.find()){
//            String grupo1 = matcher.group(1);
//            String grupo2 = matcher.group(2);
//            String grupo3 = matcher.group(3);
//            String grupo4 = matcher.group(4);
//            
//            // dimensiones en x
//            limitesLocal.add(new Pair<>(Double.valueOf(grupo1), Double.valueOf(grupo2)));
//            // dimensiones en y
//            limitesLocal.add(new Pair<>(Double.valueOf(grupo3), Double.valueOf(grupo4)));
//        } else {
//            // dimensiones en x
//            limitesLocal.add(new Pair<>(-10.0, 10.0));
//            // dimensiones en y
//            limitesLocal.add(new Pair<>(-10.0, 10.0));            
//        }
//        matcher.reset();
//        
//        pattern = Pattern.compile(".*(\\d+)");
//        matcher = pattern.matcher(lista.get(3));
//        
//        if (matcher.find()){
//            String grupo1 = matcher.group(1);
//            numDims = Integer.valueOf(grupo1);
//        } else {
//            numDims = 2;
//        }
//        matcher.reset();
//        
//        List<List<Point2D>> listaPuntosObjetosLocal = new ArrayList<>(lista.size()-4);
//        // proceso los datos que se repiten en todos los archivos
//        for (String s : lista.subList(4, lista.size())) {
//            // primer dato es la cantidad de puntos del objeto
//            pattern = Pattern.compile("^(\\d+)\\s+(.*)");
//            matcher = pattern.matcher(s);
//            
//            if (matcher.find()){
//                int cantPuntos = Integer.valueOf(matcher.group(1));
//                List<Point2D> listaPuntos = new ArrayList<>(cantPuntos);
//                String[] aux = matcher.group(2).split("\\s+");
//                
//                assert(aux.length == numDims*cantPuntos);
//                
//                if (numDims==2){
//                    for (int i=0; i<cantPuntos; i++){
//                        double posX = Double.valueOf(aux[2*i]);
//                        double posY = Double.valueOf(aux[2*i+1]);
//                    
//                        listaPuntos.add(new Point2D(posX, posY));
//                    }
//                
//                    listaPuntosObjetosLocal.add(listaPuntos);
//                }
//            }
//            matcher.reset();
//        }
//        
//        // genero mi lista de cajas
//        List<AABB2D> listaCajas = new ArrayList<>(listaPuntosObjetosLocal.size());
//        
//        for (List<Point2D> poly : listaPuntosObjetosLocal) {
//            listaCajas.add(new AnyPolygon2D(poly, false).getBox());
//        }
//        
//        return new SimulationStaticRealSAP(listaPuntosObjetosLocal, limitesLocal, frameNumber, verbose);
//    } */
}