/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rammodels.apps;

import java.util.List;
import primitives.Pair;
import primitives.Point2D;
import sap.IncrementalSweepAndPrune;
import sap.RealSPStatistics;
import sap.Statistics;

/**
 *
 * @author hmoraga
 */
public class SimulationStaticIncrementalSweepAndPrune implements Simulation {
    private final boolean verbose;
    private IncrementalSweepAndPrune rsp;
    private final int timeStep;
    private final List<List<Point2D>> listaObjetos;
    private final List<Pair<Double>> listaVelocidades;
    private final double tinicial, tfinal;
    private RealSPStatistics rspStats;
    
    public SimulationStaticIncrementalSweepAndPrune(List<List<Point2D>> listaPuntosObjetos, List<Pair<Double>> listaVelocidades, int timeStep,  double tinicial, double tfinal, boolean verbose){
        listaObjetos = listaPuntosObjetos;        
        this.verbose = verbose;        
        this.timeStep = timeStep;
        rsp = null;
        this.listaVelocidades = listaVelocidades;
        this.tinicial = tinicial;
        this.tfinal = tfinal;
        rspStats = new RealSPStatistics(listaObjetos.size());
    }

    @Override
    public void exec() {
        // recordar que el timeStep esta en FPS
        int frameMax = (int)((tfinal-tinicial)*timeStep);
        
        for (int i = 0; i <= frameMax; i++) {
            double t = (double)i/timeStep;
            // primer frame
            if (i==0){
                System.out.println("t="+t);
                rsp = new IncrementalSweepAndPrune(listaObjetos, listaVelocidades);
                List<Pair<Integer>> listaColisiones = rsp.getListaParesEfectivosColisionando();
                rspStats.agregarEstadisticas(listaColisiones, t);
                System.out.println(listaColisiones.toString());
            } else {
                // muevo las cajas
                rsp.updateBoxesPositions(1.0/timeStep);
                
                if (!rsp.validarOrdenListaPuntos()){
                    System.out.println("t="+t);
                    rsp.insertionSort(); // reordeno la lista de cajas
                    List<Pair<Integer>> listaColisiones = rsp.getListaParesEfectivosColisionando();
                    rspStats.agregarEstadisticas(listaColisiones, t);
                    if (verbose)
                        System.out.println(listaColisiones.toString());
                }
            }
        }
    }
    
    @Override
    public void writeStatisticsToFile(String filePath, String fileName) {
        /*try {
            File theDir = new File(filePath);
            
            if (!theDir.exists()) {
                if (verbose)
                    System.out.println("creating directory: " + filePath);
                theDir.mkdirs();  
                if (verbose)
                    System.out.println("DIRECTORIES created");
            }
            
            Path arch = Paths.get(filePath+"\\"+fileName);
            try (Formatter output = new Formatter(arch.toFile())) {
                output.format("%s", rsp.getTextoEstadisticas());
            }
            
        } catch (IOException ex) {
            Logger.getLogger(SimulationKinetic.class.getName()).log(Level.SEVERE, null, ex);
        } */
    }   

    @Override
    public Statistics getStatistics() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    
}
