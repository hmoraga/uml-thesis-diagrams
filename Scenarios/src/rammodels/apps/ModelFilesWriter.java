package rammodels.apps;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import kinetic.Object2D;
import primitives.Pair;
import primitives.Point2D;

/**
 * Clase que genera modelos. En rigor el model file generator me genera los 
 * objetos dinámicos y dadas sus velocidades, el tiempo de simulación y el paso
 * temporal, me va generando las fotos de los objetos estáticos. Como estos
 * modelos deben ser probados tres veces, se generan en base a copias de los 
 * objetos originales. Esta es la única forma de poder comparar todos los modelos
 * ya que el KDS no se puede comparar con archivos.
 * El primer modelo es el Sweep and Prune de 1 cuadro.
 * El segundo modelo es el Sweep and Prune real.
 * El tercer modelo es el KDS.
 * 
 * @author hmoraga
 */
public class ModelFilesWriter {    
    private final List<Pair<Double>> velocidades;    // lista de velocidades, un par por cada objeto
    private final Pair<Pair<Double>> limites;   // dimensiones limite: [a,b]x[c,d]
    private final List<List<Point2D>> listaPuntosObjetos;  // lista de puntos por cada objeto
    private final double tinicial, tfinal;
    private final int timeStepInFPS;
    private final String dirName;
    private final boolean verbose;

    /**
     * Constructor de la clase para cuando se ingresan los objetos y sus velocidades
     * iniciales.
     * @param listaPuntosObjetos
     * @param dimensiones
     * @param velocidades
     * @param tinic
     * @param tfinal
     * @param timeStepInFPS
     * @param dirName
     * @param verbose
     * @throws java.io.IOException
     */
    public ModelFilesWriter(List<List<Point2D>> listaPuntosObjetos, Pair<Pair<Double>> dimensiones,
            List<Pair<Double>> velocidades, double tinic, double tfinal, int timeStepInFPS,
            String dirName, boolean verbose) throws IOException{
        assert (tinic < tfinal);
        
        this.tinicial = tinic;
        this.tfinal = tfinal;
        this.timeStepInFPS = timeStepInFPS;
        this.limites = dimensiones;
        this.velocidades = velocidades;
        this.dirName = dirName;
        this.listaPuntosObjetos = listaPuntosObjetos;
        this.verbose = verbose;
    }
    
    /**
     * 
     * @throws java.io.IOException
     */
    public void generarArchivosEstaticos() throws IOException, SecurityException{
        List<Object2D> listaObjetos = new ArrayList<>(listaPuntosObjetos.size());
        
        // creacion de los objetos dados como dato
        for (int i = 0; i < listaPuntosObjetos.size(); i++) {
            listaObjetos.add(new Object2D(i, listaPuntosObjetos.get(i), velocidades.get(i)));
        }

        // para los sweep and prune establezco el inicio y el
        // fin de los tiempos de simulación. 
        
        // en cada paso temporal genero los polígonos de entrada
        // del Sweep and prune
        
        // genero la lista de colisiones de cada cuadro
        
        
        
        File theDir = new File(dirName + "\\static\\inputs");
        //Charset charset = Charset.forName("UTF-8");
        DecimalFormat myFormatter = new DecimalFormat("00000");
        int cantFrames = (int)((tfinal-tinicial)*timeStepInFPS);
                
        if (!theDir.exists()) {
            if (verbose)
                System.out.println("creating directory: " + theDir.toString());
            theDir.mkdirs();
            if (verbose)
                System.out.println("DIRECTORIES created");  
        }
            
        String filePath = theDir.getCanonicalPath();
        StringBuilder str = new StringBuilder();
        
        for (int i=0; i<cantFrames; i++){
            double time = (tinicial*timeStepInFPS+i)/timeStepInFPS;

            if (verbose)
                System.out.println("t="+time);
            
            // genero un archivo por cada timeStep
            String num = myFormatter.format(i);
            String fileName = filePath + "\\" + "file" + num +".txt";
            
            // escribo el contenido del archivo
            // primero los datos del frame en cuestion
            // despues cada uno de los objetos
            //listaTexto.add("//frame:"+i);
            str.append("//frame:").append(i).append(System.getProperty("line.separator"));
            //listaTexto.add("//time:"+time);
            str.append("//time:").append(time).append(System.getProperty("line.separator"));
            //listaTexto.add("//marco:"+limites.toString());
            str.append("//marco:").append(limites.toString()).append(System.getProperty("line.separator"));
            //listaTexto.add("//dimensiones:2");
            str.append("//dimensiones:2").append(System.getProperty("line.separator"));
            
            // indico cada objeto como una lista con el formato
            // cantPuntos cantDimensiones punto_0x punto_0y ...
            listaObjetos.stream().map((obj) -> obj.getPosition(time)).map((listaTempPuntos) -> {
                // cada objeto es una lista de
                StringBuilder linea = new StringBuilder(listaTempPuntos.size()+" ");
                
                for (Point2D pto : listaTempPuntos) {
                    linea.append(pto.getX()).append(" ").append(pto.getY()).append(" ");
                }
                
                linea.deleteCharAt(linea.length()-1);
                linea.append(System.getProperty("line.separator"));
                
                return linea;
            }).forEach((linea) -> {
                str.append(linea);
            });
            
            CreateTextFile arch = new CreateTextFile(fileName, str.toString());
            arch.create();
            
            str.setLength(0);
            
            if (verbose)
                System.out.println("Archivo " + fileName + " creado");
        }
    }

    /**
     * 
     * @throws java.io.IOException
     */
    public void generarArchivoParaRealSAP() throws IOException, SecurityException{
        //List<String> listaTexto = new ArrayList<>();
        // genero un solo archivo
        File theDir = new File(dirName + "\\realSAP\\input");
                
        if (!theDir.exists()) {
            if (verbose)
                System.out.println("creating directory: " + theDir);
            theDir.mkdirs();
            if (verbose)
                System.out.println("DIRECTORIES created");            
        }
        
        // escribo el contenido del archivo
        // primero los datos de tiempo marco y dimensiones de la simulacion
        // despues cada uno de los objetos
        StringBuilder str = new StringBuilder();
        str.append("//time:[").append(tinicial).append(",").append(tfinal).append("]").append(System.getProperty("line.separator"));
        str.append("//timestep:").append((this.timeStepInFPS==0)?1.0:1.0/this.timeStepInFPS).append(System.getProperty("line.separator"));
        str.append("//marco:").append(limites.toString()).append(System.getProperty("line.separator"));
        str.append("//dimensiones:2").append(System.getProperty("line.separator"));
        
        // finalmente dos lineas por objeto donde: la primera linea con los dos
        // primeros valores que son velocidades (en x e y), 
        // la segunda linea es la cantidad de vertices del
        // objeto, seguido por los x,y del objeto partiendo por un borde
        // cualquiera.
        for (int i=0; i< velocidades.size(); i++){
            Pair<Double> velocidadPoligono = velocidades.get(i);
            List<Point2D> listaPuntosPoligono = listaPuntosObjetos.get(i);

            //String auxText = String.valueOf(velocidadPoligono.first)+" "+String.valueOf(velocidadPoligono.second);
            str.append(velocidadPoligono.first).append(" ").append(velocidadPoligono.second).append(System.lineSeparator());
            
            String auxText = listaPuntosPoligono.size() + " ";
            
            auxText = listaPuntosPoligono.stream().map((punto) -> (punto.getX()+" "+punto.getY()+ " ")).reduce(auxText, String::concat);
            
            auxText = auxText.substring(0, auxText.length()-1);
            str.append(auxText).append(System.getProperty("line.separator"));
        }
        
        CreateTextFile arch = new CreateTextFile(theDir.toString() + "\\realSAP.txt", str.toString());
        arch.create();
        str.setLength(0);
            
        if (verbose)
            System.out.println("File Real SAP creado");
    }

    /**
     * Genero un archivo de texto con los datos que necesito serán leidos
     * tanto por el modelo cinetico como el estático
     * 
     * @throws IOException si no se puede crear el directorio 
     */
    public void generarArchivoCinetico() throws IOException{
        List<Object2D> listaObjetos = new ArrayList<>(listaPuntosObjetos.size());    // lista de objetos2D

        // creacion de los objetos dados como dato
        for (int i = 0; i < listaPuntosObjetos.size(); i++) {
            listaObjetos.add(new Object2D(i, listaPuntosObjetos.get(i), velocidades.get(i)));
        }

        // genero un solo archivo
        File theDir = new File(dirName + "\\kinetic\\input");
                
        if (!theDir.exists()) {
            if (verbose)
                System.out.println("creating directory: " + theDir);
            theDir.mkdirs();
            if (verbose)
                System.out.println("DIRECTORIES created");  
        }
            
        // escribo el contenido del archivo
        // primero los datos de tiempo marco y dimensiones de la simulacion
        // despues cada uno de los objetos
        StringBuilder str = new StringBuilder();
        str.append("//time:[").append(tinicial).append(",").append(tfinal).append("]").append(System.getProperty("line.separator"));
        str.append("//marco:").append(limites.toString()).append(System.getProperty("line.separator"));
        str.append("//dimensiones:2").append(System.getProperty("line.separator"));
        
        // finalmente dos lineas por objeto donde: la primera linea con los dos
        // primeros valores que son velocidades (en x e y), 
        // la segunda linea es la cantidad de vertices del
        // objeto, seguido por los x,y del objeto partiendo por un borde
        // cualquiera.
        listaObjetos.stream().map((obj) -> {
            Pair<Double> velocidad = obj.getVelocidad();
            str.append(velocidad.first).append(" ").append(velocidad.second).append(System.getProperty("line.separator"));
            List<Point2D> listaPuntos = obj.getListaPuntos();
            return listaPuntos;
        }).map((listaPuntos) -> {
            String auxText = (listaPuntos.size() + " ");
            auxText = listaPuntos.stream().map((punto) -> (punto.getX()+" "+punto.getY()+ " ")).reduce(auxText, String::concat);
            return auxText;
        }).map((auxText) -> auxText.substring(0, auxText.length()-1)).forEach((auxText) -> {
            str.append(auxText).append(System.getProperty("line.separator"));
        });

        CreateTextFile arch = new CreateTextFile(theDir.toString() + "\\kinetic.txt", str.toString());
        arch.create();
        str.setLength(0);
        
        if (verbose)
            System.out.println("File cinetico creado");
    }
}
