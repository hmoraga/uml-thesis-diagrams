package rammodels.apps;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.List;
import kinetic.KineticSortedList;
import kinetic.ListaIntersecciones;
import kinetic.ListaKSL;
import kinetic.Object2D;
import kinetic.Wrapper;
import kinetic.actuators.KineticStatistics;
import kinetic.eventos.ColaEventos;
import kinetic.eventos.ObservadorEventos;
import primitives.Pair;
import sap.Statistics;

/**
 *
 * @author hmoraga
 */
public class SimulationKinetic implements Simulation{
    private List<Object2D> listaObjetos;
    private final double tinicial, tfinal;
    private Statistics estadisticas;
    private final boolean verbose;
    
    /**
     * método para simulación de modelo cinético
     * @param listaObjetos arreglo con la lista de objetos
     * @param tinicial tiempo inicial
     * @param tfinal tiempo final
     * @param dimensiones dimesnsiones [a,b]x[c,d]
     * @param verbose
     */
    public SimulationKinetic(List<Object2D> listaObjetos, double tinicial, double tfinal, 
            List<Pair<Double>> dimensiones, boolean verbose) {
        this.tinicial = tinicial;
        this.tfinal = tfinal;
        double areaTotal = (dimensiones.get(0).second-dimensiones.get(0).first)*(dimensiones.get(1).second-dimensiones.get(1).first);
        this.listaObjetos = listaObjetos;
        estadisticas = new KineticStatistics(listaObjetos.size());
        estadisticas.setAreaTotal(areaTotal);
        
        double areaInicial = 0.0;
        for (int i = 0; i < listaObjetos.size(); i++) {
            Object2D obj = listaObjetos.get(i);
            areaInicial+=obj.getArea();
        }
        estadisticas.setAreaOcupadaInicial(areaInicial);
        estadisticas.setTiempoTotalSimulado(new Pair<>(tinicial,tfinal));
        this.verbose = verbose;
    }
    
    @Override
    public void exec(){
        ListaKSL listaKSL = new ListaKSL();
        // inicializo la lista de colisiones
        ListaIntersecciones colisiones = new ListaIntersecciones();        
        ColaEventos colaEventos = new ColaEventos(new Pair<>(tinicial, tfinal),2*listaObjetos.size(), verbose);
        
        // inicializo el observer
        ObservadorEventos observador = new ObservadorEventos(listaKSL, colisiones, colaEventos, (KineticStatistics)estadisticas, verbose);
        
        // agrego el observer a todos los objetos necesarios
        listaKSL.setObserver(observador);
        colaEventos.setObserver(observador);
        colisiones.setObserver(observador);
        ((KineticStatistics)estadisticas).setObserver(observador);

        // obtengo la cola de Eventos inicializada
        inicializarEstructuras(colaEventos, listaKSL, colisiones);
        
        // procesar todos los eventos hasta que la cola de eventos esté vacia
        colaEventos.procesarEventos();
        
        // limpiar todo
        listaKSL.deleteObservers();
        colaEventos.deleteObservers();
        colisiones.deleteObservers();
        ((KineticStatistics)estadisticas).deleteObservers();
        
        // limpiar todos los objetos del tipo Collection
        colisiones.clear();
        colaEventos.clear();
        listaKSL.clear();
    }

    @Override
    public void writeStatisticsToFile(String filePath, String fileName) {
        /*try {
            String dirName = filePath;
            File theDir = new File(dirName);
            
            if (!theDir.exists()) {
                if (verbose)
                    System.out.println("creating directory: " + dirName);
                theDir.mkdirs();  
                if (verbose)
                    System.out.println("DIRECTORIES created");
            }
            
            Path arch = Paths.get(dirName+"\\"+fileName);
            try (Formatter output = new Formatter(arch.toFile())) {
                output.format("%s", estadisticas.getText());
            }
        } catch (IOException ex) {
            Logger.getLogger(SimulationKinetic.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }

    @Override
    public Statistics getStatistics() {
        return estadisticas;
    }
    
    public void inicializarEstructuras(ColaEventos colaEventos, ListaKSL listaKSL, ListaIntersecciones colisiones){
        // se supone que tengo 2 dimensiones, por lo que debo crear dos KSL temporales
        // genero una KSL por cada dimension
        KineticSortedList KSLx = new KineticSortedList(), KSLy  = new KineticSortedList();
                
        // de todos los objetos debo generar los DimensionKinetic
        listaObjetos.stream().forEach((listaObjeto) -> {
            List<Wrapper> wrappersX = listaObjeto.getWrappers(0);
            List<Wrapper> wrappersY = listaObjeto.getWrappers(1);            
            // de cada DimensionKinetic genero su Wrapper
            // y agrego los Wrapper a cada KSL
            KSLx.addAll(wrappersX);
            KSLy.addAll(wrappersY);
        });

        // ingreso los valores en la lista SIN ORDENAR
        // seran ordenados al calcular los eventos iniciales
        listaKSL.add(KSLx);
        listaKSL.add(KSLy);
        
        // calculo los eventos iniciales
        colaEventos.calcularEventosIniciales();
    }
}
