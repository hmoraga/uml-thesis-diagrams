package rammodels.apps;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import poligons.AnyPolygon2D;
import primitives.AABB2D;
import primitives.Pair;
import primitives.Point2D;
import sap.DirectSweepAndPrune2D;
import sap.StaticSAPStats;
import sap.Statistics;

/**
 *
 * @author hmoraga
 */
public class SimulationStaticDirectSweepAndPrune implements Simulation{
    private StaticSAPStats estadisticas;    
    private final boolean verbose;
    private List<AABB2D> listaCajas;
    private final List<Pair<Double>> dimensiones, listaVelocidades;
    private double tinicial, tfinal, timeStep;
    private int totalFrames;
    
    public SimulationStaticDirectSweepAndPrune(List<List<Point2D>> listaPuntosObjetos, List<Pair<Double>> dimensiones,
            List<Pair<Double>> listaVelocidades, double tinicial, double tfinal, double timeStep,
            boolean verbose) {
        this.dimensiones = dimensiones;
        this.tinicial = tinicial;
        this.tfinal = tfinal;
        this.timeStep = timeStep;
        this.totalFrames = (int)((this.tfinal-this.tinicial)*this.timeStep);
        
        double areaTotal = (dimensiones.get(0).second-dimensiones.get(0).first)*(dimensiones.get(1).second-dimensiones.get(1).first);
        this.listaCajas = new ArrayList<>(listaPuntosObjetos.size());
        this.listaVelocidades = listaVelocidades;
        this.verbose = verbose;
        
        listaPuntosObjetos.stream().forEach((poly) -> {
            this.listaCajas.add(new AnyPolygon2D(poly, false).getBox());
        });
        
        double area = calcularAreaObjetos(listaPuntosObjetos);
        
        estadisticas = new StaticSAPStats(listaPuntosObjetos.size());
        estadisticas.setAreaTotal(areaTotal);
        estadisticas.setAreaOcupadaInicial(area);
    }
    
    /**
     *
     */
    @Override
    public void exec() {
        // ejecuto el SP One frame
        DirectSweepAndPrune2D swp = new DirectSweepAndPrune2D();            

        for (int i = 0; i <= totalFrames; i++) {
            double t = (double)i/timeStep;
            if (verbose)
                System.out.println("t="+t);
            // genero la lista de AABB2D(t) necesarias para el algoritmo
            // de sweep and prune
            List<AABB2D> listaBoxesTemporal = new ArrayList<>(listaCajas.size());
            
            listaCajas.stream().forEach((box) -> {
                listaBoxesTemporal.add(new AABB2D(box.getPoints()));
            });
            
            // genero el movimiento de cada caja
            for (int j = 0; j < listaBoxesTemporal.size(); j++) {
                listaBoxesTemporal.get(j).move(new Point2D(listaVelocidades.get(j).first*t, listaVelocidades.get(j).second*t));
            }
            
            swp.inicializarEstructuras(listaBoxesTemporal, i);
            List<Pair<Integer>> listaPares = swp.getListaParesEfectivosColisionando();

            this.estadisticas.agregarEstadisticas(listaPares, t);

            if (verbose)
                System.out.println(listaPares.toString());
        }
    }

    @Override
    public void writeStatisticsToFile(String filePath, String fileName) throws IOException{
        File theDir = new File(filePath);

        // if the directory does not exist, create it
        if (!theDir.exists()) {
            System.out.println("creating directory: " + theDir.getName());
            boolean result = false;

            try {
                theDir.mkdir();
                result = true;
            } catch (SecurityException se) {
                //handle it
            }
            if (result) {
                System.out.println("DIR created");
            }
        }
        
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath+"\\"+fileName))) {
            for (String s : estadisticas.getText()) {
                bw.append(s);
                bw.newLine();
            }
        }
    }

    @Override
    public Statistics getStatistics() {
        return estadisticas;
    }

    private double calcularAreaObjetos(List<List<Point2D>> listaPuntosObjetos) {
        double area = 0.0;
        
        area = listaPuntosObjetos.stream().map((listaPuntosObjeto) -> new AnyPolygon2D(listaPuntosObjeto, false).getArea()).reduce(area, (accumulator, _item) -> accumulator + _item);
        
        return area;
    }
}
