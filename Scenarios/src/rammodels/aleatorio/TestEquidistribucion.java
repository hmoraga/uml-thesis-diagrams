/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rammodels.aleatorio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import poligons.Polygon2D;
import poligons.Triangulo;
import primitives.Pair;
import primitives.Point2D;
import rammodels.apps.SimulationStaticIncrementalSweepAndPrune;
import rammodels.apps.Simulation;

/**
 *
 * @author hmoraga
 */
public class TestEquidistribucion {
    public static void main(String[] args) {
        int cantObjetosPorFila = Integer.parseInt(args[0]);
        int cantObjetosPorColumna = cantObjetosPorFila;
        // SOLO por DEBUG
        //int cantObjetosPorFila = 30, cantObjetosPorColumna = 30;
        int FPS  = Integer.parseInt(args[1]);
        // SOLO por DEBUG
        //int FPS = 100;
        
        double largeSize = 10.0;
        List<List<Point2D>> listaPuntosObjetos = new ArrayList<>(cantObjetosPorFila*cantObjetosPorColumna);
        List<Pair<Double>> velocidadesPorObjeto = new ArrayList<>(cantObjetosPorFila*cantObjetosPorColumna);
        
        // Se elige un marco de simulacion
        // valores posibles (-3.0,3.0)-(-5.0,5.0)-(-10.0,10.0)-(-20.0,20.0)-(-50.0,50.0)
        List<Pair<Double>> dimensiones = new ArrayList<>(2);
        dimensiones.add(new Pair<>(-largeSize,largeSize));
        dimensiones.add(new Pair<>(-largeSize,largeSize));

        // Intervalo de tiempo
        Pair<Double> tiempos = new Pair<>(0.0, Double.parseDouble(args[2]));
        // SOLO POR DEBUG
        //Pair<Double> tiempos = new Pair<>(0.0, 1.0);
        
        // calculamos el tamaño de cada objeto, por simplicidad será
        // un grupo de triángulos
        double tamanoObj = 2*largeSize/(3*cantObjetosPorFila);
        Random rnd = new Random(System.currentTimeMillis());
        double posX = largeSize/cantObjetosPorFila;
        double posY = largeSize/cantObjetosPorColumna;
        
        for (int i=0; i<cantObjetosPorFila; i++){
            for (int j = 0; j < cantObjetosPorColumna; j++) {
                Polygon2D poly = new Triangulo(true);
                poly.amplificar(tamanoObj);
                poly.desplazar(2*i*largeSize/cantObjetosPorFila+posX-largeSize,2*j*largeSize/cantObjetosPorColumna+posY-largeSize);
                velocidadesPorObjeto.add(obtenerVelocidadesAleatorias(rnd, 3*tamanoObj, 3*tamanoObj));
                listaPuntosObjetos.add(poly.getVertices());
            }
        }

        // genero el archivo para que sea de utilidad en el 
        // proyecto ShowModel
        File theDir = new File("D:\\experimentos\\equidistribucion");
        
        if (!theDir.exists()){
            System.out.println("Creating directory: "+theDir.getName());
            boolean result = false;
            
            try {
                result = theDir.mkdirs();
            } catch (SecurityException e){
                
            }
            
            if (result)
                System.out.println("Directorios " + theDir.getName() + " creado");
        }
        
        File arch = new File(theDir.toString() + "\\input.txt");
        
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(arch))) {
            bw.write("marco:"+dimensiones.get(0).toString()+","+dimensiones.get(1).toString());
            bw.newLine();
            bw.write("timeIntervals:"+tiempos.first+"-"+tiempos.second);
            bw.newLine();
            bw.write("framesPerSecond:"+FPS);
            bw.newLine();

            for (int i = 0; i < listaPuntosObjetos.size(); i++) {
                List<Point2D> obj = listaPuntosObjetos.get(i);
                
                bw.write(velocidadesPorObjeto.get(i).first+" "+velocidadesPorObjeto.get(i).second);
                bw.newLine();
                StringBuilder str = new StringBuilder(""+obj.size());
                
                for (int j=0; j < obj.size(); j++){
                    str.append(" ").append(obj.get(j).getX()).append(" ").append(obj.get(j).getY());
                }
                bw.write(str.toString());
                bw.newLine();
            }
        } catch (IOException e) {}
        
        Simulation simulaRealSP = new SimulationStaticIncrementalSweepAndPrune(listaPuntosObjetos, velocidadesPorObjeto, FPS, tiempos.first, tiempos.second, false);
        simulaRealSP.exec();
    }
    
    private static Pair<Double> obtenerVelocidadesAleatorias(Random rnd, double rangoX, double rangoY){
        double speedX = rnd.nextDouble()*2*rangoX-rangoX;
        double speedY = rnd.nextDouble()*2*rangoY-rangoY;
        
        return new Pair<>(speedX, speedY);
    }
}
