package rammodels.aleatorio;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import kinetic.Object2D;
import poligons.AnyPolygon2D;
import primitives.Pair;
import primitives.Point2D;
import rammodels.apps.ObjectRandomGenerator;
import rammodels.apps.SimulationKinetic;
import rammodels.apps.SimulationStaticDirectSweepAndPrune;
import rammodels.apps.SimulationStaticIncrementalSweepAndPrune;
import rammodels.apps.Simulation;

/**
 *
 * @author hmoraga
 */
public class TestAleatorio {
    public static void main(String[] args) throws IOException {
        long timestamp = System.currentTimeMillis();
        Random rnd = new Random(timestamp);
        
        // area de la simulacion
        double areaSimulacion, areaTotalObjetos = 0;
        boolean verbose = true;
        
        // cantidad de objetos en la simulación
        // valores posibles: 0, 1, 2, 5, 10, 20, 30, 50, 100, 200, 500, 1000...
        //int cantObjetos = Integer.parseInt(args[0]);
        // SOLO PARA DEBUG
        int cantObjetos = 200;
        
        List<List<Point2D>> listaPuntosObjetos = new ArrayList<>(cantObjetos);
        List<Object2D> listaObjetos = new ArrayList<>(cantObjetos);
        List<Pair<Double>> velocidadesPorObjeto = new ArrayList<>(cantObjetos);
        
        // Se elige un marco de simulacion
        // valores posibles (-3.0,3.0)-(-5.0,5.0)-(-10.0,10.0)-(-20.0,20.0)-(-50.0,50.0)
        List<Pair<Double>> dimensiones = new ArrayList<>(2);
        dimensiones.add(new Pair<>(-10.0,10.0));
        dimensiones.add(new Pair<>(-10.0,10.0));
        //areaSimulacion = (dimensiones.first.second-dimensiones.first.first)*(dimensiones.second.second-dimensiones.second.first);
        // tamaño máximo de un objeto (como porcentaje de las dimensiones)
        //double objectMaxSize = Double.parseDouble(args[1])/cantObjetos;
        // SOLO PARA DEBUG
        double objectMaxSize = 0.1/cantObjetos;
        
        // Se elige un tiempo de simulacion: desde 0 al valor elegido
        // valores posibles 1.0, 2.0, 5.0, 10.0, 20.0
        double simulationTime = 1.0;
        
        // Se elige un timeStep (en FPS), util solo para las simulaciones estáticas
        //int timeStep = Integer.parseInt(args[2]);
        // SOLO PARA DEBUG
        int timeStep = 10;
        
        String path = "D:\\experimentos\\experimento"+String.valueOf(timestamp);
        
        if (verbose)
            System.out.println("Generando experimento " + path);
            
        for (int i = 0; i < cantObjetos; i++) {
            // generador de objetos al azar
            ObjectRandomGenerator org = new ObjectRandomGenerator(dimensiones, objectMaxSize, simulationTime, rnd);
            // agrego a la lista de velocidades
            velocidadesPorObjeto.add(org.getVelocidades());
            // agrego a la lista de puntos por objeto
            listaPuntosObjetos.add(org.getListaPuntos());
            listaObjetos.add(new Object2D(i, listaPuntosObjetos.get(i), velocidadesPorObjeto.get(i)));
        }
            
        areaTotalObjetos = listaPuntosObjetos.stream().map((objeto) -> new AnyPolygon2D(objeto, false).getArea()).reduce(areaTotalObjetos, (accumulator, _item) -> accumulator + _item);
        areaSimulacion = (dimensiones.get(0).second-dimensiones.get(0).first)*(dimensiones.get(1).second-dimensiones.get(1).first);

        if (verbose) {
            System.out.println("Area del frame original:" + areaSimulacion);
            System.out.println("Area de los poligonos iniciales:" + areaTotalObjetos);
        }
            
        ////////////////////////////////////////////////////////////
        if (verbose){
            System.out.println("Leyendo modelo KDS-SAP");
        }

        // genero simulacion cinética
        SimulationKinetic smk = new SimulationKinetic(listaObjetos, 0.0, simulationTime, dimensiones, verbose);
        smk.exec();
        
        // leyendo archivos estaticos
        if (verbose){
            System.out.println("Leyendo modelo SAP estatico per frame");
        }

        Simulation smStaticSP = new SimulationStaticDirectSweepAndPrune(listaPuntosObjetos, dimensiones, velocidadesPorObjeto, 0.0, simulationTime, timeStep, verbose);
        smStaticSP.exec();
        
        // leyendo archivo para realSAP
        if (verbose){
            System.out.println("Leyendo modelo SAP original");
        }
        
        Simulation simulaRealSP = new SimulationStaticIncrementalSweepAndPrune(listaPuntosObjetos, velocidadesPorObjeto, timeStep, 0.0, simulationTime, verbose);
        simulaRealSP.exec();
        
        System.out.println("Fin de la simulacion");
    }   
}
